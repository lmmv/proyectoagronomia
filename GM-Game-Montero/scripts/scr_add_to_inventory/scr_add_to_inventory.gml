/// @function scr_add_to_inventory()
/// @description Agrega un elemento al inventario
/// @argument0 item_name Nombre del elemento a agregar
/// @argument1 item_object Objeto del elemento

grid = obj_inventory.inventory_grid;
item_name = argument0;
item_object = argument1;

if(ds_grid_get(grid, 0, 0) != 0) //si el grid no está vacío, se redimensiona
	ds_grid_resize(grid, ds_grid_width(grid), ds_grid_height(grid) + 1);

grid_height = ds_grid_height(grid)-1;

grid[# 0, grid_height] = item_name;
grid[# 1, grid_height] = item_object;
