/// @function scr_mouse_over_button(option)
/// @description Determina si el mouse está sobre la opción del botón que se pasa como parámetro
/// @param option Botón que debe verificar

xx = global.dw * buttons_xstart + (argument0 * x_spacing);
yy = global.dh * buttons_ystart + (argument0 * y_spacing);
sp_width = sprite_get_width(sprite);
sp_height = sprite_get_height(sprite);
	
//ubicación del sprite del botón
x1 = xx - sp_width/2;	y1 = yy - sp_height/2;
x2 = xx + sp_width/2;	y2 = yy + sp_height/2;

return point_in_rectangle(device_mouse_x_to_gui(0),device_mouse_y_to_gui(0),x1,y1,x2,y2)