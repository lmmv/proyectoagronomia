/// @function scr_stop_background(stop)
/// @description Activa o detiene el movimiento horizontal del room
/// @argument0 stop Valor bool para detener (true) o reanudar (false) el movimiento del fondo

stop = argument0;

if room == rm_limon_plane {
	if instance_exists(obj_limon_plane) {
		if stop {
			layer_hspeed(obj_limon_plane.back_layer,0);	
			layer_hspeed(obj_limon_plane.soil_layer,0);
		}
		else {
			layer_hspeed(obj_limon_plane.back_layer,obj_limon_plane.bg_speed);	
			layer_hspeed(obj_limon_plane.soil_layer,obj_limon_plane.sl_speed);	
		}
	}
	if instance_exists(obj_clouds) {
		part_system_automatic_update(obj_clouds.cloud_system,!stop);
	}
}