/// @function scr_add_cartago_items()
/// @description Agrega los elementos del invernadero al inventario

//elementos de prueba
scr_add_to_inventory("Uno",obj_inv_1);
scr_add_to_inventory("Dos",obj_inv_2);
scr_add_to_inventory("Tres",obj_inv_3);
scr_add_to_inventory("Cuatro",obj_inv_4);
scr_add_to_inventory("Cinco",obj_inv_5);
scr_add_to_inventory("Seis",obj_inv_6);
scr_add_to_inventory("Siete",obj_inv_7);
scr_add_to_inventory("Ocho",obj_inv_8);
scr_add_to_inventory("Nueve",obj_inv_9);
scr_add_to_inventory("Diez",obj_inv_10);
scr_add_to_inventory("Once",obj_inv_11);