/// @function scr_dialog_intro()
/// @description Crea los strings del diálogo y opciones según el nivel al que corresponde

str_opt_0 = "Volver al mapa";
str_opt_1 = "Jugar"

switch (room) {
    case rm_dialog_intro_map:
		next_room = rm_map;
        dialog = "Bienvenido a las aventuras de la ingeniera Susana.";
		str_opt_0 = "Volver al menú";
		str_opt_1 = "Ir al mapa";
        break;
    case rm_dialog_intro_limon_A:
		next_room = rm_limon_lab;
		dialog = "Bienvenido a Limón.";
        dialog += " Primero crearemos la molécula del agroquímico que necesitamos.";
		dialog += " Luego vamos a pilotear nuestra avioneta sobre los cultivos de banano procurando salvaguardar la vida de los habitantes.";
		dialog += " ¡Buena suerte!";
        break;
    case rm_dialog_intro_limon_B:
		next_room = rm_limon_plane;
        dialog = "";
        break;
    case rm_dialog_intro_cartago:
		next_room = rm_cartago;
        dialog = "Bienvenido a Cartago.";
		dialog += " Vamos a diseñar un invernadero.";
		dialog += " Con un invernadero bien diseñado para cada tipo de producción, podemos exportar a todo el mundo.";
		dialog += " ¡Buena suerte!";
        break;
    case rm_dialog_intro_sanjose:
		next_room = rm_sanjose;
        dialog = "Bienvenido a San José.";
        break;
    case rm_dialog_intro_alajuela:
		next_room = rm_alajuela;
        dialog = "Bienvenido a Alajuela.";
        break;
    case rm_dialog_intro_guanacaste:
		next_room = rm_guanacaste;
        dialog = "Bienvenido a Guanacaste.";
		dialog += " En nuestro moderno ingenio, recolectando bagazo de caña, generamos energía eléctrica.";
		dialog += " Tu labor será supervisar nuestra planta de energía con biomasa.";
		dialog += " Cuida la presión de vapor, la demanda eléctrica y procura no quedarte sin biomasa.";
        break;
    case rm_dialog_intro_heredia:
		next_room = rm_heredia;
        dialog = "Bienvenido a Heredia.";
        break;
    case rm_dialog_intro_puntarenas:
		next_room = rm_puntarenas;
        dialog = "Bienvenido a Puntarenas.";
        break;
    default:
		next_room = rm_map;
        dialog = "Esto no es una provincia.";
        break;
}