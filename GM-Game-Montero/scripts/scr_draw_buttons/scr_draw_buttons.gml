/// @function scr_draw_buttons(buttons_array)
/// @description Controla como se ven los botones según los eventos del ratón
/// @param buttons_array Array con las opciones de cada botón

for (i=0; i < array_length_1d(argument0); i++) {
	mouse_over = scr_mouse_over_button(i)

	if button_pressed and mouse_over
		draw_sprite_ext(sprite,2,xx,yy,1,1,0,c_aqua,1);
	else if mouse_over
		draw_sprite_ext(sprite,1,xx,yy,1,1,0,c_white,1);
	else
		draw_sprite(sprite,0,xx,yy);	
	
		draw_set_valign(fa_middle);
		draw_set_halign(fa_center);
		draw_set_font(fnt_buttons);
		draw_set_color(c_white);
		draw_text(xx,yy,argument0[i]);
}
