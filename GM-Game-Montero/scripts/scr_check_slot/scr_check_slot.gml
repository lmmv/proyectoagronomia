/// @function scr_check_slot()
/// @description Verifica si está soltando el objeto sobre un espacio para colocar elementos
/// @argument0 grid Grid en el que se debe colocar el objeto

grid = argument0;

if		(grid == inv_grid) { obj = obj_inventory_slots;}
else if (grid == sel_grid) { obj = obj_selected_slots;}

//verifica si está sobre la instancia de un espacio para colocar elementos
slot_inst = instance_position(x, y, obj);

if slot_inst != noone {
	num = slot_inst.slot_number;	//obtiene el número del espacio en el que soltó el objeto				
	//verificar si ya hay un objeto en el espacio seleccionado
	if grid[# 1, num] == -1 { //si no hay nada en ese espacio, se agrega
		grid[# 0, num] = name;			
		grid[# 1, num] = object_index;
		//se borra del espacio en el grid que ocupaba antes
		if(inv_grid[# 1, grid_y] == object_index) {
			inv_grid[# 0, grid_y] = "";
			inv_grid[# 1, grid_y] = -1;
		}				
		else if(sel_grid[# 1, grid_y] == object_index) {
			sel_grid[# 0, grid_y] = "";
			sel_grid[# 1, grid_y] = -1;				
		}								
		//se establecen los nuevos valores de inicio (x,y) del objeto
		x = slot_inst.x;	xstart = x;
		y = slot_inst.y;	ystart = y;
		
		return true;
	}
	else {
		return false;
	}
}
else {
	return false;
}