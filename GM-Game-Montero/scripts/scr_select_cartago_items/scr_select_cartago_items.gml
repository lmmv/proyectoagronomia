/// @function scr_select_cartago_items()
/// @description Selecciona aleatoriamente un tipo de invernadero

item = irandom(2);

item_list = ds_list_create();

switch (item) {
    case 0:
		greenhouse_name = "Invernadero de flores";
        ds_list_add(item_list,"Uno","Cinco","Tres","Diez","Cuatro","Dos");
        break;
    case 1:
		greenhouse_name = "Invernadero de fresas";
        ds_list_add(item_list,"Cuatro","Ocho","Dos","Once","Seis","Siete");
        break;
    case 2:
		greenhouse_name = "Invernadero de mariposas";
        ds_list_add(item_list,"Nueve","Siete","Tres","Cuatro","Uno","Cinco");
        break;
    default:
        return false;
        break;
}

//ds_list_sort(item_list,true);

return item_list;