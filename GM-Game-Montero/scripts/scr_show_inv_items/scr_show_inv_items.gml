/// @function scr_show_inv_items()
/// @description Crea los elementos de la página del inventario

grid = obj_inventory.inventory_grid;

first_item = page * items_per_page;
last_item = first_item + items_per_page;
last_item = min(last_item,total_items);

xx = global.dw * 0.73;
yy = global.dh * 0.11;
x_start = xx;


for (i = first_item; i < last_item; i++) {
	item = grid[# 1, i];
	if (i mod columns == 0) {
		xx = x_start;
		yy += y_spacing;
	}
	var inst = instance_create_depth(xx, yy, depth, obj_inventory_slots);
	with inst {
		slot_number = other.i;
	}
	if item != -1 {
		instance_create_depth(xx, yy, depth-1, item);
	}
	xx += x_spacing;	
}