/// @function scr_buttons(buttons_array)
/// @description Controla los eventos del ratón en los botones 
/// @param buttons_array Array con las opciones de cada botón

if global.allow_input {
	for (i=0; i < array_length_1d(argument0); i++) {
		if scr_mouse_over_button(i)
			option = i;	
	}

	if scr_mouse_over_button(option) {
		mouse_down = device_mouse_check_button_pressed(0,mb_left);
		mouse_up = device_mouse_check_button_released(0,mb_left);
		if mouse_down
			button_pressed = true;
		if mouse_up and button_pressed {
			audio_play_sound(snd_button,10,0);
			hit = true;
		}
	}
	else
		button_pressed = false;
}