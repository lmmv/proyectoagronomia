/// @function scr_draw_map()
/// @description Controla como se ve el botón de la provincia según los eventos del ratón

if button_pressed
	//draw_sprite_ext(sprite_index,0,x,y,1,1,0,c_aqua,1);
	draw_sprite(sprite_index,0,x,y);
else if mouse_over
	//draw_sprite_ext(sprite_index,0,x,y,1,1,0,c_red,1);
	draw_sprite(sprite_index,1,x,y);
else
	draw_sprite(sprite_index,0,x,y);