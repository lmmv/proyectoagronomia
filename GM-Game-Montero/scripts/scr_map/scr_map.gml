/// @function scr_map(province)
/// @description Controla los eventos del ratón en el mapa de CR
/// @param province Room de la provincia 

province = argument0;

if global.allow_input {
	mouse_over = position_meeting(device_mouse_x(0),device_mouse_y(0),id);
	if mouse_over {
		mouse_down = device_mouse_check_button_pressed(0,mb_left);
		mouse_up = device_mouse_check_button_released(0,mb_left);
	
		if mouse_down {
			button_pressed = true;	
		}
		else if mouse_up and button_pressed {
			scr_change_room(province);
			audio_play_sound(snd_button,10,0);
		}
	}
	else {
		button_pressed = false;
	}
}