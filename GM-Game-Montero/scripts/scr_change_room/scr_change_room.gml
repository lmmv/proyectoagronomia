/// @function scr_change_room(room)
/// @description Cambia el room haciendo un efecto de transición
/// @param room_to_go Nombre del room al que quiere pasar

global.room_selected = argument0;
instance_create_depth(0,0,0,obj_fade_out); 