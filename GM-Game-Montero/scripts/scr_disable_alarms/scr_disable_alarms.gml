/// @function scr_disable_alarms()
/// @description Desactiva todas las alarmas para el objeto que llama al script

for (i = 0; i < 11; i++) {
	alarm[i] = -1;
}