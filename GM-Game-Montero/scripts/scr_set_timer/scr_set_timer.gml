/// @function scr_set_timer()
/// @description Crea un objeto temporizador
/// @argument0 x Valor de x para el tiempo
/// @argument1 y Valor de y para el tiempo
/// @argument2 seg Tiempo en segundos

inst = instance_create_depth(argument0,argument1,depth,obj_timer);
inst.alarm_time = argument2;