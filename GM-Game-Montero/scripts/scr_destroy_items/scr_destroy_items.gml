/// @function scr_destroy_items()
/// @description Elimina los elementos que están al cambiar de página 

grid = obj_inventory.inventory_grid;

for (i = first_item; i < last_item; i++) {
	item = grid[# 1, i];
	
	if item != -1 
		instance_destroy(item);
}

instance_destroy(obj_inventory_slots);