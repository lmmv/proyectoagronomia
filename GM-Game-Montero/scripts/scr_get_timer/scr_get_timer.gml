/// @function scr_get_timer()
/// @description Obtiene un string del tiempo de la alarma en formato MM:SS 
/// @argument0 alarm Alarma

if argument0 != -1 {
	seconds = floor(argument0/60);

	minute = floor(seconds/60);
	str_minutes = string_format(minute,2,0);
	str_minutes = string_replace_all(str_minutes," ","0");

	second = seconds mod 60;
	str_seconds = string_format(second,2,0);
	str_seconds = string_replace_all(str_seconds," ","0");

	return  str_minutes + ":" + str_seconds;
}
else {
	return "";
}