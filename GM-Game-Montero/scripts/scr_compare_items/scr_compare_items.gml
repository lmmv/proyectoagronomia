/// @function scr_compare_items()
/// @description Compara los elementos de la lista con los elementos seleccionados

//se copian los elementos seleccionados para compararlos con los elementos de la receta
sel_grid = obj_selected_items.selected_items_grid;
sel_items = ds_grid_create(ds_grid_width(sel_grid), ds_grid_height(sel_grid));
ds_grid_copy(sel_items, sel_grid);

//se ordenan los elementos
if room == rm_limon_lab {
	ds_grid_sort(sel_items,0,true);
}

//crea un string con los elementos seleccionados
selected_string = "";
for (var i = 0; i < ds_grid_height(sel_items); i++) {
	st = sel_items[# 0, i];
	if st != ""
		selected_string += st + "\n";
}

//se comparan el string de elementos seleccionados con el string de la lista
if selected_string == items_string {
	return true;	
}
else {
	return false;
}