
global.playing_now = argument0;

if !audio_is_playing(global.playing_now) {
	audio_stop_all();
	audio_play_sound(global.playing_now,10,true);
}