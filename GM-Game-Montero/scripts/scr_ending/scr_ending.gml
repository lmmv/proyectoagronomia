/// @function scr_ending(win, text*)
/// @description Crea un room cuando pierde con opciones para reiniciar el nivel o volver al mapa
/// @argument0 win Valor bool si ganó (true) y si pierde (false)
/// @argument1 text* Texto opcional

win = argument[0];
global.prev_room = room;

if argument_count > 1 {
	global.end_text = argument[1];	//texto opcional
}
else {	//texto por defecto
	if win	
		global.end_text = "¡Buen trabajo!";
	else
		global.end_text = "Inténtalo nuevamente."
}

if win {
	scr_change_room(rm_win);
}
else {
	scr_change_room(rm_lose);
}