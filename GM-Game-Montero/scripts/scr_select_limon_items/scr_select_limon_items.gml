/// @function scr_select_limon_items()
/// @description Selecciona aleatoriamente una receta de agroquímico

item = irandom(2);

item_list = ds_list_create();

switch (item) {
    case 0:
		recipe_name = "Receta 0";
        ds_list_add(item_list,"Uno","Cinco","Tres","Diez");
        break;
    case 1:
		recipe_name = "Receta 1";
        ds_list_add(item_list,"Cuatro","Ocho","Dos","Once","Seis");
        break;
    case 2:
		recipe_name = "Receta 2";
        ds_list_add(item_list,"Nueve","Siete","Tres","Cuatro");
        break;
    default:
        return false;
        break;
}

ds_list_sort(item_list,true);

return item_list;