/// @function scr_mouse_events()
/// @description Controla los eventos del ratón

mouse_over = position_meeting(device_mouse_x_to_gui(0),device_mouse_y_to_gui(0),id);

if mouse_over {
	mouse_down = device_mouse_check_button_pressed(0,mb_left);
	mouse_up = device_mouse_check_button_released(0,mb_left);
	if mouse_down
		button_pressed = true;
	if mouse_up and button_pressed {
		audio_play_sound(snd_button,10,0);
		return true;		
	}		
}
else
	button_pressed = false;