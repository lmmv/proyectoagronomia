/// @function scr_pause_game(pause)
/// @description Activa o desactiva las instancias según el parámetro recibido
/// @argument0 pause Valor bool para pausar o reanudar el juego

pause = argument0;

if !pause {
	instance_activate_all();	
}

scr_stop_background(pause);


if pause {
	instance_deactivate_all(true);
}