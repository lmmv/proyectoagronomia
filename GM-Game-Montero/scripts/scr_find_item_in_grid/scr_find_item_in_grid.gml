/// @function scr_find_item_in_grid()
/// @description Busca un objeto en el grid y fija el nombre de este y la fila en que se encuentra
/// @argument0 grid Grid en el que se debe buscar el objeto

grid = argument0;

for(i = 0; i < ds_grid_height(grid); ++i) {
	if(grid[# 1, i] == object_index) {
		name = grid[# 0, i];
		grid_y = i;
		return true;
		break;
	}
}

return false;