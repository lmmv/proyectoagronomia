
scr_draw_map();

var mentor_x = global.dw * 0.12;
var mentor_y = global.dh * 0.65;
draw_sprite(spr_mentor,0,mentor_x,mentor_y);

if mouse_over {	
	var bg_x = global.dw * 0.35;
	var bg_y = global.dh * 0.68;
	draw_sprite(spr_bg_box,0,bg_x,bg_y);
	
	var box_x = bg_x
	var box_y = bg_y - sprite_get_height(spr_bg_box)/2;
	draw_sprite(spr_box,0,box_x,box_y);
	
	draw_set_font(fnt_text);
	draw_set_color(c_white);
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	draw_text(box_x,box_y,prov_name);
}