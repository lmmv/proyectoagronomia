{
    "id": "84f4a84b-a0b4-4451-af89-5b0763745496",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_inv_back",
    "eventList": [
        {
            "id": "93f85ab9-d6bc-4b25-aedd-aa4cf89c3d0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "84f4a84b-a0b4-4451-af89-5b0763745496"
        },
        {
            "id": "1509734f-fcdb-4a8f-8434-946641262917",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "84f4a84b-a0b4-4451-af89-5b0763745496"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f9e08a99-8e2b-4160-9b8b-571f0121b70b",
    "visible": true
}