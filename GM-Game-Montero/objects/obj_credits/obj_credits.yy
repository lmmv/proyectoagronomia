{
    "id": "69c5e426-e5f8-4c3e-873c-d3971d56690e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_credits",
    "eventList": [
        {
            "id": "1459f1c7-a740-464e-ab3f-a8664495a843",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "69c5e426-e5f8-4c3e-873c-d3971d56690e"
        },
        {
            "id": "14bbbf54-da71-45c9-9de0-65459720f2e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "69c5e426-e5f8-4c3e-873c-d3971d56690e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4ab0830c-06fb-4aba-9371-0de9a5a9fcc5",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}