{
    "id": "453268b6-a6dd-44b3-baae-c0dfdf4e8c5a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_map_alajuela",
    "eventList": [
        {
            "id": "863a8313-ad83-4f9e-94cc-3d04d3364c1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "453268b6-a6dd-44b3-baae-c0dfdf4e8c5a"
        },
        {
            "id": "122adaf5-31bf-49ef-ad24-abfd9e3f9672",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "453268b6-a6dd-44b3-baae-c0dfdf4e8c5a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4264db56-59ae-4882-bbab-a26f38cc1bc1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e22222e0-df24-4ef4-a1f3-d745ad1f75b0",
    "visible": true
}