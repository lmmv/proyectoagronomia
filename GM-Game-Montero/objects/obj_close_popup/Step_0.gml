
event_inherited();

if global.allow_input {
	if hit {
		switch (option) {
			case 0:
				scr_change_room(rm_map);
				break;
			case 1:			
				instance_destroy();
				break;
		}
	} 

	scr_buttons(arr_options);
}