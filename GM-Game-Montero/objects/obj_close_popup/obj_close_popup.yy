{
    "id": "08e595bd-3165-4eda-9670-7c0f1bfdc237",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_close_popup",
    "eventList": [
        {
            "id": "1abd0ac0-7fd6-46be-8199-7649183d1d51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "08e595bd-3165-4eda-9670-7c0f1bfdc237"
        },
        {
            "id": "5f0c851a-1206-4625-8295-43851d874f6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "08e595bd-3165-4eda-9670-7c0f1bfdc237"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5adf8b11-99cf-4010-bdec-87dc94e124dd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}