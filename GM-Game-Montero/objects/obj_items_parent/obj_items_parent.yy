{
    "id": "2a35dab2-5276-41e1-90c4-6b1db1e856aa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_items_parent",
    "eventList": [
        {
            "id": "6d13e747-dd4e-44b1-865a-7c8caef5d481",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2a35dab2-5276-41e1-90c4-6b1db1e856aa"
        },
        {
            "id": "b8748392-8179-49bf-ae0c-233a2a4604cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2a35dab2-5276-41e1-90c4-6b1db1e856aa"
        },
        {
            "id": "e308b1a4-adf4-415c-a647-6eecfb596af9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2a35dab2-5276-41e1-90c4-6b1db1e856aa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}