
inv_grid = obj_inventory.inventory_grid;
sel_grid = obj_selected_items.selected_items_grid;

mouse_over = position_meeting(device_mouse_x_to_gui(0),device_mouse_y_to_gui(0),id);
if mouse_over {		
	mouse_down = device_mouse_check_button_pressed(0,mb_left);
	if mouse_down {	
		pressed = true;	
		if !scr_find_item_in_grid(inv_grid) {
			scr_find_item_in_grid(sel_grid)
		}
	}
}

if pressed {	
	//el objeto sigue al cursor mientras esté presionado
	x = device_mouse_x_to_gui(0); 
	y = device_mouse_y_to_gui(0);
	
	mouse_up = device_mouse_check_button_released(0,mb_left);
	if mouse_up {
		pressed = false;			
		//si no soltó el objeto en un espacio válido, se devuelve al origen
		if !scr_check_slot(inv_grid) and !scr_check_slot(sel_grid) {
			x = xstart; 
			y = ystart;
		}
	}
}
else {
	name = "";	
}