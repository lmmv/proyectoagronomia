{
    "id": "c4edc8ee-9f4c-46f3-a1eb-b63683d51a29",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_inv_forward",
    "eventList": [
        {
            "id": "2b160332-a766-475e-9d8b-3a1007f623e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c4edc8ee-9f4c-46f3-a1eb-b63683d51a29"
        },
        {
            "id": "c92e7366-6f83-44fd-b4fa-f5c30efec098",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c4edc8ee-9f4c-46f3-a1eb-b63683d51a29"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f9e08a99-8e2b-4160-9b8b-571f0121b70b",
    "visible": true
}