
spray_system = part_system_create();

part_system_depth(spray_system,depth);

spray_part = part_type_create();

part_type_shape(spray_part,pt_shape_cloud);
part_type_size(spray_part,0.2,0.3,-0.003,0);
//part_type_scale(spray_part,1,1);
part_type_speed(spray_part,4,9,-0.1,0);
part_type_direction(spray_part,180,190,0,0);
part_type_gravity(spray_part,0.07,270);
//part_type_orientation(spray_part,);
part_type_colour2(spray_part,c_white,c_fuchsia);
//part_type_blend(spray_part,true);
part_type_alpha2(spray_part,0.7,0.1);
part_type_life(spray_part,80,100);
//part_type_step(spray_part);
//part_type_death(spray_part);
