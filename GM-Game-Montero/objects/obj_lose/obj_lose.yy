{
    "id": "363c2f94-2534-4418-870c-77513e71a8da",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_lose",
    "eventList": [
        {
            "id": "750b583d-d1d4-437e-9c6d-d466b51a798c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "363c2f94-2534-4418-870c-77513e71a8da"
        },
        {
            "id": "6d122b6e-ed16-48df-a711-53362d691fe3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "363c2f94-2534-4418-870c-77513e71a8da"
        },
        {
            "id": "e848d6bd-23d2-475d-84da-94fdc7cc7233",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "363c2f94-2534-4418-870c-77513e71a8da"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c37253b0-7015-4bc9-837d-68bed537a635",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}