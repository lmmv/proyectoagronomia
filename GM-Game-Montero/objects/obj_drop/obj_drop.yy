{
    "id": "96be6bb5-5f31-4689-add6-ad619bb9193b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_drop",
    "eventList": [
        {
            "id": "23e8a353-66c6-4cba-8a9d-add96205a064",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "96be6bb5-5f31-4689-add6-ad619bb9193b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "86dc9c4f-c359-4d13-b8ae-5a9d589643ad",
    "visible": true
}