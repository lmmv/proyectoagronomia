{
    "id": "d13bceb3-179e-416b-b6c5-f896256b51e4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_standard_button",
    "eventList": [
        {
            "id": "063f6b5a-62b7-4767-9e4d-bc63ed9cdc7c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d13bceb3-179e-416b-b6c5-f896256b51e4"
        },
        {
            "id": "e4aa7632-b5a4-4d29-96bd-87fb54b55098",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d13bceb3-179e-416b-b6c5-f896256b51e4"
        },
        {
            "id": "b319a8f8-e502-4d98-b7aa-508d24a648ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "d13bceb3-179e-416b-b6c5-f896256b51e4"
        },
        {
            "id": "d2487703-b377-4f91-86a1-49e34a51e8e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "d13bceb3-179e-416b-b6c5-f896256b51e4"
        },
        {
            "id": "977109e6-b44c-4d1e-8814-c7b4f267aca5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "d13bceb3-179e-416b-b6c5-f896256b51e4"
        },
        {
            "id": "f41dc3db-c588-42cf-8315-164da3dadb6f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "d13bceb3-179e-416b-b6c5-f896256b51e4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}