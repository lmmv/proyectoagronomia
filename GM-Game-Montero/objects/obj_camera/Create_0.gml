
width = 768;
height = 432;

follow = obj_drone; //objeto al que sigue la cámara

//x = global.dw/2
//y = global.dh/2;

view_enabled = true;

camera = camera_create_view(0, 0, width, height, 0, follow, -1, -1, width, height);

for (i = 0; i <= 7; i++) {
	view_visible[i] = false;
}

view_visible[0] = true;
view_camera[0] = camera;

//mm_camera = camera_create_view(0,0,room_width,room_height);
//view_visible[1] = true;
//view_camera[1] = mm_camera;

//view_set_xport(1, global.dw * 0.1);
//view_set_yport(1, global.dh * 0.7);
//view_set_wport(1,global.dw * 0.3);
//view_set_hport(1,global.dh * 0.3);
