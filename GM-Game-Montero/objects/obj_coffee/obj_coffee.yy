{
    "id": "87b42b0d-0445-42f4-ba4f-802e65db2f72",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_coffee",
    "eventList": [
        {
            "id": "3b0c3c31-b644-4980-a397-68407b8e4ff7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "87b42b0d-0445-42f4-ba4f-802e65db2f72"
        },
        {
            "id": "3c84c7f5-539a-474e-b445-b50191659427",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "87b42b0d-0445-42f4-ba4f-802e65db2f72"
        },
        {
            "id": "08e4c6da-e1cf-410c-bf35-2f6aad1b9a0a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "87b42b0d-0445-42f4-ba4f-802e65db2f72"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "424ffee0-df02-4502-ae9a-697e488c8349",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "356d686e-52f8-432a-bbee-c6567bbc7589",
    "visible": true
}