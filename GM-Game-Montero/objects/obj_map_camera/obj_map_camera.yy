{
    "id": "9a1f1fbd-9e0f-4286-afc1-f9baab9af323",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_map_camera",
    "eventList": [
        {
            "id": "88e5777b-0e99-43ae-a6f2-71bd1a066fc2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9a1f1fbd-9e0f-4286-afc1-f9baab9af323"
        },
        {
            "id": "0f24dcad-f0ab-4681-8171-1f5bc23ef610",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9a1f1fbd-9e0f-4286-afc1-f9baab9af323"
        },
        {
            "id": "9ba4c147-83c1-45a2-ab34-70cbf18802db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "9a1f1fbd-9e0f-4286-afc1-f9baab9af323"
        },
        {
            "id": "5ae55b35-dbf6-4ca4-a8ec-27a9a54a6247",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "9a1f1fbd-9e0f-4286-afc1-f9baab9af323"
        },
        {
            "id": "04c134b1-cf87-4217-aa17-cf8be81f8223",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9a1f1fbd-9e0f-4286-afc1-f9baab9af323"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}