
global.width = 768;
global.height = 432;


x = global.dw/2
y = global.dh/2;

view_enabled = true;

camera = camera_create_view(0, 0, global.width, global.height, 0, obj_map_camera, -1, -1, global.width, global.height);

for (i = 0; i <= 7; i++) {
	view_visible[i] = false;
}

view_visible[0] = true;
view_camera[0] = camera;

drag_x = 0;
drag_y = 0;

mouse_xprevious = mouse_x;
mouse_yprevious = mouse_y;