
if (device_mouse_check_button_pressed(0,mb_left)) { 
	drag_x = device_mouse_x(0); 
	drag_y = device_mouse_y(0); 
}
if (device_mouse_check_button(0,mb_left)) && global.allow_input {
	x += drag_x - device_mouse_x(0);
	y += drag_y - device_mouse_y(0);
	if device_mouse_x(0) != mouse_xprevious or device_mouse_y(0) != mouse_yprevious
		global.dragging_mouse = true;
	else
		alarm[0] = 4;
}
mouse_xprevious = device_mouse_x(0);
mouse_yprevious = device_mouse_y(0);


x = clamp(x,0,room_width);
y = clamp(y,0,room_height);


