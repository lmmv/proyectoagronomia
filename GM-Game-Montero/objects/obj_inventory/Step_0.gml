
if obj_inv_back.hit {
	obj_inv_back.hit = false;
	if page > 0	
		page -= 1;
	else
		page = total_pages - 1;
	scr_destroy_items();
	scr_show_inv_items();
}

if obj_inv_forward.hit {
	obj_inv_forward.hit = false;
	if page < total_pages - 1
		page += 1;
	else
		page = 0;
	scr_destroy_items();
	scr_show_inv_items();
}