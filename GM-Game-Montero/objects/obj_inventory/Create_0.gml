
inventory_width = 2;
inventory_grid = ds_grid_create(inventory_width,1);	//[string nombre, object_index]

if room == rm_limon_lab {
	scr_add_limon_items();	
}
else if room == rm_cartago {
	scr_add_cartago_items();	
}


total_items = ds_grid_height(inventory_grid);
items_per_page = 6;
total_pages = ceil(total_items/items_per_page);
page = 0;
columns = 3;
x_spacing = 136;
y_spacing = x_spacing;

//xx = global.dw * 0.73;
//yy = global.dh * 0.11;
//x_start = xx;

scr_show_inv_items();