{
    "id": "9d88d0e2-3578-430e-aabe-2203dd94ef9c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_inventory_slots",
    "eventList": [
        {
            "id": "eed506bd-9c6b-46d9-b81f-f5087e3a4960",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9d88d0e2-3578-430e-aabe-2203dd94ef9c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a89c9c5b-9859-406a-a7bc-8cde3591155a",
    "visible": true
}