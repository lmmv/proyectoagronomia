{
    "id": "07d3c121-7dda-46eb-a497-6e46988e2ba0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_menu",
    "eventList": [
        {
            "id": "08ec7d5f-03a7-44d2-8490-e75fca2acbef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "07d3c121-7dda-46eb-a497-6e46988e2ba0"
        },
        {
            "id": "b01d0331-16ec-44c2-8504-b0f8ec258c5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "07d3c121-7dda-46eb-a497-6e46988e2ba0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c37253b0-7015-4bc9-837d-68bed537a635",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}