
event_inherited();

if hit and global.allow_input {
	switch (option) {
		case 0:
			scr_change_room(rm_dialog_intro_map);
			break;
		case 1:
			scr_change_room(rm_gamecredits);
			break;
		case 2:
			game_end();
			break;
	}
} 

