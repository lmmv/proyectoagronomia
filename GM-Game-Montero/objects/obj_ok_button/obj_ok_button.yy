{
    "id": "08b4c94a-9304-4710-943d-5ff6929b05ee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ok_button",
    "eventList": [
        {
            "id": "8981ad57-3019-4b3e-8f46-0894705c88f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "08b4c94a-9304-4710-943d-5ff6929b05ee"
        },
        {
            "id": "19eabff8-73b5-4970-b746-ec5db8cf5f7c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "08b4c94a-9304-4710-943d-5ff6929b05ee"
        },
        {
            "id": "7d68e0c9-3bee-44df-bd8e-c90a213488ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "08b4c94a-9304-4710-943d-5ff6929b05ee"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ae221623-3728-43ae-9d27-df554bf3fd87",
    "visible": true
}