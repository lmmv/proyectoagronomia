
part_system_clear(cloud_system);
part_particles_clear(cloud_particle);
part_type_clear(cloud_particle);
part_emitter_clear(cloud_system,cloud_emitter);
part_emitter_destroy(cloud_system,cloud_emitter);
part_emitter_destroy_all(cloud_system);
part_type_destroy(cloud_particle);
part_system_destroy(cloud_system);