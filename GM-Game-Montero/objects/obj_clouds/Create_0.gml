
cloud_system = part_system_create();
part_system_depth(cloud_system,-2);

//propiedades de las gotas
cloud_particle = part_type_create(); 
part_type_shape(cloud_particle,pt_shape_cloud); 
part_type_size(cloud_particle,3,9,0,0); 
//part_type_color2(cloud_particle,c_gray,c_white); 
part_type_alpha2(cloud_particle,.5,.7);
part_type_speed(cloud_particle,2,2,0,0);
part_type_direction(cloud_particle,180,180,0,0);
//part_type_gravity(cloud_particle,0.1,180); 
//part_type_orientation(cloud_particle,280,280,0,0,0); 
part_type_life(cloud_particle,global.dw,global.dw); 

//emitter 
cloud_emitter = part_emitter_create(cloud_system);
part_emitter_region(cloud_system,cloud_emitter,global.dw+150,global.dw+400,0,global.dh*0.4,ps_shape_ellipse,ps_distr_linear);
//part_emitter_stream(cloud_system,cloud_emitter,cloud_particle,1); 
alarm[0] = 3;

part_emitter_burst(cloud_system,cloud_emitter,cloud_particle,5 + irandom(5));

repeat (room_speed * 10){ 
	part_system_update(cloud_system);
}

