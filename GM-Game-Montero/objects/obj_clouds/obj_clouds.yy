{
    "id": "5c7fb9c7-44f5-497a-ab1c-5147143169b1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_clouds",
    "eventList": [
        {
            "id": "576fdcb3-6fe2-4644-853e-362bf358c740",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5c7fb9c7-44f5-497a-ab1c-5147143169b1"
        },
        {
            "id": "650df819-bec0-43a9-b9c5-97aae3003d2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "5c7fb9c7-44f5-497a-ab1c-5147143169b1"
        },
        {
            "id": "605ad305-2b6a-4d41-b120-3869dab1fd51",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "5c7fb9c7-44f5-497a-ab1c-5147143169b1"
        },
        {
            "id": "e4fce6d4-b609-4de4-9987-a70206b630d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "5c7fb9c7-44f5-497a-ab1c-5147143169b1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}