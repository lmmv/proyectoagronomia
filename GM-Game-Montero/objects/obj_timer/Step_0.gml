
if global.allow_input and set_time {
	alarm[0] = game_get_speed(gamespeed_fps) * (alarm_time + 1);
	set_time = false;
}

time_string = scr_get_timer(alarm[0]);

