
draw_set_font(fnt_text);
draw_set_halign(fa_center);
draw_set_color(c_orange);

if (alarm[0] <= 600) and (alarm[0] != -1) {
	text_scale = ((sin(current_time/450)+1)/2) + 1;
}

draw_text_transformed(x,y,time_string,text_scale,text_scale,image_angle);
