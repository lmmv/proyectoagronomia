{
    "id": "45c0ede2-82ce-4fc1-b09c-86fc574179f3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_timer",
    "eventList": [
        {
            "id": "cacbf957-ebb0-4f0f-a6ef-08e0bbdb816f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "45c0ede2-82ce-4fc1-b09c-86fc574179f3"
        },
        {
            "id": "644a77d4-fc5d-409e-a229-29bb3226e1cc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "45c0ede2-82ce-4fc1-b09c-86fc574179f3"
        },
        {
            "id": "b1976d26-0584-4ba1-be88-bbb2b714488e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "45c0ede2-82ce-4fc1-b09c-86fc574179f3"
        },
        {
            "id": "284eb2c1-2045-437c-9396-123e573be29d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "45c0ede2-82ce-4fc1-b09c-86fc574179f3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}