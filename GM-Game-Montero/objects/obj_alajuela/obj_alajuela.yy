{
    "id": "e55f52ce-e49a-4258-8af4-cfe2594f79ff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_alajuela",
    "eventList": [
        {
            "id": "d4f8a1ce-15a4-487f-abd1-c2f71456d2cf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e55f52ce-e49a-4258-8af4-cfe2594f79ff"
        },
        {
            "id": "bfdff1d7-abd7-4ed2-bcc4-656cb4dd61b6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e55f52ce-e49a-4258-8af4-cfe2594f79ff"
        },
        {
            "id": "195ae93f-fe1f-4d30-a4c7-03fced972167",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "e55f52ce-e49a-4258-8af4-cfe2594f79ff"
        },
        {
            "id": "e257d27f-fc6a-4383-97df-c47771f7fab7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e55f52ce-e49a-4258-8af4-cfe2594f79ff"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}