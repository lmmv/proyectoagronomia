/// @description Desactiva las plantas fuera del view
instance_deactivate_layer(lay_id);
var vx = camera_get_view_x(view_camera[0]) - 128;
var vy = camera_get_view_y(view_camera[0]) - 128;
var vw = camera_get_view_width(view_camera[0]) + 196;
var vh = camera_get_view_height(view_camera[0]) + 196;
instance_activate_region(vx, vy, vw, vh, true);

alarm[0] = 15;