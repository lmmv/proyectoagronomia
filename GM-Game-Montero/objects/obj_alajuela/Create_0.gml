
instance_create_depth(x,y,depth,obj_close);
instance_create_depth(x,y,depth,obj_camera);
instance_create_depth(x,y,depth,obj_minimap);
instance_create_depth(global.dw/2,global.dh*0.9,depth, obj_drone);

//tiempo
time_x = global.dw * 0.15;
time_y = global.dh * 0.15;
scr_set_timer(time_x,time_y + 10,90);

found_x = global.dw * 0.85;
found_y = global.dh * 0.15;
total_plagues = 10;
plagues_found = 0;


lay_id = layer_get_id("Coffee_Layer");

//crea varias lineas de matas de café
var cof_width = sprite_get_width(spr_coffee) + 80;
var cof_height = sprite_get_height(spr_coffee);
for (var xx = 50; xx < room_width - 50; xx += cof_width) {
    for (var yy = 50; yy < room_height - 50; yy += cof_height) {
		var rand_x = random_range(xx-3,xx+3);	
		instance_create_layer(rand_x,yy,lay_id,obj_coffee);
	}
}
total_plants = instance_number(obj_coffee) - 1;

//asigna plagas aleatoriamente a algunas de las plantas
for (var i = 0; i < total_plagues; i++) {
	var random_inst = irandom(total_plants)
	var inst = instance_find(obj_coffee,random_inst)
	if !inst.plague {
		inst.plague = true;		
	}
	else 
		i--;
}

//guarda una lista con el id de cada planta
coffee_list = ds_list_create();
var i = 0;
with (obj_coffee) {
	other.coffee_list[| i] = id;
	i++;
}

alarm[0] = 30;

