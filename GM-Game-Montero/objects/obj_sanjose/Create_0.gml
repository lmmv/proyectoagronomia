
instance_create_depth(x,y,depth,obj_close);

//tiempo
time_x = global.dw * 0.85;
time_y = global.dh * 0.2;
scr_set_timer(time_x,time_y + 20,60);

correct_x = global.dw * 0.85;
correct_y = global.dh * 0.4;
correct_total = 6;
correct = 0;
change = true;
reagent = false;
move_current = false;
accept = false;
discard = false;

//botones para aprobar o descartar muestra
btn_space = 140;
accept_x = global.dw/2 - btn_space;
discard_x = global.dw/2 + btn_space;
accept_y = global.dh * 0.75;
discard_y = accept_y
instance_create_depth(accept_x,accept_y,depth,obj_cmt_accept);
instance_create_depth(discard_x,discard_y,depth,obj_cmt_discard);

max_queue = 6;
paddle_queue = ds_queue_create();
paddle_queue_copy = ds_queue_create();
var xx = global.dw * 0.1;
var yy = global.dh * 0.8;
for (var i = 0; i < max_queue; i++) {
	var paddle = instance_create_depth(xx,yy,depth,obj_paddle);
	ds_queue_enqueue(paddle_queue,paddle);
	yy = yy + global.dh * -0.15;	
}