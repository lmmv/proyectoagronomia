
draw_sprite_ext(spr_bg,0,time_x,time_y,0.3,0.4,0,c_white,1);
draw_sprite_ext(spr_bg,0,correct_x,correct_y,0.3,0.4,0,c_white,1);

draw_set_halign(fa_center);
draw_set_font(fnt_text);
draw_set_color(c_white);
draw_text(time_x,time_y-30,"Tiempo");

draw_text(correct_x,correct_y-30,"Aciertos");
draw_text(correct_x,correct_y + 20,string(correct) + "/" + string(correct_total));


//draw_text(global.dw/2,global.dh*0.20,"ACCEPT: " + string(accept)); 
//draw_text(global.dw/2,global.dh*0.25,"DISCARD: " + string(discard)); 