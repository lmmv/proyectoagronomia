{
    "id": "09da239f-ac7b-448f-8645-4c1584f2046e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_sanjose",
    "eventList": [
        {
            "id": "a49d8071-c908-4631-8c5f-a437f756ef9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "09da239f-ac7b-448f-8645-4c1584f2046e"
        },
        {
            "id": "1a1afa9a-c5e3-4ba6-afec-8f32c217f3e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "09da239f-ac7b-448f-8645-4c1584f2046e"
        },
        {
            "id": "73bf5071-4e41-492c-96f1-d16800592f01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "09da239f-ac7b-448f-8645-4c1584f2046e"
        },
        {
            "id": "72a0310f-3b5d-4960-819d-8baa03c8e398",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 7,
            "m_owner": "09da239f-ac7b-448f-8645-4c1584f2046e"
        },
        {
            "id": "d1313ca3-2f4f-4337-9013-0095f6a52bf9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "09da239f-ac7b-448f-8645-4c1584f2046e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}