
if change {
	if !ds_queue_empty(paddle_queue) {
		current_paddle = ds_queue_dequeue(paddle_queue);
	}
	move_current = true;
	change = false;
}


with (current_paddle) {
	//mueve la primera paleta en la cola y vierte el reactivo
	if other.move_current {
		var dest_x = global.dw * 0.5;
		var dest_y = global.dh * 0.5;
		var spd = 8;

		if !mp_linear_step(dest_x,dest_y,spd,0) {
			var scale = 0.009;
			image_xscale = min(image_xscale + scale,2);
			image_yscale = min(image_yscale + scale,2);
		}			
		else {
			other.move_current = false;
			//echarle el reactivo	
			if !other.reagent {
				var xx = global.dw * 0.55;
				var yy = global.dh * 0.3;
				instance_create_depth(xx,yy,depth-1,obj_reagent);
				other.reagent  = true;
			}
			other.alarm[0] = 30;
		}
	}

	if obj_cmt_accept.hit {
		other.accept = true;
		obj_cmt_parent.block = true;
	}
	if obj_cmt_discard.hit {
		other.discard = true;
		obj_cmt_parent.block = true;
	}

	if other.accept {
		if mp_linear_step(global.dw*0.9,global.dh*0.8,8,0) {
			other.accept = false;
			if cmt_positive {			
				scr_ending(0);	
			}
			else {
				other.correct++;				
				instance_destroy();	
			}
		}
	}
	if other.discard {		
		//if mp_linear_step(global.dw*0.9,global.dh*0.8,8,0) {
			other.discard = false;
			if cmt_positive {	
				other.correct++;
				instance_destroy();
			}
			else {
				//perder puntos ????
				instance_destroy();
			}
		//}		
	}
}

if correct == correct_total {
	scr_ending(1);	
}