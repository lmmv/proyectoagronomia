{
    "id": "c7ad01fa-761e-4f28-a612-85b41180580b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cmt_accept",
    "eventList": [
        {
            "id": "3784b312-ad1a-49bc-a761-5847adf58124",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c7ad01fa-761e-4f28-a612-85b41180580b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2b5f9ef0-7a3f-4d54-a02e-c4021d9c1f55",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "83eac364-43c5-4f92-a14c-24dbfaf3847d",
    "visible": true
}