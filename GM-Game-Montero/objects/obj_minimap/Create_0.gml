
pct = 0.26;
minimap_width = global.dw * pct;
minimap_height = global.dh * pct;

x1 = global.dw * 0.7;
y1 = global.dh * 0.7;
x2 = x1 + minimap_width
y2 = y1 + minimap_height;

alpha = 0.8;

drn_subimg = 0;
img_spd = 15;
alarm[0] = img_spd;