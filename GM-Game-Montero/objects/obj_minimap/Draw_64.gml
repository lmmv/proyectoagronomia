

draw_set_alpha(alpha);
draw_set_color(c_white);
draw_rectangle(x1,y1,x2,y2,false);

//var coffee = obj_coffee;
//with (coffee) {
//	var coff_x = x * other.pct + other.x1;
//	var coff_y = y * other.pct + other.y1;
//	draw_sprite_ext(sprite_index,image_index,coff_x,coff_y,other.pct,other.pct,image_angle,c_white,other.alpha);
//}

var list = obj_alajuela.coffee_list;
for (var i = 0; i < ds_list_size(list); i++) {
	var inst = list[| i]
	var coff_x = inst.x * pct + x1;
	var coff_y = inst.y * pct + y1;
	var plague = inst.plague;
	var scanned = inst.scanned;
	var subimg = 0;
	if scanned 
		subimg = 1;	
	//muestra en el minimapa donde hay plantas y las plagas escaneadas
	draw_sprite_ext(spr_coffee_mm,subimg,coff_x,coff_y,pct,pct,0,c_white,alpha);
}

//muestra en el minimapa la ubicación del dron
var drone = obj_drone;
if instance_exists(drone) {
	var drone_x = drone.x * pct + x1;
	var drone_y = drone.y * pct + y1;
	var subimg = irandom(1);
	draw_sprite_ext(spr_drone_mm,drn_subimg,drone_x,drone_y,pct,pct,drone.image_angle,c_white,alpha);	
}

draw_set_alpha(1);
