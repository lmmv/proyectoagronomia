	
event_inherited();

if global.allow_input {
	if hit {
		switch (option) {
			case 0:
				room_restart();
				break;
			case 1:			
				scr_change_room(rm_map);
				break;
		}
	}
	
	scr_buttons(arr_options);
} 

