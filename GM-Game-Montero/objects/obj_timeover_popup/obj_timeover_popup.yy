{
    "id": "62fb4a99-ac7e-4a1d-8561-9b6387492d1c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_timeover_popup",
    "eventList": [
        {
            "id": "5076974c-2cfa-4cd0-99e0-c854a6577711",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "62fb4a99-ac7e-4a1d-8561-9b6387492d1c"
        },
        {
            "id": "465ae192-b50b-4d85-bb7d-0d3551268fb4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "62fb4a99-ac7e-4a1d-8561-9b6387492d1c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5adf8b11-99cf-4010-bdec-87dc94e124dd",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}