{
    "id": "70247757-0265-468e-89f2-04d31553c116",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fade_in",
    "eventList": [
        {
            "id": "7b367841-a16e-4a56-b79b-2da00115aa9f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "70247757-0265-468e-89f2-04d31553c116"
        },
        {
            "id": "3dca76a6-7902-4ab5-aa1a-8fc138ae87fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "70247757-0265-468e-89f2-04d31553c116"
        },
        {
            "id": "6447d7cb-0ddd-41af-b48e-3aed55ec2c83",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "70247757-0265-468e-89f2-04d31553c116"
        },
        {
            "id": "a5d57e48-6d96-4ff3-aa24-ee2fd0e5a1ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "70247757-0265-468e-89f2-04d31553c116"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "21e98c0f-bd3a-462a-b423-113294e29e35",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}