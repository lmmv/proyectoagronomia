{
    "id": "34a68efe-5a92-4b97-b3af-ea7adcddb531",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_ground",
    "eventList": [
        {
            "id": "b7c7d288-cfa1-456a-8019-43ed4f8a7a5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "34a68efe-5a92-4b97-b3af-ea7adcddb531"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "930e2c56-bf31-4ccd-b272-20356bbcce89",
    "visible": true
}