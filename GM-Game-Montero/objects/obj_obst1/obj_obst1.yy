{
    "id": "7b77c6e0-82ac-4119-87a1-d4c5b8db86a1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_obst1",
    "eventList": [
        {
            "id": "6ed1eb8f-53ba-4fb1-8f3f-132415238aeb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7b77c6e0-82ac-4119-87a1-d4c5b8db86a1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3dd11904-8d3a-4609-b375-f9aafdee0dbb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "79d9c04c-d3ca-4db0-a126-f750a52a49cb",
    "visible": true
}