
event_inherited();

if instance_exists(obj_plane) {					//si el avión existe
	with (obj_plane) {
		if distance_to_point(other.x,y) < 200 {	//y está cerca de un poste
			allow_spray = false;
		}
		if distance_to_point(other.x,y) < 20 {
			if spraying {						//y está rociando el pesticida
				banned = true;					//pierde el juego
			}
		}
	}
}