{
    "id": "d4e266ae-dd2e-45e2-a7ec-807401bfc2db",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_scan",
    "eventList": [
        {
            "id": "e0e42c18-62c2-45d4-b4bb-79b3c97717f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d4e266ae-dd2e-45e2-a7ec-807401bfc2db"
        },
        {
            "id": "defe6e4f-a804-4b1d-b8aa-49448cd22ea4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d4e266ae-dd2e-45e2-a7ec-807401bfc2db"
        },
        {
            "id": "1c8834ad-0fbf-4a0c-b605-0122c4fd911f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "d4e266ae-dd2e-45e2-a7ec-807401bfc2db"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}