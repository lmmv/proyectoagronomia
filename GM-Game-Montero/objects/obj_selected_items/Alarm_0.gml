///@description Crea el grid de elementos seleccionados

selected_items_grid = ds_grid_create(selected_width,total_items);

//se inicializa el grid vacío de elementos seleccionados
for (i=0; i<ds_grid_height(selected_items_grid); i++) {
	selected_items_grid[# 0, i] = "";
	selected_items_grid[# 1, i] = -1;
}

x_start = xx;

//crea los espacios para los elementos seleccionados
for (i = 0; i < total_items; i++) {
	if (i mod columns == 0) and i > 0 {
		xx = x_start;
		yy += y_spacing;
	}
	var inst = instance_create_depth(xx, yy, depth, obj_selected_slots);	
	with inst {
		slot_number = other.i;
	}
	xx += x_spacing;
}
