{
    "id": "5adf8b11-99cf-4010-bdec-87dc94e124dd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_popup_parent",
    "eventList": [
        {
            "id": "c02b1196-0326-4517-ae29-47cf4cfcd714",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5adf8b11-99cf-4010-bdec-87dc94e124dd"
        },
        {
            "id": "9579f1ab-575d-4502-8852-3ea297657bad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "5adf8b11-99cf-4010-bdec-87dc94e124dd"
        },
        {
            "id": "031d2b21-82f6-40aa-afb3-c9da341d8425",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5adf8b11-99cf-4010-bdec-87dc94e124dd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}