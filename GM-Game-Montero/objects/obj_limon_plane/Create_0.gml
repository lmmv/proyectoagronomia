
instance_create_depth(x,y,depth,obj_close);

//crea la capa del fondo del room
soil_layer = layer_create(100);
sl_layer = layer_background_create(soil_layer,spr_ground);
layer_y(soil_layer,950);
sl_speed = -6;
layer_background_htiled(sl_layer,true);
layer_hspeed(soil_layer,sl_speed);

back_layer = layer_create(200);
bg_layer = layer_background_create(back_layer,spr_plane_background);
bg_speed = -2
layer_background_htiled(bg_layer,true);
layer_hspeed(back_layer,bg_speed);


instance_create_depth(global.dw*0.25,global.dh*0.4,0,obj_plane);	//crea la avioneta
instance_create_depth(global.dw+40,global.dh,0,obj_obst_spawner);	//crea generador de obstáculos
instance_create_depth(x,y,0,obj_clouds);	//partículas de nubes

scr_set_timer(global.dw*0.5,global.dh*0.1,180);