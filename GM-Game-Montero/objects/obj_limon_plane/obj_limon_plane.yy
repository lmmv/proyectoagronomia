{
    "id": "fbc88ba0-823e-4f0d-af4a-b5cae2f8dbdd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_limon_plane",
    "eventList": [
        {
            "id": "d87eb4d3-5cdc-472f-83b8-a471dcd4ce9c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fbc88ba0-823e-4f0d-af4a-b5cae2f8dbdd"
        },
        {
            "id": "0b13bf36-5d8a-407d-94a4-2c4c5c1e416a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fbc88ba0-823e-4f0d-af4a-b5cae2f8dbdd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}