{
    "id": "2db0fa57-80a5-47ec-a330-8f1fb3bf7e90",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_map_puntarenas",
    "eventList": [
        {
            "id": "3e824bd8-bbc2-4278-8a10-c68d3be993f2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2db0fa57-80a5-47ec-a330-8f1fb3bf7e90"
        },
        {
            "id": "44fc6c8e-6819-4eb8-b24f-a8be32eaa3c1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2db0fa57-80a5-47ec-a330-8f1fb3bf7e90"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4264db56-59ae-4882-bbab-a26f38cc1bc1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "8b0a0318-14ed-4ee2-9ee6-b7c8bd88f75b",
    "visible": true
}