
if move {
	if mp_linear_step(x,yy,2,0) {
		move = false;
		yy = y + global.dh * 0.15;
	}	
}

if place_meeting(x,y,obj_drop) {	
	//si cae la gota en la muestra
	with obj_drop {
		vspeed = 0;
		gravity = 0;
		instance_destroy();
	}
	instance_destroy(obj_reagent);
	//genera reacción
	image_index = 1;
	shaking = true;	
	alarm[0] = 220;
}

if shaking {
	shake += shake_speed;
	var offset = sin(shake) * shake_intensity;
	x = x + offset;
	y = y + offset;
}
