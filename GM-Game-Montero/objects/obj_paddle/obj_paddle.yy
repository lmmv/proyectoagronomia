{
    "id": "0571162f-90c2-4103-93ef-b99426cbefee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_paddle",
    "eventList": [
        {
            "id": "2fb86354-d88c-43a4-8a15-3410a2f7fdd7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0571162f-90c2-4103-93ef-b99426cbefee"
        },
        {
            "id": "705797f0-bee9-431b-8b40-afcab2dc2c73",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0571162f-90c2-4103-93ef-b99426cbefee"
        },
        {
            "id": "c7142b53-cef1-4ec2-8aa9-226872390f4a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0571162f-90c2-4103-93ef-b99426cbefee"
        },
        {
            "id": "8a407afb-fa98-466d-a3de-60e453227d77",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "0571162f-90c2-4103-93ef-b99426cbefee"
        },
        {
            "id": "faa3da8d-903e-4d8f-a618-49f314e5eb7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "0571162f-90c2-4103-93ef-b99426cbefee"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "35a7a401-c8db-4d7b-94af-cbb667be7e81",
    "visible": true
}