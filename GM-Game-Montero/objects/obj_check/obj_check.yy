{
    "id": "7548f75b-8734-490b-b64a-f456d05e6525",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_check",
    "eventList": [
        {
            "id": "f3742f80-ffaf-48b5-a0be-d415ec41258d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7548f75b-8734-490b-b64a-f456d05e6525"
        },
        {
            "id": "5eaf93e2-239e-46f0-8510-8f41f325fbdd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7548f75b-8734-490b-b64a-f456d05e6525"
        },
        {
            "id": "b40d93a2-bced-432d-8352-7a3b0512cf74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7548f75b-8734-490b-b64a-f456d05e6525"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ae221623-3728-43ae-9d27-df554bf3fd87",
    "visible": true
}