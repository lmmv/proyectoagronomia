
if show_info {
	draw_sprite_ext(spr_bg,0,global.dw/2,global.dh/2,1.5,1.5,0,c_white,1);
	draw_set_color(c_black);
	draw_set_halign(fa_center);
	draw_set_font(fnt_text);	
	if correct {
		draw_text(global.dw*0.5,global.dh*0.5,correct_string);
	}
	else {
		draw_text(global.dw*0.5,global.dh*0.5,incorrect_string);
	}
}
else {
	draw_self();
	draw_set_valign(fa_middle);
	draw_set_halign(fa_center);
	draw_set_font(fnt_buttons);
	draw_set_color(c_white);
	draw_text(x,y,"Verificar");
}