
if !show_info
	hit = scr_mouse_events();

if hit {
	show_info = true;	
	correct = scr_compare_items();
	instance_deactivate_all(true);
	close = instance_create_depth(global.dw*0.8,global.dh*0.1,depth, obj_close_info);
	close.owner = id;
	if correct {
		instance_destroy(close);	
		instance_create_depth(global.dw*0.5,global.dh*0.6,depth-1,obj_ok_button);
	}
	hit = false;
}

