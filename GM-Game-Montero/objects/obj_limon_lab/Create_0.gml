
//se crea el inventario y los botones de información, verificar, cambiar página y cerrar juego
instance_create_depth(x,y,depth, obj_inventory);
instance_create_depth(x,y,depth, obj_selected_items);
instance_create_depth(global.dw*0.10, global.dh*0.60, depth, obj_info);
instance_create_depth(global.dw*0.30, global.dh*0.60, depth, obj_check);
instance_create_depth(global.dw*0.65, global.dh*0.20, depth, obj_inv_back);
instance_create_depth(global.dw*0.95, global.dh*0.20, depth, obj_inv_forward);
instance_create_depth(x,y,depth,obj_close);

agro_formula = scr_select_limon_items();

//crea un string con la lista de elementos del agroquímico seleccionado
formula_string = "";
for (i = 0; i < ds_list_size(agro_formula); i++) {
	formula_string += agro_formula[| i] + "\n";
}

obj_info.name_string = recipe_name;
obj_info.items_string = formula_string;
obj_check.name_string = recipe_name;
obj_check.items_string = formula_string;

scr_set_timer(global.dw*0.5,global.dh*0.1,90);

