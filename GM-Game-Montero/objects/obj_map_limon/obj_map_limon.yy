{
    "id": "1bafcb2e-8356-43e1-9ca5-5d3424d9e3cc",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_map_limon",
    "eventList": [
        {
            "id": "fef9bc90-3358-44eb-9ca6-44e96c753f42",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1bafcb2e-8356-43e1-9ca5-5d3424d9e3cc"
        },
        {
            "id": "32126584-4582-47f4-9c3d-371f36bb2187",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1bafcb2e-8356-43e1-9ca5-5d3424d9e3cc"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4264db56-59ae-4882-bbab-a26f38cc1bc1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4f8ccdc8-ac05-4bd1-8d56-71738a1857bd",
    "visible": true
}