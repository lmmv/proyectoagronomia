
event_inherited();

if instance_exists(obj_plane) {				//si el avión existe
	with (obj_plane) {
		if distance_to_point(other.x,y) < 200 and y >= max_flight_level {
			allow_spray = true;
		}
											//está sobre la planta a una altura adecuada			
		if distance_to_point(other.x,y) < 20 and y >= max_flight_level { 
			if spraying {					//y está rociando el pesticida
				other.image_index = 1;		//la planta es fumigada
			}
		}		
	}
}