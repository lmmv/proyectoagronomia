{
    "id": "b50739b8-6881-4ddf-9f99-815d29be7e29",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_plantation",
    "eventList": [
        {
            "id": "4048dfa4-5e8a-46d4-96d8-186523e7dd6e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b50739b8-6881-4ddf-9f99-815d29be7e29"
        },
        {
            "id": "45adf016-1ec7-47ad-b6c8-08e2ce7c65db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b50739b8-6881-4ddf-9f99-815d29be7e29"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3dd11904-8d3a-4609-b375-f9aafdee0dbb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dae0d1a8-9504-43e3-a359-05fb6e7ab270",
    "visible": true
}