{
    "id": "ca525855-03e0-4ab9-951c-c8849b740c66",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_fade_out",
    "eventList": [
        {
            "id": "b707aaf2-72ed-4c02-ab1d-50267fa858c9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ca525855-03e0-4ab9-951c-c8849b740c66"
        },
        {
            "id": "e591cdda-0fec-4630-8d64-102fb57e1594",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ca525855-03e0-4ab9-951c-c8849b740c66"
        },
        {
            "id": "3d451f01-837d-4685-b7bc-aa36e862ecf6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "ca525855-03e0-4ab9-951c-c8849b740c66"
        },
        {
            "id": "c061728a-d71c-4ec8-8c2c-0696cda8afcf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "ca525855-03e0-4ab9-951c-c8849b740c66"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "21e98c0f-bd3a-462a-b423-113294e29e35",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}