{
    "id": "7d528ae3-55af-4525-8392-9f3a3904d43e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_inv_10",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2a35dab2-5276-41e1-90c4-6b1db1e856aa",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "56be057e-b218-4900-b7e6-6cf636d61c34",
    "visible": true
}