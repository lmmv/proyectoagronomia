
if global.allow_input {
	//mantener arriba la avioneta 
	if (device_mouse_check_button_pressed(0,mb_left))
		vspeed = -0.7;

	if !place_meeting(x, y + vspeed, obj_ground)
	  gravity = 0.02;
	else {
	  gravity = 0;
	  vspeed = 0;
	  image_angle = 0;
	  scr_stop_background(true);
	}
	if bbox_top < 0
		vspeed = 0;

	if (vspeed > max_grav_speed)
		vspeed = max_grav_speed;
		
	if vspeed < 0 and image_angle < 10 {
		image_angle	+= 0.05;
	}
	else if vspeed > 0 and image_angle > -15 {
		image_angle -= 0.05;	
	}

	//fumigar
	if (device_mouse_check_button_pressed(0, mb_right)) {
		spraying = !spraying;
	}
	if spraying {
		part_particles_create(part_obj.spray_system,x,y,part_obj.spray_part,20);
		pesticide -= 0.08;
	}
}