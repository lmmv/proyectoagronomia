
pest_pc = (pesticide / max_pesticide) * 100; //porcentaje de pesticida
draw_set_color(c_black);
draw_set_alpha(0.05);
draw_rectangle(pb_x1,pb_y1,pb_x2,pb_y2,false);
draw_set_alpha(1);
draw_healthbar(pb_x1,pb_y1,pb_x2,pb_y2,pest_pc,c_black,c_fuchsia,c_fuchsia,3,false,false);

//muestra el rango en el que debe volar
if keyboard_check_pressed(ord("C")) c = !c;
if c {
	draw_set_alpha(1);
	draw_set_color(c_red);
	draw_rectangle(0,min_flight_level,global.dw,max_flight_level,true);
}

//muestra una advertencia si vuela muy alto o muy bajo
draw_set_color(c_black);
draw_set_halign(fa_center);
if y < max_flight_level {
	draw_sprite(spr_warning,0,global.dw*0.5,global.dh*0.2);	
	draw_text(global.dw*0.5,global.dh*0.3,fly_high);
}
else if y > min_flight_level {
	draw_sprite(spr_warning,0,global.dw*0.5,global.dh*0.2);	
	draw_text(global.dw*0.5,global.dh*0.3,fly_low);
}

//indicador si puede o no fumigar en esa zona
if allow_spray {
	draw_sprite(spr_ok_no,0,global.dw*0.7,global.dh*0.3);	
}
else {
	draw_sprite(spr_ok_no,1,global.dw*0.7,global.dh*0.3);
}