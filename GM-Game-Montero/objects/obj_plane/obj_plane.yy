{
    "id": "9d4d079f-ae2f-466c-ad67-af9764b0b05c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_plane",
    "eventList": [
        {
            "id": "7702e7d7-9204-49a7-863b-fa4829b96264",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9d4d079f-ae2f-466c-ad67-af9764b0b05c"
        },
        {
            "id": "12c122d9-89d7-4a48-bca3-b0291c5fa6b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9d4d079f-ae2f-466c-ad67-af9764b0b05c"
        },
        {
            "id": "968f774f-0b17-45b1-a6ff-976384319f7a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9d4d079f-ae2f-466c-ad67-af9764b0b05c"
        },
        {
            "id": "2026b270-f012-432f-9e78-a6bb203bab97",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "9d4d079f-ae2f-466c-ad67-af9764b0b05c"
        },
        {
            "id": "a268ff0b-60eb-4d0f-8b08-21b07b8335fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "9d4d079f-ae2f-466c-ad67-af9764b0b05c"
        },
        {
            "id": "58cf65ea-353b-494a-9133-ab2a6da49c13",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "9d4d079f-ae2f-466c-ad67-af9764b0b05c"
        },
        {
            "id": "6047bf29-7b77-4218-822b-d1206dc54e39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "9d4d079f-ae2f-466c-ad67-af9764b0b05c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "361d7622-b273-4d20-b2c4-fd435637e29f",
    "visible": true
}