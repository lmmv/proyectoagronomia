/// @description Gane: Todas las plantas fumigadas

if instance_exists(obj_plantation) and global.allow_input  {
	win = true;
	for (i = 0; i < instance_number(obj_plantation); i++) {
		inst = instance_find(obj_plantation,i);
		if inst.image_index == 0 {
			win = false;
			break;
		}
	}
}

if win {
	scr_ending(true,"¡Buen trabajo! Has fumigado correctamente la plantación.");
	vspeed = -1;
	gravity = 0;
}
else {
	alarm[3] = check_rate;	
}
