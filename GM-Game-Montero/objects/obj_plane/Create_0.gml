
//altura mínima y máxima de vuelo
max_flight_level = global.dh * 0.5;
min_flight_level = global.dh * 0.63;

fly_high = "Estás volando muy alto";
fly_low = "Estás volando muy bajo. ¡Cuidado!";

y = max_flight_level;

max_grav_speed = 2; //gravedad máxima

max_pesticide = 100;
pesticide = max_pesticide;
spraying = false;
allow_spray = false;
crash = false;
banned = false;
win = false;

//coordenadas de la barra de pesticida
pb_x1 = global.dw * 0.03;
pb_y1 = global.dh * 0.2;
pb_x2 = global.dw * 0.05;
pb_y2 = global.dh * 0.8;

//crea el objeto con las partículas de fumigación
part_obj = instance_create_depth(x,y,0,obj_spray);

//cada cuanto comprueba las condiciones de gane o pérdida
check_rate = 50; 

alarm[0] = check_rate;
alarm[1] = check_rate;
alarm[2] = check_rate;
alarm[3] = check_rate;



//
c = false;