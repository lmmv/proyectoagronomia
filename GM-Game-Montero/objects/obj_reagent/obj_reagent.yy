{
    "id": "c1f80888-56ac-4081-a3d6-94609265ddac",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_reagent",
    "eventList": [
        {
            "id": "9ca9e75d-50fb-4365-b02b-2b65a28ddc82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c1f80888-56ac-4081-a3d6-94609265ddac"
        },
        {
            "id": "cd538716-b258-4966-8d9c-b06969503afc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c1f80888-56ac-4081-a3d6-94609265ddac"
        },
        {
            "id": "c4569869-6f54-47c2-a6ac-f75bcacfd54c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c1f80888-56ac-4081-a3d6-94609265ddac"
        },
        {
            "id": "b41b42bb-a563-4563-a5d0-a300e5080bc8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "c1f80888-56ac-4081-a3d6-94609265ddac"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "eb87b8e7-eae1-4429-a1f0-97123bcbc3c4",
    "visible": true
}