
event_inherited();

var mentor_x = global.dw * 0.25;
var mentor_y = global.dh * 0.5;
draw_sprite(spr_mentor,0,mentor_x,mentor_y);


draw_set_halign(fa_left);
draw_set_color(c_black);
draw_set_font(fnt_text);
var dialog_x = global.dw * 0.55;
var dialog_y = global.dh * 0.5;
var sep = string_height(dialog);
var w = global.dw * 0.9 - dialog_x;
draw_text_ext(dialog_x, dialog_y, dialog, sep, w);