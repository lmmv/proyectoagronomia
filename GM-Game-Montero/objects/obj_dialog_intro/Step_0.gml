
event_inherited();

if hit and global.allow_input {
	switch (option) {
		case 0:
			if room == rm_dialog_intro_map
				scr_change_room(rm_menu);
			else
				scr_change_room(rm_map);
			break;
		case 1:
			scr_change_room(next_room);
			break;
	}
} 
