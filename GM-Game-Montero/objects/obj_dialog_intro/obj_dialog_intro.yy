{
    "id": "a7618a58-acac-4989-b9d6-327ee489fbe8",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_dialog_intro",
    "eventList": [
        {
            "id": "c8a5444b-bad7-4645-990b-175527ba1fa4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a7618a58-acac-4989-b9d6-327ee489fbe8"
        },
        {
            "id": "0cd1c742-8b8c-48c4-9725-2bbab9105d54",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "a7618a58-acac-4989-b9d6-327ee489fbe8"
        },
        {
            "id": "10745a7a-2f4a-42b3-b2e6-91c95e8f43ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a7618a58-acac-4989-b9d6-327ee489fbe8"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c37253b0-7015-4bc9-837d-68bed537a635",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}