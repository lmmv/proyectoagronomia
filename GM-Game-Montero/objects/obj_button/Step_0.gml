
hit = scr_mouse_events();

if mouse_over {
	image_xscale = 0.7;
	image_yscale = image_xscale;
}
else {
	image_xscale = 0.6;
	image_yscale = image_xscale;	
}

if hit {
	switch (sprite_index) {
		case spr_play_button:
			scr_change_room(rm_dialog_intro_map)
			break;
		case spr_credits_button:
			scr_change_room(rm_gamecredits);
			break;
		case spr_exit_button:
			room_goto(rm_exit);
			break;
		case spr_back_button:
			room_goto(rm_menu);
			break;
		case spr_yes_button:
			game_end();
			break;
		case spr_no_button:
			room_goto(rm_menu);
			break;
	}
}
