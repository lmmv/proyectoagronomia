{
    "id": "d595172d-cbae-45d7-bc77-0eaa0506abee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_map_guanacaste",
    "eventList": [
        {
            "id": "ee2a7247-bd55-43f9-9c4d-47d0151bd7db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d595172d-cbae-45d7-bc77-0eaa0506abee"
        },
        {
            "id": "488b429b-f4fb-4609-b642-dede29f00452",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d595172d-cbae-45d7-bc77-0eaa0506abee"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4264db56-59ae-4882-bbab-a26f38cc1bc1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0c353fc8-8d85-4040-99aa-1c97046bf94d",
    "visible": true
}