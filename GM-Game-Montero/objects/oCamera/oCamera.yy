{
    "id": "9a187d30-b366-4b86-9ba7-7765e5d40e49",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCamera",
    "eventList": [
        {
            "id": "f130828d-8159-4b89-a78c-bf8d1cce7d08",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9a187d30-b366-4b86-9ba7-7765e5d40e49"
        },
        {
            "id": "77a78ed9-254a-4257-a98f-f411916c4eb6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 66,
            "eventtype": 13,
            "m_owner": "9a187d30-b366-4b86-9ba7-7765e5d40e49"
        },
        {
            "id": "2e29b8ca-42f5-4d9f-a5eb-0c409365b1d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 67,
            "eventtype": 13,
            "m_owner": "9a187d30-b366-4b86-9ba7-7765e5d40e49"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}