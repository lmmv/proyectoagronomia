{
    "id": "5b737d91-3df5-4ad5-904e-1afea97dfe0f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_house",
    "eventList": [
        {
            "id": "47ad8d83-9565-4e3c-afed-a54b60611a4f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5b737d91-3df5-4ad5-904e-1afea97dfe0f"
        },
        {
            "id": "f160c90a-42b7-4e94-82d1-83cf6c47e541",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5b737d91-3df5-4ad5-904e-1afea97dfe0f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "3dd11904-8d3a-4609-b375-f9aafdee0dbb",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2d8c1d69-c044-4c21-8585-54aafd63baaf",
    "visible": true
}