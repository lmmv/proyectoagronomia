{
    "id": "015c3d82-c445-4c30-8934-4c51386601ee",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_inv_11",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2a35dab2-5276-41e1-90c4-6b1db1e856aa",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "23e46ef2-fd2d-4fec-a950-3f0628548415",
    "visible": true
}