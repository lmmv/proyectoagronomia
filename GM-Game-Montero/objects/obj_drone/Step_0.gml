//mueve el dron a la posición donde se hace clic
if mouse_check_button_released(mb_left) {
	if !collision_point(device_mouse_x_to_gui(0),device_mouse_y_to_gui(0),obj_close,0,true) 
	and !collision_point(device_mouse_x_to_gui(0),device_mouse_y_to_gui(0),obj_scan,0,true) {
		target_x = mouse_x;
		target_y = mouse_y;
	}
}
mp_linear_step(target_x,target_y,spd,0);

//escanea el área en que se encuentra el dron en busca de plagas
if mouse_check_button_released(mb_right) {
	scan = true;
}
if scan {
	if scan_wave < collision_range {
		scan_wave++;		
	}
	else {
		scan_wave = 0;
		scan = false;	
		var inst_list = ds_list_create();
		var num = collision_circle_list(x, y, collision_range, obj_coffee, false, true, inst_list, false);
		if num > 0 {
			for (var i = 0; i < num; ++i;) {
				var inst = inst_list[| i];
				if inst.plague and !inst.scanned {
					obj_alajuela.plagues_found += 1;
					inst.scanned = true;
				}
			}
		}
	}
}