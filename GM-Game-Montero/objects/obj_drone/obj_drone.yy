{
    "id": "b16915d1-07a0-41cf-8a96-ab69b03ad487",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_drone",
    "eventList": [
        {
            "id": "744ac9ca-8237-4d50-970c-f21ec7639ab4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b16915d1-07a0-41cf-8a96-ab69b03ad487"
        },
        {
            "id": "54a66bd6-81f1-476f-8770-15ec65a40a93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b16915d1-07a0-41cf-8a96-ab69b03ad487"
        },
        {
            "id": "11f5e289-4e94-43a2-a1bf-f94980dd7e37",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b16915d1-07a0-41cf-8a96-ab69b03ad487"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9a2844ab-0d36-4394-8031-e575a73663e6",
    "visible": true
}