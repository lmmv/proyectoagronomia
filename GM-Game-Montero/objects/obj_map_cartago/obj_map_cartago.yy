{
    "id": "ab4a2903-f3b9-4e8d-98cf-084ba78e06ed",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_map_cartago",
    "eventList": [
        {
            "id": "2ef9daa6-75bf-4a45-b78a-bf6d056e6e28",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ab4a2903-f3b9-4e8d-98cf-084ba78e06ed"
        },
        {
            "id": "90d0aff2-eea8-4c94-a3fd-78c4a650675b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ab4a2903-f3b9-4e8d-98cf-084ba78e06ed"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4264db56-59ae-4882-bbab-a26f38cc1bc1",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b5227862-55d9-4955-988b-853dd7e670b7",
    "visible": true
}