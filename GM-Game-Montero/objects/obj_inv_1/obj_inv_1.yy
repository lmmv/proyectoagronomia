{
    "id": "1ac1df37-39a8-4abb-8af9-02c2e628816e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_inv_1",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2a35dab2-5276-41e1-90c4-6b1db1e856aa",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a9cb674d-cce7-4457-ac67-4f7dc3c33a53",
    "visible": true
}