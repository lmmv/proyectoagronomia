
//se crea el inventario y los botones de información, verificar, cambiar página y cerrar juego
instance_create_depth(x,y,depth, obj_inventory);
sel_inst = instance_create_depth(x,y,depth, obj_selected_items);
with (sel_inst) {
	total_items = 6;
	columns = 2;
	xx = global.dw * 0.23;
	yy = global.dh * 0.3;
	x_spacing = global.dw * 0.15;
	y_spacing = global.dh * 0.2;
}

instance_create_depth(global.dw*0.60, global.dh*0.50, depth, obj_info);
instance_create_depth(global.dw*0.60, global.dh*0.70, depth, obj_check);
instance_create_depth(global.dw*0.65, global.dh*0.20, depth, obj_inv_back);
instance_create_depth(global.dw*0.95, global.dh*0.20, depth, obj_inv_forward);
instance_create_depth(x,y,depth,obj_close);

greenhouse = scr_select_cartago_items();

//crea un string con la lista de elementos del invernadero seleccionado
greenhouse_string = "";
for (i = 0; i < ds_list_size(greenhouse); i++) {
	greenhouse_string += greenhouse[| i] + "\n";
}

obj_info.name_string = greenhouse_name;
obj_info.items_string = greenhouse_string;
obj_check.name_string = greenhouse_name;
obj_check.items_string = greenhouse_string;

scr_set_timer(global.dw*0.5,global.dh*0.1,40);

