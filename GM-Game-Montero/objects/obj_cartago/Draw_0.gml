
//muestra el nombre del tipo de invernadero
draw_set_halign(fa_center);
draw_text(global.dw*0.3,global.dh*0.9, greenhouse_name);

//fondo del espacio para elementos seleccionados
draw_sprite_ext(spr_bg,0,global.dw*0.3,global.dh*0.5,0.8,1.4,0,c_white,1);
draw_sprite(spr_greenhouse,0,global.dw*0.3,global.dh*0.5);

//fondo del inventario
draw_sprite_ext(spr_bg,0,global.dw*0.8,global.dh*0.3,0.5,0.8,0,c_white,1);

//*******************************************************************
//ig = obj_inventory.inventory_grid;
//if ds_exists(ig,ds_type_grid) {
//	for (i=0; i<ds_grid_height(ig); i++) {
//		item = ig[# 0, i];
//		obj = ig[# 1, i];
//		draw_text(1200,800+(i*20),"Inventario " + string(i) + ":  " + string(item) + " " + string(object_get_name(obj)));
//	}
//}
//sg = obj_selected_items.selected_items_grid;
//if ds_exists(sg,ds_type_grid) {
//	for (i=0; i<ds_grid_height(sg); i++) {
//		item = sg[# 0, i];
//		obj = sg[# 1, i];
//		draw_text(1600,800+(i*20),"Seleccionados " + string(i) + ":  " + string(item) + " " + string(object_get_name(obj)));
//	}
//}
//********************************************************************