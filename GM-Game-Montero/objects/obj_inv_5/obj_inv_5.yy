{
    "id": "a574e9f8-381c-4c86-80fa-b537d582ad6f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_inv_5",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2a35dab2-5276-41e1-90c4-6b1db1e856aa",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "64272702-afd2-4b43-8761-e442a51a1d10",
    "visible": true
}