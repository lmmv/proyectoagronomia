{
    "id": "2b5f9ef0-7a3f-4d54-a02e-c4021d9c1f55",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cmt_parent",
    "eventList": [
        {
            "id": "b330de40-3f93-4243-9f3a-3840e1b32606",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2b5f9ef0-7a3f-4d54-a02e-c4021d9c1f55"
        },
        {
            "id": "923fa298-5e55-482e-91a6-d1689ecfffad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2b5f9ef0-7a3f-4d54-a02e-c4021d9c1f55"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}