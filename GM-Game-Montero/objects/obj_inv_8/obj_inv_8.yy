{
    "id": "6f22fb96-ed1a-49c2-8e94-1c04e505c69a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_inv_8",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2a35dab2-5276-41e1-90c4-6b1db1e856aa",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "680c75be-80fe-4056-bb64-b4009e2dc727",
    "visible": true
}