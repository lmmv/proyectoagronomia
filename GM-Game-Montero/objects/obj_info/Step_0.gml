
if !show_info
	hit = scr_mouse_events();

if hit {
	//muestra la ventana de información de la receta y crea el botón para cerrarla
	show_info = true;
	instance_deactivate_all(true);
	close = instance_create_depth(global.dw*0.8,global.dh*0.1,depth, obj_close_info);
	close.owner = id;
	hit = false;
}
