{
    "id": "88ac9d60-c552-462a-809e-6d6dc570106b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_info",
    "eventList": [
        {
            "id": "571f8ce5-b8dd-4b21-b513-6361defb5f8e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "88ac9d60-c552-462a-809e-6d6dc570106b"
        },
        {
            "id": "dce0e725-5fac-4e6f-ac57-3d0a5be1899c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "88ac9d60-c552-462a-809e-6d6dc570106b"
        },
        {
            "id": "e32e8eaf-9b92-40ec-8c9b-42005643cd48",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "88ac9d60-c552-462a-809e-6d6dc570106b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a7867c3b-ebe7-4d0e-8a22-c4a0fd6fde5d",
    "visible": true
}