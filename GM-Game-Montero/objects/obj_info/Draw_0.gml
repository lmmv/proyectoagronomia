
if show_info {
	draw_sprite_ext(spr_bg,0,global.dw/2,global.dh/2,1.5,1.5,0,c_white,1);
	draw_set_color(c_black);
	draw_set_halign(fa_left);
	draw_set_valign(fa_top);
	draw_set_font(fnt_text);
	draw_text(global.dw*0.3,global.dh*0.3,name_string);
	draw_text(global.dw*0.3,global.dh*0.5,items_string);
}
else
	draw_self();