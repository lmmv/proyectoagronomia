{
    "id": "fcaa21a2-cc52-4a1d-8379-724250ab410a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_selected_slots",
    "eventList": [
        {
            "id": "94f33c18-1b34-4ff0-95f7-88ed42eaa49c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "fcaa21a2-cc52-4a1d-8379-724250ab410a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a89c9c5b-9859-406a-a7bc-8cde3591155a",
    "visible": true
}