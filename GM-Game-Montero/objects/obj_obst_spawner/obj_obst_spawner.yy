{
    "id": "d7e33687-12bf-45b9-80cd-b9bf0609073f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_obst_spawner",
    "eventList": [
        {
            "id": "f955bc33-f67e-4359-965a-26aa242451ba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d7e33687-12bf-45b9-80cd-b9bf0609073f"
        },
        {
            "id": "ffdf7774-fb15-4e55-8731-648c19a45922",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d7e33687-12bf-45b9-80cd-b9bf0609073f"
        },
        {
            "id": "7889381f-f320-4b5b-ab96-2bc6f1cfb998",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "d7e33687-12bf-45b9-80cd-b9bf0609073f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}