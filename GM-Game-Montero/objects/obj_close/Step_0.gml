
if global.allow_input {
	close = scr_mouse_events();

	if close {
		//crea la ventana de confirmación para salir del juego	
		if room == rm_map
			scr_change_room(rm_menu);		
		else
			instance_create_depth(x,y,depth,obj_close_popup);
	}	
}
