{
    "id": "49f1f6e6-90f8-4786-a477-6bce5b4a0684",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_close",
    "eventList": [
        {
            "id": "eb85780d-62d6-44ff-8185-a7b36ba14278",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "49f1f6e6-90f8-4786-a477-6bce5b4a0684"
        },
        {
            "id": "506a8634-e49d-43ae-beff-243aaa67f512",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "49f1f6e6-90f8-4786-a477-6bce5b4a0684"
        },
        {
            "id": "8c9a1377-392c-40a3-a696-f3a9548ad541",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "49f1f6e6-90f8-4786-a477-6bce5b4a0684"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}