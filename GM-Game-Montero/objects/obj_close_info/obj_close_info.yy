{
    "id": "5716c1f3-ac6f-4f81-a87d-73447038895e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_close_info",
    "eventList": [
        {
            "id": "9f4c9e5b-e56b-45e1-9e58-69e06006f3ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5716c1f3-ac6f-4f81-a87d-73447038895e"
        },
        {
            "id": "d597d519-f4b9-4861-bcd9-f13f28b2b0bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5716c1f3-ac6f-4f81-a87d-73447038895e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "661bec79-7c26-46a3-a1c2-ac7508d8458a",
    "visible": true
}