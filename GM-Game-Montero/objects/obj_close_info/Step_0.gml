
close = scr_mouse_events();

if close {
	owner.show_info = false;
	instance_activate_all();
	instance_destroy(obj_close_info);	
}
