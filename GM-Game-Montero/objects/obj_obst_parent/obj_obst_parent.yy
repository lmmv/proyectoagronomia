{
    "id": "3dd11904-8d3a-4609-b375-f9aafdee0dbb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_obst_parent",
    "eventList": [
        {
            "id": "a1e4ef6d-fda5-4db8-9519-80b420635dc3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3dd11904-8d3a-4609-b375-f9aafdee0dbb"
        },
        {
            "id": "ba30674d-e27f-4b48-9a18-01c9a980b5d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "9d4d079f-ae2f-466c-ad67-af9764b0b05c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "3dd11904-8d3a-4609-b375-f9aafdee0dbb"
        },
        {
            "id": "4399f483-34d0-4251-a2d5-4e91a6fe1d73",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3dd11904-8d3a-4609-b375-f9aafdee0dbb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}