{
    "id": "264aa7f7-730a-4181-9365-c3b55e08989e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_win",
    "eventList": [
        {
            "id": "cf26d33b-4c6d-4871-b3eb-8c65ac2c7af3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "264aa7f7-730a-4181-9365-c3b55e08989e"
        },
        {
            "id": "9cb31725-87b8-4339-a159-02ee3bf5eb68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "264aa7f7-730a-4181-9365-c3b55e08989e"
        },
        {
            "id": "f71bddeb-6d94-43f0-b10d-3d4137d08c42",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "264aa7f7-730a-4181-9365-c3b55e08989e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c37253b0-7015-4bc9-837d-68bed537a635",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}