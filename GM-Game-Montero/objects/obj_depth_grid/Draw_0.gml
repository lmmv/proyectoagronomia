
if (ds_exists(ds_depth_grid,ds_type_grid)) {
	var depth_grid = ds_depth_grid;
	var inst_numb = instance_number(obj_depth_parent);
	
	ds_grid_resize(depth_grid,2,inst_numb);
	
	var yy = 0;
	//llena el ds_grid con el "id" y el valor en "y" de todas las instancias que heredan del obj_depth_parent
	with (obj_depth_parent) {
		depth_grid[# 0, yy] = id;	
		depth_grid[# 1, yy] = y;
		yy++;
	}
	
	ds_grid_sort(depth_grid,1,true);
	yy = 0;
	repeat (inst_numb) {
		var inst_id = depth_grid[# 0, yy];
		with inst_id {
			 event_perform(ev_draw, 0);
		}
		yy++;
	}	
	ds_grid_clear(depth_grid,0);
}
