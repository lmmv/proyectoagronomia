{
    "id": "80025924-2a81-4b72-89c5-e801c8a1fae0",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_text",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Candara",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "40bb5635-106e-4a03-aa8d-6fef0078bf5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 33,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e2a77965-03b4-4964-a2f3-ca711384e0ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 33,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 204,
                "y": 107
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "20709937-34a8-424d-b785-070712593fe7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 211,
                "y": 107
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "ba011a05-f6aa-4e19-ab85-83b2252138e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 33,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 223,
                "y": 107
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "46a5dc8b-e655-4055-846c-39220d5fe0e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 239,
                "y": 107
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "aa8beec3-244e-4eae-a22a-552a7bb5bb9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 33,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 251,
                "y": 107
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "bc9d6566-af7d-46c9-bc37-c183b63bbbb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 33,
                "offset": 1,
                "shift": 18,
                "w": 18,
                "x": 266,
                "y": 107
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "2ec90c74-c1a9-450e-b34a-14811bdabea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 33,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 286,
                "y": 107
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "d00dd082-105b-4425-a447-7ecfd35db4fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 292,
                "y": 107
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "f2cb81e6-2e05-483e-997c-c9e2bbc867fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 302,
                "y": 107
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5a71d92d-0b92-4d6f-a5b4-17daa1a4005d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 327,
                "y": 107
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "168d561a-d215-476b-a905-784a3ff82d7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 442,
                "y": 107
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8c281347-ad49-4d69-92a2-a67bcfdef0fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 341,
                "y": 107
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4f186e6c-8ba2-4250-8698-402017c6fa92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 349,
                "y": 107
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "1daac349-7e8a-404b-9ef0-00b0d00ef799",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 33,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 358,
                "y": 107
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "7713135c-c05c-4b06-ac73-6346e0268700",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 365,
                "y": 107
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ef82c76e-81f9-4483-9c77-fdfe64fda1dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 375,
                "y": 107
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e5af4e65-28d6-45f2-85b5-66cde15c4a25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 33,
                "offset": 0,
                "shift": 9,
                "w": 7,
                "x": 390,
                "y": 107
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "3bd17128-a13c-4de2-aac3-46abc04ba154",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 399,
                "y": 107
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "2d5d6d4f-5be4-4dcb-9bec-13395d333e1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 412,
                "y": 107
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "58f97f3d-5b86-4aca-b7c5-871dd63846b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 33,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 426,
                "y": 107
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "ff2eacaa-664c-4f61-a49b-93e5400eb0be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 191,
                "y": 107
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "cff28e92-9084-4428-836f-cb864b0c69a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 312,
                "y": 107
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "4b0ce538-572c-42da-a4f3-35fbed6d2905",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 177,
                "y": 107
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6d962c93-d57c-46fc-9a31-69524dc14149",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 492,
                "y": 72
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "960c15d6-e5fa-49a6-8bad-59d9a101bdf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 363,
                "y": 72
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b92daa64-8412-45f9-8d9a-5fb995cd76ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 33,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 378,
                "y": 72
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "518a4022-075b-4b0c-81d8-34b62a83b1fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 385,
                "y": 72
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3770049d-2e5c-4412-abbd-af9bbe06ad16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 393,
                "y": 72
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "23f53874-bb87-4f8f-bcb8-4930f27ddb5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 407,
                "y": 72
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "03702647-31ce-4111-859b-9116b7f11a56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 421,
                "y": 72
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "564f743b-9016-48b5-8357-193e18ac6663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 33,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 435,
                "y": 72
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "2dc82ba5-74fb-46db-8531-25002c496943",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 33,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 446,
                "y": 72
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c7a12896-06e1-4aaf-80da-c5892aeb30f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 33,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 473,
                "y": 72
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8709cffe-48b2-4a9b-8a50-d54b7b5756ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 2,
                "y": 107
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "3d1e7f70-631d-4f6c-ae89-d78a268c4369",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 139,
                "y": 107
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a28705de-ee9a-4b08-8d0b-bfc9313180d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 33,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 17,
                "y": 107
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "1a141e9d-03f6-420a-99f1-7b71b44a7fd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 34,
                "y": 107
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "bbbf2c71-67b8-4ad9-8b41-80dcb36c897b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 48,
                "y": 107
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "cbf9ec07-cb56-4c67-8a82-10c7c1591924",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 33,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 61,
                "y": 107
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "61a30620-843b-4e6d-9ca7-bfb09a55ee21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 33,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 77,
                "y": 107
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "bcb98394-302c-4a06-8ae2-8c0a5c4952f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 33,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 93,
                "y": 107
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "5c55865e-1310-47a0-967f-b5c52dff201e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 33,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 98,
                "y": 107
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "8afc8365-df79-413d-80b6-e7cf8b478884",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 110,
                "y": 107
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a8eb3e23-836f-4e32-a6e7-43161c2bb665",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 33,
                "offset": 2,
                "shift": 13,
                "w": 11,
                "x": 126,
                "y": 107
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d505457a-ce60-46a3-bef1-4db1182df73f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 33,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 155,
                "y": 107
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "9c1edbd6-86fc-464b-8561-8bdf79fe7319",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 33,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 456,
                "y": 107
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "05186910-a536-4dda-b429-26abb55a0786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 33,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 472,
                "y": 107
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b5bfd513-1ddc-4bec-8fd0-3d29dee6efda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 33,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 491,
                "y": 107
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "adca4cef-d16d-4cfa-aa1e-7a4a53a630f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 33,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 316,
                "y": 142
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "d124892a-0766-403f-9f09-80f077580552",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 33,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 335,
                "y": 142
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "6bfc005f-023e-4e6a-8bd9-fa691e3ee792",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 351,
                "y": 142
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "c6ff7c7a-847e-4876-afd0-5ad51a1742b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 33,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 365,
                "y": 142
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b9723391-71aa-4e6f-8d9a-ee37e64ee4f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 33,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 381,
                "y": 142
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0c500d2f-513c-4396-90f7-396034c73d79",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 33,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 398,
                "y": 142
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "82a0dbd5-03d1-49a7-9007-4263a921d6cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 33,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 415,
                "y": 142
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "d756eba0-4290-434b-8aba-ebb13a41292a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 33,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 441,
                "y": 142
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "242349f0-d742-4005-a487-484d596e597c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 33,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 458,
                "y": 142
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "0745dcc3-fe38-45e3-81cf-fb506783bc56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 475,
                "y": 142
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ce474b9c-c69e-46c6-8f29-029c1b82ead0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 489,
                "y": 142
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "35ceb82f-db86-4fe2-9496-b1a9d55c6a0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 498,
                "y": 142
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "068466f3-942b-4f6b-b39f-c1d7d1f33cb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 7,
                "x": 2,
                "y": 177
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a77d5152-e330-4808-b64a-d5fc48dd7452",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 33,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 11,
                "y": 177
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "d33dd486-04eb-4e2c-b078-7026a5136a14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 33,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 27,
                "y": 177
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "02a28ae8-15d0-44b5-b221-3d1c04545ec7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 33,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 43,
                "y": 177
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "b0d3674e-decf-4457-9bb4-57456517824d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 51,
                "y": 177
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "0639d077-bd80-4c4e-bf13-ab413355f8e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 64,
                "y": 177
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "0f69d563-6c3a-4db0-8287-9d40a9ebe428",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 79,
                "y": 177
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "7128b1fc-423f-4930-a94d-be36b6f27487",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 92,
                "y": 177
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f58848bc-f412-46c7-b588-65a78fe8da87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 107,
                "y": 177
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c6322370-8ba1-4664-b546-de6253595a30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 33,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 304,
                "y": 142
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "9d5366a3-80ef-4dee-9c47-c19e9f4b1724",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 33,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 287,
                "y": 142
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c3bd9d82-a292-48b1-87fd-366b54076b91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 273,
                "y": 142
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "707db086-e6c4-4c97-ba03-90c06a06764e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 33,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 122,
                "y": 142
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "dd3f3ff4-698e-4085-9f62-6713ba630525",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 33,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 2,
                "y": 142
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ea431a2e-a296-45f7-a42e-ed3e3a636395",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 13,
                "x": 9,
                "y": 142
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "d5ff17c5-4d23-4ccd-ac0b-e567ccb2524d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 33,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 24,
                "y": 142
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "2da4012f-8201-4b03-a5cf-e9c70b53beb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 33,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 30,
                "y": 142
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "8c844375-5a71-49e0-bbea-731964cf901d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 52,
                "y": 142
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "55f8329e-b581-4c49-a424-fc1f2eade8d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 66,
                "y": 142
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "f9f14a8c-8817-42f4-b537-2744e671895d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 81,
                "y": 142
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "3977ec2f-501e-43d6-a2de-19b35435aae0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 96,
                "y": 142
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "96c50b07-458b-4bd1-be9b-dc5d6328cd0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 33,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 111,
                "y": 142
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "25f61f20-40d9-489a-a2ec-174167fafff6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 128,
                "y": 142
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f0c65804-2f74-4916-8942-ed19e94a12e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 33,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 262,
                "y": 142
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "55d29a3c-2c31-4d1b-85e0-7a03a427692e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 140,
                "y": 142
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "77373dfd-fd0e-49ec-978d-450d61335c9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 154,
                "y": 142
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "8d26f98f-a0e0-4bc8-926d-5ab2bae0673d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 33,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 169,
                "y": 142
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "84625392-96cb-459a-aca6-299dc9fd9e29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 33,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 192,
                "y": 142
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "86a18f9f-968c-47bc-842e-2376c3629942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 207,
                "y": 142
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "cecbe2b1-07f5-44d3-aec9-2b6880d23707",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 221,
                "y": 142
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d959c4a7-d9af-40ab-8617-b742b66fef35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 33,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 233,
                "y": 142
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "395e99bb-b9b5-4bf8-8381-9047c122899f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 33,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 245,
                "y": 142
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5709607e-1abd-4556-a0e4-f80abfdd3bb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 33,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 250,
                "y": 142
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "880b6507-c313-4398-a88c-7977482252e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 33,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 347,
                "y": 72
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "b18c11cc-5eb4-4d1c-bae3-9a1c2c7573c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 33,
                "offset": 0,
                "shift": 6,
                "w": 0,
                "x": 345,
                "y": 72
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "09e6c8d4-bac2-4c8d-9007-82e39a17ca13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 33,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 338,
                "y": 72
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "819ed5ed-fe57-4b61-a215-ec45fc28c4c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 119,
                "y": 37
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "06b6e5d9-9d6c-40a3-a0f1-17d464f0c539",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 33,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 368,
                "y": 2
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "56c34201-44ca-428c-a6b7-ecdd379319f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 382,
                "y": 2
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "61a6e3a7-4656-47f6-ae09-3d1bb7b36370",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 33,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 395,
                "y": 2
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "7863adf8-203a-409c-8a10-27b68805c0cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 33,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 409,
                "y": 2
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "e8a4cb78-f2c7-466a-8225-db0d72b1a5eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 414,
                "y": 2
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "126ba50d-9dff-48c0-9926-f04848b46e8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 33,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 426,
                "y": 2
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "4dfff048-cffd-4324-ae8a-fe69e1d557e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 33,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 437,
                "y": 2
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "b2b035ad-779c-41c3-b388-f5d92ebc2eab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 33,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 456,
                "y": 2
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "6276e602-f81b-4ebb-9921-dec227001e09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 33,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 465,
                "y": 2
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "a1fee008-942e-466f-bb42-bfc756a03ee4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 486,
                "y": 2
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "e984a072-e800-41c1-93ce-5665678f2e69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 110,
                "y": 37
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "b642c76a-26ca-4eea-a7c5-6a9e86d86871",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 33,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 2,
                "y": 37
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "7baa4f3b-a95a-4f81-9509-4879549f9b92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 21,
                "y": 37
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "58b5c129-bd98-4d5b-9f65-7ae499ad9b90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 30,
                "y": 37
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "b83dc8da-9ef8-4295-a27d-7c50f6ee1fe3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 40,
                "y": 37
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "8d4d6fde-65ba-43b3-8df9-9ec7e38279d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 54,
                "y": 37
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "a823be90-5aa4-41cc-ae80-075918b352e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 63,
                "y": 37
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "19f71896-f4bb-41b7-ab7f-fb55e8e8461e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 33,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 72,
                "y": 37
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "eca51696-e711-4410-8b58-4370e88d102d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 80,
                "y": 37
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "f7240c52-4b5b-4bba-8196-379f97f8d05b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 33,
                "offset": 2,
                "shift": 18,
                "w": 13,
                "x": 95,
                "y": 37
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "7769d878-7188-4289-9fb7-37a1d4761075",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 33,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 361,
                "y": 2
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "3b3fa4b6-5985-4898-8f7f-6db132f4c6f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 33,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 479,
                "y": 2
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "4e85f784-9fc4-4da0-ae2a-e21be9b5e5e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 33,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 353,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "760012e1-569e-49eb-8637-01517727eec8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 33,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "94cf8d3a-550f-409c-bdd1-5c06787a0253",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 33,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "b8758c24-9ffd-4ea5-8a70-38484ba0bdcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 33,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 24,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "18ec0c2e-cff8-418a-b954-a790705b2c29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 33,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 44,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "ccce1791-80b7-4db2-a5a7-4370e9704d92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 33,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 64,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "a331e6ca-5754-4835-a974-64cc5184dcf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 33,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "97d8ffdb-4198-4fca-8b85-4fbdf3bf2c90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 33,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "59f415ca-9f9e-4e33-8f91-741e1bd9a6fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 33,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "8f18ce2a-2c7f-44f0-8783-e7dca92be9ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 33,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "e237f3c4-d706-4e08-9a52-3e760c9aadf0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 33,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 152,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "cf478f6f-2d44-4ea0-b259-4ca318bccb3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 33,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "cc4bbecf-90ff-488c-a7ac-d4a5932d74ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 33,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 323,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "502aa05e-04e4-4c28-aeb6-fc7042f22e41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 33,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "26e238a8-e610-4a57-8c17-5a19cf0b1753",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 224,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "6a6dac10-fde0-4f78-86a5-0ac60465ac25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 240,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "1e1adc94-ced9-4338-8a13-93140d424dd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 254,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "09dfcaae-bfcf-4559-8a8f-fec81bddb23f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 268,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "ce51c065-3a88-44fa-9481-eada5e19277d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 282,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "909b8907-8c4e-43bd-8388-5ce8a07b8266",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 33,
                "offset": -1,
                "shift": 7,
                "w": 6,
                "x": 296,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "d0dc1942-eb38-470f-bd23-fbbde68d0371",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 33,
                "offset": 2,
                "shift": 7,
                "w": 6,
                "x": 304,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "a937b134-c667-4e95-91c8-f0d619557776",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 33,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 312,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "7b9ad802-4884-43fd-ab30-770314f44208",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 33,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 342,
                "y": 2
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "ec5dbb57-504e-40c2-bb56-fff43408e157",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 33,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 132,
                "y": 37
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "ff5c1a1f-0837-404e-85dd-394f01f70b00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 33,
                "offset": 2,
                "shift": 18,
                "w": 14,
                "x": 455,
                "y": 37
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "1a9b196f-a0dd-4251-a790-ad255f50fa1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 33,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 151,
                "y": 37
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "10ee8d9d-2f42-499c-b416-0b7e5e71363d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 33,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 2,
                "y": 72
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "66ecd2d1-9534-4c43-b354-c352401aee50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 33,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 21,
                "y": 72
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "c6165165-ac6d-4741-95e7-a20fe084b634",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 33,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 40,
                "y": 72
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "fea3f462-626d-4d5e-927a-97dbe16f2c71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 33,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 59,
                "y": 72
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "f833281d-4837-4960-8f24-a271f2395541",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 33,
                "offset": 2,
                "shift": 14,
                "w": 10,
                "x": 78,
                "y": 72
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "90eebadd-cf62-410a-980c-764121c9a9c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 33,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 90,
                "y": 72
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "58efc2b2-dad8-4482-9930-0e5a955d9994",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 33,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 110,
                "y": 72
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "cef09ec0-48a6-4643-92ad-eb7783980900",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 33,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 127,
                "y": 72
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "e374ed11-f9b5-4e6c-8511-331b55d7252e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 33,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 144,
                "y": 72
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "85f5e7ac-cea8-478f-b04e-fead71073359",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 33,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 175,
                "y": 72
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "3806d815-02e5-4ed0-80ba-39a8fb243e3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 33,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 321,
                "y": 72
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "d9955118-a72f-4ef2-8e3f-ea5b6df0acc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 33,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 192,
                "y": 72
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "374c8608-97f4-4252-a186-defbc95ece03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 207,
                "y": 72
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "0cde29e3-2c2c-4a0a-98de-2f9d468439c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 222,
                "y": 72
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "be951488-8ba0-4945-a1e4-09b3efc3eb3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 235,
                "y": 72
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "bc888573-7309-4f68-a9e4-5b3c4a7be7b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 248,
                "y": 72
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "6ec337a1-a57b-4ce8-9258-7696efb15a70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 261,
                "y": 72
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "e6cb825a-d3d0-486b-85d8-b2dc80266730",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 274,
                "y": 72
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "da9b59c3-0e31-4a02-bc7a-2d309a8d94a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 33,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 287,
                "y": 72
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "4425f31e-ba28-4aaf-a017-21acd177a338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 33,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 300,
                "y": 72
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "45e880f6-db9c-4459-a222-295b3fe45f52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 33,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 485,
                "y": 37
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "b7378da1-a067-40b8-9744-d308dd035f3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 161,
                "y": 72
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "14de7c1d-56c2-47f0-8f74-539bd0ba5a00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 471,
                "y": 37
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "12e77a41-01eb-4b0e-8dac-fd85a192bcee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 280,
                "y": 37
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "0b9807ea-034f-4f0b-9077-25e3f1de50bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 170,
                "y": 37
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "5408437d-2b31-4543-a2df-f61a572c5b29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 33,
                "offset": -2,
                "shift": 6,
                "w": 7,
                "x": 184,
                "y": 37
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "8af77839-522b-452c-902c-64f6dd5414a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 33,
                "offset": 1,
                "shift": 6,
                "w": 6,
                "x": 193,
                "y": 37
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "5da35f16-acf9-4177-9f86-229946dc0718",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 33,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 201,
                "y": 37
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "b9e30bea-5e5d-4fcf-b050-1595a5a00627",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 33,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 211,
                "y": 37
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "a69e0afc-c014-49bd-8bb6-9c39bcc73351",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 221,
                "y": 37
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "f5397ba7-dbe0-475c-b7bf-ed8b77b38713",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 236,
                "y": 37
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "a2ad437d-5822-4996-a9d5-c903c0f5f289",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 250,
                "y": 37
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "c801d89c-cde8-43d0-8c54-f7ea03b95e10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 265,
                "y": 37
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "3c306b9f-d1bd-43ac-8535-697c7f4dd097",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 294,
                "y": 37
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "b507f4d6-a820-466f-85ff-185e25351496",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 440,
                "y": 37
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "81511972-d80b-40f0-8f43-03baa5e67fdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 309,
                "y": 37
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "78dc1bba-c27e-4ba6-a185-7a0bfc3ef2ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 324,
                "y": 37
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "1dcbf94c-cbf5-4940-a85f-5e7d0e8500c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 33,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 338,
                "y": 37
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "ba060fcb-a97e-44c6-b7a7-c4691dd0e397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 355,
                "y": 37
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "fdf4643f-52b0-49e9-8e55-ae138c376959",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 369,
                "y": 37
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "dc34194a-1240-4038-bb7c-31931aba794a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 383,
                "y": 37
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "691654b6-ae8f-4b2d-83ce-ca5776fad5d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 33,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 397,
                "y": 37
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "0bc25347-faad-4114-9e1f-fe620b7c5bb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 411,
                "y": 37
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "20896ecc-67f5-4105-98b7-60993f52e5a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 33,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 425,
                "y": 37
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "c1a22a60-2332-4831-974b-d1d7bded8950",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 33,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 121,
                "y": 177
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "fcdeeb12-fed3-4fd9-9563-69123f78b4f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 33,
                "offset": 5,
                "shift": 26,
                "w": 16,
                "x": 135,
                "y": 177
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "d4e306cc-90e0-440a-abf7-0177fa21e61c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 914
        },
        {
            "id": "db2c1f89-2447-4da2-864e-4f52bf69afef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 915
        },
        {
            "id": "2ef783e3-4ac5-4e6c-b748-839159e74119",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 916
        },
        {
            "id": "5ac6f91f-cfdc-4629-a2f9-b791ca2aec4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 918
        },
        {
            "id": "38f55296-aee7-4fc3-a903-eb77ff35e67c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 919
        },
        {
            "id": "0d0dbb2d-a36e-4a08-b96e-adea1638e0c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 921
        },
        {
            "id": "0fba2b77-c1cd-4016-ba5b-ce0f269374d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 39
        },
        {
            "id": "0aea8322-97ac-45ca-b4bc-81c9bc34fe6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 84
        },
        {
            "id": "39a41999-d806-4192-a60d-aa7d34e01fea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 89
        },
        {
            "id": "12c8c7ad-c862-43f2-a0c5-a1c6cddd5657",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 92
        },
        {
            "id": "e209bf97-bc2c-40b2-889d-3807f3fba7ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 162
        },
        {
            "id": "c426fd2a-cbeb-4af9-b95f-90e06b5d961c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 163
        },
        {
            "id": "1d6d1bb2-3d89-4ee4-ab65-fa0af4aa9ae5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 221
        },
        {
            "id": "a3f42840-7105-4a86-b861-d58b87aa0afd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 354
        },
        {
            "id": "56ee42a1-2a8e-4654-b67f-c7fed3411b91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 356
        },
        {
            "id": "0a54fb0b-6c55-4492-9e79-661c1b71b518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 374
        },
        {
            "id": "62d193b7-8ea8-4595-bfd9-c5c75d6ee127",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 376
        },
        {
            "id": "05b3118a-21d5-40e0-b48c-2e8523bfa824",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 538
        },
        {
            "id": "fccef285-b02f-4799-843b-144f8ff0b041",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 7922
        },
        {
            "id": "f7969f88-41d4-48d3-8bfd-326ab04d8d4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 7924
        },
        {
            "id": "593cf316-4254-476e-9d44-198835be7e0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 7926
        },
        {
            "id": "a136b2e8-5523-485d-9a6d-c8693fe6e3b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 7928
        },
        {
            "id": "f6802611-ce28-41dc-8bff-6c469071ae77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 8482
        },
        {
            "id": "073092ff-e4d3-4e10-9ca4-aaec7b39cdbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 39
        },
        {
            "id": "85071b63-cdc9-4607-acaf-06fc3357a684",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 92
        },
        {
            "id": "596f5091-6f29-48ae-b6a2-35e8567e151b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 162
        },
        {
            "id": "1bbeb0ce-39a4-489a-b977-22b4456046a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 163
        },
        {
            "id": "a996edf3-8abe-4789-b784-fc022551a261",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 8482
        },
        {
            "id": "57faf601-c70a-49c9-a583-48e5c253b4ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 191
        },
        {
            "id": "c4329b41-a416-4dcf-86f6-751212dd4da0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 728
        },
        {
            "id": "d2a0011a-8113-426c-821c-7e4e171efdc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 730
        },
        {
            "id": "e11130e7-fd2d-4c1c-aa7d-48e8011103ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 778
        },
        {
            "id": "a8a9dc78-7fee-4082-a1b3-9bf54172d538",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 904
        },
        {
            "id": "b20e3702-1053-4686-b009-40860064b780",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 905
        },
        {
            "id": "9e8c7282-76fa-4c77-86e3-f4f4a4930e48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 906
        },
        {
            "id": "6da187a5-0d5f-4308-b1f5-8ce1935d0d1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 908
        },
        {
            "id": "c28c0b5e-39cc-4deb-98b8-ac01d1765f68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 910
        },
        {
            "id": "684cfd16-3a57-4492-83f5-9f92d29b9160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 922
        },
        {
            "id": "65451751-8f7c-4f65-9bc3-8295ae06b848",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 925
        },
        {
            "id": "b0d0e362-6a84-4e98-81e5-e2c8ed088b65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 927
        },
        {
            "id": "6b60014d-02a3-4379-863a-4dcbc53c3f08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 929
        },
        {
            "id": "22d5024a-1914-4180-af8d-06b9c1fede91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 933
        },
        {
            "id": "7183cf83-5438-4a52-942e-45a9f55c5e1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 934
        },
        {
            "id": "2f12c60a-588b-40d9-9e1e-0fcd04b5f966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 936
        },
        {
            "id": "534f3b94-263d-4591-a417-04b6c668596f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 937
        },
        {
            "id": "65486d7e-e331-4115-b43c-f40b24b15591",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 938
        },
        {
            "id": "c0e8dd22-902a-4de6-aaf4-9a7d10bc45c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 939
        },
        {
            "id": "3f9d7123-04bb-4f53-a1ef-b5a548688d8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 940
        },
        {
            "id": "1fe90764-b33f-4602-98dc-09cf75ba0feb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 944
        },
        {
            "id": "c496c26a-b988-4912-973b-eec7e9b2a547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 945
        },
        {
            "id": "dd0c019f-6a3f-4cce-91fc-56cabe2f0ffd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 946
        },
        {
            "id": "bc7fd28b-d041-46fd-a8de-27572bec4245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 42,
            "second": 951
        },
        {
            "id": "e6b12191-c186-4aba-98a2-209e6f140fd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 42,
            "second": 952
        },
        {
            "id": "3600b745-3237-4c99-8827-e8aac250f903",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 957
        },
        {
            "id": "add1c4b4-b451-44a1-9ed8-f74a535fd4a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 960
        },
        {
            "id": "797101c8-a182-4b78-bf6d-9a96b0939c89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 964
        },
        {
            "id": "416132fc-65ce-457f-9fe9-1cf6e67ab5e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 972
        },
        {
            "id": "2a6c7aa2-4154-413a-912d-b73f171693a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 8212
        },
        {
            "id": "5e634de0-ee97-4b31-bb91-8910cc1d7bd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 8216
        },
        {
            "id": "08f25e58-e9f2-447f-b3b8-6b2de317c55b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 8217
        },
        {
            "id": "24351a57-0c48-4fc2-bf5d-6e1e959030f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 39
        },
        {
            "id": "b04b8a56-4aef-469f-bb00-3d0e337b1ce0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 84
        },
        {
            "id": "8d09ffc6-5e5e-4945-b33d-378006e6c390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 89
        },
        {
            "id": "50a6e1e1-0132-4528-be67-5bf556a6cc3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 92
        },
        {
            "id": "1fd64ddf-4793-43f6-876f-62c4f326e70e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 162
        },
        {
            "id": "c8f0f65c-49e4-484e-b9e3-c74d0d1f1d73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 163
        },
        {
            "id": "6a0d7ed6-b0a0-4285-be88-c8586e9e3bb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 221
        },
        {
            "id": "ca463918-22d3-46cb-b716-11ea6cfab5f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 354
        },
        {
            "id": "a6eb8770-146d-44a6-8b46-7128fa174f01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 356
        },
        {
            "id": "d63d53e7-ac8b-41ed-a272-ef6d247cf5c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 374
        },
        {
            "id": "6b06eb0f-3a07-48de-9268-2f0ef0a496b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 376
        },
        {
            "id": "a49d182e-4ca3-40f8-b9c1-d0c949968d0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 538
        },
        {
            "id": "8134db5a-0814-472d-8aae-598cf3bce466",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 7922
        },
        {
            "id": "78043348-bf46-4c01-b3c0-366f0508d0d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 7924
        },
        {
            "id": "93d4fc90-90e7-4bf0-bfeb-3b8cfa15449f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 7926
        },
        {
            "id": "981dc10c-af0c-4090-b07c-c752c8c7f170",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 7928
        },
        {
            "id": "e96e3261-69bf-4fd9-89aa-536c192ba5d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 8482
        },
        {
            "id": "3667b375-ebb8-401c-8d85-17702fd502cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 39
        },
        {
            "id": "2b605950-19ae-4f8b-9d41-94b83c8c52c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 84
        },
        {
            "id": "425fd2f7-8224-4d70-b971-cc3a6b00bd13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 89
        },
        {
            "id": "d32e1ee4-d6e8-47bf-960d-cc78e8e0bf15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 92
        },
        {
            "id": "c80ad9fd-c783-4a07-a607-eb7b2bea81af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 162
        },
        {
            "id": "41c55609-b1ae-4b41-b1dd-140c0251b78d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 163
        },
        {
            "id": "b82a7468-c28a-4293-a82a-77bd98c28121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 221
        },
        {
            "id": "713e1ef4-6ab4-43e6-8803-905e980e4b4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 354
        },
        {
            "id": "5b5011d3-f342-4a35-9d5b-04775c117622",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 356
        },
        {
            "id": "f2d44148-5186-41ea-82a9-241850a546fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 374
        },
        {
            "id": "4cc82846-64d7-4de7-ba5d-ba520beac300",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 376
        },
        {
            "id": "53e35bb1-f15d-4ca2-ae9f-d695f5f14013",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 538
        },
        {
            "id": "5134918a-a814-4b0b-8e78-fe194efbe020",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7922
        },
        {
            "id": "3320dbdf-d911-429c-aaee-c5aae2cd2400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7924
        },
        {
            "id": "475115d4-917f-4906-afa4-6cb255708eeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7926
        },
        {
            "id": "1171b380-bda0-4006-93e4-6fcd92908e98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7928
        },
        {
            "id": "eef6472f-8fe5-4c53-89eb-1f1d420f8310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8482
        },
        {
            "id": "5deed709-8d1d-4150-8abc-9614fa980faa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 39
        },
        {
            "id": "935a5749-bd34-47d7-ae10-62437b774bae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 84
        },
        {
            "id": "7113bcc6-3f12-411d-baa7-af9f93c22449",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 89
        },
        {
            "id": "d2bba1f7-1557-4345-81d6-8ff2fa638b4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 92
        },
        {
            "id": "88efaeaf-dbef-4f3c-9af2-904613835ed9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 162
        },
        {
            "id": "f9233a93-87b6-4a4f-b7e5-8626d20e5736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 163
        },
        {
            "id": "ecc7d978-fa6f-41f8-8644-2fa845e0c7de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 221
        },
        {
            "id": "dbbc4f16-42af-4030-a563-ae961e5ae56a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 354
        },
        {
            "id": "9826ede9-d46e-4ff6-aab3-d1e54e8f2501",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 356
        },
        {
            "id": "0f7b2e19-20de-4bf0-8167-9acab8e79cf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 374
        },
        {
            "id": "d764b7ed-9f65-4281-bda1-fcd0f13725f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 376
        },
        {
            "id": "86959071-7596-499b-975e-53181f7a9437",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 538
        },
        {
            "id": "a5c9aa15-8e39-483c-a0d4-888b8bdd64f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 7922
        },
        {
            "id": "e20e7fbf-63c4-4c84-8776-7f9571b61e80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 7924
        },
        {
            "id": "af5a2440-d95f-4077-9acb-69198201e2ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 7926
        },
        {
            "id": "87e60c01-a449-46fb-b576-ed75ccc182eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 7928
        },
        {
            "id": "375f859f-3ba0-47db-ada6-f6d593ee5ef5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 8482
        },
        {
            "id": "a08b1e15-26df-4d38-8493-e213cbc7ac71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "2a0358c5-d61f-4c50-bf24-5e3a2cdc0430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "a5f2d803-db62-4e83-a1ed-6a276184d1ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 184
        },
        {
            "id": "5c922657-3458-42d5-a36d-7e19e648c6fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 221
        },
        {
            "id": "1b89638c-d0aa-4c26-949c-276b3c6ffef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 354
        },
        {
            "id": "e1de9ad9-27c7-4238-92b7-c753293fde0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 356
        },
        {
            "id": "f0419d0c-65b9-40f8-a6a7-5995ecf81d76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 374
        },
        {
            "id": "46500503-4db8-47e0-a038-8ada278b06bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 376
        },
        {
            "id": "0261db97-e875-4a56-8587-20229229b9e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 538
        },
        {
            "id": "794bb4eb-aa59-40fe-b483-c6e1511ff25b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 731
        },
        {
            "id": "3a69d25a-906d-4cd6-94bb-5edbdc38e39e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 775
        },
        {
            "id": "a2e7b404-122b-4fb4-ab75-d0212de1b254",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 806
        },
        {
            "id": "c39cc96e-f26c-4763-b47a-1d39d7b7e360",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7922
        },
        {
            "id": "38c2a975-0b9f-4147-bab6-cb7fc629acf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7924
        },
        {
            "id": "f858ba07-7d23-4332-b596-686cedf38987",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7926
        },
        {
            "id": "283268f5-081e-42d6-b616-288c6cf247e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7928
        },
        {
            "id": "ac8ca55c-e520-4adf-b746-19692eb7c7ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "ab032f39-7e09-40ac-bd4b-134b68d3f2c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "d36609a8-626a-4f85-acf0-12814c33f257",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 192
        },
        {
            "id": "0e4687ce-c04a-4f75-b4ba-b6ec30373946",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 193
        },
        {
            "id": "9ed9fcab-e60a-4f94-ad14-067e0ddac271",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 194
        },
        {
            "id": "3b3e4396-adeb-4357-b462-855de4dc6290",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 195
        },
        {
            "id": "19de5142-60b7-40e6-841a-69d5f7afebc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 196
        },
        {
            "id": "403b3375-2de0-4600-aaff-851eb0e675e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "67decf5d-0e65-4c4a-806c-7788e365ae3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 198
        },
        {
            "id": "c0b44b31-caf7-489a-a7de-5b63417986a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 236
        },
        {
            "id": "4ce7d599-ca10-4668-8d75-132c6d56e5a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 238
        },
        {
            "id": "b120a4b9-0442-460c-a97f-ba21dc43a1b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 239
        },
        {
            "id": "564cceb6-a19d-414a-b4ad-0df2de150af7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 256
        },
        {
            "id": "6edc490d-1faf-4d40-9676-732dcf243d61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 258
        },
        {
            "id": "65cd1518-52a1-4bd0-820e-99c730f77976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 260
        },
        {
            "id": "d4c02718-a05c-419f-a1a9-a115e869b1d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 70,
            "second": 297
        },
        {
            "id": "66547462-1008-4187-a830-5ac9a9f5841a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 299
        },
        {
            "id": "8143fa37-67b0-4aee-99af-adc2fc1fa00b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 70,
            "second": 301
        },
        {
            "id": "27ee3394-9565-4aa7-820a-dc28c8a14337",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 305
        },
        {
            "id": "73a32905-d7db-49df-a1df-c37bdb2596fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 308
        },
        {
            "id": "baeab8c3-adf5-40f3-a9da-ee7f831960fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 309
        },
        {
            "id": "6b3574db-47ba-472b-b559-83da6d51e2b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 506
        },
        {
            "id": "50bac97d-c3ca-46d8-bcba-930e0cf74bae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 728
        },
        {
            "id": "a481dab7-ab17-4c90-a6f1-e41fd484e7e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 730
        },
        {
            "id": "2be819eb-12d3-4867-b025-fb6265f59335",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7840
        },
        {
            "id": "dce1d7c9-3929-4fbb-bf91-9d918e674af2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7842
        },
        {
            "id": "136b1493-e79e-46f3-ad83-403e4cf2588a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7844
        },
        {
            "id": "1be30e0f-034e-41c8-ae17-fd650cb951c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7846
        },
        {
            "id": "0098f065-e68c-4d66-8e73-dd68d534a069",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7848
        },
        {
            "id": "085cccfc-1776-4917-ab93-ad9b225a0866",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7850
        },
        {
            "id": "c4d63f49-b1a9-4965-af0c-9e6a33bfcaaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7852
        },
        {
            "id": "d4f089a8-7cd8-4709-991a-9a5d81efced1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7854
        },
        {
            "id": "b2b4efb7-240d-4680-8db2-7660b1cd4553",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7856
        },
        {
            "id": "3ae496e6-5671-4f08-9aff-cfbf1f6eb513",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7858
        },
        {
            "id": "c97b4d75-d23b-482b-835f-0557e54f3230",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7860
        },
        {
            "id": "31977a92-3b47-4151-97b2-ca3eba8713f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7862
        },
        {
            "id": "1e9296c9-361c-426e-bac8-de637abfcbb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "5fbf893e-ef8f-4822-9b7b-89a166a4a75f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "e075f762-ad84-44cf-9546-cda07054e7b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "1c50af8e-ad5d-4d07-90de-026502db858a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "ee9ecdc8-07e8-47ea-b1fd-fbd012a46af5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "a8f1c10f-0ce2-4e70-baed-3c0def16590b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "99eaacb5-af83-4f4f-8fd4-0e524cb0b9f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "f1e381f1-d235-4138-9820-cd942d9c5248",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 184
        },
        {
            "id": "8362505f-fa24-4156-8323-433ef3167c51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 221
        },
        {
            "id": "354e42cf-4258-4283-83f6-24eecccd3e42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 253
        },
        {
            "id": "7e70d696-c1fc-4a82-9ccf-2078ce21279d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 255
        },
        {
            "id": "c94a672b-a667-4383-91bd-eb978e35d466",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 354
        },
        {
            "id": "811e56c8-81c4-43c8-a562-3261d6152756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 356
        },
        {
            "id": "68148c9e-3bf7-4c36-8770-63093163e918",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 358
        },
        {
            "id": "e3b04f0f-6026-4ae5-b772-cfd96ea3b469",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 372
        },
        {
            "id": "9a9a267f-ef69-4740-be80-82f74ff4660c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 373
        },
        {
            "id": "6bbf2762-ce3d-47a6-8341-c53dc86c0c93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 374
        },
        {
            "id": "e8596970-8e1d-489d-b168-becc593c4e13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 375
        },
        {
            "id": "c3b693da-2bf1-48f5-8621-3687a5a86fdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 376
        },
        {
            "id": "b99343e1-8060-40dc-a897-f5ea6dc336cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 538
        },
        {
            "id": "e8d085f0-d54e-463b-95bf-9a882706a11f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 731
        },
        {
            "id": "a94532ca-54e6-4507-aeaa-435c46672c66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 775
        },
        {
            "id": "2a4d3e4b-e7ac-4e8a-a6d1-3c3a33f81aa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 806
        },
        {
            "id": "3494841c-5ec7-4e1f-b1d9-574fa7ead1d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7808
        },
        {
            "id": "d9149eeb-6be5-458c-9206-60c6a8f28361",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7809
        },
        {
            "id": "2f449298-3c18-4ebb-8fb0-1a91a58de6d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7810
        },
        {
            "id": "8eaba850-31e1-48fa-b202-48da53cc746c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7811
        },
        {
            "id": "42d75a1d-73e0-46da-af5d-8e6e62efc5a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7812
        },
        {
            "id": "7b19a108-3d26-4d50-b6e6-bbf3d9d09804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7813
        },
        {
            "id": "24f272fa-bda3-4dfd-b34a-fc5ebe43e56a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7922
        },
        {
            "id": "497375df-3e77-4921-b63c-5fa718084b5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7923
        },
        {
            "id": "6ee634c8-35cb-4741-a62f-5bc366936bf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7924
        },
        {
            "id": "d01ca74f-6194-4eae-8576-969dd49511ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7926
        },
        {
            "id": "799c165b-4601-4870-885d-baa3d95a0842",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7928
        },
        {
            "id": "ac7ef02f-bc5b-447f-acd4-44005dfcfec2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "8b2b6ab3-a2e1-4581-b394-e6d7a80b49c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 74
        },
        {
            "id": "7756184b-1005-4aff-98c3-fa22e32a1e6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 192
        },
        {
            "id": "5c81e125-a426-46cc-9f1b-fab818d6fc66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 193
        },
        {
            "id": "c539f344-6d3d-426b-969d-0252a01cf5ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 194
        },
        {
            "id": "1dec398b-3ae1-4975-8577-551d0faa8de5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 195
        },
        {
            "id": "47f597b5-0418-4d55-9de2-1581cb9a220e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 196
        },
        {
            "id": "b4327aa8-b249-4676-9944-ca78b1be21e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 197
        },
        {
            "id": "14d9ed4a-119b-4408-8fda-ad2920c74ca4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 198
        },
        {
            "id": "a15bc87e-efc6-489e-9031-26a4f1af5d4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 256
        },
        {
            "id": "1009d510-6b72-46ee-8ba6-88ae4a049912",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 258
        },
        {
            "id": "2282a79e-6694-428d-bf96-16828a992f83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 260
        },
        {
            "id": "15a88073-de1e-4bcc-bf46-0c00ab820d71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 308
        },
        {
            "id": "47b1facd-086d-49ae-a456-9d52788525f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 506
        },
        {
            "id": "96575a2a-fcf9-4d17-a4c8-9e6ffc91380e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 728
        },
        {
            "id": "5efce6ea-363a-4637-8c1c-e2fb0988f79a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 730
        },
        {
            "id": "14659a6f-67a1-47a6-b891-f659346cd1bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7840
        },
        {
            "id": "a6e6d4bb-6ae8-47a5-8bf6-7790a9db9dbc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7842
        },
        {
            "id": "e129433d-43b5-4b2c-b331-2404d90ee0df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7844
        },
        {
            "id": "980e76aa-8229-4fac-84f4-5c728f159b9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7846
        },
        {
            "id": "7190b2d5-ec10-4180-b747-08ed303abce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7848
        },
        {
            "id": "f98cfddf-91fb-4ffc-a5a2-eea8029d63b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7850
        },
        {
            "id": "8993f816-200c-48ce-bb7e-2af17c20e969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7852
        },
        {
            "id": "e2584b36-5447-4d81-bdc2-b5110dcb7199",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7854
        },
        {
            "id": "0e70277f-0740-49b4-9e37-2e2c162dfe5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7856
        },
        {
            "id": "30dd4651-ff37-43b7-b2c2-85a2b61fc80c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7858
        },
        {
            "id": "126bb850-c950-4338-8dcf-e4d88788b57a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7860
        },
        {
            "id": "179cc892-8895-417f-97b6-85ee710eab8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7862
        },
        {
            "id": "56211899-ef50-4e20-82b2-999d5b899e40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "c9833dc2-bd8a-4941-a8b7-5cced43b6018",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "7e37fb28-bff4-4cae-a6b7-db332ed65796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 74
        },
        {
            "id": "c8e19d1a-8c17-4211-8ed1-92071fd24403",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "78486b0b-951d-47a5-8c07-e7a34d3f7edb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "f8d89693-0c9c-4a7c-8006-ec15e6fefaa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 100
        },
        {
            "id": "0f62cc4e-b38e-44fd-a703-1c780ab22f0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "8ec83822-e675-4abe-8d6d-40850b139216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 103
        },
        {
            "id": "7ac70ee0-a752-4e97-b36e-82827b4a9f42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 109
        },
        {
            "id": "a35f94c5-3f9f-4f8b-ae9d-b56e5a08bd04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 110
        },
        {
            "id": "2081b9b7-47fe-4c29-a67c-c2b4482aef5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "96076dc5-d289-4ae0-ad1a-8bd5a2633bfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 112
        },
        {
            "id": "159012c9-9407-4223-9259-eca06bd8cd00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 113
        },
        {
            "id": "f190ac4b-6ca8-44a3-ac56-31bc69ef560d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "e65dba19-57a1-4ed3-8f35-dbf804df27ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "1fbea446-f309-4f76-97be-592ff07dc541",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "152a5df1-3530-4553-b9c0-37baedd15313",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "0f17d2fb-3eae-4f0c-a973-eb4fbed5f8c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "d7ebb342-20ed-4e35-8ff3-340ce72e4fee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 120
        },
        {
            "id": "b45021c1-a580-481d-9e08-1d87515d70ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "86e0b5d8-3fae-4c77-a407-231426a5e1c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "041a9745-9e20-4eb4-b6fe-2f59665e0b19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 161
        },
        {
            "id": "e1428e00-082e-461b-9895-3d334b1137a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 191
        },
        {
            "id": "08c3e4a0-4d26-4616-844f-da454622a100",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 192
        },
        {
            "id": "52d27ff2-7cba-4a02-930e-3d25259ce62c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 193
        },
        {
            "id": "f51ab3ec-2835-48af-a413-09f1ee3bc6bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 194
        },
        {
            "id": "11dc07f8-bacc-4416-9c96-7f13d7733f5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 195
        },
        {
            "id": "5402bc76-3bff-4cd1-8d71-ba7335f3cae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 196
        },
        {
            "id": "5b575dfe-14b8-4c79-9fbc-1aade0b2c16d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 197
        },
        {
            "id": "dbdcf791-eea9-40b2-b76b-80f9040c6551",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 198
        },
        {
            "id": "d7d27bec-942c-49e0-b906-40264d405c8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 224
        },
        {
            "id": "2ec52257-6cb4-4684-9892-b4ed1a2e02d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 225
        },
        {
            "id": "33e66a5c-2320-4e0a-833f-121ed0c4b5e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 226
        },
        {
            "id": "ce482c9b-888f-405e-8e54-b35f9969797f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 227
        },
        {
            "id": "f2499d2e-fcf3-4a1c-8a87-ffe78be7a574",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 228
        },
        {
            "id": "38e1cd85-6b31-424a-91da-7201076ace78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 229
        },
        {
            "id": "f763e2b4-a43d-4e24-a8de-0213526e5fc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 230
        },
        {
            "id": "9b860fd8-0970-4a46-b6cd-2e1fcf01bdb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 231
        },
        {
            "id": "3a5889d3-3220-430d-b6b3-e3e449be1cea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 232
        },
        {
            "id": "88a92206-fcfb-4fa2-a1af-81d4c5208e61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 233
        },
        {
            "id": "2d1e804b-ab6c-4ea8-83eb-da644c6683dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 234
        },
        {
            "id": "b99ee42a-8d7c-402d-bb3d-3bcae7d968a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 235
        },
        {
            "id": "15140c70-039a-4827-b8e5-f7aca28d3dea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 236
        },
        {
            "id": "d3624d1d-5266-4dca-9364-280257ed5cf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 238
        },
        {
            "id": "b1986a4a-0573-43d6-a953-7d08c1984117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 84,
            "second": 239
        },
        {
            "id": "56c90951-a483-4872-9d4f-603fcaaba5d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 241
        },
        {
            "id": "24402d56-76ce-46dc-b8a6-fa19f9851e64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 242
        },
        {
            "id": "77cdc598-d579-433a-bbbe-6b68e08da5f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 243
        },
        {
            "id": "a86f3337-78ab-409b-9dff-02ede4e53da8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 244
        },
        {
            "id": "4a42a145-157c-48d1-b9e5-4665998c8616",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 245
        },
        {
            "id": "06b7cd04-54bb-43c8-a19f-d4fe1909b5c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 246
        },
        {
            "id": "7568b288-cba9-4f8e-9980-2b2760240bcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 248
        },
        {
            "id": "f9bd3d9f-59dc-43ff-a8d7-fc22aaf2e143",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 249
        },
        {
            "id": "352df83b-18df-473d-9b33-014cd0bcec74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 250
        },
        {
            "id": "f358bf31-21a3-47fb-a457-d21f23d422f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 251
        },
        {
            "id": "888c9520-301e-4484-b9ae-af31bef2f45f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 252
        },
        {
            "id": "c9e71871-bd92-47bb-9682-274c3a486ab5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 253
        },
        {
            "id": "8063ae44-ea68-44e4-8ef5-d7e93e0d90d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 255
        },
        {
            "id": "eafb6827-323d-4534-81f4-0780b1669510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 256
        },
        {
            "id": "da0b16d1-44fa-411c-9012-c27b80a359eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 257
        },
        {
            "id": "a40ef505-918d-403f-a08b-1a508d6f8232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 258
        },
        {
            "id": "b289b3a3-9658-4200-9510-d8ce26699c16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 259
        },
        {
            "id": "339f9f2e-d71f-4023-938a-86c840c0171c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 260
        },
        {
            "id": "1b0a8baa-80de-46fb-b930-cf339a232d40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 261
        },
        {
            "id": "1d80e9f4-7880-45b3-8e62-d1e78304710e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 263
        },
        {
            "id": "7bdd51f8-a58e-4ec8-970a-edd4e34bd6c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 265
        },
        {
            "id": "38b95993-8511-47dd-bd4e-687f327b4548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 267
        },
        {
            "id": "d17a1393-9cb5-422e-b839-622d951a4d30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 269
        },
        {
            "id": "bb6dcc82-5dfb-4391-b3cb-7dbc89838406",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 271
        },
        {
            "id": "74b44ef7-7d31-429c-89ec-308ef935d29d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 273
        },
        {
            "id": "077b9437-8f1b-4279-9eec-c2914ed20aa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 275
        },
        {
            "id": "3ffd4e3d-72e7-4923-8ac4-024e2dc1fe57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 277
        },
        {
            "id": "99d57698-b8c8-49fb-af2a-e08d10291d8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 279
        },
        {
            "id": "0646f87d-7bce-48c7-a231-04522099bc74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 281
        },
        {
            "id": "2ea30a56-00bc-4d2f-8fb5-62de651fa9e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 283
        },
        {
            "id": "cde466b0-0db7-4237-8122-f21803331421",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 285
        },
        {
            "id": "e407ca46-bf77-48c4-a415-3ee6c72eb507",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 287
        },
        {
            "id": "4f75fb1e-0dcd-4bac-853c-3031d9647413",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 289
        },
        {
            "id": "cdd87e1e-6722-43fc-b934-566cf9656125",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 291
        },
        {
            "id": "f665d31b-42e9-432b-abeb-4f7b92307151",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 84,
            "second": 297
        },
        {
            "id": "b9d3fec5-3fa7-4469-80e3-17289a75c8a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 299
        },
        {
            "id": "13835d0d-e4f0-476c-8fff-d7800049830b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 84,
            "second": 301
        },
        {
            "id": "dff80e09-4812-468e-8ba1-de2163c487b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 305
        },
        {
            "id": "3ce4cf8a-dfdd-46e3-9666-df49b65e9c34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 308
        },
        {
            "id": "130f5cf4-7076-499c-844e-731215c1eccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 309
        },
        {
            "id": "b8911ae8-62d7-4028-9e35-5208e80d0515",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 312
        },
        {
            "id": "bfda2392-a678-4b20-8063-0a30b07a9104",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 324
        },
        {
            "id": "cb7edc3d-9065-4f78-9b29-828b23a759fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 326
        },
        {
            "id": "d665c4f8-0873-4c0e-a668-3eb45c16a311",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 328
        },
        {
            "id": "6e82da37-bc8c-449a-809d-5442a71d8ce0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 331
        },
        {
            "id": "739f7202-77ad-4292-934e-d2ce7e0d02cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 333
        },
        {
            "id": "e2cf4c77-fbfd-4084-a592-0449aa5517c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 335
        },
        {
            "id": "db6310f3-6b1e-4ace-98be-7629fb6c023d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 337
        },
        {
            "id": "009db134-71af-42ad-94c0-4b399eb81e15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 339
        },
        {
            "id": "3db18aae-41e2-4282-a2b3-777a65573486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 341
        },
        {
            "id": "940c9345-825d-4db9-90c1-4d4b9174585e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 343
        },
        {
            "id": "a9f6f513-7c84-4cd8-898d-228376dc8617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 345
        },
        {
            "id": "cdc4fa50-fd4b-4b7e-b26a-ac8f454d26ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 347
        },
        {
            "id": "bac1ce14-53e9-457d-acad-20fb899a695f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 349
        },
        {
            "id": "9fb8222f-9ccc-4e3d-8fe2-d765d2992981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 351
        },
        {
            "id": "f9be0649-9c2e-4723-9751-64d274f08d9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 353
        },
        {
            "id": "9a2e909d-6650-4fd1-9157-529e45e09f5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 361
        },
        {
            "id": "ca80b942-9d15-422c-be92-309d9ab71082",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 363
        },
        {
            "id": "2c330b2c-cc39-4854-b363-c5a0a2f162a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 365
        },
        {
            "id": "79c27f9a-8fce-47ac-aa4e-d3ea6a858959",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 367
        },
        {
            "id": "3553d18c-3211-436f-ac05-a0abbae4aa8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 369
        },
        {
            "id": "739e8409-5cbf-482c-ae99-966a894a89ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 371
        },
        {
            "id": "dff82f46-443b-4404-b187-40d8fed1743d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 373
        },
        {
            "id": "9580b8a8-b121-48ff-a0be-0cd1fb4327cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 375
        },
        {
            "id": "e2a0f66b-7d99-4589-9f5a-a9b5932c82c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 378
        },
        {
            "id": "c22bdf90-5b54-49e0-ad27-952933136016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 380
        },
        {
            "id": "5e298c7a-1537-4689-aacf-d0f92bbcf247",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 382
        },
        {
            "id": "6d60a454-e53c-4283-8758-d8ab9b621737",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 417
        },
        {
            "id": "81ddb2e0-d8fe-4d84-9c7d-1c89eb5deb70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 432
        },
        {
            "id": "f166e44c-a87a-485a-a6b1-9f40400dce87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 506
        },
        {
            "id": "697b7573-1b43-4129-b19e-5f42957f6295",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 507
        },
        {
            "id": "02868431-17ee-4942-a722-b19f6ca8bdd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 509
        },
        {
            "id": "38b9750c-8642-424a-9677-c46ebe8314aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 511
        },
        {
            "id": "c0ea0d6e-e485-4b52-8a8b-8308a847486b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 537
        },
        {
            "id": "d5c98e24-3ab3-4d09-8e9d-eea53b7c124e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 728
        },
        {
            "id": "4dc7d7c7-146d-4398-8e97-7da59c18e216",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 730
        },
        {
            "id": "eab614c2-240e-4263-85e9-719d505621b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 774
        },
        {
            "id": "43d77d79-8bda-45f8-a5f7-398758b6139f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 778
        },
        {
            "id": "f17b6130-4b05-4467-a48e-aeaf251e7728",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7809
        },
        {
            "id": "122156e1-83cf-4347-8dfe-fc64a81d050f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7811
        },
        {
            "id": "44418801-55b8-459b-9d19-9ee7e4c3c881",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7813
        },
        {
            "id": "d14628c8-bfb5-475f-b86b-d0aa33417077",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7840
        },
        {
            "id": "8b76a09c-f21d-43e1-9d0c-09bfcef0777d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7841
        },
        {
            "id": "7b59cbcf-fd7e-4be5-bc42-359f477a7372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7842
        },
        {
            "id": "7608b3c2-27ed-4c44-af0e-c05629dc1629",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7843
        },
        {
            "id": "1822d83f-eccf-4c07-820a-c77b572c5dec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7844
        },
        {
            "id": "f75b4c9e-dea7-4a7c-9d2f-b03f4f2a6cf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7845
        },
        {
            "id": "bfcf7d09-df59-4633-8fea-f113bfe52965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7846
        },
        {
            "id": "f57346a0-5c1f-4517-9e86-9b53f5a5ff4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7847
        },
        {
            "id": "effca583-c338-4b04-80c9-13d9717f7de8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7848
        },
        {
            "id": "250c3294-1e2d-4dfd-86ba-baf856dcecab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7849
        },
        {
            "id": "58df717d-0b74-455f-aa32-a79dd4bb2c8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7850
        },
        {
            "id": "53317eff-a305-4c25-a2e5-8c749c8cce96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7851
        },
        {
            "id": "cabd74e6-7ec6-485d-a489-0f8b4637891f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7852
        },
        {
            "id": "b8280d8b-5139-4e1c-a869-3c977584412e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7853
        },
        {
            "id": "d95d1a5c-a187-41ca-8c04-88df33ba023e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7854
        },
        {
            "id": "91da5bc2-7329-4e04-b809-ca335a94602d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7855
        },
        {
            "id": "4e24f0a2-5ecd-44ad-b81a-6db94fb17564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7856
        },
        {
            "id": "a5394402-0273-4560-bec1-a3d4630f70a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7857
        },
        {
            "id": "c34920cd-aa6d-4624-80a5-59523b25be6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7858
        },
        {
            "id": "a7e0ff4a-9e4a-4c9b-99e2-e83ecf6aeafc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7859
        },
        {
            "id": "b303a9b1-aa07-4627-8894-9228d76ff5be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7860
        },
        {
            "id": "b41f62f8-2453-4b25-847f-edbee97748e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7861
        },
        {
            "id": "809f534f-ac8f-4aa3-bc7a-ceb113a14d8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7862
        },
        {
            "id": "0fb39a83-03a7-4ec9-8972-6f5fce08ed7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7863
        },
        {
            "id": "d0e7c7ce-e51a-4e64-9166-af8454014931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7865
        },
        {
            "id": "54804835-78ff-475a-8ace-eacbca66273a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7867
        },
        {
            "id": "594abac5-308a-4cd3-8960-ef1380ceef5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7869
        },
        {
            "id": "4cd6c040-538f-4518-bcd8-541fd0ccf0c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7871
        },
        {
            "id": "1157749e-dea9-4174-a694-6283bae86681",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7873
        },
        {
            "id": "7a18ab15-cb60-4ae6-af89-e7bf7ea92cda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7875
        },
        {
            "id": "20550215-25de-42fb-a7df-4bac67a8add6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7877
        },
        {
            "id": "9393a78a-513c-426e-b528-4e2b09ebec44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7879
        },
        {
            "id": "cca2cd51-174e-40ff-9561-b0386cdfe6ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7885
        },
        {
            "id": "bc63a70f-13a0-4a90-9b9b-91f3ca382efa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7887
        },
        {
            "id": "92e662fe-02c0-4298-a42c-9a7eb1756360",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7889
        },
        {
            "id": "b835e429-93bc-45ce-bf2a-9f8362297d5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7891
        },
        {
            "id": "19426a4d-3030-4399-80f4-5e999b900e2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7893
        },
        {
            "id": "ca8df061-d1cd-47e8-8072-54c3c9cf8de5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7895
        },
        {
            "id": "9db1c349-815c-4b48-8693-584a38c1bb2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7897
        },
        {
            "id": "36702bf9-79c9-4983-9cbf-257055cd8d83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7899
        },
        {
            "id": "31d40641-0c57-43f9-87d3-e2acd5b982d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7901
        },
        {
            "id": "03f7d66e-d9ce-465f-a51d-d9c1a70583cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7903
        },
        {
            "id": "65627719-3828-47e2-8011-bfe8d7a1d5d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7905
        },
        {
            "id": "95b31bb1-1cd7-4f72-9623-60b84af9972a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7907
        },
        {
            "id": "bc1de0af-1081-4f77-8900-d1275fecf801",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7909
        },
        {
            "id": "0610af9c-eac4-4595-b38a-0c608c1e4cb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7911
        },
        {
            "id": "6713af08-13c0-4611-a8e2-41c51e656df4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7913
        },
        {
            "id": "36c83503-0e3f-436c-8b33-8f70b9c9d1f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7915
        },
        {
            "id": "b282e086-254e-49dc-aed2-168f56c122fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7917
        },
        {
            "id": "e5de87e5-375b-414e-9e89-1be95160ed28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7919
        },
        {
            "id": "e596f5dd-f93b-435f-8ac7-d07e255b900a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7921
        },
        {
            "id": "92a1ac27-f3a9-46d0-bb2c-10aa4844ba81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7923
        },
        {
            "id": "e81b635c-9aab-4172-b599-0fe6ce5a2908",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8216
        },
        {
            "id": "db8dff73-8790-4be0-83ec-c3b5bd625a8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8217
        },
        {
            "id": "9e1cff74-3831-4141-bf22-51bd3403fdf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8230
        },
        {
            "id": "8744e73e-6d15-4413-ac10-1df624f37ffa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "d2c20b7a-b95e-4bf4-ab0c-97f0219f8c0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 74
        },
        {
            "id": "353bff16-c5e1-40e1-89aa-a87969422d52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 99
        },
        {
            "id": "a6192044-3a53-4732-9c77-091612c00802",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 100
        },
        {
            "id": "d47943d4-bd2f-4d20-9593-1d07bb486fdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "fe30203c-8c65-4440-825c-e1c636a9e35d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 103
        },
        {
            "id": "37288d35-7d65-443e-8def-330a005686b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "fc292566-2540-4cc7-81c7-5eecdf2cf2f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 113
        },
        {
            "id": "8d67c380-2e15-4477-aaf1-1bd1be77335a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 115
        },
        {
            "id": "9a5d07d1-0c27-43cf-a147-aa94a75706af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 191
        },
        {
            "id": "1d462fc4-b154-4086-bca7-ea894a01e7e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 192
        },
        {
            "id": "82ebce1d-0204-4df1-9447-a24bf1531128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 193
        },
        {
            "id": "f88aa7a5-3770-4175-b843-437e98d2b9ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 194
        },
        {
            "id": "5def961c-434a-4826-b283-50f99fdeb386",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 195
        },
        {
            "id": "120ca08a-fed4-4134-91f3-74464ceb9e53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 196
        },
        {
            "id": "4a38c146-fd8b-4b88-b07e-6269002346e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 197
        },
        {
            "id": "178ca239-ed65-445e-8c1a-3452a6eb6749",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 198
        },
        {
            "id": "9adcaf85-c66f-4f71-bdbb-b9f762e589f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 231
        },
        {
            "id": "95ee5d6b-ad7e-49fb-8256-a67baeeee5f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 232
        },
        {
            "id": "665ab362-4c5a-4aac-9b5b-c269580bdf88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 233
        },
        {
            "id": "c78518fa-81cb-43ef-8bc6-e7baa1890ebe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 234
        },
        {
            "id": "f01885b1-8789-4cae-8cb7-fb53a2cd9630",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 235
        },
        {
            "id": "edb1ce1f-fc3d-4489-9282-df0a91f5a81b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 236
        },
        {
            "id": "7fa055c4-024e-49ef-bb84-c9a8a3bd6906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 238
        },
        {
            "id": "78d6c6a2-773a-4720-8ee0-1caa0c91be41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 239
        },
        {
            "id": "38a24c33-6b15-4f7d-9a02-9960d7e8c530",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 242
        },
        {
            "id": "41d8bf33-a117-41c4-ba8e-f5c402bd0a77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 243
        },
        {
            "id": "068f98e7-320d-431d-b171-415bcb8df503",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 244
        },
        {
            "id": "cb7353f9-9138-4780-ac76-7716aae0c4f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 245
        },
        {
            "id": "a4f2eca5-c792-452b-8b6f-6e63cb504c70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 246
        },
        {
            "id": "0583c25d-d3a5-4d18-bd6a-97988f9aac8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 248
        },
        {
            "id": "5b2cfb61-8234-4c68-98bc-9441db95ae7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 256
        },
        {
            "id": "163f3a79-49eb-4c76-bb02-0402b0c9e697",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 258
        },
        {
            "id": "455e8861-fc17-4ecf-a43b-1c533ffd8591",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 260
        },
        {
            "id": "3eb49bc8-a893-47c9-a21b-e150592b6a8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 263
        },
        {
            "id": "737863e8-3062-4f2f-a4a1-7af2f2c14fac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 265
        },
        {
            "id": "5caf0663-adc3-44c8-9a37-0d07a24f4d6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 267
        },
        {
            "id": "f635ccbc-5961-4f0d-b5c8-da264277d73a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 269
        },
        {
            "id": "92f006cd-e7bc-4550-8173-05acd63557ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 271
        },
        {
            "id": "b2c6d71c-c7a6-409a-9a65-42662061011d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 273
        },
        {
            "id": "e952c8fa-aad1-4a1b-9847-b18be66c80a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 275
        },
        {
            "id": "663535e1-ec46-4c2e-abee-2cbbd5c050a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 277
        },
        {
            "id": "700821f9-ec5a-4e55-a750-8ac44463f555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 279
        },
        {
            "id": "3639c766-f50a-4259-a1b6-3d49cb734fdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 281
        },
        {
            "id": "e3088d1c-d06b-438c-81bf-90382d3e99c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 283
        },
        {
            "id": "d697acd4-456c-4740-84a8-5ec15fbbc348",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 285
        },
        {
            "id": "02ead0f0-038f-4b3b-8ae1-e67ca77aedef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 287
        },
        {
            "id": "200690c5-cab4-4b8f-b4c7-7b6b4da23e94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 289
        },
        {
            "id": "932c73da-735c-4427-86d7-fde4e8ab2bfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 291
        },
        {
            "id": "d3fbd7db-3df9-4796-9311-4f20d7baaa06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 297
        },
        {
            "id": "140f6d4d-fe90-4eeb-8ff6-e851e40947f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 299
        },
        {
            "id": "742e1058-801d-4a40-a864-5c5dc36cc606",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 301
        },
        {
            "id": "cdd38a1a-ec9a-43ac-b5b9-e4022998bccf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 308
        },
        {
            "id": "a4153e5f-0c65-4812-9405-1811d46097e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 309
        },
        {
            "id": "79a982a7-6ea1-4bc9-bd87-f4c9ba938016",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 333
        },
        {
            "id": "6036df29-46e9-4fd9-b107-b816359eb69f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 335
        },
        {
            "id": "d8648715-0763-4fd1-928a-dc7fada31bcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 337
        },
        {
            "id": "a796bad3-20a4-4dfa-b530-1e4216ab5ff3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 339
        },
        {
            "id": "3859bdd3-bd09-4b97-bd63-ccca4954ba6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 347
        },
        {
            "id": "0b52af36-aef8-45f4-ae0c-7e5d3da0c024",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 349
        },
        {
            "id": "d5d525a1-d927-441f-9904-268b50f6d2f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 351
        },
        {
            "id": "160f4d17-b3b2-4d2e-a4bb-ef7a6af3bdb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 353
        },
        {
            "id": "d1851aec-4168-4432-8d58-265afcf5a8a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 417
        },
        {
            "id": "e9cdff1d-a920-4d4d-9bf0-060f685b3f46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 506
        },
        {
            "id": "dabf3886-6728-4d4d-b8f0-4c7187a14bcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 511
        },
        {
            "id": "b3949604-595e-4276-a6bb-5a04ac48d9af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 537
        },
        {
            "id": "124c343a-3d38-4014-81f3-c434b40b5110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 728
        },
        {
            "id": "771c6111-9a44-494c-94fb-ffa35d6549ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 730
        },
        {
            "id": "53e4feaf-794d-40ff-94ab-a12e408f51ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 774
        },
        {
            "id": "f083dbe9-4b07-4639-b4e4-b9e14be29244",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 778
        },
        {
            "id": "fdb97215-13fe-4e3f-b12e-aa4c786df222",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7840
        },
        {
            "id": "49b827db-8ddb-43fa-a4d6-1fde9bb016b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7842
        },
        {
            "id": "2fcd4851-02a4-4b0a-8fab-cac53a43e7d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7844
        },
        {
            "id": "7b12c904-c841-47ab-b59e-5b0adf12edfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7846
        },
        {
            "id": "7a1b7126-38c6-46f0-aaf7-efd8025d06d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7848
        },
        {
            "id": "3fab3fb0-f553-44ab-8fd6-635691418f6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7850
        },
        {
            "id": "a938555d-2a04-40e5-b4a5-4649c549b667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7852
        },
        {
            "id": "43fbdc98-0fd7-4565-b41b-aa8e5684b8e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7854
        },
        {
            "id": "864929f3-43d2-4291-9b57-381e8a032f86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7856
        },
        {
            "id": "4cb15781-1612-450d-bc49-e911eceff9c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7858
        },
        {
            "id": "6eed16c3-2e8b-4a03-94b4-d504c6578e2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7860
        },
        {
            "id": "f28408fe-4baa-444e-9dd0-1fee5adb091b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7862
        },
        {
            "id": "8713c2a6-46c2-42af-baf8-013eeea4d875",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7865
        },
        {
            "id": "aeefa3e9-db96-49d4-82d1-d252e6e547a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7867
        },
        {
            "id": "d0d8af66-58b2-4993-80fc-691b09945d4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7869
        },
        {
            "id": "0bf73ce6-1823-42e5-99ec-aa82f68860af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7871
        },
        {
            "id": "60a63f2c-d250-4aa6-ab7e-c7f385780649",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7873
        },
        {
            "id": "60f8832f-1db9-467e-8c5e-d3841d0a10a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7875
        },
        {
            "id": "e99ab273-0609-40ff-bdba-f2ee2d7c0c02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7877
        },
        {
            "id": "9ac6a110-1b1c-426c-805e-c24e96b85a67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7879
        },
        {
            "id": "ff08abeb-06c6-41a6-89ca-fe2bb79eead1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7885
        },
        {
            "id": "5a7c4603-bbc3-4173-890f-87f7be40e10b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7887
        },
        {
            "id": "c318788a-088b-44ad-928c-144cb17f2446",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7889
        },
        {
            "id": "146aa611-4a1a-4bfe-9b33-6cbdd623f805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7891
        },
        {
            "id": "d33c46fb-ece6-4003-abc5-bf9110bb567d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7893
        },
        {
            "id": "be394cdc-7016-4176-979f-654bda2511c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7895
        },
        {
            "id": "95a4a8e0-e19f-40e9-98f7-5e696d1cb67f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7897
        },
        {
            "id": "0e43949b-2bde-44eb-a467-a7bfa65f027e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7899
        },
        {
            "id": "08e39d64-9429-46dc-b6ae-18f2d4bf5998",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7901
        },
        {
            "id": "efb6a4c0-6676-4edf-b469-20aa04dac8dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7903
        },
        {
            "id": "958c9c6f-a4f7-4fef-abd6-b47db6559eb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7905
        },
        {
            "id": "63b9ec91-884f-49c6-84f5-e201f4ba8373",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7907
        },
        {
            "id": "b2dcbc6f-13b2-424d-8831-c5a404b49e33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8216
        },
        {
            "id": "0947f062-51d4-4fa4-ba65-08d608a153f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8217
        },
        {
            "id": "33d1c8d1-957d-4ffe-8ede-805871dd7c4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "6211dd15-40fa-4419-b257-ff5bdef08c61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 74
        },
        {
            "id": "da537aac-71be-48e2-b1f0-364d541abfab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 99
        },
        {
            "id": "58bb574c-4a04-4419-b3a3-9c5e7bac9f13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "e383ed6e-e904-4219-8329-697cd7a00d34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "a0f24702-1c9a-41a8-9b68-72913037ac98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 103
        },
        {
            "id": "d1a22028-622b-4ec5-bae5-804c1e92b5b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "80ab9782-009b-4572-86e6-2589b342313b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 113
        },
        {
            "id": "919fba30-d8d3-4c60-975c-e46a2fa0b2d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 115
        },
        {
            "id": "c9920fc7-9ea8-4d11-8c33-354ff1389c50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 191
        },
        {
            "id": "2e41f6c4-f4c6-4243-b78c-e7e50d017cdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 192
        },
        {
            "id": "83ea8196-5675-4073-b246-412788b157fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 193
        },
        {
            "id": "92b72d35-ccec-42d5-8dfd-0ec90e0f4868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 194
        },
        {
            "id": "54561ad5-1272-4fd1-9b01-9adf1fba6f07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 195
        },
        {
            "id": "1e04786b-8b59-42d9-8ae3-657e9ff319f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 196
        },
        {
            "id": "6a8d8abb-cc9a-4a65-b052-996c0bce58c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 197
        },
        {
            "id": "db6c5797-33d2-4477-a3da-d6ecaec1a81b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 198
        },
        {
            "id": "8a4c5cda-6038-418c-8efe-5412ecac90cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 231
        },
        {
            "id": "4b9f614b-befb-42e6-9ef9-e5ee93c3847c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 232
        },
        {
            "id": "3826670a-a0c2-418c-b6af-5e8fd1532580",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 233
        },
        {
            "id": "c8725125-464c-4458-9a6d-ee57d4fca281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 234
        },
        {
            "id": "cab936f8-45ef-4f28-99e2-22845c870c6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 235
        },
        {
            "id": "c390e481-847d-4b37-b026-bb3ce743b425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 87,
            "second": 236
        },
        {
            "id": "32f64952-ea43-4aef-8f51-1eaa1fc5fffc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 238
        },
        {
            "id": "866abaa1-914c-496c-8b67-a99a90fb46bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 87,
            "second": 239
        },
        {
            "id": "9bbba9de-779e-448a-a165-06b08650465f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 242
        },
        {
            "id": "c18bbf5e-ac7f-4dbf-8ba6-0f901ae424e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 243
        },
        {
            "id": "5d901849-6938-4773-990b-11545e4f2d4b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 244
        },
        {
            "id": "7df97a31-f6b1-44fe-ad4a-e2bf5dfafe36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 245
        },
        {
            "id": "034678f2-d869-4412-9354-c7f4c56e2d9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 246
        },
        {
            "id": "f481fb72-4c6f-4617-a462-bde1e2637bd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 248
        },
        {
            "id": "7505e25e-45f2-4679-9fc7-853203a65df0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 256
        },
        {
            "id": "bb632c27-1edb-4b21-9b9a-22e579153921",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 258
        },
        {
            "id": "46831f18-6441-42f5-a30f-b25e15d4e0ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 260
        },
        {
            "id": "77251198-01e8-4e18-a4fd-1432c9534509",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 263
        },
        {
            "id": "1d90b143-e6fa-4b2a-870c-1f3e055e9b8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 265
        },
        {
            "id": "e3d5765b-ec44-4897-8a4d-418602e110ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 267
        },
        {
            "id": "4b9b456c-d064-4b6a-9eab-d2fafe54f422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 269
        },
        {
            "id": "4920d861-4906-4ff7-9fdc-480301a76ab6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 271
        },
        {
            "id": "be3822cc-2733-4a7e-9a3d-59612a744e40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 273
        },
        {
            "id": "0969d408-bfa6-4e2d-a659-e1afaa27dcd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 275
        },
        {
            "id": "3f10d1b5-74c5-42c8-8fca-7fc096f40f3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 277
        },
        {
            "id": "90062dd2-bb7f-4c24-99f0-f1c6b2777f94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 279
        },
        {
            "id": "7a15c559-e37e-4b91-8796-48f0ed995fc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 281
        },
        {
            "id": "96e5b5c9-ef85-44ff-ac61-08f2995e530b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 283
        },
        {
            "id": "851533f5-ffbb-4a41-be67-31b1772a122e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 285
        },
        {
            "id": "d0da8d99-58c2-4144-a1df-7e3243d61df2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 287
        },
        {
            "id": "e6e50660-68ab-4d3f-97ec-71ed950fb0c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 289
        },
        {
            "id": "29a5a0f4-ddce-44eb-9ae6-b8b6d9f92be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 291
        },
        {
            "id": "73f45643-744d-4753-b97f-c7f15184a96d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 87,
            "second": 297
        },
        {
            "id": "fcb7391e-1148-4f4d-85b5-e7bd4e8217d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 299
        },
        {
            "id": "72f83f9b-4336-4b99-bf41-24696e966176",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 87,
            "second": 301
        },
        {
            "id": "6d7d2e27-0abd-43c5-8ed7-1006ba1bf890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 308
        },
        {
            "id": "8babf78e-3228-44e8-bb36-d08b6eaa975a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 309
        },
        {
            "id": "8aea466b-88c0-4c93-929e-0719a589a324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 333
        },
        {
            "id": "c4d90d43-8514-4402-99ec-85898eeee105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 335
        },
        {
            "id": "4f493b59-83be-411d-93a8-d3560e21b8ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 337
        },
        {
            "id": "db519240-ff3c-411d-addf-5628f7fdeae2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 339
        },
        {
            "id": "2186e91e-53b4-4b52-8045-2aae2bd474ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 347
        },
        {
            "id": "e5f2a00e-5667-4126-af17-0513c89426dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 349
        },
        {
            "id": "dac26bf4-5f22-4656-aae7-e7c49bcfd153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 351
        },
        {
            "id": "de911703-b400-47a8-a486-60a3ef86ea68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 353
        },
        {
            "id": "394d7304-ad67-45e9-ae23-b861d75c4ece",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 417
        },
        {
            "id": "321246a4-56a4-4490-bc0e-15ceca2af1b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 506
        },
        {
            "id": "d008d56a-da95-47d2-bed7-9af6db1ed8e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 511
        },
        {
            "id": "dc40d385-170c-4b59-ac22-2b325c920d73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 537
        },
        {
            "id": "5975ff14-56f7-45b2-b895-a8194b60085e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 728
        },
        {
            "id": "2dbd7a1b-5356-4bc5-a4f0-3f915c3b94d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 730
        },
        {
            "id": "484f8191-5fb2-4027-8d78-d24221c8587c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 774
        },
        {
            "id": "7c5a99a8-707e-4718-af58-1da42b994a48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 778
        },
        {
            "id": "08b3462a-9548-4d14-8d84-74b0a9f219ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7840
        },
        {
            "id": "db8a02ab-9fce-4e60-8d4e-b43e855dd5b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7842
        },
        {
            "id": "db7af939-a163-44d4-8a45-7db7cd49c26f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7844
        },
        {
            "id": "e2058fe3-ebe2-4458-bc1e-c8436f48844d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7846
        },
        {
            "id": "18709711-1c60-4b8e-821c-8a7043350836",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7848
        },
        {
            "id": "03ef812c-9bf1-45a9-8cf3-0e0b170b929c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7850
        },
        {
            "id": "4bc253b5-8e7d-4be6-8479-a19230a14b74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7852
        },
        {
            "id": "7b6a8dcb-7da5-43b6-930a-9514e7e28aba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7854
        },
        {
            "id": "fa34298c-4979-4242-8900-457e84bc13f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7856
        },
        {
            "id": "7e59bdf2-2c98-4902-ad4c-1fe07892c8a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7858
        },
        {
            "id": "dff8d7c9-be34-4dd2-b90e-787fb73e30fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7860
        },
        {
            "id": "95f4c496-2c3b-4db1-a78f-dffa70dddfda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7862
        },
        {
            "id": "b6034cf6-c5a5-4523-bd79-fdf306c7c02e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7865
        },
        {
            "id": "28e2fa67-ab96-46bc-bbb1-70029df41777",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7867
        },
        {
            "id": "bfff7858-e9a5-4827-b52a-224bcd251e25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7869
        },
        {
            "id": "78695446-8db3-4568-b1b5-b8f23cddbd8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7871
        },
        {
            "id": "da7d37a7-7762-4d46-884c-67931784168c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7873
        },
        {
            "id": "fb992336-7091-4d31-bc1f-3a46cfd42a38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7875
        },
        {
            "id": "a065679f-6ec7-4e19-813f-77e8519d13af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7877
        },
        {
            "id": "6d4a39c5-6861-4180-a527-b052a7ea49b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7879
        },
        {
            "id": "a15cdba6-798c-4893-8f43-b54a25935057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7885
        },
        {
            "id": "dd8ecfa0-4610-408a-9e5e-06a7f0243358",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7887
        },
        {
            "id": "996b6322-1389-4d93-b245-5627c66e610a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7889
        },
        {
            "id": "9aa1ed19-c8c1-49e6-a25e-af9326d999aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7891
        },
        {
            "id": "3f5dbd68-5490-460e-9ea0-1e289d4fe556",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7893
        },
        {
            "id": "3cca2e1a-a455-420f-978e-e82400c26ec1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7895
        },
        {
            "id": "5c7ce01c-928e-4c5a-8dd0-030c1b8889b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7897
        },
        {
            "id": "2346b039-94a2-4d40-858f-5b2845459bf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7899
        },
        {
            "id": "c3f40919-1949-477a-b3db-c607e2a5eef3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7901
        },
        {
            "id": "2c94f915-fbe2-4bd3-a93c-82735312a445",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7903
        },
        {
            "id": "19c22026-e071-46fb-a8ac-76f48d36e8dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7905
        },
        {
            "id": "df21c62c-f829-4b9a-aeef-c9390e044c7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7907
        },
        {
            "id": "3f6075d1-379e-44f2-9f95-ba424d2ebc24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8216
        },
        {
            "id": "4aff994c-ee1c-454d-84c8-581c0e7e1d2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8217
        },
        {
            "id": "051416b6-84de-423f-8969-133b7a6a8a85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "fcb6d66b-a2b3-4b0f-8917-3fcb9498ef01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "d4e147e7-1299-4765-b61c-90e4da431f43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 99
        },
        {
            "id": "73f734ad-3793-41a8-9441-81474a4bab24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "a3fe065e-4c2e-45d5-9843-f9afc6c3daf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "d665b4ee-7bfb-4503-be5d-1300b9596396",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "250954c2-db1d-4da1-a517-4185a51a962e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "1f640cf4-68b7-47ef-adc8-2642535c2593",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "d4a618c3-596d-4aa5-9a91-55007bfe23d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 115
        },
        {
            "id": "478415c7-7249-4b73-bd5f-a979a19981b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 191
        },
        {
            "id": "101c0b76-8bc3-4d31-b137-f22b3daebda7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 192
        },
        {
            "id": "d14c5db2-1e22-43ea-8677-5e9416d9a46e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 193
        },
        {
            "id": "fceecb5e-bd26-41d3-a434-ec9ff75132c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 194
        },
        {
            "id": "3c419336-2329-4e20-928d-a7216749af41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 195
        },
        {
            "id": "eb4afe6d-5f79-41ca-8952-fab1f44bff78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 196
        },
        {
            "id": "0caf43f5-d346-46c2-bf14-d8e8aa67241f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 197
        },
        {
            "id": "4a0bf646-56de-47e3-af65-c38295320a7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 198
        },
        {
            "id": "0441db12-c711-400b-8aaa-1067592de31a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 231
        },
        {
            "id": "bfef16a6-f3cc-4328-ac28-860503f9dcfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 232
        },
        {
            "id": "901e8b81-659e-4dde-9ccd-abf3dc4245b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 233
        },
        {
            "id": "29ea24cb-45d1-48f5-976a-8cc52de4d283",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 234
        },
        {
            "id": "ec08eac8-e0f6-4bc7-a412-d377d5ad8ba7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 235
        },
        {
            "id": "f9b1ffc0-079b-4bb4-9451-6d0b7cb79eff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 89,
            "second": 236
        },
        {
            "id": "0e09605c-ca3e-4625-a311-b6643bb6cc0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 238
        },
        {
            "id": "55144b3f-7ed8-4ced-96ff-35d1f9b7bdd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 89,
            "second": 239
        },
        {
            "id": "d91ebabd-b820-4ecc-ab0c-85c04e62d0e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 242
        },
        {
            "id": "24dce1ec-79d2-46f7-9115-9fb3c2387465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 243
        },
        {
            "id": "be283b06-deb8-43fd-a9ea-009c5456cba3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 244
        },
        {
            "id": "9f394d49-b28b-45ff-b173-668821c0666a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 245
        },
        {
            "id": "37f21144-77b7-4984-bac4-cd4270898781",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 246
        },
        {
            "id": "c4e74d22-5639-4b8e-8fc8-44a77db896a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 248
        },
        {
            "id": "83e828f8-2b3c-4008-8cc8-9d1a7ea4f5b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 256
        },
        {
            "id": "d952de77-08b1-4886-9cd8-6a52642d3398",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 258
        },
        {
            "id": "f9f3c364-da63-4afc-a28e-e09762b0c563",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 260
        },
        {
            "id": "d3885e5a-c8b5-4a5b-82ba-aa2def63c612",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 263
        },
        {
            "id": "732f033d-2c75-4aec-84f7-c0834aff204e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 265
        },
        {
            "id": "d35ef688-549f-4c7b-9fd7-91525d631d63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 267
        },
        {
            "id": "fbc32894-738c-4b3b-90c4-c676722edfbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 269
        },
        {
            "id": "5193c372-0438-4d68-a684-dfbc3b9361b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 271
        },
        {
            "id": "206b0375-5c27-4d57-bae6-7e07e42d841a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 273
        },
        {
            "id": "7941a676-2f05-47ed-9631-25a60c97a694",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 275
        },
        {
            "id": "a263fd2e-ef86-45bc-921e-25f80ae00d16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 277
        },
        {
            "id": "85055433-db25-437c-9855-e674d8281b9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 279
        },
        {
            "id": "12ee7d24-5823-4eeb-ab62-dc30d2b48d5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 281
        },
        {
            "id": "c6c9bc34-6650-40cf-b2bb-146410fb5dab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 283
        },
        {
            "id": "6e351934-930d-43f7-b122-3ad0a3cdd61b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 285
        },
        {
            "id": "9717abc5-def5-4bd0-87d6-94ca2fc17c63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 287
        },
        {
            "id": "6371bba3-60c8-43ca-9e97-a3965109ea8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 289
        },
        {
            "id": "d0295746-f5cd-4c2c-92ea-7ec73e1b5c6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 291
        },
        {
            "id": "6a6d0696-e5e6-44da-b781-da536196df56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 89,
            "second": 297
        },
        {
            "id": "164562a5-9449-43ee-81fa-defdd206ba97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 299
        },
        {
            "id": "7c72699a-7bd2-4c86-91cf-521db3ab051a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 89,
            "second": 301
        },
        {
            "id": "a33b217b-4004-400d-b525-a522d970824c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 308
        },
        {
            "id": "4fac01dc-a541-4cf1-9ae6-9b717924e5b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 309
        },
        {
            "id": "4c175931-956d-41f9-bdf5-f339c8e7d472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 333
        },
        {
            "id": "23749f1f-bc53-44d8-bdbc-5fd1599b077d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 335
        },
        {
            "id": "73da0aa0-3001-4b99-bac2-7e111fb7e260",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 337
        },
        {
            "id": "43371fe0-1387-42b2-8051-543dbc8cabec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 339
        },
        {
            "id": "2229a86a-4680-4138-ae6b-e3dba258fa00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 347
        },
        {
            "id": "795736be-8017-4694-afd4-8585bb3723fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 349
        },
        {
            "id": "721978e5-edbc-48b1-81bc-a2eb209b00e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 351
        },
        {
            "id": "b1028bf4-0451-4858-889e-3b3c155ddfe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 353
        },
        {
            "id": "f330ce81-cd56-4942-b4bf-0f270e93ec37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 417
        },
        {
            "id": "ca4ebc7a-3a17-4243-95c9-8b45dda16b69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 506
        },
        {
            "id": "83167396-78a4-4b79-af06-9eef603d07c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 511
        },
        {
            "id": "d093cd4a-a059-40b9-8176-209cfd5f8fa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 537
        },
        {
            "id": "91e496c5-c778-4975-a7cc-1a6336df21f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 728
        },
        {
            "id": "6aea73b0-55ed-4dea-ab1e-d71d4dcc9e9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 730
        },
        {
            "id": "40900df7-b6a1-4792-a8c7-ae2dfc1099eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 774
        },
        {
            "id": "b64397ad-207f-4ebf-bfdb-c9d63215d792",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 778
        },
        {
            "id": "bf3ae054-c61b-4174-9e76-cf5e4eda83bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7840
        },
        {
            "id": "0aac5f9a-ef8f-4b87-b65b-86f2ad727ea1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7842
        },
        {
            "id": "3226a94d-2432-4c88-8d82-881be92cd007",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7844
        },
        {
            "id": "4c54c4f7-d051-4448-8132-7e0e37a7b4e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7846
        },
        {
            "id": "03a41bfe-89ad-4377-be49-05215e3a682a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7848
        },
        {
            "id": "826ddb96-2ada-47c2-8f97-408537bfc949",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7850
        },
        {
            "id": "add5e34b-218c-4c4e-9a79-d214e64edc4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7852
        },
        {
            "id": "8dd5ef84-eb31-4d04-967c-f1d8bcb2c49b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7854
        },
        {
            "id": "df64a023-ee94-4e43-8b38-c197521c7229",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7856
        },
        {
            "id": "61100d8c-6440-4eb2-9cbe-a70756720136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7858
        },
        {
            "id": "1edde6ab-c9f4-4016-811d-f11381ffff2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7860
        },
        {
            "id": "b37af0c3-1e7e-437a-b90c-7672e8801ce3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7862
        },
        {
            "id": "0fe03cae-fdd4-4749-98d9-6cd0f15de3bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7865
        },
        {
            "id": "a807110a-ea87-4286-8557-8d760dbf8f77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7867
        },
        {
            "id": "acc6d2f5-0e4b-41a7-bf39-cdb48db16a35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7869
        },
        {
            "id": "037a5bce-13a5-4162-ab56-31a900ebd9b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7871
        },
        {
            "id": "22a93a74-82c5-4ad9-922d-6038f0beea00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7873
        },
        {
            "id": "45f088c4-706a-4c0a-ba25-0c033ccd1a16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7875
        },
        {
            "id": "1214a7c4-b23b-49a3-8f7b-4363d8a1c66d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7877
        },
        {
            "id": "b196f66d-aab6-49d7-8e59-e7e8b5027957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7879
        },
        {
            "id": "d6f28caa-c43b-418e-a8a5-f78b97fecf5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7885
        },
        {
            "id": "51efca67-921a-4233-ae3e-1267a65a84c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7887
        },
        {
            "id": "9ac5c104-5306-41f0-a2c5-90a0446bbbaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7889
        },
        {
            "id": "96e64647-e6f3-4bb8-9882-1ee11469c54e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7891
        },
        {
            "id": "4b6a9317-1452-4ba2-aa7e-827c5ddb9957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7893
        },
        {
            "id": "b29ffe8b-f0d5-4d74-a862-eae00d3920ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7895
        },
        {
            "id": "51afb788-d6c5-4720-9e6e-aa5bb6d199ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7897
        },
        {
            "id": "0c68434a-784f-4705-b448-12abdc52fa28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7899
        },
        {
            "id": "fa2b2793-3ac4-4c12-8182-018f5b9004de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7901
        },
        {
            "id": "1f0d05f9-4066-4767-91fd-de039f475ee6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7903
        },
        {
            "id": "63224993-e7ed-49e7-82b6-f23ff807a128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7905
        },
        {
            "id": "0cff235b-1946-4499-8178-dfcbb4451d4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7907
        },
        {
            "id": "add1e3bc-a883-4a17-844d-67438083be37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8216
        },
        {
            "id": "d2ce0eaf-0896-4e59-a6fa-7b28bb4710fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8217
        },
        {
            "id": "6251991a-e9e8-4138-9d01-8da34e6525cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 39
        },
        {
            "id": "d1efef08-b58d-4a0e-9055-a9d9e128a167",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 92
        },
        {
            "id": "a88c35ef-f19a-417c-b4a7-4452de619644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 162
        },
        {
            "id": "a8fda0de-27ae-46e4-a1cd-064249205f29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 163
        },
        {
            "id": "6f8e54df-eb5c-4513-9af4-7636d62361d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 8482
        },
        {
            "id": "a8340411-893b-4b85-869c-814557d20cbd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 65
        },
        {
            "id": "f6d2995c-e430-4fa1-b03e-65264b12484c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 74
        },
        {
            "id": "c06615d2-83b8-4474-8f51-77146567110c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 192
        },
        {
            "id": "36a266e9-6ebb-46e2-80e8-c360d189972c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 193
        },
        {
            "id": "e6cf6a9a-9e4c-4cf0-9de6-5d6a717d8fee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 194
        },
        {
            "id": "4c36c201-a12b-4bf6-9ac6-d45fe54a9fd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 195
        },
        {
            "id": "313964fc-637b-4c34-89c3-ff0ee90f660e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 196
        },
        {
            "id": "1fd2254f-5e53-4b13-807e-aa566b6d40ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 197
        },
        {
            "id": "d0659b26-c318-44b0-8346-a376c1394c92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 198
        },
        {
            "id": "76e8eee8-ff25-47cf-9596-5510d8a85efb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 236
        },
        {
            "id": "6c476001-dfec-4e94-be6d-51cea5cdf303",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 238
        },
        {
            "id": "633a895f-81f9-4980-969c-e223af4d13bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 239
        },
        {
            "id": "91a21053-c6b4-45bc-9bc1-3abe7e485295",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 256
        },
        {
            "id": "d38ff493-aa38-45e1-86f1-e67896678d0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 258
        },
        {
            "id": "fcfbda8b-bb91-4b98-8dca-06cc6fdd5574",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 260
        },
        {
            "id": "4f5a8103-0883-4049-8597-223612a86a25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 293
        },
        {
            "id": "b4cf487e-3af3-4519-9fa6-903c72c3cf07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 297
        },
        {
            "id": "8f818b38-8861-487f-9600-d4bc5f068e34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 299
        },
        {
            "id": "0849cc81-383c-4395-80ea-558e54dc2b29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 301
        },
        {
            "id": "19fa530d-1593-4ab1-ba3c-038fe01c3e86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 308
        },
        {
            "id": "df2d010c-e519-4297-a256-d36b4d458fa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 329
        },
        {
            "id": "1d345f94-d34b-4fd6-ba5f-078d60449e23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 506
        },
        {
            "id": "48c1e0a2-3a93-4df9-9d79-6ce39686859f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 728
        },
        {
            "id": "33ddf654-411a-475c-b06e-5f8517718f12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 730
        },
        {
            "id": "7396260a-4d20-425b-ad64-e2850aad6277",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7840
        },
        {
            "id": "9f32abd0-fc1e-49f1-903c-5d95daf6b30e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7842
        },
        {
            "id": "d81c45a3-426f-4466-837e-7af65f77323c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7844
        },
        {
            "id": "f94ebc90-54cd-4ecb-9a51-4313be306a53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7846
        },
        {
            "id": "04d49e11-696d-4872-b9b8-247098ab695c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7848
        },
        {
            "id": "01226926-ac8a-42d6-9273-e16c61b24edf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7850
        },
        {
            "id": "07361759-df85-4408-aeea-3d5634a469df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7852
        },
        {
            "id": "b23fdb78-9b86-4254-857a-a4c6755bb731",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7854
        },
        {
            "id": "eb332914-beb9-473c-877c-e1d58d2ea0e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7856
        },
        {
            "id": "492da7df-c9b4-41ce-88af-8d93cd7558c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7858
        },
        {
            "id": "9d107382-4ebf-4e07-9bc6-261773d5fbeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7860
        },
        {
            "id": "45a39da7-1f7f-4908-8cf5-b73519eab11f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7862
        },
        {
            "id": "aa77de2d-03b3-4195-8c7c-d5ceca168523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 728
        },
        {
            "id": "32bb0680-8a1e-46d8-8f5d-e2b65b53adcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 730
        },
        {
            "id": "ed4eac42-67ca-4692-993c-d40f539965b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 728
        },
        {
            "id": "789b1db5-e607-4169-bb53-fce7859a7d20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 730
        },
        {
            "id": "db9a138d-00af-428c-916e-cf32d17b27d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 728
        },
        {
            "id": "5586c4f4-9a85-4450-b7be-8361d9305f05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 730
        },
        {
            "id": "2e11223f-ccb6-45b2-9fbc-5ffedef6c966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 728
        },
        {
            "id": "981ed75a-a8b9-40cd-871a-ee57a9a04f02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 730
        },
        {
            "id": "17a76d38-e5aa-451b-9599-cb702bb899ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 125,
            "second": 728
        },
        {
            "id": "f6dc2cd3-c08d-412b-9921-60d53e6fb058",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 125,
            "second": 730
        },
        {
            "id": "38a7e76e-3d3c-46be-9604-1c6332c5c398",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 125,
            "second": 951
        },
        {
            "id": "34d763e8-c954-412e-9f50-3fcb30703283",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 125,
            "second": 952
        },
        {
            "id": "95e193c3-fa2e-45f9-981b-580f93ccc6c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 125,
            "second": 8212
        },
        {
            "id": "323e51b7-c933-42bb-b911-315eb1f19539",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 160,
            "second": 914
        },
        {
            "id": "3c71489d-166d-454c-9a60-716a3e860197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 160,
            "second": 915
        },
        {
            "id": "1e12f56f-75d1-4f17-8b87-50fe9b35bc76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 160,
            "second": 916
        },
        {
            "id": "fc764910-9aec-47e7-a6cd-a2ba13e47b00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 160,
            "second": 918
        },
        {
            "id": "27d7db04-3296-4775-8cfb-0c287ce9b135",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 160,
            "second": 919
        },
        {
            "id": "60f3f068-e4ff-4779-bab4-4358397a20d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 160,
            "second": 921
        },
        {
            "id": "6608512c-38d5-4332-9efe-8f7135d226a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 162,
            "second": 39
        },
        {
            "id": "d8308db5-856e-42a7-9b6e-e05519d7befb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 162,
            "second": 92
        },
        {
            "id": "772fea5b-a77b-45ef-8d9a-b3d7a989b388",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 162,
            "second": 162
        },
        {
            "id": "a696a181-9465-44a1-81e2-b261393ba224",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 162,
            "second": 163
        },
        {
            "id": "dae821b6-a564-419f-9948-a519fd4115dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 162,
            "second": 8482
        },
        {
            "id": "3ee035f2-e4d9-435e-98ee-ac083d374dc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 163,
            "second": 39
        },
        {
            "id": "4f986d58-cf9a-4dd2-9e9b-2a470c5fefe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 163,
            "second": 92
        },
        {
            "id": "7f37fa1f-5c7b-4a41-82ba-b02e3b822f55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 163,
            "second": 162
        },
        {
            "id": "4c52b98d-c008-44b8-b238-a62a6f8d8e66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 163,
            "second": 163
        },
        {
            "id": "567160e6-244c-438d-8b39-a9e705205f41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 163,
            "second": 8482
        },
        {
            "id": "3167360a-f7e3-45ac-a265-2d6071230c9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 36
        },
        {
            "id": "77b1a162-1754-4589-9999-91b642ffe7cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 52
        },
        {
            "id": "ab38bc1c-4507-43b7-add7-7ff46baabd64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 55
        },
        {
            "id": "3b88362a-0a76-4176-b196-e1289fb8239e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 179
        },
        {
            "id": "77831f65-8ed4-4bfe-a442-18ec67fb6148",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 186
        },
        {
            "id": "484e2416-8784-4776-972d-f6fafd574fa3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 188
        },
        {
            "id": "4a592fee-1b02-4d8a-9d2c-77b51815a88f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 247
        },
        {
            "id": "69c94977-d418-4e9a-a62d-de8799436227",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 778
        },
        {
            "id": "9c116b78-e237-483f-aa1e-e34ab3cd82e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 8310
        },
        {
            "id": "56237fb6-fc83-43bc-b928-ab5645d83537",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 8314
        },
        {
            "id": "5e3445aa-7642-46b7-a8c9-f9a14b0094e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 8322
        },
        {
            "id": "d4b4f642-f8e5-496f-8d19-d54adf1bac33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 8325
        },
        {
            "id": "f0b77fad-3d4c-4aa0-b9c5-9411ceb542d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 8328
        },
        {
            "id": "4952f98a-ee2f-4daf-8868-8cc7495193be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 8331
        },
        {
            "id": "b30cdc89-37ab-4930-ab56-257678b7d4d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 8470
        },
        {
            "id": "90c2cd53-c2e8-449a-92ea-ac7bdcf98575",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 8722
        },
        {
            "id": "09f682e4-c282-4cc7-9b0d-431aa74f3e22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 8804
        },
        {
            "id": "cb83d5aa-dee8-4b0f-9f73-dafb03ff3eee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 167,
            "second": 728
        },
        {
            "id": "ba16e1e1-03a3-4e5e-a88f-633240953ba1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 167,
            "second": 730
        },
        {
            "id": "7abe1651-cb35-4ab7-9a09-e08ddfb6e9cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 167,
            "second": 951
        },
        {
            "id": "fb8ca17f-322c-4189-8bd6-1eef3862c548",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 167,
            "second": 952
        },
        {
            "id": "57325a50-6540-43d6-bd1f-9cc7c50189b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 167,
            "second": 8212
        },
        {
            "id": "4aba6002-51a7-4379-b246-95ebeee7e2b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 35
        },
        {
            "id": "b595d45e-9d66-4f54-9764-92b5040b6561",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 36
        },
        {
            "id": "52faf567-add8-4fc8-8574-b9a78c4f8436",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 37
        },
        {
            "id": "14590def-b581-484a-88c5-79375fe38832",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 169,
            "second": 43
        },
        {
            "id": "5ffa64be-8072-4b51-8f85-ea15e68b97b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 48
        },
        {
            "id": "475685e7-eb99-428a-b381-33d8549bbdc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 49
        },
        {
            "id": "267e9f7d-ebae-481b-a080-549d11851c7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 50
        },
        {
            "id": "1dbe427e-1b3c-44d2-a2f0-3b10ce2438e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 51
        },
        {
            "id": "4f8c75a3-9d14-41e8-91ab-bdc396ddd43d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 52
        },
        {
            "id": "4b3359bb-0b5d-4eca-a4ba-173973c62317",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 53
        },
        {
            "id": "43c79a15-e2f8-4489-b169-feb8495125a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 54
        },
        {
            "id": "f2278e89-7f36-4c78-a920-5865b5e346b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 55
        },
        {
            "id": "c8703d71-b31f-47bb-bac5-af12318b9a62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 56
        },
        {
            "id": "7cf4f36d-19ab-45d1-994e-abaa57c9671f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 57
        },
        {
            "id": "44ba385d-4f7e-4979-aa08-ae31362bb385",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 58
        },
        {
            "id": "7f968527-6287-4c79-9746-500b89080f4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 60
        },
        {
            "id": "4a0f85d0-fabd-4783-b02a-d20a140c5aed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 61
        },
        {
            "id": "5ca46618-d619-47c2-9c3d-a5472c91a41b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 62
        },
        {
            "id": "afdd1400-571b-4826-ae11-20395c2ac8ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 170
        },
        {
            "id": "86d198ed-65f6-4342-89b6-90476fb907c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 177
        },
        {
            "id": "da56b02c-91bd-471a-8d68-3838a418054f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 179
        },
        {
            "id": "9d8f6c5e-114c-4d5f-916d-11530fae9390",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 185
        },
        {
            "id": "abe826d8-bdd4-432c-a5f9-900390054644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 186
        },
        {
            "id": "191c49e4-9d05-44be-b116-e083ad274f5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 188
        },
        {
            "id": "2fbb020c-1ec0-49cf-8d0d-d04b983bed89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 189
        },
        {
            "id": "3412737d-5214-4580-94ed-0cdf6052881d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 190
        },
        {
            "id": "2a0e5d8d-d135-413b-b617-8e127a339305",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 215
        },
        {
            "id": "3479deed-fbf6-4183-ab74-fe1a9c86cc13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 247
        },
        {
            "id": "3c7b318a-5a35-4360-b92b-1505f864776e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8260
        },
        {
            "id": "770af84d-bbb6-44bc-82b7-e99b3835afca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 8304
        },
        {
            "id": "c494a115-6546-4646-9361-aaae37100acf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 8308
        },
        {
            "id": "db272f2d-3e0b-4af0-b64c-a51cd61bedeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8310
        },
        {
            "id": "cdd6cc6f-496e-42bc-88a8-d35359cb96a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8311
        },
        {
            "id": "c330191d-62b8-46ad-97ce-5ff150d6d330",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8312
        },
        {
            "id": "247dcc89-ad8e-49db-a6b2-c0db0c8a1c0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8313
        },
        {
            "id": "22674e88-4f8f-47f9-8e0d-24fc500242ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8314
        },
        {
            "id": "3e15c9f1-d79e-4bed-b276-e078e0f4f416",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8315
        },
        {
            "id": "915deecc-0c2b-425e-bf7d-216889ee2f87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8316
        },
        {
            "id": "1c4b8a5e-828f-4872-a3f5-fe249395c35b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8317
        },
        {
            "id": "14385276-4e9f-4254-9dcb-8af92b3ef0ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8318
        },
        {
            "id": "ad3efa75-4d6c-4f82-b7e8-8a0a3ec18aea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8320
        },
        {
            "id": "8ce60c5b-198a-4cb5-99cb-dd568c1595f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8321
        },
        {
            "id": "f765ec44-944a-4780-a9e3-fb2fff780a27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8322
        },
        {
            "id": "a4dced54-bbbf-44bd-ba3b-61b57d449756",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8323
        },
        {
            "id": "e81af6da-5db5-4784-af29-d01c1b1a826c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8324
        },
        {
            "id": "d7a0d386-388c-4fa3-b7ae-c0380319f6e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8325
        },
        {
            "id": "fe7cb03f-42aa-41bc-b6a8-ecb04f208804",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8326
        },
        {
            "id": "18a99948-bc84-41bd-89d9-563f94d6c1e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8327
        },
        {
            "id": "25b4a8b3-a88f-430e-a373-9c4e8c6f2b98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8328
        },
        {
            "id": "53d58452-4a46-42e7-aece-1e8d860072c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8329
        },
        {
            "id": "f2363350-e7ba-4ec6-b970-1f6a6075f91a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8330
        },
        {
            "id": "5824ce9f-6a02-463a-9b1b-b16744acbc8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8331
        },
        {
            "id": "7d5bae8d-78c6-402c-a7e5-1f55622daf1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8332
        },
        {
            "id": "45c9c921-3762-4b6f-bb10-17e50dc46758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8333
        },
        {
            "id": "963083e2-54ab-4850-a6dc-ad1d3a57cdd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8334
        },
        {
            "id": "608d0a72-991e-422f-8cd8-4d23544053ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 8470
        },
        {
            "id": "4ed9936a-c4ca-49fd-af97-7500e62261cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8722
        },
        {
            "id": "4a4bc67b-4b15-43fe-8b5c-da3b0c123404",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8725
        },
        {
            "id": "54113362-1448-43f3-bd6e-cd3f460d13ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8776
        },
        {
            "id": "33c83538-aa43-4e52-9f90-e86a4e10d1a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8800
        },
        {
            "id": "b76143cd-9fa4-4e04-8f60-4c7a2289cd10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 8804
        },
        {
            "id": "d19cfb93-0e10-428d-94ed-515ecc5b3eab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8805
        },
        {
            "id": "2fb8fccd-fc60-45cf-bdc3-84b2dce693c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 39
        },
        {
            "id": "19ccb348-b599-4051-997c-755ea5c83af2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 84
        },
        {
            "id": "48d23d12-c6af-4873-81d2-2fdd27759bb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 89
        },
        {
            "id": "97f45af3-932b-47fb-9540-e2f9c896ebe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 92
        },
        {
            "id": "e6bc56e3-a387-429b-ad05-4cac1696ed0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 162
        },
        {
            "id": "5ad0dbb8-84f8-41f2-b87b-e21a2553e917",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 163
        },
        {
            "id": "ac51de7a-5d6d-45fc-b1f4-eaa7946dcb1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 221
        },
        {
            "id": "6ea24f47-d3fb-467c-a0a4-2cc97f4dba6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 354
        },
        {
            "id": "271f2b16-3b6e-4c91-9fae-bd49f3534482",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 356
        },
        {
            "id": "9ddfcaf6-b08f-41ca-b95f-e4a0012043e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 374
        },
        {
            "id": "c371daf6-bb2e-42d2-96a0-84eccfd3a917",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 376
        },
        {
            "id": "7984937f-7ba3-4b27-ad27-ca7b8539e19b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 538
        },
        {
            "id": "02fd88a3-61fa-44e6-8bd7-16badac4cd73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 7922
        },
        {
            "id": "edf13822-7615-43b9-95f8-b573b2716b86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 7924
        },
        {
            "id": "f2cec9d1-b363-4562-8be2-a83f11d739c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 7926
        },
        {
            "id": "aef925be-bb41-4ec9-8379-13d5c7ea31d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 7928
        },
        {
            "id": "27832ef3-9394-4512-9af9-406be8caf215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 8482
        },
        {
            "id": "22fc9300-906b-4e3a-99d9-8a14b0578302",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 35
        },
        {
            "id": "7ccf1e4d-0317-4a85-b586-4d135347c2a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 36
        },
        {
            "id": "a2aa08e8-11ae-4af6-98a8-e59d5f050e1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 37
        },
        {
            "id": "b6c4305e-9e8c-45be-9887-0791f0d9622d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 174,
            "second": 43
        },
        {
            "id": "119cb355-ddc3-425b-922b-c5419415a7e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 48
        },
        {
            "id": "c72af6e5-aa1a-4cc3-94c5-f9ab9d97453d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 49
        },
        {
            "id": "37389e65-413e-4c89-af62-af3e327979af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 50
        },
        {
            "id": "e9b5d120-e73b-4189-af5a-05e9b9ee4865",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 51
        },
        {
            "id": "f26960da-a48d-486a-be68-8572bfeaafac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 52
        },
        {
            "id": "49980ba3-bfc1-457b-99af-0733f1317e6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 53
        },
        {
            "id": "42a4597f-137d-4f94-a26d-2cb5b32acfee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 54
        },
        {
            "id": "786b4d8d-1aa0-4050-a2bf-99a7cbf28fdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 55
        },
        {
            "id": "85303408-180e-4b9e-9a26-32e6c7ba0443",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 56
        },
        {
            "id": "82d7863c-2230-414e-9ff7-9dd3d21d1a50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 57
        },
        {
            "id": "f4034c14-9eaf-422f-91ff-1ca0faa25edd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 58
        },
        {
            "id": "6856c486-0735-4a32-8953-66eb70b088b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 60
        },
        {
            "id": "fd42b767-707f-4ecd-9f15-8eecdf33f0ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 61
        },
        {
            "id": "207eb219-6f91-4003-8840-45efe4aa00a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 62
        },
        {
            "id": "cf2abb2b-d168-49f7-84a7-8a146eb8476c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 170
        },
        {
            "id": "7bc86cce-91e5-4b7a-8db4-e939d961b981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 177
        },
        {
            "id": "9f0cbb83-bd28-411a-a20c-2d9c6dcd5d13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 179
        },
        {
            "id": "b2282cce-ba09-4457-b490-083647d6341d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 185
        },
        {
            "id": "4dd28978-b38c-4ad0-aac1-7f47e657e5dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 186
        },
        {
            "id": "48b63f44-bdeb-42d3-b801-9cd14b3d6177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 188
        },
        {
            "id": "7e101cb2-aa3f-4d67-8e02-9b793287aaba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 189
        },
        {
            "id": "8fe983d6-c281-4970-b7c8-5cff8dd29c95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 190
        },
        {
            "id": "e282e7ab-bdbc-4a0f-9c43-e9ee32496bc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 215
        },
        {
            "id": "7e9c803e-031a-4a26-8975-926b53b3236b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 247
        },
        {
            "id": "882064c0-0c20-497c-a02d-a0bb84a2204e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8260
        },
        {
            "id": "a0fdd874-1a53-46b2-8ff5-896a36a2668f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 8304
        },
        {
            "id": "4d4612c5-cf39-4934-990c-20dc3aaea125",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 8308
        },
        {
            "id": "a03f234e-db7e-4cf8-8084-fd183e11dd6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8310
        },
        {
            "id": "7dd9da20-7165-45f2-81e6-3368f5951d27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8311
        },
        {
            "id": "ba40d9fe-fd74-4fdb-a231-8bed76af161b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8312
        },
        {
            "id": "7bd9bb1d-c6ac-4e53-b123-6b1afadb4850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8313
        },
        {
            "id": "c304b335-5b9b-416f-80ab-af8e57de3d13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8314
        },
        {
            "id": "161d3fb2-36c0-413d-8262-3e78e0aa59dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8315
        },
        {
            "id": "6d3dc95e-5d9c-495c-b9df-9b7b17cef6ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8316
        },
        {
            "id": "2b1aea23-435c-4632-92ad-5d4e477cdabc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8317
        },
        {
            "id": "38273af8-9908-41f6-80b4-9ef0d99bbe07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8318
        },
        {
            "id": "f8b3535b-290f-4f86-a63d-d7299ff7e7b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8320
        },
        {
            "id": "1e678c04-2a94-433c-b2c9-60fd1dcecdfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8321
        },
        {
            "id": "5568e1bf-e7f8-4ceb-aac9-4ab04940ec0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8322
        },
        {
            "id": "b5d38ac3-d040-46d8-bcda-d97b4516f244",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8323
        },
        {
            "id": "907deafe-a706-4124-b2e0-9fecd015f734",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8324
        },
        {
            "id": "06d3a3d9-084c-4768-87e7-bb9a3a45dfc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8325
        },
        {
            "id": "b5504a42-eea1-4294-88ce-46da92568c52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8326
        },
        {
            "id": "2c8aa783-1eac-478b-aa67-b25c0f8e880a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8327
        },
        {
            "id": "2a430092-7537-48af-ad9e-b08250733b53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8328
        },
        {
            "id": "0e49bf2e-4a9e-42d5-b7cf-8e55dfbab6ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8329
        },
        {
            "id": "379958ec-f888-46b3-96ae-0cf56b0c9514",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8330
        },
        {
            "id": "c16fa299-06e3-4766-90dc-8b7dc50974a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8331
        },
        {
            "id": "04a46c13-4f6d-46a6-bc33-00f295ea4a2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8332
        },
        {
            "id": "a720a202-633f-4ccf-85c2-7f6bf32d3a9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8333
        },
        {
            "id": "517c4c7e-0f6b-48f7-8b2b-e2e57d9f997f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8334
        },
        {
            "id": "3d5675d1-5136-4d12-8bad-0702397c14fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 8470
        },
        {
            "id": "7356d6d3-d35e-43b9-bf26-84df053a8325",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8722
        },
        {
            "id": "6b9b8ebd-a9c4-42ef-9ba8-fd5fee7bf573",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8725
        },
        {
            "id": "aafc818d-43f1-4647-96b1-0af8421c6ff9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8776
        },
        {
            "id": "e82f6535-d43d-4ac1-b6e3-f61445adf6c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8800
        },
        {
            "id": "5c89e641-540c-4a38-94e6-d36be4da7a23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 8804
        },
        {
            "id": "915ea128-160d-4d17-a8b0-7158555a15b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8805
        },
        {
            "id": "1868c6e8-9d13-4f05-9031-bedbf8a35c64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 184,
            "second": 36
        },
        {
            "id": "ac65a9fd-e18f-4f9b-aa71-72fb8366b0f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 65
        },
        {
            "id": "ffc661cc-87a2-462f-a6a5-dd7f73640bfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 192
        },
        {
            "id": "011bfbca-d87d-4373-b0ba-1e524da9e773",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 193
        },
        {
            "id": "8e0dc3bf-5056-44e2-9ed7-038bde78758c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 194
        },
        {
            "id": "e6c40f09-639c-4152-bd21-ded2e9452a66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 195
        },
        {
            "id": "e5cebda7-cbd8-430f-981f-1f442eab18d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 196
        },
        {
            "id": "df8e1bd2-b894-4a22-9bc5-00fa68bc033a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 197
        },
        {
            "id": "f054d3bf-f7fe-4549-af95-0d2f1122525e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 198
        },
        {
            "id": "9c304580-fd35-4ebb-8c5f-5c1c99d906fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 184,
            "second": 247
        },
        {
            "id": "1a060ac7-9bf4-4c32-b855-4b575aad3fd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 256
        },
        {
            "id": "9c67d72e-8d82-4a9e-b5c0-c6b517b58b3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 258
        },
        {
            "id": "4290f5d8-1de9-42c5-9173-a84ddc4de5fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 260
        },
        {
            "id": "b584d26b-9de1-477f-a8a7-649eefe4ae3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 506
        },
        {
            "id": "dc0ce16b-54b9-4e37-97a7-543663c2f71d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7840
        },
        {
            "id": "20589766-86b8-461c-be74-8183c9b34831",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7842
        },
        {
            "id": "061f3881-eae8-408b-a85d-c708a65d28a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7844
        },
        {
            "id": "3725814d-c117-438a-b97e-25b1b01e6270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7846
        },
        {
            "id": "73b74658-4a6c-43f2-927d-b0556e62b29e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7848
        },
        {
            "id": "3e3ee1cb-a2e1-4dee-bb21-e21706110da3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7850
        },
        {
            "id": "84d66b70-553d-4216-a14f-22a57efb8d14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7852
        },
        {
            "id": "6a591635-88cb-4afc-978b-2ca9911d6e6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7854
        },
        {
            "id": "ef73c9cc-d6b6-4b75-8e1b-161f88945142",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7856
        },
        {
            "id": "b1b5d315-be62-455b-94aa-cb212a121192",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7858
        },
        {
            "id": "baf14df0-daec-47e8-a7cd-d17fda30d163",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7860
        },
        {
            "id": "7c06ff44-84c9-4433-890e-b75390da16d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7862
        },
        {
            "id": "f5a1f1b7-28cc-4e61-852b-ea7dfd1d07ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 8212
        },
        {
            "id": "d33480be-fee1-4166-9d44-c44e275aadb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 184,
            "second": 8310
        },
        {
            "id": "4524ae10-1aff-4d59-8dc9-129f8b1226d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 184,
            "second": 8314
        },
        {
            "id": "1f1364fd-7def-433b-bec9-06fd03a253aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 184,
            "second": 8322
        },
        {
            "id": "fda0333d-590c-4265-95a3-e04150647692",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 184,
            "second": 8722
        },
        {
            "id": "9e52cfe7-cdbf-44a4-b37d-ef1baefed968",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 39
        },
        {
            "id": "df18170e-351e-4c08-9782-3d9c13a5d09a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 42
        },
        {
            "id": "5702b7cd-74b7-4041-94e9-1ded9ff59eff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 84
        },
        {
            "id": "7b527d90-6cd7-4243-983a-bfa3a509a7cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 86
        },
        {
            "id": "2e68c933-83b5-47d9-9e06-9eec97da053b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 87
        },
        {
            "id": "d0213616-7f4c-4142-a3a9-1124d56f2e1a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 89
        },
        {
            "id": "a7e5ae15-407c-4133-b962-e38bdd6c633b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 92
        },
        {
            "id": "dc88cdd1-1c66-4ee1-ac79-d08295c002b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 162
        },
        {
            "id": "406b5a5f-1091-4dec-8e05-e0968215a667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 163
        },
        {
            "id": "72b7c81d-8347-4959-b519-6c967b1976eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 221
        },
        {
            "id": "92bbbb09-fd7d-4c07-aae8-e93d1ab13a3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 354
        },
        {
            "id": "31e56e6a-22c2-4f28-9433-3731ab320730",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 356
        },
        {
            "id": "6a4ed660-a490-439e-9cac-0f45aa956e39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 358
        },
        {
            "id": "5a14f8e3-a862-4c42-8c57-641fcad6674e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 372
        },
        {
            "id": "17fe59c6-adb2-4b2d-b378-37679c5d7a2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 374
        },
        {
            "id": "61a875f9-d80f-409d-9bc2-51029e985fd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 376
        },
        {
            "id": "7b26921d-fd41-4973-bee5-5e157f6319bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 538
        },
        {
            "id": "166ca6b6-9acf-4b21-9ba8-afb961d3c422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 912
        },
        {
            "id": "f47523ac-9c83-45d3-9412-96b1a5888899",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 920
        },
        {
            "id": "6b344698-c8b3-462c-895b-a948e693d272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 970
        },
        {
            "id": "9b3c100c-711a-4eb4-93ca-d9349b86b0cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 7808
        },
        {
            "id": "26ea2583-97c4-4bbe-aefb-47ebbbf29bd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 7810
        },
        {
            "id": "52d078ed-b2dc-4a73-939c-ebd80429c506",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 7812
        },
        {
            "id": "a64ee426-5edf-46e3-9daa-5c19023b8cb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 7922
        },
        {
            "id": "f49111a8-e23a-43ad-b84e-74bc8f9e66a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 7924
        },
        {
            "id": "c6a40f95-c749-4a53-8f95-41843011adb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 7926
        },
        {
            "id": "f10efee3-43f6-4e64-bebe-51e2a00defdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 7928
        },
        {
            "id": "00cdbbab-6f72-43d1-a9b5-eae801fea862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 8482
        },
        {
            "id": "481fcffe-8c91-4236-99d5-dec964110dd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 84
        },
        {
            "id": "85caecb4-1a11-4c8a-9e07-89c43bb7d243",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 89
        },
        {
            "id": "9db6bb6b-da3e-4d81-a3d5-ce19a9783bc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 184
        },
        {
            "id": "42fdbda6-48f9-424a-9720-ea9a0443439b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 221
        },
        {
            "id": "0510653c-8b59-4084-b701-7ba4ebe2cdd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 354
        },
        {
            "id": "c072e7ec-df1d-4f40-b2f7-c0ba7415799e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 356
        },
        {
            "id": "d55700e8-421e-4094-a657-7b23b3ad08fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 374
        },
        {
            "id": "6fcd8a68-53c7-44d6-a560-6c8e40e07917",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 376
        },
        {
            "id": "375a8cc9-4d2a-47c7-a73f-3d68734b6d12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 538
        },
        {
            "id": "508bb712-cd5a-4b7e-8b42-9d2bd0fda9ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 731
        },
        {
            "id": "0b2a8e38-c417-4e3c-a0e3-30d153c6cd79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 775
        },
        {
            "id": "9c45536e-7436-441c-9d7c-8157bfbbd85c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 806
        },
        {
            "id": "4c610046-c1ad-4734-a101-49392a7001ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 7922
        },
        {
            "id": "cd070b7f-1f71-42b5-b579-bfe3d794c066",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 7924
        },
        {
            "id": "4c0eadda-b1c5-44b2-8d10-5d12353107a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 7926
        },
        {
            "id": "25d546cd-3f73-4862-9def-d3bbda0aa29d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 7928
        },
        {
            "id": "e4c24aa5-1e6f-42f4-8b74-6f861db8e6fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 84
        },
        {
            "id": "63955ffe-ff5d-40dd-935f-045c31c641a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 89
        },
        {
            "id": "d80abc18-ee6b-4606-ad01-9b27fffda4fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 184
        },
        {
            "id": "74812e8d-c704-4b38-b942-239e0d90f5d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 221
        },
        {
            "id": "bc50e2c2-e5af-4ab7-b3ca-0fb16b1d891d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 354
        },
        {
            "id": "187c355f-04c1-4d5e-91ed-72e7954cb7c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 356
        },
        {
            "id": "232f43c8-c585-4ae5-aef6-d260ca2d9e6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 374
        },
        {
            "id": "5e084392-64ce-462e-ad52-e659fe7f5fe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 376
        },
        {
            "id": "09329f3b-be89-4f25-b594-0fd9547bd150",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 538
        },
        {
            "id": "43a8baea-80d6-47f2-b977-c4d58008c357",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 731
        },
        {
            "id": "3062793c-84a3-46e3-b427-79880175cd6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 775
        },
        {
            "id": "38240422-cf50-43b2-a5ae-cd41f31f2e2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 806
        },
        {
            "id": "77069d6f-d374-406c-88ef-beea3015ee8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 7922
        },
        {
            "id": "bced6f58-2254-415d-bccd-67bbacfbff6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 7924
        },
        {
            "id": "9c7b063d-b7d9-433c-a14a-56e7d2f63430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 7926
        },
        {
            "id": "17d6588f-4801-4a05-969e-6e7b82783b9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 7928
        },
        {
            "id": "0d56ecf8-9344-4b86-a9ce-885fd0da1af2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 84
        },
        {
            "id": "5f31c5b4-5b0f-4f10-9b1f-9655d2c048fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 89
        },
        {
            "id": "f2fe81fb-577b-42c6-9b34-3a7e7f26f47b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 184
        },
        {
            "id": "c95ec485-347f-4593-9421-bf16ad8e0519",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 221
        },
        {
            "id": "d941742d-9f9d-40f5-903c-c831cf6a113d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 354
        },
        {
            "id": "2a308d73-0a5c-47d5-81f1-7714dba36981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 356
        },
        {
            "id": "73c68e55-b0d5-4abb-814c-0079b8fa1dd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 374
        },
        {
            "id": "404a1bf4-4584-4238-be72-4cf7b9ae7c67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 376
        },
        {
            "id": "3a027015-4b8f-4401-b02d-bc05c529d2fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 538
        },
        {
            "id": "09948cc5-bdd7-47d6-85b0-fe11c9ae600c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 731
        },
        {
            "id": "c577b211-db0d-4006-8301-6ee3fcb91542",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 775
        },
        {
            "id": "7a5c8581-cb92-449b-b023-4e65ddd5ea00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 806
        },
        {
            "id": "1b48d6b0-c988-4e71-ac5e-41ec79ea5d4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 7922
        },
        {
            "id": "ece07387-6b12-4853-b978-664bf51672e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 7924
        },
        {
            "id": "631c9f7f-4f7f-4137-abb0-188a4c715ad3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 7926
        },
        {
            "id": "ca356e26-a415-461a-b97b-91e7444168f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 7928
        },
        {
            "id": "eefb1d1d-c034-4fcc-9a0d-6c790953ac3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 84
        },
        {
            "id": "ea5a52e2-9c86-4871-b086-320e94b0c726",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 89
        },
        {
            "id": "745751ca-3976-4d44-a9dc-ba9923c7eb71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 184
        },
        {
            "id": "c589b64c-28fe-4bb6-b8ad-a2f00acd24ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 221
        },
        {
            "id": "a1ebd69d-0b7d-469e-8ba5-f85cbec1010b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 354
        },
        {
            "id": "2dea479b-a482-47ee-9946-fcee851a9b7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 356
        },
        {
            "id": "1c8c0196-1216-47e9-8499-1ac01723cb3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 374
        },
        {
            "id": "561c4b07-d046-43ee-a10c-f62be540e79c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 376
        },
        {
            "id": "90f70fc0-4966-442d-845e-6380ce8d1623",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 538
        },
        {
            "id": "f3039e36-5186-4885-989a-048733876fad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 731
        },
        {
            "id": "9cc0e5d5-3794-462a-9c01-c584f3753f33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 775
        },
        {
            "id": "65c7ea9a-bcf3-46c1-b037-59a45edcdc8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 806
        },
        {
            "id": "fcd0f955-d8eb-47a2-a18a-59d018cf3b2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 7922
        },
        {
            "id": "7163e825-7a83-48a9-8178-302be477c819",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 7924
        },
        {
            "id": "d25710ed-7e01-406d-b353-496fb31b3ca3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 7926
        },
        {
            "id": "fe789cd9-ab49-4145-9935-c971e563d928",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 7928
        },
        {
            "id": "c0b401b0-09fb-4ede-bb6d-f034ee6955e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 84
        },
        {
            "id": "0f436b6a-6601-41f5-9c0d-b5c778e17618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 89
        },
        {
            "id": "a504a84b-b756-4c29-a25f-39e4b50f7452",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 184
        },
        {
            "id": "8f00d4c9-f236-426c-88fd-b1b08913966a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 221
        },
        {
            "id": "a0999eda-dbab-4ff1-aee5-a18946af8343",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 354
        },
        {
            "id": "d9e841de-1b7a-4072-b47c-fb08adee5f18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 356
        },
        {
            "id": "59f45966-ec19-4b34-ab46-c8c93f3657fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 374
        },
        {
            "id": "f386e568-43df-43b6-a783-34c22a74fb51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 376
        },
        {
            "id": "853462a7-e2de-4098-9d2b-ea50f3972efa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 538
        },
        {
            "id": "c132cd6f-550a-4867-9bd3-ecf699c89af6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 731
        },
        {
            "id": "3f385375-f640-4712-bf41-de055ca0c06c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 775
        },
        {
            "id": "a404fb8b-65c6-4213-9768-d86971caefb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 806
        },
        {
            "id": "501131a6-77c5-4e26-a366-2b2a0b20502e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 7922
        },
        {
            "id": "23b16596-242d-4d89-99d2-dde88003cd5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 7924
        },
        {
            "id": "eac41381-06f4-4747-90ef-170536bae07d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 7926
        },
        {
            "id": "2c6bbe1d-62d6-4cc7-aa9b-4545eedef3b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 7928
        },
        {
            "id": "568d6634-61d2-4a9f-91dd-3d2c854394c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 84
        },
        {
            "id": "ead873bf-548a-4aaa-9c3e-b815fd210372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 89
        },
        {
            "id": "6dcbe8e9-ded0-4601-8e3a-e950d4fd9c4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 184
        },
        {
            "id": "7e5ba85d-9dd2-448b-84b7-6808d2d93df1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 221
        },
        {
            "id": "574ae8b9-ed57-421b-80c6-eb2e6e798c61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 354
        },
        {
            "id": "f9a8d92c-9709-4416-810f-70ccf48f7486",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 356
        },
        {
            "id": "55571006-0c8e-40e2-a43a-a371ffc5e12f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 374
        },
        {
            "id": "17a1885b-19a4-4db8-ab0b-9bde82408965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 376
        },
        {
            "id": "48b08908-7518-4465-b60e-5a882334956f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 538
        },
        {
            "id": "f8e00e7d-1836-407f-8e17-b3325741c1b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 731
        },
        {
            "id": "d07db003-5979-4c16-ae3b-00ff81e29bee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 775
        },
        {
            "id": "36364097-f993-4ac0-a827-b0d05e30eed4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 806
        },
        {
            "id": "50233b54-facd-4da3-b01c-e80d41c08f43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 7922
        },
        {
            "id": "6fc15a7f-1fac-46f6-8e8e-e7f9f9cd357e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 7924
        },
        {
            "id": "2699071a-85c7-4e6d-9e61-bd9c549d58d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 7926
        },
        {
            "id": "ad8caf03-c0bd-45b5-a169-d4377bedf1b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 7928
        },
        {
            "id": "60c99109-914b-4896-ab96-b660dbcd4b11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 65
        },
        {
            "id": "9533c426-75eb-4a04-80cc-c70e55873066",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 74
        },
        {
            "id": "2ed8e6bd-3203-4c5d-ac5b-25b49ebae644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 99
        },
        {
            "id": "4d8d769d-15fa-476b-873f-7e9b1150e733",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 100
        },
        {
            "id": "91144b02-dd91-4122-a060-5a7c7fa8fbe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 101
        },
        {
            "id": "0397e557-2515-455f-81f2-78565e0d5567",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 103
        },
        {
            "id": "50f8cde2-3526-4f8d-820d-32d4e37a770b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 111
        },
        {
            "id": "a61807a8-ceea-4467-ba71-2841fb2a4797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 113
        },
        {
            "id": "81d94330-0a37-4b11-805f-ca29cac0a796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 115
        },
        {
            "id": "92d5c53d-98fe-48fe-b7a4-14af00884f2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 191
        },
        {
            "id": "1f7f1861-73ef-4ea1-874d-7d8306d09bda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 192
        },
        {
            "id": "afc61716-d713-4db4-bb37-ea3df150fbce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 193
        },
        {
            "id": "954a61b7-701b-44a6-9f0d-ce429fde93ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 194
        },
        {
            "id": "95d719c0-75ed-41eb-8942-da25ca92716d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 195
        },
        {
            "id": "b4eb135d-2395-4638-81a3-e7b0cff0c6bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 196
        },
        {
            "id": "25d31200-586d-4973-9d6c-e66928760a02",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 197
        },
        {
            "id": "69199e3a-1763-49eb-8c7a-12e9b243c794",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 198
        },
        {
            "id": "7f860bc2-c435-4fa0-be98-f29eb657bbaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 231
        },
        {
            "id": "35ab4f30-ae88-4ba7-9553-2fd26ed712a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 232
        },
        {
            "id": "ed158708-ef4c-4ebe-94eb-752c7ef70ce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 233
        },
        {
            "id": "6c852993-af16-4339-bc9e-83dc47d10288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 234
        },
        {
            "id": "6a8a5cb3-9f01-4c40-b5cc-5ec5cf82a8e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 235
        },
        {
            "id": "4f584f91-04fd-4236-b02b-4d1680abf370",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 221,
            "second": 236
        },
        {
            "id": "173e7bd9-4821-43e8-956b-250806a1b3c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 221,
            "second": 238
        },
        {
            "id": "00b4a156-dad0-4081-9383-0f5b5c017b53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 221,
            "second": 239
        },
        {
            "id": "10938f4a-985f-443d-b6bf-b006a8faca84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 242
        },
        {
            "id": "344b33e6-edcd-4229-a281-89998872edfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 243
        },
        {
            "id": "2bd41ea3-c87e-4aa7-9d84-396c7fb52492",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 244
        },
        {
            "id": "7534374f-03ca-4172-8f46-8883a54b4636",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 245
        },
        {
            "id": "aae871d7-4a04-4e2e-a225-c3da84e5754f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 246
        },
        {
            "id": "dbb93e76-7cce-4bc7-bcff-be846c3da0b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 248
        },
        {
            "id": "658922b4-73fc-4ceb-b2c9-b33935db18d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 256
        },
        {
            "id": "1d390158-3086-4e61-9513-afb9d612af26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 258
        },
        {
            "id": "b270971e-61f4-44cd-a88d-1489fb09fa4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 260
        },
        {
            "id": "05668834-7695-4f5c-b2f9-1063cde876dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 263
        },
        {
            "id": "79eb63bf-4a6d-41fe-9d2a-3a95e65cea04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 265
        },
        {
            "id": "df5f9640-c258-4a13-bf29-cf4e52b6852c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 267
        },
        {
            "id": "76f65edd-1944-4c36-addb-44ffbf9a8957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 269
        },
        {
            "id": "8f042cd3-a948-4b4d-8947-e59ef7729dc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 271
        },
        {
            "id": "ece4ef2e-c733-42ae-8833-c88cef137843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 273
        },
        {
            "id": "dfd13c9b-0ee7-4802-b09d-14e0121b2172",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 275
        },
        {
            "id": "bdee4e22-800b-459c-af23-a02a3301fa5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 277
        },
        {
            "id": "00072aef-0a0e-4990-8965-eca8a5005f28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 279
        },
        {
            "id": "f8d97cb3-dbfe-4429-aaa2-9d1f2e370134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 281
        },
        {
            "id": "7da3f5b5-df7a-4f1a-bc7b-c054d245986c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 283
        },
        {
            "id": "7cc83561-284f-448b-a0b8-b768204781a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 285
        },
        {
            "id": "cd31e093-2683-4629-9201-f553034b9ff6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 287
        },
        {
            "id": "b74b08a5-d2b0-4dc0-826e-f11cb147d6e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 289
        },
        {
            "id": "c7eb9a6d-9895-4b69-ade4-67323481258a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 291
        },
        {
            "id": "09293238-9650-4ddf-895e-157dd07bc990",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 221,
            "second": 297
        },
        {
            "id": "bedae6fa-6859-482d-8444-04c4ec98012c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 221,
            "second": 299
        },
        {
            "id": "6a3170b2-b3bf-46ba-9c6d-745f1422d8d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 221,
            "second": 301
        },
        {
            "id": "5ca53287-286b-443e-8c07-d5e4814c1e98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 308
        },
        {
            "id": "aac7d9cd-255c-4564-a6be-8ae93b680e86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 333
        },
        {
            "id": "79333c67-ba8c-4bdc-9586-d5891d288f05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 335
        },
        {
            "id": "94b11e5e-670a-40b3-b401-3f188025d797",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 337
        },
        {
            "id": "af3809b8-586f-402a-bf94-b54037b13b28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 339
        },
        {
            "id": "0fcb8242-2bf6-4034-9b64-513cc7238e9e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 347
        },
        {
            "id": "71ac455b-9e9e-4ea7-9e73-e4074b294308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 349
        },
        {
            "id": "fbc2fbe1-5659-417a-b1db-207c5369d3db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 351
        },
        {
            "id": "ae21fe79-32af-4a3f-8a37-73b5f402a4ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 353
        },
        {
            "id": "cd17b704-8b66-4531-813f-a6bd9bf90a33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 417
        },
        {
            "id": "e6cd616d-0365-4b67-bf50-a13e4e0e10ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 506
        },
        {
            "id": "17e3255e-cbb0-401f-9dde-0db390006318",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 511
        },
        {
            "id": "c3dd2c41-94a3-48cc-b56c-be6d56de018d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 537
        },
        {
            "id": "e0f6d014-1329-4b4d-ab36-4d20b66fe3c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 221,
            "second": 728
        },
        {
            "id": "1b181b24-ae00-440f-91e1-c52bcc8b5036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 221,
            "second": 730
        },
        {
            "id": "5952ba37-4715-4fa8-87e6-321a4168aaae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 774
        },
        {
            "id": "ee57ad5e-52d0-43e8-8834-966e8a511073",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 778
        },
        {
            "id": "dbc49da7-7de1-4f29-acbf-be86d645c13d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7840
        },
        {
            "id": "b92f02c0-a862-4974-a50a-6143b9fd3134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7842
        },
        {
            "id": "748556ad-6656-4b7e-aec8-60799fcecc3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7844
        },
        {
            "id": "49367be5-6db3-4a9a-8d27-8f23a2648b54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7846
        },
        {
            "id": "ae5683b9-64d9-4f76-ad55-cd58fd0561ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7848
        },
        {
            "id": "5d89901e-c017-4cda-a3df-604a62516cf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7850
        },
        {
            "id": "89a0b108-5faf-4139-8fb4-eecea719419a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7852
        },
        {
            "id": "a094588e-e814-4b3f-9171-abd195277911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7854
        },
        {
            "id": "738dd1bf-e58e-49c2-83c1-0d2790f64b22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7856
        },
        {
            "id": "14479efd-23d3-4984-b8ec-a22e9af0955c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7858
        },
        {
            "id": "0afbf299-91a3-470b-9af3-ed70c7bce379",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7860
        },
        {
            "id": "08901335-150f-4578-bc9e-227a4a412aea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7862
        },
        {
            "id": "c6db496c-bdce-45ab-9843-aaaabc5e50d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7865
        },
        {
            "id": "dcb21f37-20a2-4932-8408-b6277b3ed9b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7867
        },
        {
            "id": "5f21f11f-4251-4a98-b814-cda92d273ab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7869
        },
        {
            "id": "ff9a9db5-5d27-436d-8a2c-60ab3be7e43f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7871
        },
        {
            "id": "f6c17687-afcb-4a66-91e8-eb77d0cc5e8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7873
        },
        {
            "id": "2b9b7c80-b3da-4265-ab9e-a1d08fd00a73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7875
        },
        {
            "id": "f0e40165-3a0d-45a7-bc0d-9852995fa83b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7877
        },
        {
            "id": "4055888e-a3c2-4490-b0ab-c1962fccdcd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7879
        },
        {
            "id": "6b7cf195-875b-4ca0-a543-863abcaff73b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7885
        },
        {
            "id": "d2f7cde4-b2a0-47a3-9fac-6a5b047126e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7887
        },
        {
            "id": "40be3102-c9aa-4f4e-9fa4-455d2db6643c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7889
        },
        {
            "id": "ef1e9d2c-ff2b-42f4-b394-3cee7fc02c60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7891
        },
        {
            "id": "74923ebd-0521-4ad6-a9a5-f636e9873038",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7893
        },
        {
            "id": "ce852708-0a8d-4b7d-ad57-f433422ec0c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7895
        },
        {
            "id": "dc96363f-45ba-4814-ac0a-099a32b2bd97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7897
        },
        {
            "id": "c637ab5b-6de5-4a48-acee-8cc1405f08dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7899
        },
        {
            "id": "f4a64d77-892a-4134-8321-61a59d1361bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7901
        },
        {
            "id": "6cc9de1d-8c8d-4e23-81e3-bc5981f08c5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7903
        },
        {
            "id": "a841b961-da01-4b50-8a32-abc4557ceda2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7905
        },
        {
            "id": "0b1f6778-a074-4783-b3e4-928cfd35ac66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7907
        },
        {
            "id": "63a99772-8245-4d69-a4b5-289e888bbe4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 8216
        },
        {
            "id": "05480e38-ecc9-4bce-8779-57381e2a5257",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 8217
        },
        {
            "id": "4fa312a6-bbca-48d5-a41b-08b724419599",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 65
        },
        {
            "id": "cb957687-ddc7-492c-8d04-6694d37eb5e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 74
        },
        {
            "id": "2867c565-0599-4b03-aee3-0a9ca410dd2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 192
        },
        {
            "id": "4c3bb2bf-d3d7-4d12-8b8f-e272304d6e8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 193
        },
        {
            "id": "6387cd75-782c-4ede-8fee-fc667f357349",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 194
        },
        {
            "id": "50365b38-f0dc-4a00-9688-5ca335d3df1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 195
        },
        {
            "id": "b3dedef1-ce97-48cb-a171-b95f4f2f5e41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 196
        },
        {
            "id": "c9105cee-0407-429e-b821-a000c12d68f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 197
        },
        {
            "id": "d5db8c77-cece-4465-ba3b-eb661794d4aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 198
        },
        {
            "id": "fda163d4-1824-403d-93c4-b490398acb19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 256
        },
        {
            "id": "e001ae99-e9a4-4f0c-a8db-29e4e535f740",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 258
        },
        {
            "id": "0c10ebc0-9d7b-4e7e-b9af-070c9908de56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 260
        },
        {
            "id": "56ed1cf7-ef5f-4eae-961f-f5f272658ff2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 308
        },
        {
            "id": "3490e948-354f-4382-880c-2608fb825779",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 506
        },
        {
            "id": "5e6b94ed-46b8-43f7-a228-c2989ca95005",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 222,
            "second": 728
        },
        {
            "id": "bf87c4cf-9d4b-41ed-81c2-0f19ccab6030",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 222,
            "second": 730
        },
        {
            "id": "4318f970-8301-4d83-b399-f3480cc5f77e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7840
        },
        {
            "id": "6ce6b254-3eaf-414c-98ec-9ef490ab9f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7842
        },
        {
            "id": "f07a70a1-70c3-4b02-b98a-24a548224dda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7844
        },
        {
            "id": "59a72d81-ed66-4e98-8019-05752853479b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7846
        },
        {
            "id": "8e6c477a-80d7-403d-b30d-42510529c890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7848
        },
        {
            "id": "eaa4d92a-670e-4432-abb2-721b551b7d8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7850
        },
        {
            "id": "44f1fdad-f6e6-4397-81fe-e1b4a4011cc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7852
        },
        {
            "id": "e5585bdf-4ade-4be7-b6d5-1efe10083934",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7854
        },
        {
            "id": "b488aa33-e7b1-4195-a157-a422fb7c4921",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7856
        },
        {
            "id": "8c6ad1b6-8b00-4f12-92a7-7d07820266e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7858
        },
        {
            "id": "a7d8cc11-5a81-4ade-9169-b3be10cd85ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7860
        },
        {
            "id": "5f35245e-b106-44f3-975e-248c1e7e5c3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7862
        },
        {
            "id": "f215568a-4f6a-4327-ac57-55e10767dd08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 253,
            "second": 728
        },
        {
            "id": "940deeee-d256-4cff-ad78-3816a292e2ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 253,
            "second": 730
        },
        {
            "id": "a4c9a6a8-45c2-410d-90fc-13a58c4d2608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 255,
            "second": 728
        },
        {
            "id": "6d8e1518-5d51-4e69-be35-09d187ce034e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 255,
            "second": 730
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 20,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}