{
    "id": "80025924-2a81-4b72-89c5-e801c8a1fae0",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_text",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Candara",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "f702d5d2-5b88-4486-8f31-631e8b600e74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 36,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "00acc987-1cb2-411c-b6f1-3cc778588da3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 36,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 295,
                "y": 116
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "9fbe7cee-3551-4e07-80b6-e787ab470d1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 36,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 302,
                "y": 116
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "b711a431-5d66-443a-b9dd-90b3836c5bec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 36,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 313,
                "y": 116
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "a6e900e0-9693-472c-b31e-63b80b6a6d7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 36,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 330,
                "y": 116
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "8c3a0b5f-aae7-4463-a498-f97e1a33bb6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 36,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 343,
                "y": 116
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "775ec984-d7a8-4a0f-a560-f0d3a7f97276",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 359,
                "y": 116
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "f30d80ce-2b13-4029-a841-7226674566fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 36,
                "offset": 2,
                "shift": 7,
                "w": 4,
                "x": 380,
                "y": 116
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b405ac63-c36b-4596-85bd-272c6be09ffe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 36,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 386,
                "y": 116
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "9e1d038b-c717-48f6-9371-3c00b3fe7f19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 36,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 397,
                "y": 116
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "5dcc7013-3187-47dd-81e5-5b560a3f93c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 424,
                "y": 116
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "41b47ba0-8d62-4613-b18b-2f981ef3afc2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 47,
                "y": 154
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "d946de87-5a1c-4296-b70b-fecf36f8c1b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 36,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 439,
                "y": 116
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "6d42734d-1bfa-41c0-aa14-8d9b33c39dd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 36,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 447,
                "y": 116
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "805cba09-fed3-4072-a204-122dd189962e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 36,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 456,
                "y": 116
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "fcf99395-7a82-4444-ae1c-e6e022075700",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 36,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 463,
                "y": 116
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "c73cc6e6-78f9-4007-b965-b76f9a75e2a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 473,
                "y": 116
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "fd117bdb-0d6b-4234-8fd5-4e0587bdd9a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 36,
                "offset": 0,
                "shift": 10,
                "w": 8,
                "x": 489,
                "y": 116
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "67777c50-0aac-413d-8461-24ff10a6044e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 36,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 2,
                "y": 154
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "a13d16fc-b797-4f8d-93f7-ed6bf7ce57ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 36,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 15,
                "y": 154
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "457041de-296e-4433-b9b5-74ad02c2b821",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 36,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 30,
                "y": 154
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "bd18638e-5769-49b7-baaf-0366220feb2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 281,
                "y": 116
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "47ecf638-3c1b-4530-adc1-f2342dbb44cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 408,
                "y": 116
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "967aef58-ed41-42f9-9ae1-4c730f8bf2cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 36,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 266,
                "y": 116
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "51dd72e3-3bae-4bbb-b753-efd135e86301",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 63,
                "y": 116
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "de8a22e4-f170-46e1-b570-8005d4def4a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 433,
                "y": 78
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f831d601-6cb0-44c0-be53-c0857291f5da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 36,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 449,
                "y": 78
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "f4e0ea63-a9fb-4270-97e2-156eafd21968",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 36,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 456,
                "y": 78
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "4cf22476-893f-415b-b95e-a3db88cb9b57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 464,
                "y": 78
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "f934d315-ba92-43e1-9eb6-66d638403647",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 479,
                "y": 78
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "1885eb67-6501-49e5-b8f7-a867f481f1b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 494,
                "y": 78
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "69950b2d-2ecc-416f-8a3b-13d0e59dc6e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 36,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 116
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "e597f2dc-131a-4f12-9eb5-cde7c330ae38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 36,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 14,
                "y": 116
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "37371ab3-3836-4c33-ae64-89d712d8791f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 43,
                "y": 116
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "96da8956-6fa0-41d4-8cb1-cc4a01fd7d9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 36,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 79,
                "y": 116
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "a1ed43ca-390b-421d-9fb5-5c65a9d35c5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 226,
                "y": 116
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "02f6e76c-febe-497a-a5d5-2d6a37fe6237",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 36,
                "offset": 2,
                "shift": 19,
                "w": 16,
                "x": 95,
                "y": 116
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "5016d672-5e0e-4599-8001-39c5fafc772d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 36,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 113,
                "y": 116
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "d15ec87f-3855-427f-b26f-7c828d96d9e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 36,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 128,
                "y": 116
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "6523baf4-65d8-4e64-9e64-e663669c81ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 36,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 142,
                "y": 116
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "a6c3c0c0-fe33-4cb8-9c5a-baaf5825b2a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 36,
                "offset": 2,
                "shift": 19,
                "w": 15,
                "x": 160,
                "y": 116
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "c527598f-279e-41d3-af51-23db49142f5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 36,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 177,
                "y": 116
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "04c5376a-03c8-457f-8043-589714a5956d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 36,
                "offset": 0,
                "shift": 12,
                "w": 10,
                "x": 183,
                "y": 116
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "159d621b-f5df-4170-93fa-6ca5aff2abba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 36,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 195,
                "y": 116
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "31c1a4c2-e6c2-4a31-8204-53f6a94cd9a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 36,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 212,
                "y": 116
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "7d871c29-6f28-4885-8599-f91ce6a32910",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 36,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 243,
                "y": 116
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "45ca3fb0-e1f9-40a7-ab91-cdef4569c175",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 15,
                "x": 62,
                "y": 154
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "d2ed8c15-0646-4cc8-b649-694f322f9852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 79,
                "y": 154
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "81f537e5-5e00-4a81-9b07-0eb1299cc478",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 36,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 99,
                "y": 154
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "9a9340da-393c-4666-8cdd-b8c0056d7800",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 19,
                "x": 446,
                "y": 154
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "f4ed0a70-2439-4d3a-b205-4cb83f048ca4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 36,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 467,
                "y": 154
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "fb82be6e-43bc-4d83-adf5-5785339db1d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 484,
                "y": 154
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "b1c888b5-e9c7-43b2-8dce-48581cb2d0c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 36,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 2,
                "y": 192
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "003d1d01-9729-4188-a13d-635bc67e19ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 19,
                "y": 192
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0422e9c7-29cb-45ea-ba80-851f447f84ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 37,
                "y": 192
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "4b96a475-9e27-456b-bb33-9b09b6192c7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 36,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 55,
                "y": 192
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "333eb36f-f2f5-4e3b-8a54-2dfb2784f0bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 83,
                "y": 192
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "1b5e3f69-a72b-4fce-990d-1487ae20594b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 102,
                "y": 192
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "3dd6e326-d4d2-4e72-a7dd-8a21133ac109",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 120,
                "y": 192
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "0806fc46-43b3-4885-88b7-4968a3d9fb02",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 36,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 135,
                "y": 192
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "0f8448d3-9699-4062-9745-34d2dd0e4bcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 36,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 145,
                "y": 192
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "de91548a-2b36-4b5a-9e57-63b8c91515b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 36,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 155,
                "y": 192
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "aa388f24-0fc6-428b-bc50-c0f7b04031af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 36,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 165,
                "y": 192
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a9fe3298-c1c0-4ddf-945e-29220ee3229b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 36,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 182,
                "y": 192
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "1116f5cf-4d68-4fb6-a04e-56bc9cb332c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 36,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 199,
                "y": 192
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "009ba369-429f-4440-aa17-e36e55160561",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 207,
                "y": 192
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "d08094b5-97e7-4947-9fa1-ba1167165655",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 221,
                "y": 192
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "fc5f30a2-b0cf-4c63-9e5a-e633df9b6ab0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 36,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 237,
                "y": 192
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "1a1389b7-413e-4176-9159-9911922c39d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 251,
                "y": 192
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "29c49a84-cec3-4e74-9513-d63308a69e0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 267,
                "y": 192
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "47d397f5-13ff-4524-b05d-736add4623f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 36,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 434,
                "y": 154
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "14b424aa-0693-4de8-97e0-7e0d098b3965",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 416,
                "y": 154
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "846265b6-0a22-4494-8a7f-937ff634fd87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 401,
                "y": 154
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "2560acf8-640b-4441-a0ac-91c681266de8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 242,
                "y": 154
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "28a7dfdc-07a6-458a-a3e2-ec92c90d8595",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 36,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 115,
                "y": 154
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "5985759c-eb03-41ef-95a5-82caa6940d53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 123,
                "y": 154
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "61e4ec44-fd71-4157-8f10-fc6edf7e28df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 36,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 138,
                "y": 154
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "233a4fba-6780-439e-9eb4-7e0b7b10ee94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 36,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 144,
                "y": 154
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "7b52cdaf-d5c2-40e2-9d64-fafed9515c20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 167,
                "y": 154
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "4ac54a17-d73d-425d-a1eb-c4ddf6714719",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 182,
                "y": 154
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "1fb4bcdc-2c59-4560-b3e9-85bf6baf2db3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 198,
                "y": 154
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "740770d2-e812-4232-be93-9e88e1fbec6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 214,
                "y": 154
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "bbbf5e81-9dda-4363-a1ce-306dfc75bc8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 36,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 230,
                "y": 154
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "314124ed-c146-464c-b8a4-4400679e1a10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 36,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 249,
                "y": 154
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "6ddfc841-490a-4ca6-9dc9-db2907ffb517",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 36,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 389,
                "y": 154
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "10396b39-4bfe-46fc-9244-bb341d2e54db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 261,
                "y": 154
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0f9e1727-e3b0-4bb0-85da-c0c4c60891a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 36,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 276,
                "y": 154
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "fa9c5b7c-c57d-4921-a964-39799a3d05ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 36,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 292,
                "y": 154
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "c17a16e3-f10b-4f6e-a40e-46ec34074ef4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 36,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 316,
                "y": 154
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "799a05f4-4f39-4721-ae27-a76707aa567c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 332,
                "y": 154
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "2f0ab8a1-23a2-48ff-84bb-a820e99ee08b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 36,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 347,
                "y": 154
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d171457f-24be-4935-a3ff-53589136ecaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 36,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 360,
                "y": 154
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "270176e0-2f2d-46b0-9419-2055898bbb13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 36,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 372,
                "y": 154
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e5683657-ef18-48ad-9928-f0f7a3cee6b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 36,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 377,
                "y": 154
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f0ae8690-39f9-4c0d-9216-152d6ebbb3d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 36,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 416,
                "y": 78
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "2b16c704-510a-4b6f-84a8-c41f2b960ee4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 36,
                "offset": 0,
                "shift": 6,
                "w": 0,
                "x": 414,
                "y": 78
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "a1d14eb0-6d44-4fa5-80ba-cd255cb81096",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 36,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 407,
                "y": 78
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "1a51a5f2-a95a-496b-85ca-800991c15279",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 144,
                "y": 40
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "e7276212-1ff4-4322-9b49-00a15ad892d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 392,
                "y": 2
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "ab8628e1-dd63-491f-97c5-ee8fef31b366",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 406,
                "y": 2
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "91683ea5-09ee-4f15-95ec-dd219a7d368d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 420,
                "y": 2
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "16fbdaf0-2cef-4666-9875-71ee5d1b6954",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 36,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 435,
                "y": 2
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "31e961bf-a8d8-4801-bba7-82a3c0a5b486",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 36,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 440,
                "y": 2
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "8f557292-4a7a-43e6-81b9-cda016783451",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 36,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 452,
                "y": 2
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "902e043f-c569-4c57-bcda-4fded179a589",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 36,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 464,
                "y": 2
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "3514cbf5-7004-4999-939d-3dbd8d33b011",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 36,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 484,
                "y": 2
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "debf3755-2806-4475-97c5-cc7f0029d9b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 494,
                "y": 2
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "d5103b81-7af9-4563-bcf6-2f92bc899194",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 9,
                "y": 40
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "3baba8e1-e6c4-4989-bb52-5348d64f952a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 36,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 135,
                "y": 40
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "da4636ab-208b-45f8-a0b1-d08f933a2408",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 36,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 24,
                "y": 40
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "658371ae-851e-4b38-ad5e-4ba2256b5aa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 36,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 44,
                "y": 40
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "011d5bec-a5f4-4fe4-81ba-44cb1209bc50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 36,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 53,
                "y": 40
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "c0a28af6-0395-4f89-b4ca-3c91ed2f8ba5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 63,
                "y": 40
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "37f6282c-c64b-4012-a31e-4d9330b61c82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 36,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 78,
                "y": 40
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "a217bd90-e61c-486e-8118-f89c00aa423a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 36,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 87,
                "y": 40
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "cca91d5b-0f4b-4d29-b9ae-c574bdda08f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 36,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 96,
                "y": 40
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "1f772be6-27fc-4d27-a6d0-b6537253d5dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 104,
                "y": 40
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "238ca80b-96c1-4702-a717-f3368d4acdea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 36,
                "offset": 2,
                "shift": 19,
                "w": 14,
                "x": 119,
                "y": 40
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "3cad0eaa-7957-4295-b855-61e137480151",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 36,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 385,
                "y": 2
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "f95045dc-7bc8-4a13-bb7f-7506102947ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 36,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "ccb5dedd-1b93-4c93-bd8b-f54d83be0b42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 36,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 377,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "bf87eee5-5021-4ba4-9067-963853d8b79f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 36,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 182,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "262f5e17-ebc4-4ec5-842b-9b37197cfba8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "92880e73-4266-42e6-b4ca-6c5cc8989ae9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 36,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "c5af95ae-80bd-49d8-84ab-c23e42dfc3b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 36,
                "offset": 1,
                "shift": 22,
                "w": 19,
                "x": 47,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "c5f3219f-8d51-470c-853e-3ae1d24c4384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 36,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "22382fa8-5504-46e3-832c-d4919e7e549c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 36,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 90,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "d6f136e5-600d-4ce4-8a0f-b754c6fd2c6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 102,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "7db3af04-c13e-4c55-9d40-533ae5132598",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "068b38e0-3a71-4520-b981-9a0cddfa5abc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 142,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "6b9ccac7-a9b2-4ad6-92eb-19ae35d5705e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "910ec918-e719-4e81-9705-703fe5920587",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "dc878d71-3875-4bee-b8f1-2d7a92657891",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 36,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 345,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "3945f2c6-6ca9-4b9c-89a5-e66f8458f21d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 36,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 213,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "faa57c53-8a8c-4862-8682-3a74a76e83c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "741906a3-0ed1-41a6-b307-7f61dbabfd15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 36,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 255,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "25f5876b-d247-4ad0-bb5b-a12be7567a19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 36,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 270,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "cf31172d-bb7f-4cb3-a3fb-72871c8edaf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 36,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 285,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "54c749db-af71-4931-92c2-38cd5a169d9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 36,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 300,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "24720659-10dc-4551-a15b-2da8a9923d06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 36,
                "offset": -1,
                "shift": 8,
                "w": 7,
                "x": 315,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "24c84cab-df50-4492-86bf-ed8cd7ed2534",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 36,
                "offset": 2,
                "shift": 8,
                "w": 7,
                "x": 324,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "5a9111a7-920a-44e9-a423-36aa0f4fb38e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 36,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 333,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "6947907a-8fb5-40d9-93bf-96ea7ee96b33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 36,
                "offset": -1,
                "shift": 8,
                "w": 10,
                "x": 365,
                "y": 2
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "08baf616-83fa-4078-a4a9-375e5a879dff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 36,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 158,
                "y": 40
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "ced321cc-aa1a-46e2-84e9-20a757f3e090",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 15,
                "x": 2,
                "y": 78
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "d4e9652a-88d5-4ff1-b35f-3108d1f5d601",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 178,
                "y": 40
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "371e97a6-bef8-4f2d-9318-c7a3ab633e12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 48,
                "y": 78
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "4dc8865e-4171-49ae-9c7c-782a608e9dea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 68,
                "y": 78
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "6c491b81-3e9f-4fd7-8d53-b07b9998ad0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 88,
                "y": 78
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "4e63e864-309c-4f20-b225-fd5356080c82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 36,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 108,
                "y": 78
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "32f87e7e-1871-46da-bbe7-675fa4e735f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 36,
                "offset": 2,
                "shift": 15,
                "w": 11,
                "x": 128,
                "y": 78
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "527d71d8-8aed-4b3c-aab3-1a7941f4be2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 36,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 141,
                "y": 78
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "4bb31075-ce71-4a87-9d97-02aa84a17f25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 163,
                "y": 78
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "e4524e4e-9b66-4278-a860-24e5f77c5d71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 181,
                "y": 78
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "d615ffe7-87e0-4fd2-9a12-8e422315e751",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 199,
                "y": 78
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "59dc2c04-cd4b-4ee4-a07b-d4428aa4a9fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 36,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 232,
                "y": 78
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "b289a3b8-b311-4bee-b0fc-5e9d1e4ba314",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 389,
                "y": 78
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "3569e62c-85b4-478f-b4b0-92fb46fb99f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 36,
                "offset": 2,
                "shift": 16,
                "w": 14,
                "x": 250,
                "y": 78
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "af404aed-6642-40f4-aa6a-12f9c0f8ee09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 266,
                "y": 78
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "24878574-164b-47cc-9147-b9ef05736a67",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 282,
                "y": 78
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "1627970c-1235-4c71-807c-627003ab7802",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 296,
                "y": 78
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "93769f61-49f0-4007-83fb-8f292266b3d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 310,
                "y": 78
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "35384c5b-f903-46f0-b016-2663f2fad92c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 324,
                "y": 78
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "a773ec12-eb84-4e75-8033-b9b149aa4f8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 338,
                "y": 78
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "f4bff2a9-77bb-4445-82da-1b754ab87144",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 36,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 352,
                "y": 78
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "8ff90e3c-426e-49d1-8593-586953a36d3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 36,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 366,
                "y": 78
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "9801f782-e24a-4c48-aff4-39dc84b3a099",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 36,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 34,
                "y": 78
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "f54c4bfa-a585-472d-ba1e-a5880686a1e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 217,
                "y": 78
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "857b4913-463c-4f7a-b962-007b40c1e7c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 19,
                "y": 78
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "65ee70ee-1f52-4dfe-9410-bfcbb886d1ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 317,
                "y": 40
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "9e705375-96cc-4ef3-a848-cab7cefc17a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 198,
                "y": 40
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "a4e5f068-45b1-41cc-8f11-59169ce264d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 36,
                "offset": -2,
                "shift": 6,
                "w": 7,
                "x": 213,
                "y": 40
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "2bb7a609-f09d-4172-b64c-29a46001fede",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 36,
                "offset": 1,
                "shift": 6,
                "w": 7,
                "x": 222,
                "y": 40
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "bb2793d6-2718-4c79-9a38-c98bbc609180",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 36,
                "offset": -1,
                "shift": 6,
                "w": 9,
                "x": 231,
                "y": 40
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "1470edce-729d-4929-bd50-04077057ece0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 36,
                "offset": -2,
                "shift": 6,
                "w": 10,
                "x": 242,
                "y": 40
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "c2e038c8-bc89-451d-a24e-3467889d5910",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 254,
                "y": 40
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "c3d8ecd8-4d42-403f-9979-702d22bb13fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 270,
                "y": 40
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "9fa24880-6f15-45ea-abcc-764e77770a04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 285,
                "y": 40
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "cd17d1e0-2097-4ec9-bcf3-e453db52d4ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 301,
                "y": 40
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "9a7e0447-5406-42cf-a66a-42e5fa974a30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 332,
                "y": 40
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "2e54c51a-2a6f-475d-bb44-61fa9ced82ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 488,
                "y": 40
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "251bb673-24e5-4269-b7b5-71409fc624f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 348,
                "y": 40
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "77ef6509-648c-49a1-a924-76c3677c6419",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 36,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 364,
                "y": 40
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "41fd5ca6-ff7b-4917-ba0d-be3e76d7ecf4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 36,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 379,
                "y": 40
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "e352d4ee-a367-40fa-83f0-02571bf48ecd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 397,
                "y": 40
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "dab587b9-1972-481a-8fe0-5e5daed80768",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 412,
                "y": 40
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "49419844-9540-4e3b-b41c-33e40d11e69d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 427,
                "y": 40
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "c6111621-f274-44c4-954e-997023e5bba2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 442,
                "y": 40
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "fbec208d-6054-4a80-9204-8691d923fcef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 457,
                "y": 40
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "643f7b3c-3e05-47b2-a740-3603b8fce990",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 36,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 472,
                "y": 40
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "e2294ec6-8cca-43a0-843f-2e0dbfdc794b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 36,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 282,
                "y": 192
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "b6da78ac-459c-4858-a7ce-c8da5c16bd10",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 36,
                "offset": 5,
                "shift": 28,
                "w": 18,
                "x": 297,
                "y": 192
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "7281814a-ad43-4686-b7f7-8a0ffbe345a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 914
        },
        {
            "id": "8175e092-123b-464e-90d5-7e1abdc94788",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 915
        },
        {
            "id": "6f16db7d-288d-4e34-b59d-05f68cf4690d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 916
        },
        {
            "id": "b8cd6ae3-4c8d-4699-b440-c39876915543",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 918
        },
        {
            "id": "b4200bbd-655e-412d-a5d4-59e54afcae20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 919
        },
        {
            "id": "b05321a5-1964-47c6-b42f-3d4da19f1bff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 921
        },
        {
            "id": "7df82891-350e-4150-a373-b638641666c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 39
        },
        {
            "id": "ca27183b-1f21-4b44-ac70-1b9a0ad5289a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 84
        },
        {
            "id": "2d0df02a-fe7a-4772-9140-8a21b58c761f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 89
        },
        {
            "id": "b0f39526-abea-4646-a188-db8873adc82d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 92
        },
        {
            "id": "9984ffc0-bae9-4f32-8714-f5da891ae2d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 162
        },
        {
            "id": "8db8f3c1-ad83-46e8-b01c-1ab14b68a5ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 163
        },
        {
            "id": "e710a71a-c014-43b8-a202-c236206fbaae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 221
        },
        {
            "id": "28accba3-557d-4b6a-8a67-d308d67870e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 354
        },
        {
            "id": "b820ed63-1aab-43ce-a991-11eb829ec465",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 356
        },
        {
            "id": "ea76b565-7107-4ab5-bde5-f03d1f5dd3a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 374
        },
        {
            "id": "d34311ad-03d7-461b-a4be-087632672865",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 376
        },
        {
            "id": "36148888-39d6-469c-82d0-ff00d2a83ac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 538
        },
        {
            "id": "f5819f2b-fe7b-4f33-b09d-4374ead41d3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 7922
        },
        {
            "id": "252aaf8d-5d36-4bc7-af52-15251984f9ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 7924
        },
        {
            "id": "1970365c-4e1d-4830-ab8f-a001db90953d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 7926
        },
        {
            "id": "58eecefd-6019-4810-bdb0-c21704601b7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 7928
        },
        {
            "id": "fbb5c142-7ce7-46c5-8735-013669ef4990",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 33,
            "second": 8482
        },
        {
            "id": "efff047e-dcd2-4ef9-9311-a21f9be1f6e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 39
        },
        {
            "id": "50115f30-ee70-4cb8-a948-2f9f288d269a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 92
        },
        {
            "id": "fa15e6ac-8ab3-4bbd-9698-b9f9309d460e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 162
        },
        {
            "id": "607cd557-be5b-49a5-8711-a4cc5f5cac51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 163
        },
        {
            "id": "b719a705-5316-4258-90de-57bb7ff6647f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 8482
        },
        {
            "id": "cc116a82-90e3-4ddd-87dd-5b86ce390f55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 191
        },
        {
            "id": "4c29a8dc-8ea6-4781-aa9c-d18e0ba9b17a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 728
        },
        {
            "id": "caadef5d-9223-4db3-a0c9-640476cf6412",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 730
        },
        {
            "id": "aa676cd9-dfe6-4fa1-97e0-3a62876391b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 778
        },
        {
            "id": "831e8470-a941-4860-b268-a9937558961d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 904
        },
        {
            "id": "8f0590fe-a4a1-4492-bbd1-c325742770b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 905
        },
        {
            "id": "0b8d5d6d-1835-4c84-a190-071713a1f3b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 906
        },
        {
            "id": "2a0a7f13-6e3f-4b72-8932-948c7c3a4550",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 908
        },
        {
            "id": "0deeac74-babd-4415-820b-93b11a2a2a76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 910
        },
        {
            "id": "dad690f4-c13e-4f83-bfa4-aaa7e149a02f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 922
        },
        {
            "id": "a8e7a337-421a-4ef5-aedd-393503cd0219",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 925
        },
        {
            "id": "d5d7b80c-8c51-4cb7-a2a5-89d6657a389e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 927
        },
        {
            "id": "8a223918-7e69-4ad4-b867-267b21036262",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 929
        },
        {
            "id": "bbcc0ea4-44b7-4da6-903d-093a62e10711",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 933
        },
        {
            "id": "34a93e45-c1fe-4dd7-bafb-9b2858900147",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 934
        },
        {
            "id": "9fb78393-2620-490a-956b-41b1d7ea31ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 936
        },
        {
            "id": "0bd120e8-2584-4683-85a9-82e68a799c19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 937
        },
        {
            "id": "8d2e2b52-a27f-49fa-90c3-63a5fa11e1f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 938
        },
        {
            "id": "8b6b76c8-e06b-4a22-b073-6e4c4410121d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 42,
            "second": 939
        },
        {
            "id": "c5bda089-6572-4d19-b091-b29aea458c79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 940
        },
        {
            "id": "a04380ea-beeb-44ca-b0a3-5a3c6c63649c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 944
        },
        {
            "id": "b5537e00-7592-407d-9e82-fe8be9a7ba49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 945
        },
        {
            "id": "39a92464-f8e0-4050-9d5b-4415a4c390b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 946
        },
        {
            "id": "db240913-4a80-468c-8b20-6f935cf73876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 42,
            "second": 951
        },
        {
            "id": "c15f2fbf-2c74-4820-9bf9-282f8c6aca67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 42,
            "second": 952
        },
        {
            "id": "f9d3d655-bb04-4d79-ad33-e2e3cc017c3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 957
        },
        {
            "id": "7febaee2-769d-4526-b73a-3e95891abc81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 960
        },
        {
            "id": "292ac20a-e6ec-44b1-82a5-cd6c474b25f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 964
        },
        {
            "id": "ae1bdc09-5399-49be-bbf6-fc88fac6d7bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 972
        },
        {
            "id": "a8f43607-5091-4810-b41b-2adac62a8363",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 8212
        },
        {
            "id": "fb702f2e-b4f3-4dcf-9b83-e65591edfebc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 8216
        },
        {
            "id": "e9ab6f9d-7b08-40f5-b74c-45db80d74ac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 42,
            "second": 8217
        },
        {
            "id": "0623b062-8b5a-47e3-a020-2432747078b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 39
        },
        {
            "id": "c090bc74-d695-46d3-bda3-478b493df985",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 84
        },
        {
            "id": "28fe961c-ccc0-4efd-952b-22a3e4b6d4f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 89
        },
        {
            "id": "237cba05-9f0b-48e5-a6b0-4037af2e7116",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 92
        },
        {
            "id": "877d13ef-7dc6-487e-a298-11b4217eade1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 162
        },
        {
            "id": "57be2b31-7d07-4543-9888-7619c3604594",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 163
        },
        {
            "id": "9865d59a-f732-47ee-a590-0c20641715d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 221
        },
        {
            "id": "bc711cfd-f6fd-491a-8877-400ed66fb648",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 354
        },
        {
            "id": "9480f8ae-2a79-43bd-a622-f415cf4cfbdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 356
        },
        {
            "id": "af7a3197-6de4-47d5-b4d5-75173d74fc5f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 374
        },
        {
            "id": "20752dae-4bff-45f2-a6be-0adfc714654a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 376
        },
        {
            "id": "0de79aae-f4d0-4fef-b6c5-ebc7266fb906",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 538
        },
        {
            "id": "522c3a83-9077-4111-9954-53a55774f987",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 7922
        },
        {
            "id": "14b2c698-58f8-46b8-a5be-5fc55de1c3d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 7924
        },
        {
            "id": "9277e86c-cb22-41e7-ae58-861cdc88f03e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 7926
        },
        {
            "id": "3c7cc259-3244-443e-a99e-f3af5eba2347",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 7928
        },
        {
            "id": "9e03876b-d1ce-4ca2-add9-7e08c5a49255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 8482
        },
        {
            "id": "0f87d45b-db7a-4425-9e94-4d8a43ed1c21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 39
        },
        {
            "id": "d77d72cc-2781-4f22-befd-e3c7b52f31fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 84
        },
        {
            "id": "308a9a6f-7125-4f9a-87d3-6ce31f6aecb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 89
        },
        {
            "id": "26d357fb-66bb-444f-bde8-7305ca736deb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 92
        },
        {
            "id": "1634eae2-53ff-4ffc-b25d-eeae2240c781",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 162
        },
        {
            "id": "241ea6c0-37b0-4fe5-afe0-dd7ea6a7190c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 163
        },
        {
            "id": "0e46fb6c-3800-4e2d-a52f-20ece8bdec96",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 221
        },
        {
            "id": "0342c77c-3d69-4da2-a2fc-55a89b3607c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 354
        },
        {
            "id": "223e4c91-1867-433a-8bb5-a783a7fd10ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 356
        },
        {
            "id": "b44e4228-f6ec-41c4-82cc-bccb5d4354bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 374
        },
        {
            "id": "7cdbc611-513d-4a69-81db-098a92842cea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 376
        },
        {
            "id": "7ec20517-8af2-48b5-8419-34a4824f76d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 538
        },
        {
            "id": "010dcf4d-ae11-4acc-a7a0-ca8be446b9a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7922
        },
        {
            "id": "772ff8a0-12c8-4568-9ec5-aad6202ac53a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7924
        },
        {
            "id": "57564460-4266-4cbd-a97e-5edc29185a3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7926
        },
        {
            "id": "c5a62966-533a-429c-8484-335505ff8605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 7928
        },
        {
            "id": "5737b62c-c999-4955-84e0-53bf5d2377bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8482
        },
        {
            "id": "bd1b19c5-72b7-47bc-ba4e-b0754d2d7a54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 39
        },
        {
            "id": "9bcc9b64-187f-4fce-ae0e-7eb4e94d1400",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 84
        },
        {
            "id": "1fc562d4-07c5-47fa-a889-db99ff36d5b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 89
        },
        {
            "id": "53878350-4de8-402a-afd1-4d0456748956",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 92
        },
        {
            "id": "48bc1d5d-6551-4fdd-9b7b-92641986c156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 162
        },
        {
            "id": "8836b696-533c-44aa-a8fc-a8f15d349adb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 163
        },
        {
            "id": "043f6113-d642-4a31-aec4-0bdaf16ccba5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 221
        },
        {
            "id": "8fc6d0a2-2434-42bf-9658-0d729c94ef7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 354
        },
        {
            "id": "86ac0e4a-a929-4bc9-8afb-838b5ccc7de3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 356
        },
        {
            "id": "a83589a7-962e-44a7-ba99-f90602e5e36c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 374
        },
        {
            "id": "835ec28b-fd86-4555-bec0-15307f107c77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 376
        },
        {
            "id": "2725b73b-aa77-4e5d-a88f-b4b617269c6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 538
        },
        {
            "id": "c71e9005-e058-490c-8d6a-cba0e9ab4f2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 7922
        },
        {
            "id": "b734ee7f-d90c-4a93-838b-ece234d15eb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 7924
        },
        {
            "id": "41dd3030-e187-4ec6-ac1b-5012d11992d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 7926
        },
        {
            "id": "24febc06-ff9f-4667-b2fe-67e78feb2270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 7928
        },
        {
            "id": "df3fbf55-6598-42f2-b4a0-a3857645287c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 59,
            "second": 8482
        },
        {
            "id": "aad852f2-227b-4856-accc-b4d35388f0e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "fa9ec8a1-abe5-4e33-ae53-a9b978d52dd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "24d5a5f3-f29e-48d0-a6bb-841ebff5e2df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 184
        },
        {
            "id": "8ca5c49d-9255-4c7a-a8ba-27373887e0f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 221
        },
        {
            "id": "e99d52a9-8a62-48b0-a063-b159c1389ded",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 354
        },
        {
            "id": "583f6e50-e806-4fdf-90c4-09aea9c5a691",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 356
        },
        {
            "id": "a27603cb-967f-4f29-9070-51c530a4e3c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 374
        },
        {
            "id": "7faa0e0a-8c99-4065-bb79-1bd0020faa54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 376
        },
        {
            "id": "fe9e37ca-4053-4c0c-b93d-cd78b35bcde3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 538
        },
        {
            "id": "06c230db-366e-4c9b-9474-4c9f14068136",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 731
        },
        {
            "id": "9ba18aba-b249-4186-a9fc-d4001be6c8ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 775
        },
        {
            "id": "106fe53d-1319-4e90-b561-e16d4346b44a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 806
        },
        {
            "id": "7822809d-23a5-4453-b26c-9a4facd54ee5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7922
        },
        {
            "id": "607513e1-7fd6-45ef-b0e0-96edc583651d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7924
        },
        {
            "id": "860bfc01-3171-4d99-ade0-c95026fa7e6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7926
        },
        {
            "id": "def909f3-9e34-4fff-b1f7-a448fdf0f1a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 7928
        },
        {
            "id": "909c344c-635f-4333-b817-228f6f71f611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "91985a4e-3562-42be-862f-1e761013da53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 74
        },
        {
            "id": "78e1b1af-f6b8-442f-b2fb-4bf6f112e269",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 192
        },
        {
            "id": "9fe8dccf-5378-4020-996c-a167299aa1f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 193
        },
        {
            "id": "b43a23ae-3099-4ea6-a413-8e8c22df1c3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 194
        },
        {
            "id": "a085b71c-045b-47c7-b330-93571f16d569",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 195
        },
        {
            "id": "68011c4c-737a-4e4d-b671-93a0b024d37e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 196
        },
        {
            "id": "bc87634b-1a21-4e8c-9dc3-7be51ec02d98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 197
        },
        {
            "id": "22181be6-469a-4956-838d-0d0f13960d6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 198
        },
        {
            "id": "17df8a5a-5cd9-46d7-9f99-414235211d74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 236
        },
        {
            "id": "95f8f7bb-8453-4c63-9734-8b35c5678982",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 238
        },
        {
            "id": "59356da6-77a5-4e24-ac5e-4ff6d44c1380",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 239
        },
        {
            "id": "542cdbd3-8450-447d-a9bf-f3f9be47a6fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 256
        },
        {
            "id": "260aeadf-531e-49c5-9101-aa10e77ee762",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 258
        },
        {
            "id": "c2eefe55-9854-4501-bc03-01106a719ae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 260
        },
        {
            "id": "b602b6a9-03c8-48bf-98e6-5655b4301926",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 70,
            "second": 297
        },
        {
            "id": "bc17da47-e5fd-4f4d-928d-d15a6ad6f2c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 299
        },
        {
            "id": "9b12b080-6b81-41c1-9bc3-0acd7c35181e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 70,
            "second": 301
        },
        {
            "id": "497931c0-1c40-4fd2-b985-0e53dc2ab55a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 305
        },
        {
            "id": "c4aa2380-a25e-445a-930a-0b1c29dc82d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 308
        },
        {
            "id": "f4cf89dd-9b3a-4f17-b157-160ef24402e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 70,
            "second": 309
        },
        {
            "id": "186d394f-28e6-46aa-b790-37364e48321b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 506
        },
        {
            "id": "6c7b592e-8ed7-47ef-9f34-d0c1057d5339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 728
        },
        {
            "id": "587e117c-4555-4743-8c77-705a4163ad51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 730
        },
        {
            "id": "f7391bff-e276-49d6-8f5d-9c96fb0be7d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7840
        },
        {
            "id": "acf04fa9-590d-4eea-8772-993ea379d69b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7842
        },
        {
            "id": "039f628d-1edc-4860-8ed3-d9351a0b47eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7844
        },
        {
            "id": "22c64284-4d2b-406c-bc5f-91419789746d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7846
        },
        {
            "id": "3298ec08-4f77-42f5-a9fa-dea27f4ffd1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7848
        },
        {
            "id": "7638018d-1537-448f-90ad-e4ed3ccef0a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7850
        },
        {
            "id": "fbcaa249-a74b-4ce6-9ffc-07c188f1d3b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7852
        },
        {
            "id": "fa2e41f7-2967-44c5-bdb3-e9962c7517c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7854
        },
        {
            "id": "0b5f5169-5298-4012-a4ed-2179436ef18f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7856
        },
        {
            "id": "368471f0-2f26-4491-8ea6-34a0b0e2292f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7858
        },
        {
            "id": "6de1a5b9-b30f-440f-8c64-c60ec2213272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7860
        },
        {
            "id": "e83a92e8-40ae-489d-a395-28d026f22796",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 7862
        },
        {
            "id": "55e7cc99-9345-42ff-83bc-02237d17c092",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "5237102f-1ba9-4272-8fcb-0d75f98d89d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "5f99d98e-d7fe-4517-b94e-0bcf72b989c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "c5821a8d-374e-4333-b0bf-77e9385ccc3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "8f784267-d5aa-480c-8ba0-4507cbefc417",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 118
        },
        {
            "id": "0c401c7c-5857-4a59-8411-de9751855cd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 119
        },
        {
            "id": "bf25d70e-1283-4ca5-8d1a-40a6d23275fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "93d60360-d7cc-4bea-9856-fae53fe89257",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 184
        },
        {
            "id": "028abc38-e0e6-4cac-9f05-4beb41df9ca7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 221
        },
        {
            "id": "fd831c3f-d510-4e06-9e69-a4fe9da6853c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 253
        },
        {
            "id": "eaf18498-80fa-4347-851c-b257be8bb2fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 255
        },
        {
            "id": "28edd170-cef7-4253-bb95-c5ebecffb863",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 354
        },
        {
            "id": "b402f661-b187-4f25-8f32-0179434e2f33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 356
        },
        {
            "id": "a9b29042-0ee2-4c12-9045-6dd2c68f506a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 358
        },
        {
            "id": "24ce36e2-af5d-4327-b136-9fcb1a64585f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 372
        },
        {
            "id": "e9c2c309-ef21-4828-bd63-de1ab030301a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 373
        },
        {
            "id": "fb3e8f9d-3310-4f3d-87f7-579da19b4e3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 374
        },
        {
            "id": "2e091f6d-865b-4f4f-8a04-5491dd6ebc33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 375
        },
        {
            "id": "f6bc0720-879a-4caf-a3e2-2838c3b0b238",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 376
        },
        {
            "id": "44cc6029-93fc-4b8a-94b3-6913f90a1fcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 538
        },
        {
            "id": "35bbe7c4-636f-4b3d-a3d4-0eb19b09c73d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 731
        },
        {
            "id": "133623a8-8365-459c-9940-c9e40d3f5dec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 775
        },
        {
            "id": "b5b0e036-517a-47e4-9891-ed4205787000",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 806
        },
        {
            "id": "d17cdc70-2c54-4349-8d20-d87e934abd23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7808
        },
        {
            "id": "8dcef091-c68e-41f8-b745-fcf4b2094889",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7809
        },
        {
            "id": "f58f76f7-9c2d-43cf-b459-a50f09d0aee2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7810
        },
        {
            "id": "2bc3942f-ffb7-4159-ba7b-c15f7fbb0a3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7811
        },
        {
            "id": "5c8300b7-3715-4153-9f0e-82b9ce7683c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7812
        },
        {
            "id": "4ea069a9-77d7-4c80-8af2-5f873ae2f217",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7813
        },
        {
            "id": "79c51c6d-4691-4bca-9e5a-f815c538935e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7922
        },
        {
            "id": "d16e5a2b-dfd2-4c6f-beed-5c8ee5c57aaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7923
        },
        {
            "id": "f3fe369b-c4f4-4f50-a275-71feb7f4d416",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7924
        },
        {
            "id": "ded1f61e-a478-4c55-a01f-3c5cb11bad34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7926
        },
        {
            "id": "31f7b2da-f5d1-4835-97fe-d6cd9cde2082",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 7928
        },
        {
            "id": "23f7b475-8c4f-46d4-85ed-dc073ab186d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "4ed3ab3d-6d78-4bf2-9057-232280d37dac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 74
        },
        {
            "id": "548ba471-7f05-4d9f-872b-703c20702483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 192
        },
        {
            "id": "3f3acae4-8afa-4c7a-8f95-6483662276bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 193
        },
        {
            "id": "54f2bd34-e581-481c-8161-1ae482f1f217",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 194
        },
        {
            "id": "022074ad-526a-4547-86e6-090b64d9de3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 195
        },
        {
            "id": "352da2bc-066a-4553-803c-87ec7ccb3d32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 196
        },
        {
            "id": "d258733d-3da5-4ca4-9a5c-cd4ce6130284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 197
        },
        {
            "id": "b8800f73-e421-4e6f-a105-52d27e9ec50e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 198
        },
        {
            "id": "bf13df75-88b5-45ba-8cb3-d5ceae044f29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 256
        },
        {
            "id": "4ad4cc4d-51d1-4437-b778-a2c0a9e48ee3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 258
        },
        {
            "id": "e43e1eda-4e7b-44ef-8f08-d40c83eacd34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 260
        },
        {
            "id": "53dd04fc-948a-43c8-bee7-1275af6ef638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 308
        },
        {
            "id": "1eb3bb15-7471-40b4-9354-07f9594d1a30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 506
        },
        {
            "id": "57d7edca-f360-46cf-932a-c24c16dcb9b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 728
        },
        {
            "id": "d2fdc112-01e7-46dd-815b-deef01956972",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 730
        },
        {
            "id": "d8554a22-64de-4729-9835-34d4c30da171",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7840
        },
        {
            "id": "eb973210-18c9-4710-abb3-dc7d25416e3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7842
        },
        {
            "id": "bf043afb-f847-4c75-88b7-78258e360dd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7844
        },
        {
            "id": "979ea98f-2dd0-43da-93d1-97aeff71abd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7846
        },
        {
            "id": "d98d39dd-3930-47f6-8f8c-19dbcca700f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7848
        },
        {
            "id": "81b0b3ca-2d99-4de8-bb48-36db0d357786",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7850
        },
        {
            "id": "dfed262d-68d6-4f54-9824-8e4ed775eccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7852
        },
        {
            "id": "9174eb02-cc02-4d4e-b96a-0979dc9fa606",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7854
        },
        {
            "id": "dd1a269b-4de6-45b4-9a02-35f13f9d68a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7856
        },
        {
            "id": "50097b7c-5ad5-4b3b-8b40-3a8a4d79d420",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7858
        },
        {
            "id": "4e81dfcd-3676-4a7a-bc32-3799fc7b00bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7860
        },
        {
            "id": "3156c223-d1a2-48ba-a36f-8d3d7726c9a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 7862
        },
        {
            "id": "8116e515-9b1a-4279-a9c7-1b9ff50b10f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "e2f9d2cc-db84-41fe-98fe-969fcb6362bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "c72b49ea-6fb8-4cf8-9940-f5f2aa180780",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 74
        },
        {
            "id": "f02fc6b7-d43a-4547-bd30-583eb534304e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "f9208164-ba53-4d59-8031-4f07342a3e11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "329b16f2-5197-4837-ac3e-64e1addfe1a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 100
        },
        {
            "id": "01002741-72a8-4050-b9cd-ee299d191fe9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "15153e26-b281-473d-a15b-53fa2203b13b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 103
        },
        {
            "id": "ab7f8c07-c8a4-4163-b8bd-311e2e3ec981",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 109
        },
        {
            "id": "e6631535-f2b4-43ac-ba37-6c608868cf05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 110
        },
        {
            "id": "e78c93e5-4b69-4fa9-88cb-fa2a183258fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "aa07e4aa-d219-4e81-94e2-b77006895cd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 112
        },
        {
            "id": "fdf1acab-992b-45af-bf03-a564ba368cb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 113
        },
        {
            "id": "d56f4b5a-dda3-4393-8b3c-9eb28b3e64ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "659869ff-3bf4-40ee-9685-234b1172f743",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "13d1b8e9-443a-438b-b8dd-198ec6ab660d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "9ce2ca65-2a1d-435a-b58c-458f729325f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 118
        },
        {
            "id": "fd12d425-ca5e-4583-b79a-a1f8e2920bf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "9d58710f-b7e5-4550-9ebf-7c2daed0005c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 120
        },
        {
            "id": "0b1c2a9c-1d1c-49fb-abb7-2ee533080aa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "5459b1b7-df34-4b42-912c-4b28e37840d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "b110af8c-cd69-4cfc-9155-224b8614e9c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 161
        },
        {
            "id": "3efebd79-37c3-4dd8-a68e-b4059f8d1bc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 191
        },
        {
            "id": "b4de737e-cf53-425a-8255-24c8e9a40b33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 192
        },
        {
            "id": "967de053-5009-40d5-a58a-8a720b85636d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 193
        },
        {
            "id": "d4deb54b-d7aa-40d8-b28f-3f31affcb7c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 194
        },
        {
            "id": "ece3ed3f-1861-423f-8db3-7611554a2dac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 195
        },
        {
            "id": "936085eb-88ee-4eb2-9df2-9fb1a062ac8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 196
        },
        {
            "id": "0935a66d-c0d2-4a61-ac98-aec0f559c9ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 197
        },
        {
            "id": "c9361839-3cbd-41ae-83b5-eecbb570d315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 198
        },
        {
            "id": "f50c9969-e57c-4ce5-9083-619aea27bb9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 224
        },
        {
            "id": "e5fc5480-4909-47b5-b1b7-fe0b168c4081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 225
        },
        {
            "id": "58f1a1f7-89ac-482b-aa09-6ddbf53568d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 226
        },
        {
            "id": "510fdeab-b8af-4306-97b3-3d7ac94f7f3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 227
        },
        {
            "id": "3c955ee5-8b6e-4fd4-8a0d-6fae3ece9d34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 228
        },
        {
            "id": "517bce58-c390-4fdc-9343-d443ff963fa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 229
        },
        {
            "id": "520e27d9-1a85-42b0-97d5-6e9c8ea14ca0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 230
        },
        {
            "id": "c6305c19-f937-453a-b614-b64f6bab2fc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 231
        },
        {
            "id": "fe4849f1-12d9-4b00-a756-97dbfbdb3fe2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 232
        },
        {
            "id": "9906b7f1-68bd-45e5-b086-4d81e31bd35b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 233
        },
        {
            "id": "b5aa46f0-6797-4af9-9876-2f68f15796fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 234
        },
        {
            "id": "53bca6ea-0bf9-42fe-b314-19aa2113da5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 235
        },
        {
            "id": "e266b88e-4e71-4cb5-aad4-41da23b2ffb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 236
        },
        {
            "id": "2cace551-62c1-48b9-8c57-c0f43fc08617",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 238
        },
        {
            "id": "955bfe75-f08f-4163-a6f1-49994c2ac625",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 84,
            "second": 239
        },
        {
            "id": "df4b8719-870a-4751-aa45-ae9792ea1cf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 241
        },
        {
            "id": "eb838cce-3a4b-4377-8c0f-6fdcb6d37808",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 242
        },
        {
            "id": "404c0ad5-de65-46ea-9f80-8541d33463bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 243
        },
        {
            "id": "73b29ddf-2297-4f7a-b420-b2809b844316",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 244
        },
        {
            "id": "a3930400-0798-420c-a759-46c7a2f1c884",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 245
        },
        {
            "id": "878a822d-bd54-4852-81ba-32141f5abce3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 246
        },
        {
            "id": "5c7a7457-f8dd-4504-b72b-2844bbde1447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 248
        },
        {
            "id": "4660197e-7aa8-4543-a266-a3782996fc2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 249
        },
        {
            "id": "24657749-66ee-4c03-8711-561090783272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 250
        },
        {
            "id": "f91932e1-9645-4e6a-a07b-275aeb912f5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 251
        },
        {
            "id": "33d34d16-0b44-4f11-9409-a63127e487a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 252
        },
        {
            "id": "441464d1-b851-4989-b2af-8cffd6619fc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 253
        },
        {
            "id": "98a5b568-687e-45bf-a557-ff6242c09850",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 255
        },
        {
            "id": "bfcaed12-938b-44cf-a419-e48801431a21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 256
        },
        {
            "id": "aa38bb14-daa8-4498-84fa-91428cbcc381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 257
        },
        {
            "id": "bd6c0c8c-0f68-41ff-ab78-3381635ce21e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 258
        },
        {
            "id": "37f4bdfb-3d29-472c-a4c9-eeded1460889",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 259
        },
        {
            "id": "5a75a10e-c107-4e62-8f4f-4c32559e6544",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 260
        },
        {
            "id": "d8a31f21-5efb-4bb3-ba93-a76d290c0339",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 261
        },
        {
            "id": "4a732a5e-34cf-4983-bc55-47c211f87b00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 263
        },
        {
            "id": "875e8901-859f-4231-aaa2-37c4abb1e976",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 265
        },
        {
            "id": "ea46f8a3-98eb-47e5-8bd1-bbb92f786a38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 267
        },
        {
            "id": "161d86ae-1248-4d2e-90e2-dc98ac48ae1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 269
        },
        {
            "id": "c48b4f91-231f-4b38-8fc6-68b97cd67749",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 271
        },
        {
            "id": "d8e74573-0c53-4858-afec-e1195e116dbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 273
        },
        {
            "id": "c6bea917-893d-4a64-aa7e-a110579ada80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 275
        },
        {
            "id": "27a30fb5-c722-4993-9e22-3199e8d21db2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 277
        },
        {
            "id": "9c9e518f-06d0-43ac-bf8e-6f5cfb3ae414",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 279
        },
        {
            "id": "7f501227-542c-495c-be4a-1fa0e805c45e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 281
        },
        {
            "id": "a3501c6c-ceae-40d2-94c3-a38ae6ffcb15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 283
        },
        {
            "id": "6f2dc3fe-b0a3-4813-b502-461d29d685eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 285
        },
        {
            "id": "20b93e57-11be-497a-a45b-34ec23fad01f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 287
        },
        {
            "id": "fb6e0201-a93a-4268-aa83-1c0bc926c196",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 289
        },
        {
            "id": "5523ad6d-0a98-409f-87c4-1a79b75524ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 291
        },
        {
            "id": "ec31a79c-8177-451d-93b3-7f8c83b2c89b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 84,
            "second": 297
        },
        {
            "id": "194675a6-3d8a-42eb-97fa-ffdf21d3abf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 299
        },
        {
            "id": "41cc97ec-b849-4f81-9019-ff67df9e8c6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 84,
            "second": 301
        },
        {
            "id": "c46a3744-1674-46ab-861d-2e50226ac6f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 305
        },
        {
            "id": "846e9a5d-7960-48e3-93d0-baee618fd6f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 308
        },
        {
            "id": "702390ed-7b60-40ba-9921-4b25b6974338",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 84,
            "second": 309
        },
        {
            "id": "ef818ba9-ff90-4400-a278-3c2e3d897d6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 312
        },
        {
            "id": "e18a25a7-c701-4862-ba14-c154e7152db2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 324
        },
        {
            "id": "64a93540-26e7-4eb3-8f1f-f3303a548686",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 326
        },
        {
            "id": "d587fa96-07ba-494f-b853-8a31f8cf5b84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 328
        },
        {
            "id": "a851bc13-f1b7-48c8-a4ba-88ef60c7494f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 331
        },
        {
            "id": "ebd926a7-a8b0-4bbf-8166-09584ff58f37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 333
        },
        {
            "id": "e7be53c5-c9a6-4b40-8440-6aa3db4abe34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 335
        },
        {
            "id": "0eccb52c-eb50-44d9-a8b7-94c26de71b86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 337
        },
        {
            "id": "b83d883a-5014-49db-ba49-ce8791187e31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 339
        },
        {
            "id": "5d2bcced-12de-4d4b-97fe-ef0e6a3e6c10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 341
        },
        {
            "id": "b7c1c20f-24cc-4d02-aef1-f53448a4666f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 343
        },
        {
            "id": "d5c5f159-b44e-49cd-895d-a5b81c04eedb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 345
        },
        {
            "id": "1cba0605-1555-43f4-a7a1-602b0269baec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 347
        },
        {
            "id": "069044d9-021b-4921-90bc-eb53462bf281",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 349
        },
        {
            "id": "8f0311ce-501c-49b2-816f-4548f389ee93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 351
        },
        {
            "id": "343ee4fe-226c-4451-b277-de0ec6b2a068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 353
        },
        {
            "id": "e2273cdc-d185-4374-8977-230871af2b25",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 361
        },
        {
            "id": "aea37ae4-6d21-4dfc-8786-f60dcb2349d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 363
        },
        {
            "id": "cfc0f37b-5d45-4c00-b952-0772c39d50e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 365
        },
        {
            "id": "a57158fc-cc83-4ab7-9955-2832ae8c3f0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 367
        },
        {
            "id": "12f20f77-2d9c-47a6-9d76-f1b78302adaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 369
        },
        {
            "id": "551e811b-9705-49da-955a-ecebcad546fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 371
        },
        {
            "id": "cb1ff680-1ea4-4ea8-b8e7-6596660b3ee0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 373
        },
        {
            "id": "ec9a61a6-7489-429f-8ab1-d45a05f6a3d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 375
        },
        {
            "id": "0d0f9226-9ee7-4e3f-90c8-9ac8391b355d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 378
        },
        {
            "id": "3a34a898-b1f4-4e74-ba47-8dc44e2d1fe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 380
        },
        {
            "id": "114f4dd0-0e73-49ab-b86a-5fef704304d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 382
        },
        {
            "id": "0a6ea268-8a58-4d8b-9752-9ca303738399",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 417
        },
        {
            "id": "28b0887b-02b6-40df-a3e8-a3aaca84e3a7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 432
        },
        {
            "id": "3c6d131f-e27c-48c4-b050-b3f2692f9aa1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 506
        },
        {
            "id": "7bc6274d-3941-4a38-964b-57e921374a89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 507
        },
        {
            "id": "afbfc737-3d9f-476e-9b37-a029d79ed1fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 509
        },
        {
            "id": "43a35712-b4b3-4d15-9ee9-521488d8d966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 511
        },
        {
            "id": "748a09ff-7492-49f5-b8b4-ee5652baadb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 537
        },
        {
            "id": "f10c19e0-09b7-407a-b611-047919e70d72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 728
        },
        {
            "id": "9cb1d0fb-ffe6-4123-8b67-093008cdec3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 730
        },
        {
            "id": "c677e413-4464-4224-a187-7a058afc4654",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 774
        },
        {
            "id": "c9c5e7ce-5d69-435a-bcf3-c8a34b94f52c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 778
        },
        {
            "id": "caef16b9-ba97-4a1a-8cdc-6a48b1c33c17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7809
        },
        {
            "id": "e92004d6-d816-4157-8756-1fd6076cd500",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7811
        },
        {
            "id": "1b286eef-d952-4147-915f-2a7528d2e9ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7813
        },
        {
            "id": "0b217d7f-c47b-450c-82de-3eabf9e544fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7840
        },
        {
            "id": "e2915a0b-8c9a-49b7-99f4-6c352835f1ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7841
        },
        {
            "id": "207a6381-78fb-4909-8104-13f6409007af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7842
        },
        {
            "id": "392a6259-46e2-46b5-b6e6-0041334c16c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7843
        },
        {
            "id": "6623fe27-dde5-459d-93a2-2e698450a8de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7844
        },
        {
            "id": "148793a5-8a38-4531-8b57-463659fac59b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7845
        },
        {
            "id": "1e2c1819-a6d1-4702-9aa5-f980eb99d197",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7846
        },
        {
            "id": "4e367e1f-52ce-4881-b71a-440dd752d60d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7847
        },
        {
            "id": "b9da07fc-f4c7-4f62-97bc-8b021bfe8be9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7848
        },
        {
            "id": "44b054f4-70da-4c45-afde-66ac92b51440",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7849
        },
        {
            "id": "7bccd3ed-2d88-4068-af69-753cf9d8d4d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7850
        },
        {
            "id": "1bb0d6fd-196d-4b5f-a185-266efb7ea43b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7851
        },
        {
            "id": "893bf7d1-4476-49bb-babe-20740500c140",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7852
        },
        {
            "id": "1183debc-a54d-4379-80bc-a4c21a49f3ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7853
        },
        {
            "id": "5bef61bf-a9ac-4af9-8d23-e15d29a056b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7854
        },
        {
            "id": "a9dfa9a5-f471-4949-a8e0-4fdc73fa346b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7855
        },
        {
            "id": "011bd54e-75d2-4152-b14a-11d783e40ef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7856
        },
        {
            "id": "e6f4c9b7-b37c-46bb-85d8-7aed84fd869d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7857
        },
        {
            "id": "d3891b57-3212-463c-b3e1-9a4cbc469592",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7858
        },
        {
            "id": "5a31be5d-96c2-4aa3-8391-3ffa766ab23a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7859
        },
        {
            "id": "a59ad59c-c3c0-41b2-b99e-2a79ba6c458c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7860
        },
        {
            "id": "7fc50342-5486-492a-8c50-7f0d4340c9c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7861
        },
        {
            "id": "4419816b-6fcf-475d-b21a-d0d8cf9a28a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7862
        },
        {
            "id": "a3ce364f-ccf4-415b-9608-668495d33a09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7863
        },
        {
            "id": "a6984ad3-81e9-4777-929c-9def78941555",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7865
        },
        {
            "id": "ea0bbf08-a120-4cf5-ac6b-47c0405fb2d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7867
        },
        {
            "id": "5376cb17-a536-4a92-8521-f11ed2411dcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7869
        },
        {
            "id": "4e5317de-0dee-4eef-bc32-0e951cf93b8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7871
        },
        {
            "id": "51071e12-c53b-43bb-a788-77306b08dd72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7873
        },
        {
            "id": "75b3777e-3d9e-4272-8a5a-cf6a2a98f53b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7875
        },
        {
            "id": "c7ad17a6-43c8-416f-8f99-120b7cf216d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7877
        },
        {
            "id": "f834572a-58a0-48e3-b5b2-ef4b77646764",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7879
        },
        {
            "id": "7978e06c-dfe3-4baf-bf04-3ec54b09033b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7885
        },
        {
            "id": "04efbcdf-4d93-4721-bfb9-661c8acd598b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7887
        },
        {
            "id": "76e400bd-cf29-4912-a5ea-d42fa1374791",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7889
        },
        {
            "id": "2a4dcc76-fc15-4f14-b310-56d8655dbc88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7891
        },
        {
            "id": "760eaf49-53a8-41fd-a5e1-a1825461cf16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7893
        },
        {
            "id": "3955d1d0-3d6f-4348-866a-ec5b9f7b5405",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7895
        },
        {
            "id": "095cee66-da10-4231-9e40-a5244f0148a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7897
        },
        {
            "id": "6b1b20b6-0999-4ec7-9f2b-cdcec44b3cad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7899
        },
        {
            "id": "604ac57b-6ee3-49d5-8375-60694f3c99e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7901
        },
        {
            "id": "675f8b37-581c-4279-bfe3-b6d9ef0c6ef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7903
        },
        {
            "id": "6102c164-58ef-4d06-aa6d-8e0d0511a08b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7905
        },
        {
            "id": "359aaf5a-8c4c-433c-81bb-5924de8a801f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 7907
        },
        {
            "id": "8fd16a17-e8e0-44e0-aa47-9d705b4e558f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7909
        },
        {
            "id": "6db49c59-9797-47a3-b497-65a1d3f914fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7911
        },
        {
            "id": "2d5e8949-9213-4a97-9f19-004158aa2b67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7913
        },
        {
            "id": "4f800a5b-52eb-44de-b159-cb97895de8bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7915
        },
        {
            "id": "10eae584-16ec-4528-9046-37d3db718a94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7917
        },
        {
            "id": "7e98fd13-28a7-427d-a1b3-6d33a54abb1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7919
        },
        {
            "id": "86bcce2c-f5aa-457a-b9b6-6a626bc80d59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7921
        },
        {
            "id": "0fa22ca9-763e-4a90-a0f1-d821fd93dac6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 7923
        },
        {
            "id": "23adcc3a-d350-4816-b395-0fbbc70d3ec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8216
        },
        {
            "id": "cee40ead-94ee-48af-aa1c-6027af10cec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8217
        },
        {
            "id": "216974a5-9ab1-4a87-8026-ac483b807aeb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8230
        },
        {
            "id": "38e2765b-acd4-46ce-9bb0-f2443325a34a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "99e0dc76-fae0-4fcd-b388-bba1436c710d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 74
        },
        {
            "id": "b55d6edd-d4e9-4a70-a1eb-56f9cd59b20f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 99
        },
        {
            "id": "0851da9b-f78f-420f-b755-8b51c2954367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 100
        },
        {
            "id": "c4c8ae6f-7ca6-4873-ab56-8587f3b1f12c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "ada4028c-4d1a-471c-bbf6-ef8a129c2f1c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 103
        },
        {
            "id": "38280c80-4e65-4d4d-859e-ef62f2b2c3be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "c549bcb1-9dda-4dd5-bddf-f77accbc1268",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 113
        },
        {
            "id": "708fa01b-88c3-465b-b884-e586ab351c0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 115
        },
        {
            "id": "74691ebc-f993-4a27-9e52-6ce2ffed363e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 191
        },
        {
            "id": "45e6ee1e-4d70-45ef-8bc7-de46959c8920",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 192
        },
        {
            "id": "c22fc694-54d7-4f96-b6d4-58a3db3eaef9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 193
        },
        {
            "id": "1ef217fc-02f2-4755-a224-a14a2da32c29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 194
        },
        {
            "id": "75353225-bee9-4284-afc7-13d41d4177de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 195
        },
        {
            "id": "1e576177-a386-475f-b3b2-71ca3bce227d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 196
        },
        {
            "id": "8f7c5457-737c-4535-b816-811d3641e045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 197
        },
        {
            "id": "c1268021-0061-4205-9261-53850eb1a911",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 198
        },
        {
            "id": "2b0c76c1-7125-434c-96d1-306af5664f1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 231
        },
        {
            "id": "de7696f6-f3b3-4312-82a8-0ae98b605a27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 232
        },
        {
            "id": "e2a31b61-82ed-4c71-9702-c531eab058df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 233
        },
        {
            "id": "0cbf5756-7223-4840-b3dd-bbf64d0258a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 234
        },
        {
            "id": "dab86bc6-46fd-471b-b8fb-f9dc9c2fd935",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 235
        },
        {
            "id": "ef1f184c-37e0-49b4-bc03-d3b0ef7c55a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 236
        },
        {
            "id": "28173848-ffb4-4cc4-8957-828499cb79ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 238
        },
        {
            "id": "9876b043-01cc-405a-9bca-435eb145841c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 239
        },
        {
            "id": "8cf53ddf-fe68-4143-bfc9-7a4715436cfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 242
        },
        {
            "id": "a305a109-db28-4e65-af2c-c32dbec8ba77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 243
        },
        {
            "id": "583a9258-6bad-4a12-97e5-3007342c4fa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 244
        },
        {
            "id": "393f602a-5c96-473f-a2c2-109e44dde7ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 245
        },
        {
            "id": "3f3e3988-118e-46e7-a7ab-e684aa1faa23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 246
        },
        {
            "id": "92ed55e5-7f22-4a50-94da-2fdd2fd7a6d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 248
        },
        {
            "id": "00531650-7b04-468f-a62b-0a6c0aec3553",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 256
        },
        {
            "id": "e9942658-dccc-4c74-a82c-30873ed771aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 258
        },
        {
            "id": "fd2a8aff-633a-4f98-b1f2-366f8dd843ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 260
        },
        {
            "id": "632cce17-3cec-45ef-91e4-a21756c47460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 263
        },
        {
            "id": "fe483653-2b64-42a9-92b5-4e147a06698a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 265
        },
        {
            "id": "e4b5d574-72bb-4b4e-ae8a-939bb0d013ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 267
        },
        {
            "id": "4dceadb4-cdb8-4453-9c9d-7edf87fdc342",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 269
        },
        {
            "id": "fb3db2d6-0371-42ef-ac8e-b5b73eca5d41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 271
        },
        {
            "id": "7a3e7790-e6af-44f6-a02e-04c3bf197f90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 273
        },
        {
            "id": "04aa7d28-868e-422c-b222-caa4dc63915b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 275
        },
        {
            "id": "35678ed3-356a-4a83-b62d-1f358f85ebab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 277
        },
        {
            "id": "ab8c71fe-1b9b-4ed2-8fa2-b4567b0da8d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 279
        },
        {
            "id": "9f8824fb-1968-4ec6-a88f-22551dc56926",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 281
        },
        {
            "id": "ce103242-69da-47c7-ad46-67d4d428cab1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 283
        },
        {
            "id": "1b400e8c-fbb7-48eb-9872-2ded975d4c3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 285
        },
        {
            "id": "e4215b52-8a79-458b-8743-7e6441c56b6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 287
        },
        {
            "id": "ac58b4de-3802-4390-a5ce-7bc2e99b4cdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 289
        },
        {
            "id": "bd7ece0b-4248-45e8-b9a2-f94eb872af3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 291
        },
        {
            "id": "248671fd-cd29-417e-afa6-440352e2edb6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 297
        },
        {
            "id": "3044159a-a476-43da-8c22-0a3b7670067e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 299
        },
        {
            "id": "ce19cee7-60f3-4660-b51d-5db5e98ea7f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 86,
            "second": 301
        },
        {
            "id": "f20fa739-63d5-4a3b-93a1-4431f7d02fae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 308
        },
        {
            "id": "36839d21-85ba-4f7a-a641-3647e8ac2890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 86,
            "second": 309
        },
        {
            "id": "98a8c448-d328-4a6c-85b0-f3e788704af8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 333
        },
        {
            "id": "b233bbe8-96a0-47c5-b522-efcb467d8ae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 335
        },
        {
            "id": "dcc7c88f-4aab-4a07-839b-f9826d60b39c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 337
        },
        {
            "id": "f7a4322d-77ac-4277-93e7-345663a15801",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 339
        },
        {
            "id": "91946d4b-aafa-4073-a04e-4e5b2f3a9256",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 347
        },
        {
            "id": "245cea92-db5e-48f8-a94a-3c3888432251",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 349
        },
        {
            "id": "2e13d11e-9eb3-4ac6-85fa-d443f40c4fe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 351
        },
        {
            "id": "e4a667d0-5ff7-41a8-80c3-278e9bdbe1fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 353
        },
        {
            "id": "c9cbf4b5-d90b-4f1a-84c2-c7404625f822",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 417
        },
        {
            "id": "1f917829-a44d-4922-8f5d-7bd0774b7b3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 506
        },
        {
            "id": "2d634ddb-f714-4eb0-8177-7f232f46af71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 511
        },
        {
            "id": "14ccb824-be63-4ab9-9cf1-56c34e320e3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 537
        },
        {
            "id": "e042a244-7546-405a-ab11-880d55d66f9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 728
        },
        {
            "id": "1a3b5c12-1ede-48f2-a647-740b68567009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 730
        },
        {
            "id": "648fb9af-4fe6-411e-a0b7-0525a5eab1b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 774
        },
        {
            "id": "3e6ae8a7-2150-4f82-8bec-f0516295ea3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 778
        },
        {
            "id": "f08463f4-e1b5-44f2-a83c-3a830b149bcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7840
        },
        {
            "id": "a791594b-004d-42bc-bc08-607a245dbc07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7842
        },
        {
            "id": "5f28956f-5682-4bbd-8270-97cfb16ec4b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7844
        },
        {
            "id": "2d5b735a-6dee-498c-92cf-855a08e84c2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7846
        },
        {
            "id": "b73bb6b0-b7e7-418a-935a-66379f561d4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7848
        },
        {
            "id": "721e060b-a2fa-4462-a8f6-e62eae53690e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7850
        },
        {
            "id": "e2ec10eb-f441-4fb9-bf9c-8d18091355b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7852
        },
        {
            "id": "9488700e-3e6d-4aa2-9ca8-b41dc4ffaf71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7854
        },
        {
            "id": "8bd5dd74-23b9-4534-8da1-a3c1a19387aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7856
        },
        {
            "id": "593bd7be-25c3-44ff-93f0-5f9d8d5d6ac8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7858
        },
        {
            "id": "6eff9390-44ce-4a65-9d22-bbebf468184e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7860
        },
        {
            "id": "4040b206-692f-4e4f-9745-99dd7db2a488",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7862
        },
        {
            "id": "dc6191f8-b5b7-4926-bf46-c008bdbae9af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7865
        },
        {
            "id": "7bc27c4c-9711-4032-8bea-49f520bb98d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7867
        },
        {
            "id": "7aa8be9e-4cd3-4d6c-86bb-11d849267f07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7869
        },
        {
            "id": "e3772d8f-3ca5-4997-9ac5-34311f8ccdef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7871
        },
        {
            "id": "d0ea343c-34e0-4035-a7d9-69c0e6d514cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7873
        },
        {
            "id": "e1c6a7e8-a86f-4716-b2cd-b25bbe477398",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7875
        },
        {
            "id": "880b562c-dd72-4f59-b728-6b792a437128",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7877
        },
        {
            "id": "32e506cc-1bf9-4eee-b9b6-e3de0a83d1de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7879
        },
        {
            "id": "00e28b59-282d-4364-be91-8c87276839c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7885
        },
        {
            "id": "feb41f04-bbb3-4005-8ea2-d9bba17d0ae9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7887
        },
        {
            "id": "fb526fd8-5e8e-430b-b83d-8aa5975e7154",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7889
        },
        {
            "id": "6f4d8a08-195e-413b-b5d1-b86280b70532",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7891
        },
        {
            "id": "d498376c-d12a-497e-ada9-412c41fb5be3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7893
        },
        {
            "id": "976eacb9-e27c-4c2c-86d2-95aa9922aafd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7895
        },
        {
            "id": "0ca6d41a-07fb-4a80-a1e4-f5c8f4c2e405",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7897
        },
        {
            "id": "d9c54a69-bb54-4f7a-b4a6-12b87b912bb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7899
        },
        {
            "id": "a180ef52-7c36-418d-8d1a-1aec116e0081",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7901
        },
        {
            "id": "ab0f63e8-7c00-4389-aca0-11af70f24f5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7903
        },
        {
            "id": "b0e64dba-5978-4eac-8da0-f1db6a4a23e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7905
        },
        {
            "id": "40a2d27c-b750-4e39-ae50-2dbb7b0dfde8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 7907
        },
        {
            "id": "0fd2b859-0e62-46d5-9783-13de6c510900",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8216
        },
        {
            "id": "e201ffe7-6a48-4108-ae5d-cecce7e6762a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 8217
        },
        {
            "id": "efd5dab5-747b-499a-95fb-8527cf244503",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "8ef4146b-6604-4e60-9f22-3d9c352bb3e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 74
        },
        {
            "id": "9b457231-680c-45d2-8752-955dacf0160e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 99
        },
        {
            "id": "969b5fee-5117-44d6-972a-016e663bb61d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 100
        },
        {
            "id": "e470494a-f006-4838-9ff0-f6eef579fdf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "e30a5d82-457f-45eb-8f9e-8ca34899a141",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 103
        },
        {
            "id": "9b731dad-f521-4422-899f-5754798a118f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "8d46d6c9-da69-4f5c-8dd9-c92e4b7ed550",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 113
        },
        {
            "id": "cd4bc0c6-b531-4010-8951-4f206494be88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 115
        },
        {
            "id": "79e40167-72da-4fb7-bfe5-4e6c83d7c557",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 191
        },
        {
            "id": "569b3651-c611-4966-b269-18e4928076d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 192
        },
        {
            "id": "a1b2b152-1c7e-4389-9fcb-6d16b0184977",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 193
        },
        {
            "id": "ea3bd69e-7255-4513-b013-9d751f85f77f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 194
        },
        {
            "id": "9f390162-a9b6-4669-9b0c-333051da42fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 195
        },
        {
            "id": "f08d4ab7-c4b6-4f0e-9159-b18209f4b845",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 196
        },
        {
            "id": "2f8577f9-814f-417f-aeb0-f77ae229e03d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 197
        },
        {
            "id": "e366d84e-5709-49a4-97a9-20b9f1a93e0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 198
        },
        {
            "id": "e21a9736-4784-4d02-9c3a-61ca7b577605",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 231
        },
        {
            "id": "85d9b0ee-07ef-47f9-a889-31665766e0c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 232
        },
        {
            "id": "64d0b423-43e1-4003-96b4-25ebd133cccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 233
        },
        {
            "id": "5a3d1190-dc95-4e8a-96af-23a9f8a18642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 234
        },
        {
            "id": "02da4c44-55a5-4602-976d-85d5714c01ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 235
        },
        {
            "id": "ac03d1b3-f8a9-4a7e-b2e5-703c667dd8c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 87,
            "second": 236
        },
        {
            "id": "08ffae7a-2176-4079-b94f-81d62ab9d6d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 238
        },
        {
            "id": "d01fbc44-861e-4ae6-92d9-16d4a8c821ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 87,
            "second": 239
        },
        {
            "id": "cb379946-231f-4e7c-83ba-d0046992bf91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 242
        },
        {
            "id": "d2863176-f8e7-4d0f-b0f5-2970e575158c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 243
        },
        {
            "id": "986c848e-9e58-400c-b9ac-8b911166116a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 244
        },
        {
            "id": "a72412e4-8158-4043-850b-f7322d594c6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 245
        },
        {
            "id": "a7507b41-0c6a-46d4-a89a-4b7729cfa051",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 246
        },
        {
            "id": "588fecb6-086c-4404-893b-c9cdbc42cc3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 248
        },
        {
            "id": "86611779-9233-456e-921f-031c7d3c64dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 256
        },
        {
            "id": "bb0c78ac-ce9f-48f1-8d40-f5e09f474e33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 258
        },
        {
            "id": "e9e4b1d7-7161-4abd-b080-0d70fb9eb22f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 260
        },
        {
            "id": "e8ee6e12-f43a-4b90-9cd3-dde21fbf4a48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 263
        },
        {
            "id": "9c6f342f-764d-490d-af8c-eaba4b257f45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 265
        },
        {
            "id": "319d64c5-58d1-45b6-a6a7-ce6da469d95f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 267
        },
        {
            "id": "07195514-d5a6-402a-baf8-be7764718e8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 269
        },
        {
            "id": "af441890-7dd0-44cf-9b24-bd41db70279a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 271
        },
        {
            "id": "41b81124-9a96-4961-831f-581b2fc4a0f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 273
        },
        {
            "id": "b6f5b86f-b969-4c9d-b478-f218f6b56376",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 275
        },
        {
            "id": "eb2f5ac7-2c93-40f7-9f8e-3357f72d1a75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 277
        },
        {
            "id": "09413c15-907e-4a61-aa48-f147260e6d83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 279
        },
        {
            "id": "d0db2054-0953-4789-bbb6-2b82a45321c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 281
        },
        {
            "id": "256bfeba-8d8e-4268-b938-a9c368404d97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 283
        },
        {
            "id": "8e77eb1c-6d6c-4c8a-966d-b23aa22f2506",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 285
        },
        {
            "id": "96e30a10-85a6-4956-8689-cdbdbba81434",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 287
        },
        {
            "id": "9d746f21-aaf2-4113-aee4-27719e523068",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 289
        },
        {
            "id": "9cf36607-14ff-4b3e-9d55-f5f266fcde44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 291
        },
        {
            "id": "64544542-9464-451a-a08f-f73fa9167e8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 87,
            "second": 297
        },
        {
            "id": "c364949a-0f75-4244-bf19-0d785c71ff71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 299
        },
        {
            "id": "ec27a95b-33cc-48a7-aa31-f2840647094a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 87,
            "second": 301
        },
        {
            "id": "534abe83-0ba5-4ed4-8718-5e99df54ca7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 308
        },
        {
            "id": "99016113-fabf-4f17-b507-30ca80459fa7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 87,
            "second": 309
        },
        {
            "id": "f4173176-c50f-4a00-a7dd-7fde687ca438",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 333
        },
        {
            "id": "da983407-3d5e-4d77-b87b-c5a1390895b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 335
        },
        {
            "id": "e83cdb81-3284-49a6-a96d-d1a3b1e81ecf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 337
        },
        {
            "id": "0e7de425-8b12-433d-a505-7c6ce14a18ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 339
        },
        {
            "id": "e6d1f67f-52ae-4358-9868-617c75fb2f9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 347
        },
        {
            "id": "ee6a0b08-bb7f-416b-b0dd-95b12bf48e03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 349
        },
        {
            "id": "d623a76c-0b24-4044-9c0c-985f961fefef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 351
        },
        {
            "id": "d23709ae-125f-4da1-a9b6-53dc4e60a177",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 353
        },
        {
            "id": "beea07d2-3855-4713-81b9-c71d27b48fec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 417
        },
        {
            "id": "0bd975ad-7f79-4c35-be7f-58a0dff906d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 506
        },
        {
            "id": "64c4eff4-be8f-41c6-81eb-fb59dfac545b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 511
        },
        {
            "id": "93a60112-2c84-4da3-b5bc-e4c0ff0ec49c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 537
        },
        {
            "id": "34f29108-eeb3-4330-9a7b-6e07c9bd57e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 728
        },
        {
            "id": "e233be30-ffd0-42f8-9a02-609fcb9a2a2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 730
        },
        {
            "id": "396c9f16-b838-47c0-a4e7-1d8455a3a36d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 774
        },
        {
            "id": "570db6ef-ebc4-4498-8c5f-39510692f010",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 778
        },
        {
            "id": "ce7e5f70-774b-4418-bbce-3df03aeaa5ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7840
        },
        {
            "id": "d60175d3-f620-421d-8d78-ceb2702f7623",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7842
        },
        {
            "id": "ca7aafe3-b3c2-4060-bb8e-b6c90a6e56e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7844
        },
        {
            "id": "40df9264-7d99-456c-953b-6274b06cf2fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7846
        },
        {
            "id": "af57f092-494f-4065-924b-818ef275dc5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7848
        },
        {
            "id": "b2d634d8-4781-4b41-82cd-5c24ca695caa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7850
        },
        {
            "id": "44fece3c-5e8f-4fdd-92cc-b9566d4658e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7852
        },
        {
            "id": "5577720c-cf76-4f00-afdf-4716eaf4604f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7854
        },
        {
            "id": "9be81425-9258-4e0e-a20c-fd8f3ec9298b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7856
        },
        {
            "id": "7ef98fa9-aaf4-4b69-8724-b04c80ba5fee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7858
        },
        {
            "id": "9ce761e5-41c2-4acf-96f1-c4f16c1e5b41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7860
        },
        {
            "id": "5bf141fa-0634-473c-9b98-9e7982af5f3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7862
        },
        {
            "id": "8b633997-9bc5-4774-8478-838cb32c52b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7865
        },
        {
            "id": "bc236e1a-2001-48c0-ad33-630b48037b1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7867
        },
        {
            "id": "b762613d-cc93-4383-9879-95bb267dfd63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7869
        },
        {
            "id": "b3c1c3ab-2704-4a8e-8d74-95db182cfa93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7871
        },
        {
            "id": "68825157-ce25-4044-8795-496475c917a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7873
        },
        {
            "id": "c7130cc1-1aec-45b0-a081-9fa92ed6e1d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7875
        },
        {
            "id": "d83c36d3-5517-40b5-80b6-30a0d241a324",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7877
        },
        {
            "id": "62f3bf23-2e4b-45f1-8d6a-78849f1bf39d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7879
        },
        {
            "id": "cb5955f0-b64f-4ebe-91e5-75ab7446fec5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7885
        },
        {
            "id": "5fb77b37-27fd-43f8-b9b0-59313da12fc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7887
        },
        {
            "id": "37e1019c-dcc3-49ba-9d08-96090edb0734",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7889
        },
        {
            "id": "5775ddf7-9a4f-4e92-89f0-1155b72d2af9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7891
        },
        {
            "id": "5379cd07-be6c-449d-9ecf-eefcd8dc43f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7893
        },
        {
            "id": "60edbfe2-640a-42e3-8f03-aeaadbe9f8dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7895
        },
        {
            "id": "0291111c-57b6-42ad-aa54-2bb1c0a0fbf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7897
        },
        {
            "id": "336f6cef-52f3-4e5e-a5d0-8abbd4b6e97c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7899
        },
        {
            "id": "47f5a8f7-7903-48ba-935a-f070e511787a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7901
        },
        {
            "id": "b7ee9cf7-94ba-41e5-b597-df54b1969571",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7903
        },
        {
            "id": "4ca3570e-7c6e-415f-9df2-6dcab59b79e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7905
        },
        {
            "id": "425e4f36-8c46-4cb4-ad65-f618457621ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 7907
        },
        {
            "id": "69b50f93-3727-4f3c-b84b-59d97b94305e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8216
        },
        {
            "id": "2996ec7d-76ad-484e-9a93-f0edffd5d6df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 8217
        },
        {
            "id": "769c2199-3e50-4244-9d1b-00ba7c4a2cf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "cc790963-7f68-4235-97cd-b8502ce78080",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "fa5b4df8-d473-406e-a7e0-dca551788a59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 99
        },
        {
            "id": "ec677f41-9d59-47c0-bc6c-f8aaa2f6b970",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 100
        },
        {
            "id": "d84888bd-9862-4463-9680-34a94a57da37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "ddb03835-e057-4d14-9f31-d83c3e2c60ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 103
        },
        {
            "id": "4df90d0f-9c20-4db8-a3e4-abb1ea88ea09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "595e3664-10d8-4e23-bd6f-811f597c19b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "7ac9cca8-0831-400c-808e-576ef50f477d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 115
        },
        {
            "id": "216b082b-51e0-45b3-b249-1d373c721b1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 191
        },
        {
            "id": "21e359ef-3c79-467d-a41a-03afdf068ce1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 192
        },
        {
            "id": "b25a5985-bb4f-4d4e-82b2-cfb50748d33e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 193
        },
        {
            "id": "3e21307c-637d-42e8-9c62-f7984065cb20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 194
        },
        {
            "id": "729420f2-6898-4fb4-8164-9ff233a4bdc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 195
        },
        {
            "id": "a2c2f8f6-646e-4e33-84dd-08864128c945",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 196
        },
        {
            "id": "bef47321-b931-42ab-93c9-7b41104428c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 197
        },
        {
            "id": "17e428eb-5cf7-4d3b-890a-df0ada090aef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 198
        },
        {
            "id": "3ea985e7-032b-4d77-be64-441b75933a14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 231
        },
        {
            "id": "50877cb9-3983-439a-8717-d4545626d5a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 232
        },
        {
            "id": "d06be988-a932-45bb-a04d-600c47a050ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 233
        },
        {
            "id": "0a3accaa-0f00-4a06-ae98-d5b5f5e4573f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 234
        },
        {
            "id": "b79c84cf-7998-4a51-b75b-18d6c80fc8ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 235
        },
        {
            "id": "1005212d-8b5c-46bb-a9bf-9cc929fc87b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 89,
            "second": 236
        },
        {
            "id": "85d1b2a4-a6f1-4b66-be1e-b4c1ad4a0e1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 238
        },
        {
            "id": "296f95fe-6abb-4c3f-9494-e379679bcb56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 89,
            "second": 239
        },
        {
            "id": "c5eaf193-f273-4437-81f1-c82e3d2153b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 242
        },
        {
            "id": "cec45e76-5d66-4ad7-ace2-004e25f8f2da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 243
        },
        {
            "id": "e16b004c-3d9a-49d0-8915-015113b52e43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 244
        },
        {
            "id": "bc992115-faa6-431d-8c2a-bbda0890931f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 245
        },
        {
            "id": "f16c95fc-6870-4ae3-9803-5741ff6f4e74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 246
        },
        {
            "id": "a0ecbab2-a95f-445d-aff7-4f6133cdb1dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 248
        },
        {
            "id": "f16c0815-6d14-4e6c-b020-2b939e5ae769",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 256
        },
        {
            "id": "b9875244-2a80-4296-9667-9eb5f9b88f44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 258
        },
        {
            "id": "fa604c79-e45c-476c-b7af-8a8b44f9fb24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 260
        },
        {
            "id": "f47e617d-29c7-4724-a091-533c439509d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 263
        },
        {
            "id": "b3556b84-d5b6-4fae-be0f-006a33ea7df1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 265
        },
        {
            "id": "dc398900-de6e-4fc4-b0c6-6d1b228c1f66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 267
        },
        {
            "id": "182c618c-a593-4a2d-9947-a23c19413d6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 269
        },
        {
            "id": "19ebfe81-04b1-457f-b9e9-04dae2287ed9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 271
        },
        {
            "id": "a23c0194-efd5-4b7f-9e1d-c4010853460c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 273
        },
        {
            "id": "5ebc6bdf-2aaa-4dc9-a000-a4c53069e1ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 275
        },
        {
            "id": "b5677fb2-9cc5-4289-bdee-9949fe0e0adf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 277
        },
        {
            "id": "91d62be9-7f26-4fcc-9176-0781c1d32121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 279
        },
        {
            "id": "1e8059f7-373f-44a0-ba30-433dde1bc6e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 281
        },
        {
            "id": "195081f7-16d6-464b-892c-1a1ea9a4a508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 283
        },
        {
            "id": "a6953ccd-816b-4bac-b8c3-8f43340d22ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 285
        },
        {
            "id": "c179055c-0c79-4387-b487-cbfad5bba27c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 287
        },
        {
            "id": "36169d22-fea8-4335-b41e-775286791889",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 289
        },
        {
            "id": "5fc142d8-1203-4264-9e00-71367ecedd13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 291
        },
        {
            "id": "32640a39-73c2-4624-82c0-9c02e76357bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 89,
            "second": 297
        },
        {
            "id": "6d639f54-5cc2-4be8-8be4-761cf5e68809",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 299
        },
        {
            "id": "a342b677-739c-4f84-8503-7794364b48c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 89,
            "second": 301
        },
        {
            "id": "6805b8a0-433f-4e03-9c62-af6d46c62f49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 308
        },
        {
            "id": "14c71851-4d41-41af-ba74-5468baef29a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 89,
            "second": 309
        },
        {
            "id": "08ca06b2-0344-4d21-adfc-93409cfa70e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 333
        },
        {
            "id": "9a271c74-50b3-4556-8d9e-003b99980253",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 335
        },
        {
            "id": "879aaa2f-35b7-4f23-8429-91b1fac6a1ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 337
        },
        {
            "id": "9afe9681-42d3-418f-ae0e-b636b02115a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 339
        },
        {
            "id": "879ea570-5e6f-4937-a42c-6d876889d3aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 347
        },
        {
            "id": "0fa8d7c7-e73d-43d3-8ef6-7e8c18d202d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 349
        },
        {
            "id": "eeac9e6f-758a-4266-96be-e96cec4ead26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 351
        },
        {
            "id": "964d7934-62d4-4742-9c6b-1ff77bcf3e8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 353
        },
        {
            "id": "2cc63bfc-7597-43b9-907d-400059f0407b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 417
        },
        {
            "id": "5179fa30-2719-4d31-bff9-3df6e0d76106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 506
        },
        {
            "id": "06e33bf0-5140-4973-bf97-0108cfc8bcfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 511
        },
        {
            "id": "15f0dc4a-ad4c-4f65-b97d-f2d0d3e03789",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 537
        },
        {
            "id": "07959897-b000-435a-ab64-98aaeed09009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 728
        },
        {
            "id": "8ffcf336-599a-4e4a-9252-d618f5b141c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 730
        },
        {
            "id": "dc32a7da-6f14-4851-bd42-66d39a7d5711",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 774
        },
        {
            "id": "2f0b084c-c147-4233-b649-e75d873f4447",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 778
        },
        {
            "id": "17cf3412-ed79-4e58-82b7-aa3d3d38eb33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7840
        },
        {
            "id": "9f0850db-e251-45f0-bf2c-06d7a68da5dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7842
        },
        {
            "id": "005b6f47-bf82-437f-bdb2-0572e6db4638",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7844
        },
        {
            "id": "d429dcd4-05aa-4325-bad1-f45eef7dc168",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7846
        },
        {
            "id": "cba9d6ad-9cb5-4eee-8474-5fa4174f11ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7848
        },
        {
            "id": "134277df-935b-454b-a8f3-b2b018579d9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7850
        },
        {
            "id": "fce5eeca-5ba5-4e24-a0ff-fcf24a0e6f41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7852
        },
        {
            "id": "361f73af-cd31-4565-9aa4-a8068313e134",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7854
        },
        {
            "id": "9e04ce2c-57f3-4654-8110-a3d6c9344883",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7856
        },
        {
            "id": "b3c5dfff-4e41-4000-8ecc-955525c8c179",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7858
        },
        {
            "id": "20417da9-9b46-408d-95a9-5dd551917fc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7860
        },
        {
            "id": "95857b27-c58f-4885-b904-ccdee0549ce9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7862
        },
        {
            "id": "50cb426b-d656-4e29-aeec-77abbd6f87c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7865
        },
        {
            "id": "8915a04e-6aa9-4b8f-ab70-3efd41a8343b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7867
        },
        {
            "id": "ad0b1278-9b6d-4138-ab46-6c5b5afb33a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7869
        },
        {
            "id": "748c7d05-3696-432e-adc5-ab6a4b96ae4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7871
        },
        {
            "id": "44407b25-d379-4e81-850e-74ba42b67767",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7873
        },
        {
            "id": "19118c20-cee7-44b5-9c57-0feb544e2d51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7875
        },
        {
            "id": "4b12b442-ac17-4e06-8f38-dc991a5f8fea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7877
        },
        {
            "id": "46cd275a-3ec0-4603-967b-d48c5a126d50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7879
        },
        {
            "id": "61853052-0665-478f-820d-587ade4f7532",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7885
        },
        {
            "id": "edda7a42-463f-4360-9f87-6c7244fbb6c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7887
        },
        {
            "id": "245ed0ef-8140-4b3b-bf22-388ade0be38c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7889
        },
        {
            "id": "5f7ff49e-7a0d-4be4-871b-008d5adf1310",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7891
        },
        {
            "id": "c9fc3f4e-b032-4196-9358-d629b566d4e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7893
        },
        {
            "id": "7b4e35ed-0633-45cf-9dc4-5662ace6224c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7895
        },
        {
            "id": "0367f9dc-81be-42c4-80e9-15778dd01b92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7897
        },
        {
            "id": "d130c3f0-0f0a-4348-a299-6a58b887ce3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7899
        },
        {
            "id": "8ed30343-49bb-43dc-951d-49b5883ad270",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7901
        },
        {
            "id": "f77d8da6-ea8e-40c5-a627-d7e5c6770f5a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7903
        },
        {
            "id": "8cc69389-5f12-4178-9647-67470920ed16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7905
        },
        {
            "id": "6b712246-27d5-483e-9081-b51c41483401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 7907
        },
        {
            "id": "12101890-991d-4980-af0a-4b8df055e3b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8216
        },
        {
            "id": "af013ab9-82ed-440f-bca6-045b6dbc5299",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 8217
        },
        {
            "id": "1743d3b0-fc6b-429a-8e9e-f8a3b892be37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 39
        },
        {
            "id": "bd12f46c-ea00-4315-81e6-c13ecea283df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 92
        },
        {
            "id": "989b4b95-46bc-4177-823d-453298036d6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 162
        },
        {
            "id": "150c0c63-f5ab-4f09-aedc-308c3de165f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 163
        },
        {
            "id": "459eca88-1c34-47fb-8ec6-db11fde0ecce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 92,
            "second": 8482
        },
        {
            "id": "f8f8099c-2152-464a-aab2-68e831d4aea5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 65
        },
        {
            "id": "7bf0535d-8cbb-4748-ad22-3e4e3686dbd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 74
        },
        {
            "id": "310dc23b-8ede-4093-927c-c24394111cf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 192
        },
        {
            "id": "a8bec02b-5788-46b5-95f9-2d23f1f0de14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 193
        },
        {
            "id": "8c06c711-fab0-42d8-8663-bbe3727b7ac7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 194
        },
        {
            "id": "cf46e7ce-4acb-4f04-841c-2fe1b5983b6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 195
        },
        {
            "id": "968b8b4d-7e2a-4beb-9393-f4479fbc5843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 196
        },
        {
            "id": "ad612b99-2a55-4984-82ec-168e5ebcecc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 197
        },
        {
            "id": "26a539c4-721d-4979-a326-c5487be9d1c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 198
        },
        {
            "id": "da2dc654-c75f-4f64-a931-05825bd6bf80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 236
        },
        {
            "id": "0dac8875-4d75-4943-8cab-a5a05fa255d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 238
        },
        {
            "id": "d7f70356-282e-480c-849c-17929012f3c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 239
        },
        {
            "id": "f3c9a484-46b5-4c5c-b709-aaba1c39adaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 256
        },
        {
            "id": "361b5d19-53a2-4904-8e4d-f841f43a5187",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 258
        },
        {
            "id": "260ee216-ee7b-4b57-8e93-ed92f22c7b90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 260
        },
        {
            "id": "e1382240-b435-4448-951d-d5258c554153",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 293
        },
        {
            "id": "63fa26f1-3baa-46cf-af70-eb45bbec5e98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 297
        },
        {
            "id": "b948103e-2938-4f22-a9a4-ebc9c58d2abe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 299
        },
        {
            "id": "cfe43629-343d-4e00-956e-5e1be8a5fcd5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 102,
            "second": 301
        },
        {
            "id": "b924b234-6c04-4631-8987-294140ef23b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 308
        },
        {
            "id": "e376384b-a5ff-4625-9d3f-9e6b3d097c15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 329
        },
        {
            "id": "35a652b2-45cc-422b-ad09-f0becbf2f91d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 506
        },
        {
            "id": "5a4e646d-60de-44fb-b333-85a3c6efb48a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 728
        },
        {
            "id": "00798877-abff-4f7f-a97e-0867335f77fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 102,
            "second": 730
        },
        {
            "id": "bca7bb72-7f36-4ea4-b1f3-ddd9b417eade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7840
        },
        {
            "id": "8c96ec67-d2a4-4a6a-9d4d-0271693bcb07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7842
        },
        {
            "id": "7f2830fd-6217-4fc4-826c-26251e1b7258",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7844
        },
        {
            "id": "002547aa-d62d-4afe-9fe1-483f880e40d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7846
        },
        {
            "id": "8c19feab-8ee6-4fb1-97b9-8e857cf0aae9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7848
        },
        {
            "id": "233d70b1-ee97-40f7-a26a-40d297990b71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7850
        },
        {
            "id": "5a360961-5f66-4d84-93fa-5e8821c9c558",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7852
        },
        {
            "id": "95d5c2ff-eb24-47a2-a0c9-e789b6282ea2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7854
        },
        {
            "id": "b6c8b17c-e55b-45e6-9056-f56e7e4115b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7856
        },
        {
            "id": "896f273c-9846-4af2-8dff-74b58c7fc5cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7858
        },
        {
            "id": "cca3c941-82f8-4947-8360-05f7903e4089",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7860
        },
        {
            "id": "732207f0-100b-4d3d-96d5-51d27164081b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 7862
        },
        {
            "id": "4140e5e3-2f40-4fcf-9cda-9c0eec056d71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 728
        },
        {
            "id": "46326fcc-1608-463b-9b95-b1f403e1b597",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 730
        },
        {
            "id": "cca1d2bb-84e2-4d0e-b76b-072172cb6e6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 728
        },
        {
            "id": "e249e51e-c5c7-45dd-96c3-330f990af75b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 730
        },
        {
            "id": "89b9248d-e39b-4767-ad0f-c843075bfc92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 728
        },
        {
            "id": "266440c1-33cb-4bc1-aec5-a991b10c6442",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 730
        },
        {
            "id": "c31fe5d7-a41f-4eed-a3e9-dea60697acd3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 728
        },
        {
            "id": "e9c81749-9ee0-446e-abe4-77f444e18e67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 730
        },
        {
            "id": "662399d0-36ae-4054-8e8d-9a791cdc8ce8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 125,
            "second": 728
        },
        {
            "id": "1a5593ae-93f1-4fe3-b1f7-366c00fe1e54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 125,
            "second": 730
        },
        {
            "id": "e6596122-f05c-440f-8f55-0f035deef8b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 125,
            "second": 951
        },
        {
            "id": "ee4b9554-1ae6-461a-9123-7b9bf011fbb5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 125,
            "second": 952
        },
        {
            "id": "9215a998-216f-485a-b7de-08bf91ced97d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 125,
            "second": 8212
        },
        {
            "id": "051f5778-b1e2-43b1-9e8d-3f9998908193",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 160,
            "second": 914
        },
        {
            "id": "898d72ff-b775-44fe-b7ef-0b9cf6fcf056",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 160,
            "second": 915
        },
        {
            "id": "09e5d4e1-a566-4253-930f-b31db09a2394",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 160,
            "second": 916
        },
        {
            "id": "c7c65914-0630-4fa1-ab82-22db77260935",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 160,
            "second": 918
        },
        {
            "id": "9bea445d-2f92-4505-b3a0-d7e33a43f37c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 160,
            "second": 919
        },
        {
            "id": "053d4a7c-694d-436b-bc4c-7324855136ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 160,
            "second": 921
        },
        {
            "id": "c155d8de-27d1-47a8-9a9f-b13ac6b05093",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 162,
            "second": 39
        },
        {
            "id": "308b0166-14fc-4042-a76a-c096403b5cf8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 162,
            "second": 92
        },
        {
            "id": "f63b047b-8791-4690-93e8-52e91c8eeb0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 162,
            "second": 162
        },
        {
            "id": "4a8d6923-e00b-476c-84b1-e4562804460a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 162,
            "second": 163
        },
        {
            "id": "489d6c59-8cd2-496b-abf8-04aa752c00d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 162,
            "second": 8482
        },
        {
            "id": "b455cc3e-50ca-411e-bde2-1385b7823904",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 163,
            "second": 39
        },
        {
            "id": "1719c278-ee72-4ece-8caf-4fc5e4256f44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 163,
            "second": 92
        },
        {
            "id": "901d81c1-9133-4ebf-8526-7d17d529cd1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 163,
            "second": 162
        },
        {
            "id": "06dc8b04-0398-4fb8-9f6d-72fbfcaf0e3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 163,
            "second": 163
        },
        {
            "id": "a855704a-800d-4365-8992-b864ccee4476",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 163,
            "second": 8482
        },
        {
            "id": "bb4558a0-ab33-400c-81d6-3813696b405a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 36
        },
        {
            "id": "401775e0-bf2a-4d41-b0c3-7d287918f4fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 52
        },
        {
            "id": "5798c7ed-bc7e-4157-ae4c-b1903d6b575e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 55
        },
        {
            "id": "888060d2-cfc2-4828-9e4d-417cb6c17499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 179
        },
        {
            "id": "d486b552-7176-4645-b9e5-5cc9ec6bde27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 186
        },
        {
            "id": "c75c4fab-c6c9-4b28-bc60-ba73a644c4d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 188
        },
        {
            "id": "8e95f947-53f7-46c7-8a76-b2873c752035",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 247
        },
        {
            "id": "102fdcac-5cbd-40af-9d4a-00009ec62bca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 778
        },
        {
            "id": "d3871e3c-5270-4ee9-873d-c78ea76d4060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 8310
        },
        {
            "id": "6da179bd-7c7e-49da-9ccb-095f684e2dfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 8314
        },
        {
            "id": "a753391f-7725-41cf-9261-25a56d070fa0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 8322
        },
        {
            "id": "c3d341cd-09fd-4411-91e6-6572715aea8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 8325
        },
        {
            "id": "79d817c0-84fd-4d24-87b1-36b72c4a266e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 8328
        },
        {
            "id": "bf523149-ae86-4b68-9072-783256f016e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 8331
        },
        {
            "id": "eb09095b-b3ac-4de9-945b-19c2b5e58893",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 8470
        },
        {
            "id": "52e31d01-8aeb-4c89-93f9-45e6b3dbf20b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 8722
        },
        {
            "id": "b34a6f63-fda1-4f02-ac1d-086311e8d224",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 165,
            "second": 8804
        },
        {
            "id": "e490274f-9e27-42ed-952b-f4c8082c7444",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 167,
            "second": 728
        },
        {
            "id": "5727b17b-5b3c-4c81-9b6a-f274298af26a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 167,
            "second": 730
        },
        {
            "id": "b8a1858c-67b1-4ee7-a8e3-fa878bcef8a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 167,
            "second": 951
        },
        {
            "id": "adc6fa62-9fde-4372-b82a-37eddb1f6e9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 167,
            "second": 952
        },
        {
            "id": "77f27fbc-dfac-4cb2-bba4-b1fe61093b52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 167,
            "second": 8212
        },
        {
            "id": "82752b9f-718e-4177-8245-ace13dbd8344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 35
        },
        {
            "id": "702bd680-af62-4d75-ac72-08cb7501f6e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 36
        },
        {
            "id": "b17a958c-1940-4e50-b1a6-be9f3b5ea993",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 37
        },
        {
            "id": "6cd725eb-3185-497e-9078-d33fd79d5880",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 169,
            "second": 43
        },
        {
            "id": "01f71adf-b8aa-4bd8-8991-8d71df052b28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 48
        },
        {
            "id": "cb04b824-16a0-40dc-ad22-c71b11113efb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 49
        },
        {
            "id": "064b3189-ae55-4ae1-ab51-b4b1eeb2a44a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 50
        },
        {
            "id": "e3b90dbe-fe32-40fc-9187-67032e80d35f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 51
        },
        {
            "id": "1991a66b-9677-41d7-a2f3-1a95998c5b24",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 169,
            "second": 52
        },
        {
            "id": "795c5d2a-bb23-4985-b3bb-2a6fe113119d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 53
        },
        {
            "id": "71356dbd-6270-4f5b-9e47-c4d70b6b5124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 54
        },
        {
            "id": "5f01b331-c536-4a06-8af9-afa590465042",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 55
        },
        {
            "id": "9a8f8613-7f91-494b-9c21-18273ff07dc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 56
        },
        {
            "id": "2474e2f5-1e10-46b1-b4b0-f64fdfca91fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 57
        },
        {
            "id": "202ae282-f656-4de6-b3ab-60824ccbdf8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 58
        },
        {
            "id": "5d7d26cd-6fb9-45cb-aa92-84534ab17122",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 60
        },
        {
            "id": "a43b181a-0d55-4d00-9b10-0b777a66fcbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 61
        },
        {
            "id": "d7ff5fc8-4bd1-42ba-98f6-09a085d9b8fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 62
        },
        {
            "id": "68176ce6-f5db-4cd9-8a8b-4909a01e0fc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 170
        },
        {
            "id": "2a0c07d4-7a30-407e-aab5-c665407a877b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 177
        },
        {
            "id": "e65eb226-6ec7-42d8-b6d6-ed4e9b6a8d83",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 179
        },
        {
            "id": "c33d4b97-8311-4a86-a387-f269c89b639c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 185
        },
        {
            "id": "a6bf1cfc-5ab3-46b3-be86-cc9aeec44d00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 169,
            "second": 186
        },
        {
            "id": "1d3499b5-ee70-4346-9046-d0cd49fc01d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 188
        },
        {
            "id": "10e173c8-a27e-4863-88ae-be8affa4b074",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 189
        },
        {
            "id": "99a864d7-dccd-4003-98fa-11c04f7d28cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 190
        },
        {
            "id": "bbcadc0c-9c2f-4b67-8280-d644d7948056",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 215
        },
        {
            "id": "6946254e-ca86-4ccc-b37e-ab3742813e13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 247
        },
        {
            "id": "e64dac1a-686f-4c13-9db4-f901e5c27f9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8260
        },
        {
            "id": "ac83be0a-a153-4c98-8fd3-91fffa259196",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 8304
        },
        {
            "id": "ff655203-b9e7-492a-bccf-8994b149c931",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 8308
        },
        {
            "id": "4aa56993-94ee-4621-aef4-3683c007951b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8310
        },
        {
            "id": "2c8c395e-de44-4007-b0a4-002d120b88f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8311
        },
        {
            "id": "81694734-f11f-44d0-8ddd-9da34cb00b21",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8312
        },
        {
            "id": "2fa64eed-6d0c-494f-aa7a-fe863ae5dd6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8313
        },
        {
            "id": "c414fe93-ce4e-4180-9a40-8ba37af0934e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8314
        },
        {
            "id": "2b51f158-70e2-465a-a324-69d3c751f0cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8315
        },
        {
            "id": "07dcb35e-b977-40da-9052-e2dce07b83f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8316
        },
        {
            "id": "414ea1a7-c27c-47e5-8219-a7b549ec8105",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8317
        },
        {
            "id": "cf827c89-8d2f-4400-8fc5-6dd8d346bd07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8318
        },
        {
            "id": "91db978d-c06a-48fc-9aa2-b96376867b58",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8320
        },
        {
            "id": "5d3c46b7-c9bf-4b74-a61a-8f70b0d8b794",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8321
        },
        {
            "id": "893dbb7a-0fbd-4365-9f83-84976c40b722",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8322
        },
        {
            "id": "b01fd93b-a6ac-4b6a-a47e-2bc72e50f0fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8323
        },
        {
            "id": "7c62f3d6-1481-4483-84df-4e57d71a5d89",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8324
        },
        {
            "id": "0b528ed4-2cb4-47f4-9fd1-8a0f8ca45e98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8325
        },
        {
            "id": "66cf55ac-d309-4240-b9dd-53f34750b160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8326
        },
        {
            "id": "e8af1659-b092-4ae4-9e97-a3e73e04dd2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8327
        },
        {
            "id": "fc50a29c-5ff0-40c5-b35e-63d36074221f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8328
        },
        {
            "id": "f23a08c7-bc75-4baa-85ca-24cb55193d35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8329
        },
        {
            "id": "fe4deb91-a210-47df-a004-00324c12a3f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8330
        },
        {
            "id": "f11e31aa-2eb0-4af8-b10c-673c7bde839b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8331
        },
        {
            "id": "0ea954ee-4244-44b8-8f22-36613a17980b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8332
        },
        {
            "id": "a2fb87f2-67b0-41d2-81b4-b0bc8e15a402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8333
        },
        {
            "id": "8e21e85f-ac12-4755-adff-326a2ee4196d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8334
        },
        {
            "id": "4caf8bab-3337-479f-8d79-f4673ca69ae9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 8470
        },
        {
            "id": "490aad80-9bb6-4a4f-b29c-21ee2343dbe8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8722
        },
        {
            "id": "701c1cf2-80c6-46b9-a434-d3ded9c94e9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8725
        },
        {
            "id": "62cbc9b5-b418-4cd9-ae8d-50382f0427af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8776
        },
        {
            "id": "e01bae2d-8ddf-4090-87b3-9eeccdc000bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8800
        },
        {
            "id": "be2d87ef-4f64-422b-bdd5-f356e1b0aed9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 169,
            "second": 8804
        },
        {
            "id": "aa8e7e52-086a-4fa2-9380-9e86254da33a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 169,
            "second": 8805
        },
        {
            "id": "cbf14549-f69a-490e-9f55-3a83b4ff924e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 39
        },
        {
            "id": "616031ef-2bb2-4792-bf4b-aa99a5548beb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 84
        },
        {
            "id": "49f60cfc-f3c9-421e-be2e-94bb2d29eadf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 89
        },
        {
            "id": "c283fdad-20cc-4684-ac33-dd9d12b4f336",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 92
        },
        {
            "id": "bcc27013-bd7a-4f4e-bec2-adb2683e29b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 162
        },
        {
            "id": "3837f91e-3db0-4f9d-a1ee-1d5ebc925023",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 163
        },
        {
            "id": "40022ee5-90d0-4121-ba61-9e2cea1b72c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 221
        },
        {
            "id": "3a2b6147-5941-4d08-a67d-eab02326981a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 354
        },
        {
            "id": "d77ff7af-c66d-412c-a184-e567a9677ea1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 356
        },
        {
            "id": "fae716db-b7e2-4070-84e1-988429acc5e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 374
        },
        {
            "id": "5d63d0fd-c4f6-46cd-a8d1-65d8357d3053",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 376
        },
        {
            "id": "fbbb9f91-590d-4dd0-b03f-04ed7ea52c5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 538
        },
        {
            "id": "39333c4b-fb39-45d5-b3ff-e8e73e85c856",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 7922
        },
        {
            "id": "e8451dda-62e4-46f1-a7a3-15ee919ee5b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 7924
        },
        {
            "id": "6d8017ba-7e0f-4870-b484-38e002a2f667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 7926
        },
        {
            "id": "f289f2f3-ce1a-4af7-8fd4-37d56aa28a36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 7928
        },
        {
            "id": "e6bc5a0e-e932-4889-b480-41cd8e499564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 173,
            "second": 8482
        },
        {
            "id": "5bdb9fc2-5cee-4f26-84dd-ee7fa2a17a0a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 35
        },
        {
            "id": "6a0dab22-137b-4040-83ae-c7b94f087539",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 36
        },
        {
            "id": "74699330-b6b5-47a0-a501-48f8be06a326",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 37
        },
        {
            "id": "06066ce7-dac0-49c8-8d11-e23b6b2b035b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 174,
            "second": 43
        },
        {
            "id": "5d895b7a-b128-4809-ad71-d8895dbdc173",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 48
        },
        {
            "id": "3a104ebb-6d05-4ba0-b9e5-ef955ef15a7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 49
        },
        {
            "id": "a313f669-c59b-490e-b812-7a719c11256b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 50
        },
        {
            "id": "4a6fbdc0-c94b-404d-9fb9-af76b812e43e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 51
        },
        {
            "id": "4fa7ca68-5adf-4da7-92e9-389143f68eb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 174,
            "second": 52
        },
        {
            "id": "b07cbe72-dacf-4589-8d59-5d96d6f66874",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 53
        },
        {
            "id": "0cd4aaf9-2ed0-486a-9ea0-6049deb24b8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 54
        },
        {
            "id": "15f02f90-a4b4-49ad-8d7b-c1ad729012b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 55
        },
        {
            "id": "be090b94-66a8-419c-9c43-663d838924ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 56
        },
        {
            "id": "c353809f-fb3d-4992-8d1c-4d0699099e14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 57
        },
        {
            "id": "b189890d-5f8c-45aa-93ad-6c8d9f72da71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 58
        },
        {
            "id": "9d505f71-eef6-47b3-82d0-e97579f0de2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 60
        },
        {
            "id": "c8ccaf63-1d6f-4b6a-9b5a-e5969627b358",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 61
        },
        {
            "id": "c09be02f-38e6-4988-9099-e37e8d7e93d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 62
        },
        {
            "id": "cfd1ada9-e45f-488e-914c-62be98acea05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 170
        },
        {
            "id": "17ddbf6d-143c-4e92-9be2-d5541fff263a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 177
        },
        {
            "id": "8086d12c-4212-48a0-a2d5-511fc9d96e63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 179
        },
        {
            "id": "62539066-04bb-4f98-af93-1b1ca863383d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 185
        },
        {
            "id": "ddb7bdf0-93d2-4065-82c4-b355e64fcf03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 174,
            "second": 186
        },
        {
            "id": "76f82580-5b54-4531-99a5-2f2eaf6d3bf0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 188
        },
        {
            "id": "88708fbe-2de3-4ce3-a12e-dce5cb1c77df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 189
        },
        {
            "id": "32743ddd-7ca5-4bf7-a20a-d1ea91118067",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 190
        },
        {
            "id": "4ae31823-16bf-4ff9-9e9c-6bb8ebd8461e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 215
        },
        {
            "id": "ee7d87c9-8f41-47a1-b814-6b179646af14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 247
        },
        {
            "id": "37ebeb20-7f7b-4288-8bef-8fc2dbd5dcc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8260
        },
        {
            "id": "55585a0f-7305-426f-bf53-d912bb055f27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 8304
        },
        {
            "id": "6792a630-f240-479f-9efc-1121337e7ab8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 8308
        },
        {
            "id": "7c9684d0-c0be-4c28-8c0a-f6c3393bfb44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8310
        },
        {
            "id": "a86a6c14-77dc-4648-b83e-3330e92cb2a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8311
        },
        {
            "id": "04aae78c-fae9-4fc6-bb2d-c830f0c4e78c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8312
        },
        {
            "id": "be8534a2-d3ce-422f-97ad-53a4d09090f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8313
        },
        {
            "id": "05268225-4767-47ef-aef2-03ef84320989",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8314
        },
        {
            "id": "1f2e6bba-3a52-4f27-b403-429d40725bdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8315
        },
        {
            "id": "1614b4b1-ec48-482b-bc50-edc80a312e5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8316
        },
        {
            "id": "0acdb236-7425-40f5-8ba9-34aa1f4e231f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8317
        },
        {
            "id": "0e55efee-262c-48ea-b02e-9f787444bda6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8318
        },
        {
            "id": "57b8cadb-0cd7-4bd2-b140-7745be1b35dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8320
        },
        {
            "id": "578e83d3-1f9b-46e4-a77a-f3656f10c890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8321
        },
        {
            "id": "4a015eac-f1e5-4d60-bcaf-c3e1d758f082",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8322
        },
        {
            "id": "ca57c26f-95a2-45b1-be8c-e2b0f6d7ec6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8323
        },
        {
            "id": "fa8ea9bd-2aac-4200-95ef-80390d97e355",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8324
        },
        {
            "id": "7ad7e41d-7976-404f-8e70-46c3626b715d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8325
        },
        {
            "id": "2e93ba02-5415-4d63-8ef4-cf3680705e90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8326
        },
        {
            "id": "28744f82-b923-4521-9100-da518b013bce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8327
        },
        {
            "id": "f5ab8c2a-6532-42f9-be35-461037fd91a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8328
        },
        {
            "id": "a7d74c7c-3cd8-49bb-9ca3-cdbd770a6d3f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8329
        },
        {
            "id": "e731e122-fa3e-44fd-9fc2-1a5bbf9a01bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8330
        },
        {
            "id": "9e5aad56-901d-429f-bea0-4cbe02db81ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8331
        },
        {
            "id": "ae1e64fe-532d-421c-b213-6081ff1908f4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8332
        },
        {
            "id": "70fe43da-eefa-497b-b9a5-069bcd8e90cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8333
        },
        {
            "id": "31b740ef-4df7-4d56-bf69-27205c640c84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8334
        },
        {
            "id": "6fc68870-6d91-4df2-a36f-9ef04b453fca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 8470
        },
        {
            "id": "f06a1d82-92c1-4f34-8583-87a24de54eab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8722
        },
        {
            "id": "6a6b2912-2390-4e40-bab8-84f551bb4628",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8725
        },
        {
            "id": "a0d22880-a34a-4c1e-8b17-7ddf4cb54f3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8776
        },
        {
            "id": "153950a3-dd70-416f-9de5-20e56ca19c53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8800
        },
        {
            "id": "57766d2d-bfe5-4f0c-aea5-143a7c642807",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 174,
            "second": 8804
        },
        {
            "id": "987d94a7-6ef6-4acf-8742-ffc081a1c73f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 174,
            "second": 8805
        },
        {
            "id": "9728b863-d20d-4a6e-909f-6a6d3088d17c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 184,
            "second": 36
        },
        {
            "id": "0f1e9df9-eed5-49b2-87b5-669d05ea7481",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 65
        },
        {
            "id": "11c95f85-7f79-48d3-bb61-9fb46d96ddbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 192
        },
        {
            "id": "31f5635e-e37b-404b-9ab1-664f6827829c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 193
        },
        {
            "id": "ba091f77-972c-47c9-aa0e-555b0acf5bf6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 194
        },
        {
            "id": "02a20848-0acf-4f51-afba-17411808aa40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 195
        },
        {
            "id": "fbfe2685-e1c9-4f35-b3b2-075281f586e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 196
        },
        {
            "id": "41bbeab9-fa93-4dba-8ad4-981ae24939e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 197
        },
        {
            "id": "4e8ebfaa-fac2-4751-be17-5eeb5baa7c5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 198
        },
        {
            "id": "68ffdaa2-b112-4797-8123-d9d5ca2b4c54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 184,
            "second": 247
        },
        {
            "id": "41d59273-6617-4c10-bca8-19886b6c0b92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 256
        },
        {
            "id": "5a06ec7b-822c-4c93-a03c-bd8e571b214a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 258
        },
        {
            "id": "b1774e09-64ce-4cd0-b74b-39bd3bbdeff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 260
        },
        {
            "id": "229c9cad-c695-4ada-9df4-4585b7108c4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 506
        },
        {
            "id": "3f1c4d66-c561-468a-98a4-295587b48b0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7840
        },
        {
            "id": "1741eb72-cba1-4021-9f79-d24b6a59c2fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7842
        },
        {
            "id": "3abc6da6-935a-4cad-b2b9-b6380250c5af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7844
        },
        {
            "id": "df8bdcc2-a3ca-4bd0-b675-38a7e5d5def9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7846
        },
        {
            "id": "68f836e3-57e0-4906-b4f8-3aa7bdaa4215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7848
        },
        {
            "id": "2b901d82-20e6-4c39-bacc-6a93452368c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7850
        },
        {
            "id": "be073b33-bbe4-44cb-9039-8855f12eb61b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7852
        },
        {
            "id": "b097ca80-7da8-481e-96ec-2f86a37236a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7854
        },
        {
            "id": "db780ffa-f7d3-473c-aece-66edbb0e0f68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7856
        },
        {
            "id": "596d8e78-8375-4d64-91ea-b2333e954afa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7858
        },
        {
            "id": "3b5b3787-c306-49fc-8481-a7018e6348c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7860
        },
        {
            "id": "f33240e9-9143-439c-ae98-bea241489d8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 7862
        },
        {
            "id": "c84c599a-d300-4837-ae6c-6d7f01ea99a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 184,
            "second": 8212
        },
        {
            "id": "d25ebc89-5097-4d9a-a8fe-f96e9905bf4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 184,
            "second": 8310
        },
        {
            "id": "87e67800-c038-4cab-af73-201ae39024fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 184,
            "second": 8314
        },
        {
            "id": "e7f752ae-205e-4075-9ca7-2caaaad1d1ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 184,
            "second": 8322
        },
        {
            "id": "dbf26fd4-adce-457d-b5c6-ed9683385d70",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 184,
            "second": 8722
        },
        {
            "id": "c00cc865-4251-454a-a42d-a0eb9a627634",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 39
        },
        {
            "id": "daaec241-686b-42d1-88a7-a39809f534ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 42
        },
        {
            "id": "7050218f-54e7-41bb-b3dd-c6e782b09854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 84
        },
        {
            "id": "12742b3b-d17a-4a8b-baf5-4c27ba92703d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 86
        },
        {
            "id": "691955f8-4e69-46fd-84ce-26fe2def3ccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 87
        },
        {
            "id": "c2887cfe-531b-4a76-80a7-a82a6c121312",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 89
        },
        {
            "id": "c25f07e1-1ecb-49bd-bd71-64275d856dfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 92
        },
        {
            "id": "071c69f2-08fa-442c-bd47-f0a080df92d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 162
        },
        {
            "id": "603ab4c5-508e-40f7-a6bd-dda526fcbe91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 163
        },
        {
            "id": "c1d6c12c-c00e-4f48-842b-916c47380a85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 221
        },
        {
            "id": "4a4bc6ff-7f3c-4488-8456-880961bd672e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 354
        },
        {
            "id": "de539f70-39e9-44ec-8335-dba201e37e74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 356
        },
        {
            "id": "89a05d97-6e45-4af8-99e4-46476d525025",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 358
        },
        {
            "id": "13631fc4-9ac8-46fe-9aa8-f9950d3758db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 372
        },
        {
            "id": "2848a6bf-8c0c-484e-9701-4bd3cb61302d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 374
        },
        {
            "id": "87a87209-c9e0-49b6-a030-6e36d9e5ae87",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 376
        },
        {
            "id": "d5721640-1d18-4381-9821-6d0e94febee3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 538
        },
        {
            "id": "22e54643-04e0-47ae-bfe6-de211e82cc14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 912
        },
        {
            "id": "6011fa00-757b-42de-a4ec-97531ca4caea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 920
        },
        {
            "id": "8d98717e-1d78-417a-94b8-fa3b232187dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 970
        },
        {
            "id": "8b2d925d-ec55-4bb9-8973-92890ef02295",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 7808
        },
        {
            "id": "a7e755c6-b38c-4e4c-98b5-eb986ee06f4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 7810
        },
        {
            "id": "0385e041-303a-47fc-aa87-9b4f90b30579",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 7812
        },
        {
            "id": "dd5ffc79-6cd9-4a94-85a9-b04c4eed0994",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 7922
        },
        {
            "id": "44c73d16-820b-40cf-b0fc-b7b03bc9227a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 7924
        },
        {
            "id": "eb2e9176-b357-45e9-8a94-3928877608a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 7926
        },
        {
            "id": "5b387de4-e41f-44a0-946d-669f44027b15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 7928
        },
        {
            "id": "274e488e-ff10-44c9-9207-f9a6438367de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 191,
            "second": 8482
        },
        {
            "id": "e29a6c27-2945-4ba0-baae-7705f560091d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 84
        },
        {
            "id": "a4aff986-b16c-4479-bfca-9d517aeaa8cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 89
        },
        {
            "id": "552ab2ad-0598-4ecc-a3eb-24960dfb51b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 184
        },
        {
            "id": "9f311657-b9b3-4da7-b8b3-803724ed9d80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 221
        },
        {
            "id": "cfcf2257-7d42-4223-b75c-62341b6e5eb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 354
        },
        {
            "id": "a3575561-c519-4066-8dda-0377953a15d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 356
        },
        {
            "id": "eba87695-6bed-427d-b3d5-652a081e96cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 374
        },
        {
            "id": "dc9fc9b0-02cf-426d-9228-97029459e4fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 376
        },
        {
            "id": "06e30eab-0ac2-4082-9f00-17521f6883b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 538
        },
        {
            "id": "13e3eb5e-79a6-4d6c-b706-e59515fb7a66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 731
        },
        {
            "id": "712cd7ed-e823-4460-a6c5-3b51abda70eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 775
        },
        {
            "id": "c70eac5d-ff18-462f-9d7b-1b28f31a833e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 806
        },
        {
            "id": "488afce4-b0b7-43c1-900a-eb5e432aca50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 7922
        },
        {
            "id": "62e473fb-6681-4dcd-b62a-0a8fffc42c45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 7924
        },
        {
            "id": "d2bb14a1-ffbb-474d-a725-5a8288328556",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 7926
        },
        {
            "id": "6ce426f2-32a2-43ab-abbb-dd81b1badf2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 192,
            "second": 7928
        },
        {
            "id": "eaa85ebc-18dd-49e9-a562-85cde947690a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 84
        },
        {
            "id": "49f3c217-9117-4a8f-997c-b59a864afbba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 89
        },
        {
            "id": "ad909b8f-fff1-4d57-af7e-ca309b172ad4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 184
        },
        {
            "id": "252a32ba-eec3-48e5-ac0b-597a68c1e6c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 221
        },
        {
            "id": "b82d7f02-29be-400e-9af3-31ef319cb8b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 354
        },
        {
            "id": "28b92fb8-7d24-4242-b245-76b0547cb379",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 356
        },
        {
            "id": "5c427200-8d69-4aae-a46f-eafee05a7de0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 374
        },
        {
            "id": "50e2e469-fa31-457e-8e8d-7a25791f6fe5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 376
        },
        {
            "id": "4d426adb-76bf-4b51-9e9e-16e33c12f0a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 538
        },
        {
            "id": "48eff568-c51a-4eb9-91d2-8dd0cb27c25e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 731
        },
        {
            "id": "52305eeb-a391-40bd-98e4-d499020c2c93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 775
        },
        {
            "id": "8c65b3dd-2a5c-463e-92c1-80595e28adbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 806
        },
        {
            "id": "06418ace-08de-48a9-ba2f-70ba562bec9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 7922
        },
        {
            "id": "3bf340b6-d092-4af7-908f-9446c3bb9072",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 7924
        },
        {
            "id": "03d0a537-7ad3-4cb8-8e5a-cb90e7add034",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 7926
        },
        {
            "id": "be6c7762-3d93-4d23-9e6b-c10b92a87942",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 193,
            "second": 7928
        },
        {
            "id": "bd943175-ef7d-4db7-92fd-8ee0294a2443",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 84
        },
        {
            "id": "34588544-9870-4eee-a197-15ed39867943",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 89
        },
        {
            "id": "d99f4d09-f8df-4bf5-bb45-d365e01e7ca8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 184
        },
        {
            "id": "a7cd01bb-c244-4ed0-b42b-c41fb5944175",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 221
        },
        {
            "id": "6ae5e80f-6105-4718-a742-0b042b58e4f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 354
        },
        {
            "id": "da188db4-ac97-4f09-852c-724e11c0cb61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 356
        },
        {
            "id": "fc6f6156-e3d2-4ac3-b992-6e5cfc19035b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 374
        },
        {
            "id": "f59184d6-5f50-4f98-b85e-94724a630bf1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 376
        },
        {
            "id": "829d48a5-3e50-480f-881b-40583866d367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 538
        },
        {
            "id": "5ed87c3c-1277-429e-b341-e2e51842616b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 731
        },
        {
            "id": "de688d8b-0207-485a-b14a-a3daf38793f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 775
        },
        {
            "id": "fbeb72aa-b67f-4bfa-89c0-9207336e46c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 806
        },
        {
            "id": "cabca923-42a9-4257-ab88-9c2f1db97472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 7922
        },
        {
            "id": "767c813f-de2c-406c-8c3e-8bb255152e08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 7924
        },
        {
            "id": "932c88c1-461f-4008-a46d-f4a0104704e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 7926
        },
        {
            "id": "31cb54d7-c5ed-415b-9d3d-9e8d48cebff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 194,
            "second": 7928
        },
        {
            "id": "78632276-a9c9-4c7e-8ec6-80960f157fbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 84
        },
        {
            "id": "df7c6bf9-a57d-483a-8ac1-63b679013f99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 89
        },
        {
            "id": "9923214a-988d-4743-9ef0-0b29f6663813",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 184
        },
        {
            "id": "f85b7ce5-646c-41ac-9a98-d9a2f6ad048b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 221
        },
        {
            "id": "ad0ddf26-1395-403a-b4ca-9c653f9a4695",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 354
        },
        {
            "id": "86a18512-ffaf-4990-a3b6-7a9f0e889e1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 356
        },
        {
            "id": "82084b7d-255a-4553-a2bf-c0be325e2e4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 374
        },
        {
            "id": "116ce11a-07ae-4e63-bb2d-3675513a332f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 376
        },
        {
            "id": "f0363b4c-0735-4d79-9910-23d31bda0884",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 538
        },
        {
            "id": "7c581057-6de7-4e3a-a7b8-9c509edfc2ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 731
        },
        {
            "id": "3db03001-e81b-4fb8-86dd-09d161f73d40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 775
        },
        {
            "id": "ca097621-ee05-4280-b81c-38a80eff8b7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 806
        },
        {
            "id": "0860705c-99c3-4531-b5b9-f612ccdf2148",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 7922
        },
        {
            "id": "ee09cde6-7100-4f26-a6dc-58427c7929d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 7924
        },
        {
            "id": "1a596330-8ae8-44da-838f-ec235549ca13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 7926
        },
        {
            "id": "1964482f-eaee-428d-8962-6cfef28e8877",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 195,
            "second": 7928
        },
        {
            "id": "cc93ea39-bb70-4f23-9f03-c672ce60a97c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 84
        },
        {
            "id": "970b08ad-cff5-4b28-9855-3ee74c5dec2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 89
        },
        {
            "id": "1681a2a1-6c07-4878-8a2d-26f6b87dd2c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 184
        },
        {
            "id": "ae8ce5e8-ce2f-4858-befc-3c7f43a4024d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 221
        },
        {
            "id": "2d3b934d-8b76-43e7-b881-afaebc5dc9dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 354
        },
        {
            "id": "b8c28140-3638-41bb-a65b-72b936f62f6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 356
        },
        {
            "id": "6e995418-75b4-47d4-961f-d2eb65a49a5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 374
        },
        {
            "id": "b1923abd-e500-4113-b6da-ad0a6132d231",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 376
        },
        {
            "id": "9d343466-e02c-4675-88e6-5057793c0ac1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 538
        },
        {
            "id": "d28ddf34-80a0-4d62-bdb6-9dd5591cbd47",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 731
        },
        {
            "id": "16f29159-bfa4-4dc3-a893-13503ba505ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 775
        },
        {
            "id": "15aec9b7-15af-4b5b-a2c0-fd725b996f6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 806
        },
        {
            "id": "d1d82ffc-22b0-4275-8b6e-64d012b61cb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 7922
        },
        {
            "id": "b5212aca-32be-4994-a500-df4603dee871",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 7924
        },
        {
            "id": "b60a60c6-a457-4c24-85f3-3dffa906d7ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 7926
        },
        {
            "id": "6afcfd80-b953-4c07-993c-07bb9c176ea6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 196,
            "second": 7928
        },
        {
            "id": "3249f298-b8a6-4d24-a9fc-1f5ca9833bf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 84
        },
        {
            "id": "91aad8f8-5946-4b38-8194-58cbed96a156",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 89
        },
        {
            "id": "de179e9d-ffe0-4bdc-88f0-bd2c19c36969",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 184
        },
        {
            "id": "7c5fad87-13c0-4ba3-b82a-a3068292b7de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 221
        },
        {
            "id": "52f5ec3f-585a-4312-ace8-a277ed97890a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 354
        },
        {
            "id": "c6d75786-9cd6-48bf-a135-ca4b14981484",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 356
        },
        {
            "id": "f056c6e9-cbc3-45f7-bf20-18b6088094a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 374
        },
        {
            "id": "107d6c2f-4d02-453a-9512-8d2bc20e11df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 376
        },
        {
            "id": "3c99b7c9-df14-4f98-95ac-a366ad1dda91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 538
        },
        {
            "id": "54ee89af-385e-430a-b00e-95089fb59987",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 731
        },
        {
            "id": "cd449086-3a7b-41ba-bc83-d25debe7ae7d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 775
        },
        {
            "id": "4b0d9b8d-92cd-49a6-96e0-cf66eb673ec7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 806
        },
        {
            "id": "2b153204-3595-4311-803f-b7b57f7ac4f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 7922
        },
        {
            "id": "8fd2be92-5b50-4248-91fc-e14ea17148fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 7924
        },
        {
            "id": "c63f9364-7401-44bf-a2ed-a3286d534532",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 7926
        },
        {
            "id": "66c38a53-8df4-4fa5-b058-2de4fe92f9a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 197,
            "second": 7928
        },
        {
            "id": "a41c17f6-793e-4590-bf73-9f572722d464",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 65
        },
        {
            "id": "ef5296d3-0fe6-443c-bbb6-5002274b6cfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 74
        },
        {
            "id": "2ece6c1c-9fda-43f0-8411-0922c0ee3e64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 99
        },
        {
            "id": "c95bad00-8df3-41fe-b19a-4ab6580ffccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 100
        },
        {
            "id": "b41d9a94-623b-4531-a966-f604cdf4abba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 101
        },
        {
            "id": "d2b08e81-7eda-4491-9d2c-930137983aba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 103
        },
        {
            "id": "431b920b-8a13-43e2-b1d9-3d992cf2b7db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 111
        },
        {
            "id": "26a3869d-6535-4873-876f-8fd3c00c8ae7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 113
        },
        {
            "id": "add25ddc-8235-4a4d-b28e-b1a082759836",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 115
        },
        {
            "id": "71465d06-3a2f-4948-b6e5-5c8492bd703d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 191
        },
        {
            "id": "b80da1b2-0c17-49cb-bdb0-43ced6b21b6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 192
        },
        {
            "id": "99538ebe-4e21-444c-a729-8328fa62e0d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 193
        },
        {
            "id": "c911257e-aa90-4f5d-b74d-0302774575a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 194
        },
        {
            "id": "717c585c-b666-4f35-b041-b1d7ec222392",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 195
        },
        {
            "id": "7d30de7d-b370-4438-bc18-f6b0e0f18cfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 196
        },
        {
            "id": "f84c1a2f-439c-4168-8ca4-205445e0acfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 197
        },
        {
            "id": "655fe83b-3083-42a1-8fdc-9a68a30391f6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 198
        },
        {
            "id": "c5fe6e4b-4b75-455d-b756-3543094a14a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 231
        },
        {
            "id": "70f0863b-195c-4c1a-8e06-311f64b90bdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 232
        },
        {
            "id": "79000204-92f9-4603-9168-09348020a0fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 233
        },
        {
            "id": "65d72ad0-33fd-4c1a-83b6-4c9174ae5b18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 234
        },
        {
            "id": "c7db064b-3bac-4ba3-9f02-ab48781205a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 235
        },
        {
            "id": "e99cf45e-9bdf-4245-b860-2ae2b55a04e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 221,
            "second": 236
        },
        {
            "id": "04990d44-e517-4e1a-874f-8856fb68474e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 221,
            "second": 238
        },
        {
            "id": "2fdb2e9b-24fa-41f1-89ca-b687d54b061b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 221,
            "second": 239
        },
        {
            "id": "f1c217fd-dcc3-47f9-b4ae-53a54cb4c78c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 242
        },
        {
            "id": "e130f154-3b85-49e4-96a3-7c8160e5bff0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 243
        },
        {
            "id": "17c064f9-794c-498e-9963-5752bc6cd215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 244
        },
        {
            "id": "f6ec853d-e15e-44e0-a24d-562558edb036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 245
        },
        {
            "id": "c31ffd56-89c4-4f9a-921d-0c8d82ef0c3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 246
        },
        {
            "id": "9420aa80-a1ce-4a59-881b-05375fb72585",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 248
        },
        {
            "id": "455ea331-a9da-45e1-85ac-b9cd69d5883c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 256
        },
        {
            "id": "95410145-06cc-4ead-856d-34777b7a8f5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 258
        },
        {
            "id": "4ea92ed0-6eb9-4335-bb18-29bf6ef390a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 260
        },
        {
            "id": "42e3c215-3b7b-4261-9c24-0666b8cf9e26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 263
        },
        {
            "id": "ab190331-6413-4fa6-ad72-61f8cc9b8381",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 265
        },
        {
            "id": "82c656f8-f09a-4476-9e26-874e91501e93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 267
        },
        {
            "id": "16050b1b-9018-4640-9870-142a5e587e6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 269
        },
        {
            "id": "25f50dbd-815d-44ab-a5a6-972550692ed9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 271
        },
        {
            "id": "b3ae4174-4512-425f-8a45-b5c68e627ad0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 273
        },
        {
            "id": "a7058e66-1b09-4773-bbdd-f8e3e9eea062",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 275
        },
        {
            "id": "75100d6a-e88a-4d84-9697-22f37660c663",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 277
        },
        {
            "id": "a9c930ae-2429-486a-8657-c41e50298232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 279
        },
        {
            "id": "1f5642ba-477d-4416-a14a-ffcc24d3c2f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 281
        },
        {
            "id": "fdfb84a2-8248-4735-808b-c7a4d17a7225",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 283
        },
        {
            "id": "d7d4f001-6585-459e-8db5-09923f0afe69",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 285
        },
        {
            "id": "0b2cd4eb-faed-4bec-bad7-cfd722d5d022",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 287
        },
        {
            "id": "d353f468-5494-4fe6-ab7a-78498400cbc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 289
        },
        {
            "id": "95ace625-6344-486d-a276-ddae9263a8f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 291
        },
        {
            "id": "d117e288-98f0-49c3-aa71-83238b17885e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 221,
            "second": 297
        },
        {
            "id": "9d84eb39-a9cf-4c69-86eb-939d7d35577c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 221,
            "second": 299
        },
        {
            "id": "ed3f46ff-6416-4302-9cbd-b2161f32dc38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 221,
            "second": 301
        },
        {
            "id": "1b78610a-a8c6-45cc-8614-3f74fe1479bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 308
        },
        {
            "id": "1b4b5faa-6acd-4e75-8f71-bb2542108506",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 333
        },
        {
            "id": "20a64c80-ddff-47e1-aa2f-60a270b41c6b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 335
        },
        {
            "id": "a01feff6-af47-41cc-8264-8f437f637eed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 337
        },
        {
            "id": "a6e670ed-3a88-4e63-83f9-67a4b89a6a30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 339
        },
        {
            "id": "367a92c0-09a0-4feb-baea-350bca4e7667",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 347
        },
        {
            "id": "2eb2d3ba-d094-4952-8bd7-bcac19e15567",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 349
        },
        {
            "id": "9a681e7e-416e-407c-8b12-17528c2fa8ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 351
        },
        {
            "id": "442808fd-6d1e-4a8b-9d3a-96ac5b9cb882",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 353
        },
        {
            "id": "1064f097-6ab5-4375-aa85-270de129145a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 417
        },
        {
            "id": "40cf4096-b84e-4ef8-b6a9-85567718e9fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 506
        },
        {
            "id": "34851f81-8f37-4852-862f-708088c90b19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 511
        },
        {
            "id": "c6fb77a2-2c58-4fa3-84cc-d2519f137dfe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 537
        },
        {
            "id": "e3bc3622-b78c-4bde-b828-b9eebd5b464f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 221,
            "second": 728
        },
        {
            "id": "501d7744-db49-4ad8-ad43-f346c5d05bf2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 221,
            "second": 730
        },
        {
            "id": "4ea76438-5691-4964-a318-a13732b3e3ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 774
        },
        {
            "id": "d70a5775-695a-4908-ba45-5fedb743dc4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 778
        },
        {
            "id": "7b35f9ae-59ba-4c55-9f19-9d2405a93121",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7840
        },
        {
            "id": "5d8a1511-2eaf-4c55-a12e-738129db9e84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7842
        },
        {
            "id": "8f7adb6a-d6ae-463f-8599-31bf93f1b456",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7844
        },
        {
            "id": "bb710e7f-408d-4ab1-b51d-8c352dd19fa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7846
        },
        {
            "id": "025a9bd0-0590-4187-8f8d-0ede67c447f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7848
        },
        {
            "id": "d1d98227-4567-4229-9612-83c58fbd0237",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7850
        },
        {
            "id": "ec873ef3-ec4a-40b4-8cc3-3c980d13206b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7852
        },
        {
            "id": "18959469-0449-4420-96e4-4d8d46c38608",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7854
        },
        {
            "id": "08ef13e2-60ab-44ef-af4f-ccb69435cc36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7856
        },
        {
            "id": "8833130a-d634-448d-9d92-3205a16347a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7858
        },
        {
            "id": "732406d1-b1f7-4c4b-b148-311908a8efda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7860
        },
        {
            "id": "5566a9e9-c98c-4ff9-929d-5a097148782e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7862
        },
        {
            "id": "8b4811f0-1cc0-45ee-98e3-3a49c7da66cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7865
        },
        {
            "id": "cc02fa53-e5c4-44a1-b0d3-0a61a7ec13d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7867
        },
        {
            "id": "3698cd88-b51e-4c1d-938b-b10b8e489b23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7869
        },
        {
            "id": "e1dcbe70-edb9-4d53-9da7-b245a49aec4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7871
        },
        {
            "id": "61439c08-f1ed-49df-b215-32fe101f4511",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7873
        },
        {
            "id": "234a8b9b-d2c0-4262-8df8-4b35740ab92e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7875
        },
        {
            "id": "8a027a38-1df7-4550-9f69-ff96d3a63297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7877
        },
        {
            "id": "343a1d00-306c-4fd8-9d0d-5aebd8f8e627",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7879
        },
        {
            "id": "ebbb7104-4cb6-4978-8a1a-541b8bd227d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7885
        },
        {
            "id": "acd209a1-048d-43da-93e3-84a2210fd144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7887
        },
        {
            "id": "600594b7-ede7-4edc-9fca-aa6d93d83a19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7889
        },
        {
            "id": "de83ffdb-244e-4b6f-a19c-6a96617b0bc3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7891
        },
        {
            "id": "8a70ecad-7e3a-4168-8dc6-2f7bbc89b96b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7893
        },
        {
            "id": "2a0d1870-62dc-4436-a572-c9eb9b5ad34f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7895
        },
        {
            "id": "7a849334-f7d5-4b1e-8b9a-f1266067c68b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7897
        },
        {
            "id": "dea12a89-a673-4df4-95f6-82e8a82fa3e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7899
        },
        {
            "id": "56e0f8e5-3bd4-4c1d-9b7a-22f5ce13d601",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7901
        },
        {
            "id": "04422e18-f397-4934-9918-4c63fd504ac4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7903
        },
        {
            "id": "9a73f75e-65c8-461e-9cb6-db132d39a1a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7905
        },
        {
            "id": "6c501459-11a6-4115-b396-5de804e3d9e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 7907
        },
        {
            "id": "7d8bcc40-ac3f-49c0-bf91-b289463bda05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 8216
        },
        {
            "id": "2b93e548-59e5-4834-88f6-a8774e44b44b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 221,
            "second": 8217
        },
        {
            "id": "19b25a31-7383-4cdf-b21a-80307cf50036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 65
        },
        {
            "id": "0b2ae7cb-c102-4b4a-8a44-e0aaaa6b5f0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 74
        },
        {
            "id": "3cfe5a6a-97b3-4269-96e0-b6a77dfccef1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 192
        },
        {
            "id": "b6924a61-eb42-47dc-9c11-ea51dbe36c2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 193
        },
        {
            "id": "54845a7e-eb53-46d0-a553-abefb6297fd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 194
        },
        {
            "id": "23f21a7e-4cd0-49ff-82af-a7e63e766c36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 195
        },
        {
            "id": "c9030591-9f78-4dc1-aeca-4afa961c87a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 196
        },
        {
            "id": "c701b9f3-b180-4b40-a4c0-1883ff426d6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 197
        },
        {
            "id": "bb1713ad-67bc-4a4f-b341-4f3fb4784259",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 198
        },
        {
            "id": "2abc0c8f-1e76-4f63-b299-cdcff89da8d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 256
        },
        {
            "id": "28040bbc-42eb-4ee5-b483-6ec9c74a4483",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 258
        },
        {
            "id": "d8b48d99-b537-4e60-b274-3ab9a60f8569",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 260
        },
        {
            "id": "416a3b1a-3e9a-49d4-b430-937c379009c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 308
        },
        {
            "id": "ec85f753-f379-4e57-aab6-831cfe6d2c52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 506
        },
        {
            "id": "2c2f46d3-033d-4ab0-97a1-76b08ac766fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 222,
            "second": 728
        },
        {
            "id": "6652d922-e2ca-4e52-bdb6-91a615783499",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 222,
            "second": 730
        },
        {
            "id": "450505d7-30e4-443e-80ad-7d43b9a86060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7840
        },
        {
            "id": "d36df43f-1bf2-432b-8ef0-424e267a34da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7842
        },
        {
            "id": "9a383dfd-ea3b-41fa-87d6-21847072a939",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7844
        },
        {
            "id": "8067ab20-cc7c-4b64-83e3-d682a38fd736",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7846
        },
        {
            "id": "c046b660-c7ac-4249-b317-a4f204d4ac1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7848
        },
        {
            "id": "8e4b95a2-9128-4a8c-a518-82c3bdd6f6d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7850
        },
        {
            "id": "c1dcf100-afcf-4f92-b820-9a173004e757",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7852
        },
        {
            "id": "6c6ddfcc-c4fe-4ca1-b27c-2634388e5826",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7854
        },
        {
            "id": "dfd060ff-d56a-4c0c-b3f7-7519a28ac139",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7856
        },
        {
            "id": "ac0645cd-d4b3-47b8-8ad1-4866fd8684af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7858
        },
        {
            "id": "e7233453-daf3-4de0-92b1-4e1b79c60cce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7860
        },
        {
            "id": "e9275540-8217-416e-baeb-dc38691ebf91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 222,
            "second": 7862
        },
        {
            "id": "054a9db7-fef4-47be-9ea7-12cd3a2f7d8c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 253,
            "second": 728
        },
        {
            "id": "d24dd9d6-7c15-4e3b-89d9-d677eec5bade",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 253,
            "second": 730
        },
        {
            "id": "bb90784c-eb57-4405-a5d8-2fc21b479c5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 255,
            "second": 728
        },
        {
            "id": "0472278c-e041-4c72-a6c6-1344d762ba0b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 255,
            "second": 730
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 22,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}