{
    "id": "f23eca60-7074-4cb7-b259-9045a3861f01",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_buttons",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "557eed21-a3c2-44fa-8a41-74f9d55c662f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d2335499-48bd-4a23-a0ef-597e1e179d6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 72,
                "y": 83
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "0eb30299-6035-49ec-a99a-8450b369f6b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 78,
                "y": 83
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "2b378d4b-fa55-441a-ba67-3950dba293ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 87,
                "y": 83
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "fdd95f2e-7a1e-4b88-b3c8-5596aaf60983",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 101,
                "y": 83
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "f54a8d70-1605-4391-b857-ccb7f5af7246",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 25,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 114,
                "y": 83
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "9b6e7f21-837c-463d-8ab5-2f89021dfc16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 133,
                "y": 83
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "ff621bf2-9421-41a7-b893-5dfa5ddb1caf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 25,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 149,
                "y": 83
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "e79f1b80-d096-4436-b017-51ef862cf1a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 155,
                "y": 83
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "c39f0662-5968-484f-ba76-f1b4919ec2ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 6,
                "x": 163,
                "y": 83
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "8bcc87a0-7320-4c20-885f-54aaa12af83f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 184,
                "y": 83
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "da3b5145-7b74-4795-af74-60691ce1b1c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 282,
                "y": 83
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "7ddda4bd-d765-4d03-9416-4ab46df62a73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 194,
                "y": 83
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "153301c5-9e67-4539-ad1a-5142a118d9a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 199,
                "y": 83
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "fc275c2b-2893-4194-98da-f8ff1416fc7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 208,
                "y": 83
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "83b82269-8ec4-43ad-a742-94a523a74b0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 214,
                "y": 83
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3bcd7b55-20ca-4aec-b7af-3efcff54bc71",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 222,
                "y": 83
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "dab0946d-a6dc-48e6-be21-f4220dc5df78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 25,
                "offset": 2,
                "shift": 12,
                "w": 6,
                "x": 235,
                "y": 83
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "3366b525-0a1a-43dd-b42e-3b28dd8c0548",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 243,
                "y": 83
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d0df7b3e-a26b-4d60-bf18-4f3ea631d4e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 256,
                "y": 83
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "8ff497bb-1430-4d80-9788-408a341dc002",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 269,
                "y": 83
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "0938c735-ed81-410b-aeb9-0573c7bbe2db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 59,
                "y": 83
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "4063694a-7ace-4df0-91a3-125db5c68e62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 171,
                "y": 83
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "acc48c13-4ff1-49f7-9756-3e0e3f892827",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 47,
                "y": 83
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "7e10f290-eccf-4819-a629-054baf156572",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 372,
                "y": 56
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "a9147a8a-23bd-4c23-bd79-3822834f130f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 257,
                "y": 56
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f8f340e0-d84e-4dad-83d6-6500a1fa14c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 270,
                "y": 56
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "5dbfe564-4e98-49a0-ae86-23fb8fc7ccb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 275,
                "y": 56
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "ca42de03-23bb-4c44-908d-e8074bc92815",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 280,
                "y": 56
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "20e49afa-ef36-4ca1-a52c-b9cc33eaba5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 293,
                "y": 56
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f442a05e-51a6-4acc-9a11-0bca61e9987e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 306,
                "y": 56
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "3b51cb80-8d8e-4cb4-b0f4-caf565f36c23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 319,
                "y": 56
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "81f197c8-db5e-411d-b8cc-0524313c972c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 25,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 332,
                "y": 56
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f3374f0e-936f-474e-ad21-81f1a1c2d756",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 25,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 354,
                "y": 56
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "265ca11f-532d-4930-8d3c-ff2ab38e47fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 385,
                "y": 56
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "33e67ffd-9ec7-4f8c-bf20-b7081e4a23a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 14,
                "y": 83
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "a80567e1-5d4b-4991-bb23-5f6ff87eb8dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 399,
                "y": 56
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "044b6641-d520-4256-8d39-c1309db9a0f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 415,
                "y": 56
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "e6595ba4-d563-413b-a081-e90bc1ce9188",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 25,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 429,
                "y": 56
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "7d17d6b8-6d00-49fa-9b88-6153fc77ad9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 442,
                "y": 56
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "fcc29a34-11c2-4a53-8adf-0af1803ccafa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 459,
                "y": 56
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "ad5f5bc5-3016-4456-8d1d-21fb376fd705",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 3,
                "x": 474,
                "y": 56
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6ecf0e53-8992-4474-8824-9ea590055056",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 479,
                "y": 56
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "78aa6835-08df-4431-ad2f-671e05a4477e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 490,
                "y": 56
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "199b6779-86dd-46c4-84f3-d947fc50d2e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 2,
                "y": 83
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "5d8db128-f5ba-4362-9007-5d8abbc60f5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 15,
                "x": 30,
                "y": 83
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "42f6d89b-cddf-4afc-b8bf-1629a90f4286",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 295,
                "y": 83
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "3f3f93e2-1041-44bd-a9c9-933e51d38b90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 310,
                "y": 83
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "7ccf9e17-1880-479f-bd34-99294b178540",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 327,
                "y": 83
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "371801ca-adf1-4897-ba93-ef6e31e97a51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 25,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 106,
                "y": 110
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "0aa3d1ec-c753-4f21-907e-36a5c8e7da78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 124,
                "y": 110
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "61721c2c-ab7c-4840-96a4-e691357a7e8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 140,
                "y": 110
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "39daacce-5a0a-4ded-bf7e-ba43f58a94ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 25,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 155,
                "y": 110
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "794119c1-63e2-4764-9601-884ea5d56fef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 170,
                "y": 110
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "8124b974-e388-4127-96af-57b356530b56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 185,
                "y": 110
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "05c3c596-11a1-47dd-9ee0-663e58749e1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 25,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 201,
                "y": 110
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "7fda5b44-dc0b-4f7e-ab07-605065215df3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 223,
                "y": 110
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "fe4e8782-51cb-427c-b9c6-51a74570eef3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 239,
                "y": 110
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "8be9ac37-ff78-4f36-a07b-461235b255e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 25,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 255,
                "y": 110
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "53f69137-f0bc-4ad8-a1d2-a019c96c7aff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 270,
                "y": 110
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "67422f48-1bf3-45f6-8064-9a1cc59d9b95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 277,
                "y": 110
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "e532d769-8844-4608-9caf-95bbbceb9ca4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 285,
                "y": 110
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6124a62c-06f4-4fff-b322-74a8e2152515",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 25,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 292,
                "y": 110
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "936f6a00-c062-4d25-86f2-628afff222aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 25,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 304,
                "y": 110
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "5ebdf2e8-8806-4e0f-b0c2-a861832bbb1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 319,
                "y": 110
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "c90598a5-cd48-4a5f-8591-152ac8f79bd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 326,
                "y": 110
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "7d9ada3f-d39f-41eb-bdbe-023ab781e329",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 339,
                "y": 110
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "ce24fcd0-2114-4671-93a8-eede731d838f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 351,
                "y": 110
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "e2af934d-6464-47fd-a07f-e71718c4c81e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 364,
                "y": 110
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "be566879-2ef7-4bd4-8371-de248c4e8e20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 377,
                "y": 110
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "95baedf0-58cd-4ee6-a5bb-c05655850133",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 97,
                "y": 110
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "789bca3a-25fd-4eef-bd43-1e3dfe39cd55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 84,
                "y": 110
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "7b75b99a-b7c1-4ff4-a38a-8368d0e40dd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 72,
                "y": 110
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "499ada98-08a8-44ad-9869-fd34cc0d13ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 443,
                "y": 83
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "395ff37a-5732-4d73-981e-acc5430104d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 25,
                "offset": -1,
                "shift": 5,
                "w": 5,
                "x": 342,
                "y": 83
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "ba40bfe6-3ff5-4bdc-be6f-508100937d4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 25,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 349,
                "y": 83
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "53f77d3e-d805-4451-82a4-687cd4d9bb6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 361,
                "y": 83
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "5921d44b-0f8f-42eb-81ed-1c4656fe84d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 366,
                "y": 83
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "2dab0e6a-e1c8-498e-9dd2-7200780b4208",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 384,
                "y": 83
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "4ab064e9-7d74-42d0-a41b-30ab14e4e50d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 396,
                "y": 83
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "2b6ff816-bad3-48c2-bc20-c04a689e11a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 409,
                "y": 83
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "dcde1ecd-533c-4090-8b73-30ced4ec1bbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 421,
                "y": 83
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "4ac00c22-ea84-4372-b48f-4d12eadd2307",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 434,
                "y": 83
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "731527c7-2811-4850-b204-89dd29c32032",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 448,
                "y": 83
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "b9d1a787-f8f1-4c9b-8860-00c268964d5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 64,
                "y": 110
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d90a27ff-8351-49c1-8494-4382a65782e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 460,
                "y": 83
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "174db4a5-b7c2-4fb0-b56c-14cacdd0bcda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 472,
                "y": 83
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "6234166f-32ab-42b2-a73d-1ad408201684",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 485,
                "y": 83
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "31aec417-708f-428c-a1f2-30cef298f8ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 110
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "294c7baa-3091-41f3-b435-c55183939c66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 15,
                "y": 110
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "e0c27b92-f761-4005-b340-58e444f2ea08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 28,
                "y": 110
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "93a4e776-f8ff-430f-8e1f-7a99715be0fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 41,
                "y": 110
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "64560d8a-a00b-453b-bb41-0571d388b68f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 50,
                "y": 110
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "028a187c-967d-49ca-b202-f9ff0e536b43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 55,
                "y": 110
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "e9ba7381-d2da-4524-ac3e-cf985279f9e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 243,
                "y": 56
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "b13e8e0f-9f1e-4c03-a5ff-4e120ec761f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 0,
                "x": 241,
                "y": 56
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "71923ecc-eb21-4de8-b7f1-ced6bbbabc8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 25,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 236,
                "y": 56
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "04a83478-6fcd-44c0-acfe-eb3167d8d671",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 83,
                "y": 29
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "cee82ea2-0dee-4fd4-9648-de65640e7fd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 348,
                "y": 2
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "0c4895fe-4feb-4a83-af87-c151ffdd14d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 362,
                "y": 2
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "aa021f96-9964-4ac9-bbbc-bc0a5bc980b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 25,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 375,
                "y": 2
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "61c8be72-2694-4039-966e-aab879b006b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 25,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 390,
                "y": 2
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "c28bb6ef-0d10-4d0b-83fd-749805bd8ab1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 395,
                "y": 2
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "cdc334b5-db6c-4c13-8b6c-da934b2f1d25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 408,
                "y": 2
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "044280f3-e0eb-4b90-8177-053cfe645348",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 417,
                "y": 2
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "6cd2ea90-b5b4-4365-adf8-e91d91925df6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 435,
                "y": 2
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "279f4058-bba9-4fa8-8987-c3b652f990c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 445,
                "y": 2
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "e15c0c63-09f5-44b5-8a1d-c70b556c1ed0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 464,
                "y": 2
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "d0504865-8a48-43ba-bd5c-c11052791c7e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 74,
                "y": 29
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "ed2856e3-d9dd-4e01-b5ef-01a2b0cec549",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 25,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 477,
                "y": 2
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "51eb159a-6641-4021-817d-796fc0efeab1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 25,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 495,
                "y": 2
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "29fadcb0-75b9-41cb-a426-5b6b3ac36028",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 25,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 2,
                "y": 29
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "d0afd9af-6bb2-4020-898e-9f1e581020b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 10,
                "y": 29
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "576a7bbf-c329-433a-8c02-f66bc9e74bf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 23,
                "y": 29
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "f50c4212-ca32-4881-8771-6c18bf999582",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 25,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 32,
                "y": 29
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "a678b9c3-e5f8-40c5-bfbd-2c22d97f653c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 25,
                "offset": 2,
                "shift": 7,
                "w": 5,
                "x": 41,
                "y": 29
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "b365892d-e0c5-4491-ad5e-5a3142f54986",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 48,
                "y": 29
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "62950e61-dc65-42c9-84d2-9fcf89a83dd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 12,
                "x": 60,
                "y": 29
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "6b2537b2-a7ce-43a5-9851-17827f0b8919",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 25,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 343,
                "y": 2
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "ba9cc3ac-611a-4a59-a01a-86fcb7c18e0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 457,
                "y": 2
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "6803be00-2dac-41f6-b35f-5a4f92c224e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 25,
                "offset": 1,
                "shift": 7,
                "w": 4,
                "x": 337,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "6e7f0c83-89d6-4bb9-8fe9-84465a456ccd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 25,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 165,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "6ae70037-66ed-48d2-8d90-e083eaf48e16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 10,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "50e9483a-67f4-4472-a050-d78fa5004434",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "82297da0-5cc6-4cdb-b4e2-8cf71e043ee1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 25,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "78510039-4c23-42f6-b63f-bc2b99a9b051",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 25,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 60,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "d20bbe87-86ba-42c3-9a5d-08ca33e4b87f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 25,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 80,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "b90d336a-7754-4725-aed5-81ec2b94513c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 25,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "d22ad711-82d6-4bf2-a2f5-5cb4a1ebce30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 25,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "4920d48c-bc3e-4a74-8042-13b56250aa98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 25,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "8ef1b9c2-14b3-48b0-8347-579aec4d3f12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 25,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 147,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "8a589443-a52b-44de-a5fc-30837f42e999",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 25,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 175,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "1aaa6235-9ee9-4dd9-9934-bc618d06c7ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 25,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 311,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "b08a0c2c-9904-43b2-ac89-b61ac030e96c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 25,
                "offset": 0,
                "shift": 21,
                "w": 20,
                "x": 193,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "b33160d3-ad12-46a6-a8b2-e72c5bda28f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 215,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "1915f510-b25f-4434-b156-6be57f8efb23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "73529bb7-9f58-4e92-bbb1-e28e7158d90f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 245,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "e2faff04-14d9-4129-801b-cf6ddb008391",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 259,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "961e312d-8ac3-468f-b2e1-4c1c4a32ffeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 273,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "4e2dba35-ddeb-4413-b5c8-296a32cccf40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 287,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "b31c1f6c-cc73-4361-b6c4-15e37868fd52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 294,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "1af0e3de-5397-4f63-a8df-b9a07f25f4c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 25,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 301,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "d8933da5-9219-4e3a-a6d8-64394d6800ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 329,
                "y": 2
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "53658aaf-47fc-46a1-93bd-850730e21622",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 25,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 95,
                "y": 29
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "639c4ecb-f139-4d21-8997-7d97b34bc0df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 377,
                "y": 29
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "cb250ad9-9a63-4c3f-85f5-c7ac2bd78b48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 113,
                "y": 29
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "d55bd78d-2856-417d-aede-b97d108df672",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 418,
                "y": 29
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "608f2fb2-c4e8-439b-aa47-ac9485a593da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 435,
                "y": 29
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "a7998f75-91ac-4161-8a54-56ac3fd1b924",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 452,
                "y": 29
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "25244702-a45c-415f-ba20-0349128f702e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 25,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 469,
                "y": 29
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "6a4b3b9f-b57c-4cd0-89d8-dc9c39b13436",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 486,
                "y": 29
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "e6893b9a-a1cf-4af2-83a7-925d2444e389",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 25,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 2,
                "y": 56
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "f8650b12-bd08-4ca0-9f75-a3f189801376",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 20,
                "y": 56
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "f03ab113-606b-4097-8825-b1dfe2c64306",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 35,
                "y": 56
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "b8f66f7c-a59d-4ec5-87b5-22952b36c211",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 50,
                "y": 56
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "d37a3df6-2a49-49b2-a46a-ddecf7eccb8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 25,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 78,
                "y": 56
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "e0dc23d4-f531-4d61-aaa4-033cfac4cfa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 25,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 220,
                "y": 56
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "d9b428aa-30d6-4f78-9d5b-a17df09ff31d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 25,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 93,
                "y": 56
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "617665fe-0be4-467f-9d54-05ad8b3db817",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 25,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 108,
                "y": 56
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "ad65f775-6fcd-4399-adc1-85e7d60fbc80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 122,
                "y": 56
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "75a74f75-ada3-4334-b7c5-51034fbe29ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 135,
                "y": 56
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "06e36f1e-a3de-4349-9947-39b469e47f9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 148,
                "y": 56
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "4370f5f9-e5e7-4a57-b645-5503c8f35e9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 161,
                "y": 56
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "cb428425-2758-4d50-93ed-c4bd228cc53c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 174,
                "y": 56
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "8051b1b4-cf7a-4296-a6b8-231b8a975070",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 187,
                "y": 56
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "3a6f09e4-b7fb-47cd-8f9b-70754008fb8e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 25,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 200,
                "y": 56
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "8a303c31-26f8-4025-afde-02176e7a56b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 405,
                "y": 29
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "a223fd50-edf6-42c4-9258-273c6d6cc19a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 65,
                "y": 56
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "894dc644-a29b-4731-9f48-76a6040966a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 392,
                "y": 29
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "658fc315-ac56-4315-9d62-9614206a566d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 226,
                "y": 29
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "45bfdea9-e05c-4a59-8981-482599ac231a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 130,
                "y": 29
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "cac98ca6-1da8-4fa5-9eb2-fcf28e55e6a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 143,
                "y": 29
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "10ec2081-6061-4b95-b039-58127b852a01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 25,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 150,
                "y": 29
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "79d87cbf-ef2e-48ca-8a51-9c75f0410f43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 25,
                "offset": -1,
                "shift": 6,
                "w": 8,
                "x": 157,
                "y": 29
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "7e870cd2-4b9e-4da2-8a81-be188f484a5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 25,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 167,
                "y": 29
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "310f3e19-f9f7-4088-a298-c3fe10c70232",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 175,
                "y": 29
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "faf637d9-171c-46e4-918e-900b1ee74f18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 188,
                "y": 29
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "55c0d128-4d08-49bc-9a52-2b5a39f82487",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 200,
                "y": 29
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "65efc977-7e49-483a-8c4d-1a9ae068bac7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 213,
                "y": 29
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "79bb3aae-2ed8-4955-b8c5-2b5454c92e14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 239,
                "y": 29
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "3cee200a-0fe2-4d46-8acd-6136e92c21c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 364,
                "y": 29
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "a2f621d3-80c4-49f4-8ff5-55dc2d033a85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 252,
                "y": 29
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "3a2780ae-107a-4f63-ad63-e3b8ac872c95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 25,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 265,
                "y": 29
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "2b7eb18e-1331-4f55-824e-d0bf58a3fd2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 25,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 278,
                "y": 29
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "f220bce9-de38-448c-a0bc-aad797b876a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 291,
                "y": 29
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "8f147835-ddb1-425c-afd8-325facfce23f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 303,
                "y": 29
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "b90ed1eb-0d7d-4428-84d9-3c973ba68382",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 315,
                "y": 29
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "ca97ca7f-7c46-434d-84b3-48626c2576eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 327,
                "y": 29
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "fdaf021a-b212-4374-860d-fdba40f24281",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 339,
                "y": 29
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "aa44411b-5e07-434f-bd96-2c46e9d5d341",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 25,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 352,
                "y": 29
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "03e47bc1-17f9-4ba0-b891-9fe082e56d52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 25,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 390,
                "y": 110
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "76724f2f-9211-4429-a87b-db6f34d3cd24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 25,
                "offset": 4,
                "shift": 20,
                "w": 13,
                "x": 403,
                "y": 110
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "a9117fe3-a0e3-403e-8f85-b6a437ad1afe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "d2b980f5-a634-475a-abd1-0aa4692acc31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "1f34e750-33d4-44a4-97a4-d8b5ef912425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "528c3dd6-d938-4573-982f-ed323379ab55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "aaa84745-f0a4-4612-8f6a-0ea8f9b6087f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "90faced6-e83e-4005-a625-bcdfa6ed3dfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "27a096c8-8431-4728-b46f-ef62f54f1e34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "da4a7594-79e4-4b96-8e35-0461b3aea4c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "cc6be03c-18c9-4306-9ac0-90d24bef4848",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "43a65d45-f87e-42c4-80f1-22887552a8de",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "f27b7891-7b95-44e2-8b96-d4b178c4d58f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "3d7a8d18-03c3-4548-928d-dbc6d4341e5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "c13a6a45-eb0c-4aad-88e3-484f3047ce66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "7504ce7c-4c86-447a-a7b2-4d2e434c89a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 44
        },
        {
            "id": "7e06dd07-799a-4afc-97df-ec63d001dfac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 46
        },
        {
            "id": "e05750a2-e2b8-4b9c-b0e7-beb235ad6108",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "33afd7ab-a63b-43c9-9ed3-76a1a2a03e61",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "95973c71-e310-4f59-99c3-4c1437c55cd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "f982a114-e95a-439f-ab75-0a83549b77f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "3ccc5354-2865-49b3-a753-e12943bc5e5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "1f59b02b-7d52-4491-85a6-444f73b4eb85",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "408f2c83-7e60-464b-abce-7ec87feb0ef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "0bea4cd8-003b-4ddc-9ca2-5c56d41dcd4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "eb4b75cb-9f19-4516-a605-e8f630b7deb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "106f582a-d8a5-46da-be8a-cad67873398b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 44
        },
        {
            "id": "02739bad-50f4-4895-a1ad-5bb8b635d589",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 46
        },
        {
            "id": "1f8fa045-694a-43ae-890c-7ffd7fcbbc4a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "84ba2b4e-b762-4406-9fe9-61c12fccb0bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 44
        },
        {
            "id": "3c68d585-56e8-4511-83b1-91666165ffb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "43cb82a6-93b2-4770-aa26-cf3670373aaf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 46
        },
        {
            "id": "72407e21-3623-4f73-9076-5dba558fd948",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 58
        },
        {
            "id": "b6700456-6882-40df-bdbb-7ce3ed52a665",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 59
        },
        {
            "id": "11a5c9d9-1c78-4746-b86d-d019e542658c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "55288c1c-2926-4c0e-8dc0-83158a94328c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "aa8c3d76-931c-4d68-81b9-ac7a8183d392",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "301f4a63-6ea6-4a52-87b2-feb287ff6044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "5a000bd5-b27a-427e-baa0-ed97657b9eb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "19b0ffbd-508e-461a-91cb-670db23e9b79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "21845bc5-fb0f-4a75-92d7-50ea188dd95d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "664004f4-e53b-4b68-95d1-1dd0afbf7f67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "80ecb730-135b-4c44-8629-a3d7919a8d59",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "63c2c9d2-be68-44f6-b77f-4e5ea523df73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "23b9d313-d6e2-4288-b59a-41f57045afcb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "0968212f-8d7f-4b6b-9222-36f2398016e8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "484dc160-f11b-4efd-a2f0-bd38f13ab57f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 894
        },
        {
            "id": "356fe5e8-50eb-4833-a033-38f82c42d570",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "7ad546bc-e7cd-4475-be00-73ffd9c1e4e0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "ce4c1164-31b9-4c8f-b2b7-b7c6a6182688",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "e3e9046f-8719-459a-b013-777d5e20b5c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "56b7de10-3568-42f6-8aed-05931bcd041c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "fd090d58-b188-4f34-a9bf-e1e5104998be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "d9ee87fb-1837-4f0a-8fc6-1408afb1179d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "9fd33aa4-5e3c-4021-a2ff-1c67470ed275",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "d7b7f117-3130-4ac6-92b3-d850832e09e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "bfd8a236-54e1-4929-9a50-4f018e3077d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "23b4d2db-796d-4867-9804-dff1900a99a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "c0a9c91d-294b-4fd2-b09e-4b659a258332",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "d45ef89f-a4f8-42dc-a9d6-457fed088529",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "c978d15a-6559-4de3-9b2c-b4220230004b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "a5c43f04-56e5-497c-88f3-0b93752e052f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "4e9260f1-a3b4-4482-9c0c-a28eb2111ed2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "e37e944a-539d-4e84-be80-6f48481e6787",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "5037c06d-8f2c-4944-9ed2-acb03316cae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "2bcf3a4e-d76d-48d9-8709-29cdcc8f05af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "f73abb52-139b-49ae-ac74-a17ae02ca41d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "ab03ef0e-4c07-4aa5-8882-7332fe9f83ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "02e2f913-07e7-4d10-9ecd-30e862958972",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "c84c663f-7563-47d3-99e1-14ffae090b7a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "6b8275f2-9e2a-4763-9383-c3211c9a344c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "7a865bf5-df6c-44e0-9595-cfe5573cf110",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "eb3ea487-d327-48ca-9ca5-7cf4269d1f32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "afc1c652-3f56-42ee-bf7a-f4db3cf0d012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "ccb17d65-e0e7-43a0-adfb-f9fa695fae11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "0d623849-a198-42ad-86a3-60873e7f66f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "5212d5ad-7202-426d-983b-32dceb958b9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "8fb93a0c-23cb-4332-874d-c36a68da00b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "510e74c6-c734-4379-8829-afce376e2472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "5cee577f-818e-42a5-aa55-950175b83114",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 173
        },
        {
            "id": "aab38004-17a3-4c7b-8db3-84b66f8a98a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "8a4e8d7b-996b-48db-896a-cc8c384e3def",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "78deda90-65b3-4bc1-a2ce-92e359117033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "19b42ffe-9d45-4edb-bf4f-fa0e8d207b7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "4e329f6a-704e-48a6-a15c-0e03a74de9d4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "96b38ad0-9efa-4523-bf86-3265fa3d3c97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "12349207-b417-4258-8146-61fb1a2492cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "e07eaf7e-783c-4f15-8532-9df22be6efd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "6ffa4ac0-e573-4ea6-8103-4269307a9c8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "810806c0-9c92-4495-aa76-95cfe1f1f509",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "fa874828-476e-400a-bec5-8342b6fa29db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 160,
            "second": 65
        },
        {
            "id": "943c4874-885b-48d8-a81b-34657123c3bf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 160,
            "second": 902
        },
        {
            "id": "de261e8f-f4ef-46fa-897a-9bd16b1c1057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 160,
            "second": 913
        },
        {
            "id": "2b2be6f6-ff55-4e5a-b9ab-5b6263411ab2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 160,
            "second": 916
        },
        {
            "id": "2bb2745a-4bd3-46e0-9a08-de8432f3e750",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 160,
            "second": 923
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 16,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}