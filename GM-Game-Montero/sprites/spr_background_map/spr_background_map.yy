{
    "id": "9f337c56-226e-4b2e-ba81-b1eb950efc60",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background_map",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1799,
    "bbox_left": 0,
    "bbox_right": 3199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0d6eab0e-c0f5-44fc-982d-445aeedb1a3a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f337c56-226e-4b2e-ba81-b1eb950efc60",
            "compositeImage": {
                "id": "1de485bd-c146-4611-af4d-16cbc3bafba4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0d6eab0e-c0f5-44fc-982d-445aeedb1a3a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "63bbf9dc-e537-443b-83e4-f89fbf9a2c56",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0d6eab0e-c0f5-44fc-982d-445aeedb1a3a",
                    "LayerId": "704ebcee-9664-459f-ae91-8f1ad450731d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1800,
    "layers": [
        {
            "id": "704ebcee-9664-459f-ae91-8f1ad450731d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f337c56-226e-4b2e-ba81-b1eb950efc60",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 3200,
    "xorig": 0,
    "yorig": 0
}