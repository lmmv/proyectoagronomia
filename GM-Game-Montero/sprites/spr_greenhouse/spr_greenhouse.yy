{
    "id": "3a765296-9972-4518-bab3-869c7f0cfd2b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_greenhouse",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 581,
    "bbox_left": 0,
    "bbox_right": 559,
    "bbox_top": 76,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc5d310e-d1a3-4118-ae75-d1d708801b3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3a765296-9972-4518-bab3-869c7f0cfd2b",
            "compositeImage": {
                "id": "dc95f3e2-dc1f-4617-9c22-faecba27bae8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc5d310e-d1a3-4118-ae75-d1d708801b3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c122f9b-04bc-4a00-8256-e5d0d95cdeaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc5d310e-d1a3-4118-ae75-d1d708801b3b",
                    "LayerId": "349eb698-6b14-4e54-8bfa-c2e69db7397c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 650,
    "layers": [
        {
            "id": "349eb698-6b14-4e54-8bfa-c2e69db7397c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3a765296-9972-4518-bab3-869c7f0cfd2b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 560,
    "xorig": 280,
    "yorig": 325
}