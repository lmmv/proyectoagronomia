{
    "id": "b59dcb44-6f1a-4874-bf48-03b6835b6b82",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_box",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 82,
    "bbox_left": 0,
    "bbox_right": 329,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8660bd3-ec5d-467d-81b7-6e8d262f1823",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b59dcb44-6f1a-4874-bf48-03b6835b6b82",
            "compositeImage": {
                "id": "3515699c-c3f1-4fdb-88c8-3a7e5957b904",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8660bd3-ec5d-467d-81b7-6e8d262f1823",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3a57647-79b7-4220-a84f-d371b4a9b7e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8660bd3-ec5d-467d-81b7-6e8d262f1823",
                    "LayerId": "952e10d8-4b6d-46d2-bbac-5b0ffe81d55b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 83,
    "layers": [
        {
            "id": "952e10d8-4b6d-46d2-bbac-5b0ffe81d55b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b59dcb44-6f1a-4874-bf48-03b6835b6b82",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 330,
    "xorig": 165,
    "yorig": 41
}