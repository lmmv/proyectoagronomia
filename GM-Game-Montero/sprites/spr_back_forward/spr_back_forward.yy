{
    "id": "f9e08a99-8e2b-4160-9b8b-571f0121b70b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_back_forward",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "910b3e9a-e96c-493d-86b9-7e5fb025ec48",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9e08a99-8e2b-4160-9b8b-571f0121b70b",
            "compositeImage": {
                "id": "769fecd9-5774-4a52-b0c0-7733c29f727f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "910b3e9a-e96c-493d-86b9-7e5fb025ec48",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ca77e00-9009-4d9b-9821-2f1baba86e62",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "910b3e9a-e96c-493d-86b9-7e5fb025ec48",
                    "LayerId": "ab0ec27c-3074-417f-a1eb-94df45122289"
                }
            ]
        },
        {
            "id": "65509b98-f22d-459a-a385-713bd9e1a7ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f9e08a99-8e2b-4160-9b8b-571f0121b70b",
            "compositeImage": {
                "id": "9fc43130-3dde-4acd-8370-9a9f8ef71045",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65509b98-f22d-459a-a385-713bd9e1a7ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d9e4253-0e23-43a8-997d-17755a77f1a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65509b98-f22d-459a-a385-713bd9e1a7ae",
                    "LayerId": "ab0ec27c-3074-417f-a1eb-94df45122289"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ab0ec27c-3074-417f-a1eb-94df45122289",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f9e08a99-8e2b-4160-9b8b-571f0121b70b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}