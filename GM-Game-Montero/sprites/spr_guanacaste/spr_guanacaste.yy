{
    "id": "0c353fc8-8d85-4040-99aa-1c97046bf94d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_guanacaste",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 409,
    "bbox_left": 0,
    "bbox_right": 320,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c438ced3-3644-462b-a0ed-cb080eb597a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c353fc8-8d85-4040-99aa-1c97046bf94d",
            "compositeImage": {
                "id": "66756a43-cde7-43e9-a689-fc8a266b5bd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c438ced3-3644-462b-a0ed-cb080eb597a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9cf49f82-2cca-4402-a424-7c5f85c92d8e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c438ced3-3644-462b-a0ed-cb080eb597a0",
                    "LayerId": "198b2556-bb44-48ba-b348-a9a4ab4de922"
                }
            ]
        },
        {
            "id": "95884af0-82f4-4655-89f3-b87d8287f8cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0c353fc8-8d85-4040-99aa-1c97046bf94d",
            "compositeImage": {
                "id": "e60ab655-02d4-4a7d-a29e-2d756fd32945",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95884af0-82f4-4655-89f3-b87d8287f8cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a2e587c2-9225-49a8-be4b-c78efc724161",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95884af0-82f4-4655-89f3-b87d8287f8cb",
                    "LayerId": "198b2556-bb44-48ba-b348-a9a4ab4de922"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 410,
    "layers": [
        {
            "id": "198b2556-bb44-48ba-b348-a9a4ab4de922",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0c353fc8-8d85-4040-99aa-1c97046bf94d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 321,
    "xorig": 0,
    "yorig": 0
}