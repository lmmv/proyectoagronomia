{
    "id": "f8389823-0320-4188-ac41-eec9291d51e3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_close_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 0,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "75c5b416-50e2-41f0-bd3a-f1686b0ea803",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f8389823-0320-4188-ac41-eec9291d51e3",
            "compositeImage": {
                "id": "f761d862-d534-4d60-8d5c-07f2ca96ad93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75c5b416-50e2-41f0-bd3a-f1686b0ea803",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b309656-6354-4fa6-b1ed-7f4d4092a443",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75c5b416-50e2-41f0-bd3a-f1686b0ea803",
                    "LayerId": "20348d1a-6f4d-4b0f-bd64-86964edbe6cd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 60,
    "layers": [
        {
            "id": "20348d1a-6f4d-4b0f-bd64-86964edbe6cd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f8389823-0320-4188-ac41-eec9291d51e3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 60,
    "xorig": 30,
    "yorig": 30
}