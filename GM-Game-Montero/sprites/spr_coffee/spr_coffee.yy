{
    "id": "356d686e-52f8-432a-bbee-c6567bbc7589",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_coffee",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f9951ec4-1c8a-4276-b5ad-7eded0df07f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "356d686e-52f8-432a-bbee-c6567bbc7589",
            "compositeImage": {
                "id": "e1c67a0c-6137-4e6a-afb4-b7ae3335239a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f9951ec4-1c8a-4276-b5ad-7eded0df07f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57661378-03cf-445c-98a3-b6ea3b2ec57b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f9951ec4-1c8a-4276-b5ad-7eded0df07f5",
                    "LayerId": "ad5adf2c-200a-4c98-996b-bd33e5c5c949"
                }
            ]
        },
        {
            "id": "0595efdb-c3c4-4d08-af58-3439fc1277c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "356d686e-52f8-432a-bbee-c6567bbc7589",
            "compositeImage": {
                "id": "1f16d0f4-844f-4ae7-a873-d21a01a9ee8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0595efdb-c3c4-4d08-af58-3439fc1277c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99ce7ad4-7b58-4d87-b65d-a0924c8ec67f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0595efdb-c3c4-4d08-af58-3439fc1277c4",
                    "LayerId": "ad5adf2c-200a-4c98-996b-bd33e5c5c949"
                }
            ]
        },
        {
            "id": "659280e3-c65c-4b1c-bba7-67294d9bbcac",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "356d686e-52f8-432a-bbee-c6567bbc7589",
            "compositeImage": {
                "id": "1a5ab513-33b6-4971-9e1f-b7c4109dc4de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "659280e3-c65c-4b1c-bba7-67294d9bbcac",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "17108ca9-f7c6-4241-95be-089426469817",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "659280e3-c65c-4b1c-bba7-67294d9bbcac",
                    "LayerId": "ad5adf2c-200a-4c98-996b-bd33e5c5c949"
                }
            ]
        },
        {
            "id": "7b70fa63-a348-4210-a42c-c84a255a810e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "356d686e-52f8-432a-bbee-c6567bbc7589",
            "compositeImage": {
                "id": "a9b6b185-3134-438c-b55b-58e19eed168a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b70fa63-a348-4210-a42c-c84a255a810e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a613d59a-3446-4388-8a49-c17a6efb79e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b70fa63-a348-4210-a42c-c84a255a810e",
                    "LayerId": "ad5adf2c-200a-4c98-996b-bd33e5c5c949"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 49,
    "layers": [
        {
            "id": "ad5adf2c-200a-4c98-996b-bd33e5c5c949",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "356d686e-52f8-432a-bbee-c6567bbc7589",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 30,
    "xorig": 15,
    "yorig": 48
}