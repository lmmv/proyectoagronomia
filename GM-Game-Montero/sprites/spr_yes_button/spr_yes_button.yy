{
    "id": "0bd1914b-088b-4a87-b218-1a59cd2bffe7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_yes_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 138,
    "bbox_left": 0,
    "bbox_right": 500,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea6fcafd-5b2c-4dd1-83bb-3c498a2f65f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0bd1914b-088b-4a87-b218-1a59cd2bffe7",
            "compositeImage": {
                "id": "037577e8-884a-4548-9fdd-260f6e72222d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea6fcafd-5b2c-4dd1-83bb-3c498a2f65f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c4f77b26-eb76-4bdf-8e40-b5c722439593",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea6fcafd-5b2c-4dd1-83bb-3c498a2f65f3",
                    "LayerId": "6eb89a07-f121-4068-a980-92349481c587"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 139,
    "layers": [
        {
            "id": "6eb89a07-f121-4068-a980-92349481c587",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0bd1914b-088b-4a87-b218-1a59cd2bffe7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 501,
    "xorig": 250,
    "yorig": 69
}