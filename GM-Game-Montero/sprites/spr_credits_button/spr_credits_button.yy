{
    "id": "28953fb7-197d-4d1e-a20b-b4be11192b82",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_credits_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 326,
    "bbox_left": 0,
    "bbox_right": 522,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c22f1a1d-81ee-462e-a3dd-92835caad349",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "28953fb7-197d-4d1e-a20b-b4be11192b82",
            "compositeImage": {
                "id": "d996caf3-eb2d-417f-8cd4-1c0b8bf5b2fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c22f1a1d-81ee-462e-a3dd-92835caad349",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5194cbbb-b947-499e-9f83-32097dc38e6d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c22f1a1d-81ee-462e-a3dd-92835caad349",
                    "LayerId": "8d56cd80-43d1-432d-a4b2-33a4aca0ab6e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 327,
    "layers": [
        {
            "id": "8d56cd80-43d1-432d-a4b2-33a4aca0ab6e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "28953fb7-197d-4d1e-a20b-b4be11192b82",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 523,
    "xorig": 261,
    "yorig": 163
}