{
    "id": "f54bf0bb-eb8c-4fca-9c85-74f00b1a08ff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_exit_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 326,
    "bbox_left": 0,
    "bbox_right": 520,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e2012e51-416b-42be-ab52-ac103b007fb8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f54bf0bb-eb8c-4fca-9c85-74f00b1a08ff",
            "compositeImage": {
                "id": "1d4c2f1b-843e-411c-b809-dc2ab2d536bf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2012e51-416b-42be-ab52-ac103b007fb8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ad9642c7-0172-4ffe-8ea9-df8f7f2f0c6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2012e51-416b-42be-ab52-ac103b007fb8",
                    "LayerId": "1b8a7457-2b71-4eaa-8cd7-d7f58722e69d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 327,
    "layers": [
        {
            "id": "1b8a7457-2b71-4eaa-8cd7-d7f58722e69d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f54bf0bb-eb8c-4fca-9c85-74f00b1a08ff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 521,
    "xorig": 260,
    "yorig": 163
}