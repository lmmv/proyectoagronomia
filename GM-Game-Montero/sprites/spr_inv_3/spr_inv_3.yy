{
    "id": "83ffa9c4-2225-4f37-9a81-d1a0d1c7dcc3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inv_3",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e25afd26-8a78-4a05-b6de-54597b4438da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83ffa9c4-2225-4f37-9a81-d1a0d1c7dcc3",
            "compositeImage": {
                "id": "7b8f3cf1-a64b-48da-9b47-44a95a2feaa0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e25afd26-8a78-4a05-b6de-54597b4438da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44388b76-e15d-4ed4-8759-6914da05f04a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e25afd26-8a78-4a05-b6de-54597b4438da",
                    "LayerId": "6320284d-a257-4b84-a4a4-c58cf373d7c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "6320284d-a257-4b84-a4a4-c58cf373d7c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83ffa9c4-2225-4f37-9a81-d1a0d1c7dcc3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}