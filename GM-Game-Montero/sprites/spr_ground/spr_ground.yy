{
    "id": "930e2c56-bf31-4ccd-b272-20356bbcce89",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ground",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7da9ba60-95d7-414a-87b1-b132a5d2d762",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "930e2c56-bf31-4ccd-b272-20356bbcce89",
            "compositeImage": {
                "id": "c74d5ff9-a600-47ab-a64e-10846f8d23ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7da9ba60-95d7-414a-87b1-b132a5d2d762",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ef6efb4-80ce-48ac-b733-e8a92db99df1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7da9ba60-95d7-414a-87b1-b132a5d2d762",
                    "LayerId": "12fa564d-e058-4ffc-a876-ebc81109ed3a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "12fa564d-e058-4ffc-a876-ebc81109ed3a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "930e2c56-bf31-4ccd-b272-20356bbcce89",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 0,
    "yorig": 0
}