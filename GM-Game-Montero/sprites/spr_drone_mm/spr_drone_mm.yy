{
    "id": "2870adea-5c0a-445c-ab61-b680c83154a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_drone_mm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 86,
    "bbox_left": 10,
    "bbox_right": 84,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26fa7068-690d-4e17-85b1-02cf1248b86c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2870adea-5c0a-445c-ab61-b680c83154a8",
            "compositeImage": {
                "id": "846f3c13-8144-469d-87ea-03f990b9f002",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26fa7068-690d-4e17-85b1-02cf1248b86c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "896fb59d-e365-4a13-92cc-1435e7e404f6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26fa7068-690d-4e17-85b1-02cf1248b86c",
                    "LayerId": "c37c173e-9e74-4106-a150-355ac976f650"
                }
            ]
        },
        {
            "id": "a58b2dc7-b639-403b-9081-fbc66d9030d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2870adea-5c0a-445c-ab61-b680c83154a8",
            "compositeImage": {
                "id": "8f2784b8-3dc5-402f-b19c-e34245f08f3e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a58b2dc7-b639-403b-9081-fbc66d9030d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90108ba2-dd4f-45a2-8598-205e376792fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a58b2dc7-b639-403b-9081-fbc66d9030d8",
                    "LayerId": "c37c173e-9e74-4106-a150-355ac976f650"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "c37c173e-9e74-4106-a150-355ac976f650",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2870adea-5c0a-445c-ab61-b680c83154a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}