{
    "id": "a6cad7cd-8cff-4ae5-b7c3-de598286878b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_logo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 865,
    "bbox_left": 0,
    "bbox_right": 1150,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "12b23112-daae-4208-afee-907a912c3130",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6cad7cd-8cff-4ae5-b7c3-de598286878b",
            "compositeImage": {
                "id": "9a7e0dec-71b5-4890-a36c-4acd9b4fe5c0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12b23112-daae-4208-afee-907a912c3130",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "308aa2e9-0807-4aac-93bd-639c32c49082",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12b23112-daae-4208-afee-907a912c3130",
                    "LayerId": "7aea1599-167a-45fb-9775-5736ddd6731e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 866,
    "layers": [
        {
            "id": "7aea1599-167a-45fb-9775-5736ddd6731e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6cad7cd-8cff-4ae5-b7c3-de598286878b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1151,
    "xorig": 575,
    "yorig": 433
}