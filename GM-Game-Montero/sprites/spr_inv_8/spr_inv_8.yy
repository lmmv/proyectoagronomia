{
    "id": "680c75be-80fe-4056-bb64-b4009e2dc727",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inv_8",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e236f52d-5682-4ae1-b07f-e689c595dd60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "680c75be-80fe-4056-bb64-b4009e2dc727",
            "compositeImage": {
                "id": "2b892ffc-b0f7-4111-b98e-c954b515a8b8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e236f52d-5682-4ae1-b07f-e689c595dd60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e10078b6-fc49-4956-99da-55c910a31354",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e236f52d-5682-4ae1-b07f-e689c595dd60",
                    "LayerId": "faff4e0d-f16e-4a19-8487-e4efc4c3ecee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "faff4e0d-f16e-4a19-8487-e4efc4c3ecee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "680c75be-80fe-4056-bb64-b4009e2dc727",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}