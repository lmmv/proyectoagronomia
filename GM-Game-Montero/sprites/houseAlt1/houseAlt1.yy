{
    "id": "f30c9379-52fa-4753-9258-26de6df1315a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "houseAlt1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 0,
    "bbox_right": 167,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f86468fb-d040-4303-a9fa-e44cd1c0919a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f30c9379-52fa-4753-9258-26de6df1315a",
            "compositeImage": {
                "id": "3fe7b97c-1d72-42f9-b073-80ae610e1f7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f86468fb-d040-4303-a9fa-e44cd1c0919a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbacf1fa-b98e-4b62-8d3f-4a5f9bf187e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f86468fb-d040-4303-a9fa-e44cd1c0919a",
                    "LayerId": "de1aaa00-7e76-4189-8db0-11cfcc64813b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "de1aaa00-7e76-4189-8db0-11cfcc64813b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f30c9379-52fa-4753-9258-26de6df1315a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 168,
    "xorig": 0,
    "yorig": 223
}