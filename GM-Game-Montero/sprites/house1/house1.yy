{
    "id": "bdd3133c-7ca7-4529-9675-4050e6fbac2d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "house1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 223,
    "bbox_left": 0,
    "bbox_right": 207,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "166a7a68-3dbb-4e32-a44b-520cff19de99",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bdd3133c-7ca7-4529-9675-4050e6fbac2d",
            "compositeImage": {
                "id": "25def8b5-a925-4acf-82fb-53a30a9e3bc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "166a7a68-3dbb-4e32-a44b-520cff19de99",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "48033549-800e-4270-a9c5-ffb9727b9c44",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "166a7a68-3dbb-4e32-a44b-520cff19de99",
                    "LayerId": "af586239-d806-41c3-b9a6-de486fd2ebee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 224,
    "layers": [
        {
            "id": "af586239-d806-41c3-b9a6-de486fd2ebee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bdd3133c-7ca7-4529-9675-4050e6fbac2d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 208,
    "xorig": 0,
    "yorig": 223
}