{
    "id": "0ff36c80-e906-4733-b8d2-1b77f5952e95",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inv_4",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "742fe70b-b4e0-4087-8bb7-6b9fbe74e6b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0ff36c80-e906-4733-b8d2-1b77f5952e95",
            "compositeImage": {
                "id": "ec87a4bd-9354-4ac0-96d7-02af205f111e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "742fe70b-b4e0-4087-8bb7-6b9fbe74e6b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45d58f92-3012-4c32-b316-c675b44745e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "742fe70b-b4e0-4087-8bb7-6b9fbe74e6b2",
                    "LayerId": "1dfdd4da-a5e5-4b33-87c1-f6237fbd3798"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "1dfdd4da-a5e5-4b33-87c1-f6237fbd3798",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0ff36c80-e906-4733-b8d2-1b77f5952e95",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}