{
    "id": "79d9c04c-d3ca-4db0-a126-f750a52a49cb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_pole",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 287,
    "bbox_left": 84,
    "bbox_right": 140,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4213d04b-79ac-4819-917a-4a64f1c2c46d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79d9c04c-d3ca-4db0-a126-f750a52a49cb",
            "compositeImage": {
                "id": "675b4e33-2d22-40e7-9233-bb5b8b9a39ee",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4213d04b-79ac-4819-917a-4a64f1c2c46d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d38695e9-c770-4a27-9227-6c82de06999d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4213d04b-79ac-4819-917a-4a64f1c2c46d",
                    "LayerId": "6fd3feac-7ccb-47dc-be69-3eae08f98e7f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 288,
    "layers": [
        {
            "id": "6fd3feac-7ccb-47dc-be69-3eae08f98e7f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79d9c04c-d3ca-4db0-a126-f750a52a49cb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 287
}