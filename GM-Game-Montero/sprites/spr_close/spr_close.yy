{
    "id": "661bec79-7c26-46a3-a1c2-ac7508d8458a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_close",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a3f2a2fc-5f89-4c62-9bba-65175e1b01f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "661bec79-7c26-46a3-a1c2-ac7508d8458a",
            "compositeImage": {
                "id": "7f116035-b13c-4c68-bc9b-8f187f5edb75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a3f2a2fc-5f89-4c62-9bba-65175e1b01f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3c770a9-8659-46dd-9462-c03aea54d986",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a3f2a2fc-5f89-4c62-9bba-65175e1b01f2",
                    "LayerId": "0a7d604d-ac8b-4cca-8045-9149cf0da8be"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0a7d604d-ac8b-4cca-8045-9149cf0da8be",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "661bec79-7c26-46a3-a1c2-ac7508d8458a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}