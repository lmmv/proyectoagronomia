{
    "id": "3ab64b41-16b6-46de-b154-610c11f0cc6e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stop_spawn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "de8fcf4d-18ba-42d8-a193-43a5d8d6eeda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ab64b41-16b6-46de-b154-610c11f0cc6e",
            "compositeImage": {
                "id": "be8216ee-3a5d-477b-85d0-ac9bd31192f1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de8fcf4d-18ba-42d8-a193-43a5d8d6eeda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9d21272-8d97-45fd-aae9-b69a4f31b348",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de8fcf4d-18ba-42d8-a193-43a5d8d6eeda",
                    "LayerId": "50606ae6-2e09-48b8-a991-084ed521a4dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "50606ae6-2e09-48b8-a991-084ed521a4dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ab64b41-16b6-46de-b154-610c11f0cc6e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}