{
    "id": "6621024c-f4d1-491f-866b-6010c7a12fd5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1799,
    "bbox_left": 0,
    "bbox_right": 3199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c0cfd72-c4d6-4b58-832b-7c8b473fb059",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6621024c-f4d1-491f-866b-6010c7a12fd5",
            "compositeImage": {
                "id": "e4625dfd-b775-477c-b060-ed9fa9d49526",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c0cfd72-c4d6-4b58-832b-7c8b473fb059",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acc0629c-08f9-409c-b139-6b99592de3f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c0cfd72-c4d6-4b58-832b-7c8b473fb059",
                    "LayerId": "8cce06ef-727f-431a-8007-1d5cb17f65bf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1800,
    "layers": [
        {
            "id": "8cce06ef-727f-431a-8007-1d5cb17f65bf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6621024c-f4d1-491f-866b-6010c7a12fd5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 3200,
    "xorig": 0,
    "yorig": 0
}