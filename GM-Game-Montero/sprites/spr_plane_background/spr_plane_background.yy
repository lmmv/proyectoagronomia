{
    "id": "aa964e94-9256-4329-808b-e46b2e8edb36",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_plane_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 1919,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca986285-3c69-4a04-b819-1b515ce8ae28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "aa964e94-9256-4329-808b-e46b2e8edb36",
            "compositeImage": {
                "id": "d70dfeb0-014c-4210-81df-b41e34925478",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca986285-3c69-4a04-b819-1b515ce8ae28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8b015f5-9f97-47ea-803c-8b943e688ce9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca986285-3c69-4a04-b819-1b515ce8ae28",
                    "LayerId": "a8cb405d-f79c-4c86-a9b9-b04eb75f37a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "a8cb405d-f79c-4c86-a9b9-b04eb75f37a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "aa964e94-9256-4329-808b-e46b2e8edb36",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 1165,
    "yorig": 901
}