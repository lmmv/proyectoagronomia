{
    "id": "a7867c3b-ebe7-4d0e-8a22-c4a0fd6fde5d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_info",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9765b0c7-1e20-4965-89bc-aa74605e22bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7867c3b-ebe7-4d0e-8a22-c4a0fd6fde5d",
            "compositeImage": {
                "id": "044aafbf-e6f8-403c-8ffa-a193cb5a5801",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9765b0c7-1e20-4965-89bc-aa74605e22bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a5dadeb-d571-4e12-b82a-8a5840470886",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9765b0c7-1e20-4965-89bc-aa74605e22bd",
                    "LayerId": "b6fa0b8c-4de3-460a-88d1-619b59e812a2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b6fa0b8c-4de3-460a-88d1-619b59e812a2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7867c3b-ebe7-4d0e-8a22-c4a0fd6fde5d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}