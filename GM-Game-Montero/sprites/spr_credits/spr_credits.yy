{
    "id": "1072a54e-5322-49ef-b8cf-d7519d8eb2e4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_credits",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1799,
    "bbox_left": 0,
    "bbox_right": 3200,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eda1fd1d-aff3-49e1-b366-fbe21234e2b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1072a54e-5322-49ef-b8cf-d7519d8eb2e4",
            "compositeImage": {
                "id": "e9463756-c35d-4eb0-be77-27047625d828",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eda1fd1d-aff3-49e1-b366-fbe21234e2b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe83a2a1-de99-48bb-8e37-d84b44892a3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eda1fd1d-aff3-49e1-b366-fbe21234e2b8",
                    "LayerId": "771e8b55-9b12-4c79-8797-ca7fa019b78f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1800,
    "layers": [
        {
            "id": "771e8b55-9b12-4c79-8797-ca7fa019b78f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1072a54e-5322-49ef-b8cf-d7519d8eb2e4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 3201,
    "xorig": 0,
    "yorig": 0
}