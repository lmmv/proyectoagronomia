{
    "id": "b5227862-55d9-4955-988b-853dd7e670b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cartago",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 183,
    "bbox_left": 0,
    "bbox_right": 205,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2faedf2-0aad-4f66-8db2-ed32565243a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5227862-55d9-4955-988b-853dd7e670b7",
            "compositeImage": {
                "id": "63634691-7a9b-42f1-9ec3-a99113a045c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2faedf2-0aad-4f66-8db2-ed32565243a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3a8b2294-ea93-455d-a2a4-bd8219079a5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2faedf2-0aad-4f66-8db2-ed32565243a3",
                    "LayerId": "93c2f51e-10fd-4c7b-9a9b-1f7e7e57ec47"
                }
            ]
        },
        {
            "id": "cde2fca2-0cc5-40c1-b0a9-560caa43169f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b5227862-55d9-4955-988b-853dd7e670b7",
            "compositeImage": {
                "id": "2814af4f-5b6d-43b4-8371-ab5906085e08",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cde2fca2-0cc5-40c1-b0a9-560caa43169f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33b8519e-cfe3-49cd-b550-684795ae112f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cde2fca2-0cc5-40c1-b0a9-560caa43169f",
                    "LayerId": "93c2f51e-10fd-4c7b-9a9b-1f7e7e57ec47"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 184,
    "layers": [
        {
            "id": "93c2f51e-10fd-4c7b-9a9b-1f7e7e57ec47",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b5227862-55d9-4955-988b-853dd7e670b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 206,
    "xorig": 0,
    "yorig": 0
}