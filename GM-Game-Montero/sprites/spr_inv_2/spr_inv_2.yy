{
    "id": "dabe42c8-ca42-4a3a-aefb-e528a561576a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inv_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "082c14bd-5fbc-475d-96e2-2fa7a35191aa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dabe42c8-ca42-4a3a-aefb-e528a561576a",
            "compositeImage": {
                "id": "39750ee9-aa88-4c2b-aed8-093dcdebea8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "082c14bd-5fbc-475d-96e2-2fa7a35191aa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c6d8227-6cee-4212-9c2c-d46a99d81b23",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "082c14bd-5fbc-475d-96e2-2fa7a35191aa",
                    "LayerId": "7f722e30-0953-4c5f-afa8-ec805269898b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "7f722e30-0953-4c5f-afa8-ec805269898b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dabe42c8-ca42-4a3a-aefb-e528a561576a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}