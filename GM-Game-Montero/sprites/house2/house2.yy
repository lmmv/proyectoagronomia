{
    "id": "2d8c1d69-c044-4c21-8585-54aafd63baaf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "house2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 216,
    "bbox_left": 0,
    "bbox_right": 280,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c610031a-c639-4d9a-98b9-75b7668dff2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d8c1d69-c044-4c21-8585-54aafd63baaf",
            "compositeImage": {
                "id": "aee16fa5-6459-472c-bc8f-b582dbaacde9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c610031a-c639-4d9a-98b9-75b7668dff2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4202b34f-599e-4a92-9709-646b7107193a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c610031a-c639-4d9a-98b9-75b7668dff2b",
                    "LayerId": "5b0f0a62-433e-42ed-ae03-4583c8cd03d5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 217,
    "layers": [
        {
            "id": "5b0f0a62-433e-42ed-ae03-4583c8cd03d5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d8c1d69-c044-4c21-8585-54aafd63baaf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 281,
    "xorig": 0,
    "yorig": 216
}