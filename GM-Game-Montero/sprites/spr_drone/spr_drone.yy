{
    "id": "9a2844ab-0d36-4394-8031-e575a73663e6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_drone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 45,
    "bbox_left": 7,
    "bbox_right": 56,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "32023d4a-deb2-4b76-ae79-138438f46bdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9a2844ab-0d36-4394-8031-e575a73663e6",
            "compositeImage": {
                "id": "1ea662ff-cfc8-448f-995b-ea1f996ef1b6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32023d4a-deb2-4b76-ae79-138438f46bdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e333ca85-7465-43fc-a8e5-2ba20f62c62b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32023d4a-deb2-4b76-ae79-138438f46bdb",
                    "LayerId": "4cc4c602-7020-4366-949c-5bc7856f19e8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4cc4c602-7020-4366-949c-5bc7856f19e8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9a2844ab-0d36-4394-8031-e575a73663e6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 30
}