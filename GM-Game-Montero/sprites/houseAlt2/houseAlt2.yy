{
    "id": "2d0e92c9-98e6-4704-9818-3d59b2622692",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "houseAlt2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 216,
    "bbox_left": 0,
    "bbox_right": 240,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6bc9bb24-9ced-44d8-9835-7a217bcffb04",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d0e92c9-98e6-4704-9818-3d59b2622692",
            "compositeImage": {
                "id": "e8d4e571-e373-4a69-8846-123fc376ec1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bc9bb24-9ced-44d8-9835-7a217bcffb04",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d45b76c7-63f9-40cf-8525-3950f9c319f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bc9bb24-9ced-44d8-9835-7a217bcffb04",
                    "LayerId": "1aa54a68-8d82-4c7d-98f7-496e70058967"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 217,
    "layers": [
        {
            "id": "1aa54a68-8d82-4c7d-98f7-496e70058967",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d0e92c9-98e6-4704-9818-3d59b2622692",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 241,
    "xorig": 0,
    "yorig": 216
}