{
    "id": "a9cb674d-cce7-4457-ac67-4f7dc3c33a53",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inv_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c00568a-4779-407c-b2b8-b2f26f0a1ab7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a9cb674d-cce7-4457-ac67-4f7dc3c33a53",
            "compositeImage": {
                "id": "bc635737-2a0b-4e34-b135-f96819d098a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c00568a-4779-407c-b2b8-b2f26f0a1ab7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "06a7f95f-82aa-4b8c-9555-f354a9d94370",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c00568a-4779-407c-b2b8-b2f26f0a1ab7",
                    "LayerId": "d191eaf8-619d-46b5-a8b5-c901ab9349c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d191eaf8-619d-46b5-a8b5-c901ab9349c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a9cb674d-cce7-4457-ac67-4f7dc3c33a53",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}