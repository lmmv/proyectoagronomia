{
    "id": "75d2ee36-a9e0-4ee0-81fc-f024279078b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_standard_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 4,
    "bbox_right": 253,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa604b77-4d75-4909-9985-3c2fe40d7913",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d2ee36-a9e0-4ee0-81fc-f024279078b5",
            "compositeImage": {
                "id": "15e5db19-0d42-484c-bea7-63ed6d3c3863",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa604b77-4d75-4909-9985-3c2fe40d7913",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4353a803-6018-4bdf-8d24-db3c21d1c90b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa604b77-4d75-4909-9985-3c2fe40d7913",
                    "LayerId": "9d9fec88-1604-48cb-baa7-3b497a5c882e"
                }
            ]
        },
        {
            "id": "e201d5c1-d32b-4978-b182-bdf1849f14fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d2ee36-a9e0-4ee0-81fc-f024279078b5",
            "compositeImage": {
                "id": "7bd3472c-e3f3-484b-8982-af1c6bbc6941",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e201d5c1-d32b-4978-b182-bdf1849f14fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "019d48cc-fd59-4113-be87-3be4e807e55f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e201d5c1-d32b-4978-b182-bdf1849f14fc",
                    "LayerId": "9d9fec88-1604-48cb-baa7-3b497a5c882e"
                }
            ]
        },
        {
            "id": "c2954508-74cf-4dad-9c29-79d2692624e9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d2ee36-a9e0-4ee0-81fc-f024279078b5",
            "compositeImage": {
                "id": "e4a54464-49aa-4e1b-93f3-794177bc5370",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c2954508-74cf-4dad-9c29-79d2692624e9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f03f969d-cdf9-4485-8ac5-dafa5670fadc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c2954508-74cf-4dad-9c29-79d2692624e9",
                    "LayerId": "9d9fec88-1604-48cb-baa7-3b497a5c882e"
                }
            ]
        },
        {
            "id": "4a4116cf-082d-4a09-a4dc-955c2646924a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "75d2ee36-a9e0-4ee0-81fc-f024279078b5",
            "compositeImage": {
                "id": "738f2cab-b121-4e19-a674-7c22927547b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a4116cf-082d-4a09-a4dc-955c2646924a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d14da55a-f343-435f-b43e-8a3ed4b468a3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a4116cf-082d-4a09-a4dc-955c2646924a",
                    "LayerId": "9d9fec88-1604-48cb-baa7-3b497a5c882e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9d9fec88-1604-48cb-baa7-3b497a5c882e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "75d2ee36-a9e0-4ee0-81fc-f024279078b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 32
}