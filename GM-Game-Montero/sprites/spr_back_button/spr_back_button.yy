{
    "id": "4babf1fd-b8b5-42a6-aa9b-7afa4b794f42",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_back_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 191,
    "bbox_left": 0,
    "bbox_right": 313,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "71a6629a-17bc-4880-9bbb-0e6405285842",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4babf1fd-b8b5-42a6-aa9b-7afa4b794f42",
            "compositeImage": {
                "id": "7537d68a-05db-4a71-b2bc-5242de5bf8f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "71a6629a-17bc-4880-9bbb-0e6405285842",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d50203ef-2747-4bb9-960d-a8f7d5fe48c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "71a6629a-17bc-4880-9bbb-0e6405285842",
                    "LayerId": "8318c94c-2c6f-415c-a178-2f8bd2e88352"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 192,
    "layers": [
        {
            "id": "8318c94c-2c6f-415c-a178-2f8bd2e88352",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4babf1fd-b8b5-42a6-aa9b-7afa4b794f42",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 314,
    "xorig": 157,
    "yorig": 96
}