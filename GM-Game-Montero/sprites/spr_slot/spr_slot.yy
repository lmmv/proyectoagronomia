{
    "id": "a89c9c5b-9859-406a-a7bc-8cde3591155a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_slot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 118,
    "bbox_left": 10,
    "bbox_right": 118,
    "bbox_top": 10,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5dc2fe14-af77-4e2a-9674-b94eec1719a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a89c9c5b-9859-406a-a7bc-8cde3591155a",
            "compositeImage": {
                "id": "7ec9966f-61c0-47b5-9f83-2c9ae6cec92b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5dc2fe14-af77-4e2a-9674-b94eec1719a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d209201-ade9-488b-b3b4-dfe009011561",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5dc2fe14-af77-4e2a-9674-b94eec1719a1",
                    "LayerId": "d54dea99-f2d3-46b8-aade-40a5569ac637"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "d54dea99-f2d3-46b8-aade-40a5569ac637",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a89c9c5b-9859-406a-a7bc-8cde3591155a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 75,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}