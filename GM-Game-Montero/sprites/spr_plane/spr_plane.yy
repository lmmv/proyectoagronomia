{
    "id": "361d7622-b273-4d20-b2c4-fd435637e29f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_plane",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 5,
    "bbox_right": 199,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5df5b7a8-9662-486c-a23e-acaf4bb1f7a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "361d7622-b273-4d20-b2c4-fd435637e29f",
            "compositeImage": {
                "id": "4e759b52-bf16-4b30-96ed-d1dfe874ac7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5df5b7a8-9662-486c-a23e-acaf4bb1f7a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69228748-98fe-4444-97fe-0f49849c00e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5df5b7a8-9662-486c-a23e-acaf4bb1f7a1",
                    "LayerId": "0462c1f5-ccfa-4ade-be3f-617def77c74d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 118,
    "layers": [
        {
            "id": "0462c1f5-ccfa-4ade-be3f-617def77c74d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "361d7622-b273-4d20-b2c4-fd435637e29f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 217,
    "xorig": 108,
    "yorig": 74
}