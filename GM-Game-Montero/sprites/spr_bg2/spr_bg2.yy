{
    "id": "6e48b238-bbcf-42b8-8912-cad5d4f25a92",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1079,
    "bbox_left": 0,
    "bbox_right": 3839,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0cec0138-3246-43bf-acb2-96d5aa233fcd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6e48b238-bbcf-42b8-8912-cad5d4f25a92",
            "compositeImage": {
                "id": "00118d86-e864-4949-91df-91b63e0e296b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cec0138-3246-43bf-acb2-96d5aa233fcd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a16417e2-4ce7-40c3-9f94-a861584c1da3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cec0138-3246-43bf-acb2-96d5aa233fcd",
                    "LayerId": "b690456d-14b4-4e6a-9246-82f09ebf36df"
                },
                {
                    "id": "8b27749c-091a-428b-aee8-4ab27520e935",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cec0138-3246-43bf-acb2-96d5aa233fcd",
                    "LayerId": "7e18ad66-e2cc-4a2c-9286-8556fd18b45b"
                },
                {
                    "id": "180c9f59-08c4-4465-94e7-d5c890be7303",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cec0138-3246-43bf-acb2-96d5aa233fcd",
                    "LayerId": "2a60e37a-5c59-4ed4-b29e-af980c2d01e7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1080,
    "layers": [
        {
            "id": "7e18ad66-e2cc-4a2c-9286-8556fd18b45b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e48b238-bbcf-42b8-8912-cad5d4f25a92",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 1",
            "opacity": 100,
            "visible": true
        },
        {
            "id": "b690456d-14b4-4e6a-9246-82f09ebf36df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e48b238-bbcf-42b8-8912-cad5d4f25a92",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 67,
            "visible": true
        },
        {
            "id": "2a60e37a-5c59-4ed4-b29e-af980c2d01e7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6e48b238-bbcf-42b8-8912-cad5d4f25a92",
            "blendMode": 0,
            "isLocked": false,
            "name": "Layer 2",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 3840,
    "xorig": 0,
    "yorig": 0
}