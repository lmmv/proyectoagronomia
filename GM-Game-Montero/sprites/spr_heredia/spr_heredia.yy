{
    "id": "82b30984-7275-4aa3-b417-2e3947034038",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_heredia",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 232,
    "bbox_left": 0,
    "bbox_right": 116,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6bee13cd-5248-450c-abe8-1da961ae3c5c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82b30984-7275-4aa3-b417-2e3947034038",
            "compositeImage": {
                "id": "816490f3-b125-45c4-bc25-cc66ea6298ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6bee13cd-5248-450c-abe8-1da961ae3c5c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cadd2059-cb5d-4c4d-a7e9-f4eeb8515bd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6bee13cd-5248-450c-abe8-1da961ae3c5c",
                    "LayerId": "96e7485f-611d-44cb-b0c6-1b392b14e3c3"
                }
            ]
        },
        {
            "id": "e5ce5378-1917-4eac-a3e0-400e40e03c96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82b30984-7275-4aa3-b417-2e3947034038",
            "compositeImage": {
                "id": "21c3929e-7c70-4b41-8ad1-01bfe6db6df9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5ce5378-1917-4eac-a3e0-400e40e03c96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2338fffc-d17e-4259-ab2b-f031a09f8e0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5ce5378-1917-4eac-a3e0-400e40e03c96",
                    "LayerId": "96e7485f-611d-44cb-b0c6-1b392b14e3c3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 233,
    "layers": [
        {
            "id": "96e7485f-611d-44cb-b0c6-1b392b14e3c3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82b30984-7275-4aa3-b417-2e3947034038",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 117,
    "xorig": 0,
    "yorig": 0
}