{
    "id": "86dc9c4f-c359-4d13-b8ae-5a9d589643ad",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_drop",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 26,
    "bbox_left": 9,
    "bbox_right": 22,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4f2928f1-ed09-4235-bcd1-b898d18cbb97",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "86dc9c4f-c359-4d13-b8ae-5a9d589643ad",
            "compositeImage": {
                "id": "bd3d263a-1eb3-4cef-835c-d9584e226695",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4f2928f1-ed09-4235-bcd1-b898d18cbb97",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "967b97b6-d6c9-4e61-a2e7-b54d4955553d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4f2928f1-ed09-4235-bcd1-b898d18cbb97",
                    "LayerId": "87847254-58df-409d-b087-c160bfbbc9dd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "87847254-58df-409d-b087-c160bfbbc9dd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "86dc9c4f-c359-4d13-b8ae-5a9d589643ad",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 5
}