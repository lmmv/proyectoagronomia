{
    "id": "51b0f740-fd7f-4724-ab69-e5106e518475",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_exit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1800,
    "bbox_left": 0,
    "bbox_right": 3200,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0b48ed42-8f55-4efe-947a-ec6d592f806c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "51b0f740-fd7f-4724-ab69-e5106e518475",
            "compositeImage": {
                "id": "f5f1e708-7592-4b79-907a-d7054a216c0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0b48ed42-8f55-4efe-947a-ec6d592f806c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f96b9507-14e0-474a-848e-449604c34fed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0b48ed42-8f55-4efe-947a-ec6d592f806c",
                    "LayerId": "5e4fb02f-befe-450d-a7aa-cd8640a0125d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1801,
    "layers": [
        {
            "id": "5e4fb02f-befe-450d-a7aa-cd8640a0125d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "51b0f740-fd7f-4724-ab69-e5106e518475",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 3201,
    "xorig": 0,
    "yorig": 0
}