{
    "id": "83eac364-43c5-4f92-a14c-24dbfaf3847d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ok_no",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9c7ec79e-acf1-402e-b02e-48796866277f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83eac364-43c5-4f92-a14c-24dbfaf3847d",
            "compositeImage": {
                "id": "bd51521a-8997-4a3b-90b3-73fde0edf56d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c7ec79e-acf1-402e-b02e-48796866277f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "da5696ce-f750-4c3d-af3a-9bb818e5be4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c7ec79e-acf1-402e-b02e-48796866277f",
                    "LayerId": "8e4b697b-4c6b-4075-a392-6bca386feee1"
                }
            ]
        },
        {
            "id": "6a3fcf7a-acf3-4196-a04e-6656ef8211c6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "83eac364-43c5-4f92-a14c-24dbfaf3847d",
            "compositeImage": {
                "id": "82fa84be-bda3-4cbc-849f-96a781aa505e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6a3fcf7a-acf3-4196-a04e-6656ef8211c6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0683e37-e4eb-495b-9711-05d848679c34",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6a3fcf7a-acf3-4196-a04e-6656ef8211c6",
                    "LayerId": "8e4b697b-4c6b-4075-a392-6bca386feee1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "8e4b697b-4c6b-4075-a392-6bca386feee1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "83eac364-43c5-4f92-a14c-24dbfaf3847d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}