{
    "id": "dae0d1a8-9504-43e3-a359-05fb6e7ab270",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_treePine",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 253,
    "bbox_left": 0,
    "bbox_right": 105,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9a2a2a99-cc77-4552-8168-2adfd3cf1e3e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dae0d1a8-9504-43e3-a359-05fb6e7ab270",
            "compositeImage": {
                "id": "48a36294-a2f3-4f04-b38c-17aadb6744f0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9a2a2a99-cc77-4552-8168-2adfd3cf1e3e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "65d0c2e0-c3ba-4c9a-8721-907abb3ea27b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9a2a2a99-cc77-4552-8168-2adfd3cf1e3e",
                    "LayerId": "28221f36-06d7-4845-8420-8f0927884d02"
                }
            ]
        },
        {
            "id": "0880b7f7-4961-45e1-8461-571f317517d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dae0d1a8-9504-43e3-a359-05fb6e7ab270",
            "compositeImage": {
                "id": "b1dc8083-48f3-4423-a1f2-8d5c01b2a730",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0880b7f7-4961-45e1-8461-571f317517d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "786b5c19-580f-4622-8b29-9ffa95ae2c33",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0880b7f7-4961-45e1-8461-571f317517d1",
                    "LayerId": "28221f36-06d7-4845-8420-8f0927884d02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 254,
    "layers": [
        {
            "id": "28221f36-06d7-4845-8420-8f0927884d02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dae0d1a8-9504-43e3-a359-05fb6e7ab270",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 6,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 106,
    "xorig": 0,
    "yorig": 253
}