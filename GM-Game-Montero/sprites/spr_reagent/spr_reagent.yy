{
    "id": "eb87b8e7-eae1-4429-a1f0-97123bcbc3c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_reagent",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 107,
    "bbox_right": 118,
    "bbox_top": 27,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2925f43e-85db-44e8-945b-bb9d7273a8fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "eb87b8e7-eae1-4429-a1f0-97123bcbc3c4",
            "compositeImage": {
                "id": "60887cfa-ef5a-439b-858c-126697df0ae8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2925f43e-85db-44e8-945b-bb9d7273a8fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bdbc4b2-bb5b-420d-bd5c-b8cb9865ddbc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2925f43e-85db-44e8-945b-bb9d7273a8fc",
                    "LayerId": "83927ce4-d691-4f1c-aebb-a15116d25f0f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 256,
    "layers": [
        {
            "id": "83927ce4-d691-4f1c-aebb-a15116d25f0f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "eb87b8e7-eae1-4429-a1f0-97123bcbc3c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 128,
    "yorig": 128
}