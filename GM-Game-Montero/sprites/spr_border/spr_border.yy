{
    "id": "501969a0-e73c-4cfe-92c5-64c7c50705d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_border",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f68fee3-a2b6-4f50-a14a-98a44cd6052d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "501969a0-e73c-4cfe-92c5-64c7c50705d6",
            "compositeImage": {
                "id": "db5a2fff-b2a0-4bf5-a882-c6d54117234f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f68fee3-a2b6-4f50-a14a-98a44cd6052d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d5758200-843f-467a-996c-f92eb8eb9775",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f68fee3-a2b6-4f50-a14a-98a44cd6052d",
                    "LayerId": "12c521f4-70e2-450f-8fbe-c4cc730f6c9d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "12c521f4-70e2-450f-8fbe-c4cc730f6c9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "501969a0-e73c-4cfe-92c5-64c7c50705d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}