{
    "id": "35a7a401-c8db-4d7b-94af-cbb667be7e81",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_paddle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 69,
    "bbox_left": 29,
    "bbox_right": 100,
    "bbox_top": 47,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2bf3c59d-75d4-41ca-8aa2-bbb31d15e876",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35a7a401-c8db-4d7b-94af-cbb667be7e81",
            "compositeImage": {
                "id": "eb83951c-ad00-4c95-a4d4-d8a0d61ce2e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2bf3c59d-75d4-41ca-8aa2-bbb31d15e876",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee4d9fb5-36f2-4d5f-92d7-c2874bab69e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bf3c59d-75d4-41ca-8aa2-bbb31d15e876",
                    "LayerId": "5ceb358f-48cf-464e-9032-0caa78440b21"
                },
                {
                    "id": "f39309de-e1d3-4836-b771-0a138db40821",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2bf3c59d-75d4-41ca-8aa2-bbb31d15e876",
                    "LayerId": "389e2e4d-9b27-4344-bfa4-53b620a8ccf3"
                }
            ]
        },
        {
            "id": "2915a813-da16-4386-8c69-6ef418e0b924",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35a7a401-c8db-4d7b-94af-cbb667be7e81",
            "compositeImage": {
                "id": "6a477912-631f-4ef1-8043-9f71e33044ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2915a813-da16-4386-8c69-6ef418e0b924",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d04ae18f-c683-4fc9-b487-29757e9a20de",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2915a813-da16-4386-8c69-6ef418e0b924",
                    "LayerId": "5ceb358f-48cf-464e-9032-0caa78440b21"
                },
                {
                    "id": "3b41d799-4900-4c0a-bba1-35b0e02093e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2915a813-da16-4386-8c69-6ef418e0b924",
                    "LayerId": "389e2e4d-9b27-4344-bfa4-53b620a8ccf3"
                }
            ]
        },
        {
            "id": "fcfa7f02-74ad-441f-b3c7-9e677e21cca5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35a7a401-c8db-4d7b-94af-cbb667be7e81",
            "compositeImage": {
                "id": "11b8c2d3-cc18-42a8-879a-e2819b531e45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcfa7f02-74ad-441f-b3c7-9e677e21cca5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "57a11782-b044-475f-a418-583633b23039",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcfa7f02-74ad-441f-b3c7-9e677e21cca5",
                    "LayerId": "5ceb358f-48cf-464e-9032-0caa78440b21"
                },
                {
                    "id": "cd7b3925-8f99-4c8d-a515-1b54257bc4ca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcfa7f02-74ad-441f-b3c7-9e677e21cca5",
                    "LayerId": "389e2e4d-9b27-4344-bfa4-53b620a8ccf3"
                }
            ]
        },
        {
            "id": "88acdbbc-5be3-4ee7-a76f-ea4416edcb8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35a7a401-c8db-4d7b-94af-cbb667be7e81",
            "compositeImage": {
                "id": "8d82d4b1-ae02-402d-a86e-2ce691f7841f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "88acdbbc-5be3-4ee7-a76f-ea4416edcb8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "149d7101-0dd4-4033-9191-5c4760d51564",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88acdbbc-5be3-4ee7-a76f-ea4416edcb8f",
                    "LayerId": "5ceb358f-48cf-464e-9032-0caa78440b21"
                },
                {
                    "id": "4cc8939a-ff4a-4b31-9ad8-b0c2b24209b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "88acdbbc-5be3-4ee7-a76f-ea4416edcb8f",
                    "LayerId": "389e2e4d-9b27-4344-bfa4-53b620a8ccf3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "5ceb358f-48cf-464e-9032-0caa78440b21",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35a7a401-c8db-4d7b-94af-cbb667be7e81",
            "blendMode": 0,
            "isLocked": false,
            "name": "default (2)",
            "opacity": 28,
            "visible": true
        },
        {
            "id": "389e2e4d-9b27-4344-bfa4-53b620a8ccf3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35a7a401-c8db-4d7b-94af-cbb667be7e81",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}