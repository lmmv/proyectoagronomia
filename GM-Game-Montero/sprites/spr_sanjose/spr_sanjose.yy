{
    "id": "2cd81e5b-f1dc-4c45-9005-879f24095143",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sanjose",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 310,
    "bbox_left": 0,
    "bbox_right": 314,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b4562f3d-1e0b-4b68-81c6-512fa0d68cea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2cd81e5b-f1dc-4c45-9005-879f24095143",
            "compositeImage": {
                "id": "2085d892-0393-4d11-a2e6-b96081afe62d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b4562f3d-1e0b-4b68-81c6-512fa0d68cea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96ee1699-ea31-4956-8e83-2939f50e4f88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b4562f3d-1e0b-4b68-81c6-512fa0d68cea",
                    "LayerId": "cbb7a764-0b75-4f6c-abf4-85822e852649"
                }
            ]
        },
        {
            "id": "ddcc2fdc-b8cb-4b8d-bdae-a0acecff4457",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2cd81e5b-f1dc-4c45-9005-879f24095143",
            "compositeImage": {
                "id": "05ffb8b5-4010-4f8c-a966-3b6c3fd698f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ddcc2fdc-b8cb-4b8d-bdae-a0acecff4457",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08d24124-d8ef-411c-906f-425f0edc8c09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ddcc2fdc-b8cb-4b8d-bdae-a0acecff4457",
                    "LayerId": "cbb7a764-0b75-4f6c-abf4-85822e852649"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 311,
    "layers": [
        {
            "id": "cbb7a764-0b75-4f6c-abf4-85822e852649",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2cd81e5b-f1dc-4c45-9005-879f24095143",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 315,
    "xorig": 0,
    "yorig": 0
}