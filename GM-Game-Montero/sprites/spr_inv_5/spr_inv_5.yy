{
    "id": "64272702-afd2-4b43-8761-e442a51a1d10",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inv_5",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f671e3ce-6c48-42cc-b0ff-2dd3fc44b393",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "64272702-afd2-4b43-8761-e442a51a1d10",
            "compositeImage": {
                "id": "e7f1ad82-d791-41fb-b01e-9c2a6da5089f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f671e3ce-6c48-42cc-b0ff-2dd3fc44b393",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "341e9773-1c02-41ed-ad4d-f62b6c892e39",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f671e3ce-6c48-42cc-b0ff-2dd3fc44b393",
                    "LayerId": "25fb24e8-c0ff-42d9-a262-e8de4283092d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "25fb24e8-c0ff-42d9-a262-e8de4283092d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "64272702-afd2-4b43-8761-e442a51a1d10",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}