{
    "id": "47d7e4e5-dc90-4713-bc6b-8cbb1341264e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background_2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1799,
    "bbox_left": 0,
    "bbox_right": 3199,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c8290f67-0d62-461d-bf55-d364abcd8952",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47d7e4e5-dc90-4713-bc6b-8cbb1341264e",
            "compositeImage": {
                "id": "5fde3b37-31e1-409a-8458-b471dd47ef83",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c8290f67-0d62-461d-bf55-d364abcd8952",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5adb5c5a-f200-4df5-87da-1f9ef5e45d74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c8290f67-0d62-461d-bf55-d364abcd8952",
                    "LayerId": "5e5ab4d4-ecb8-4298-9ca0-1d2e7c41b983"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1800,
    "layers": [
        {
            "id": "5e5ab4d4-ecb8-4298-9ca0-1d2e7c41b983",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47d7e4e5-dc90-4713-bc6b-8cbb1341264e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 3200,
    "xorig": 0,
    "yorig": 0
}