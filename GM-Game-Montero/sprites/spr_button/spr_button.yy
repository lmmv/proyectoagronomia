{
    "id": "ae221623-3728-43ae-9d27-df554bf3fd87",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 74,
    "bbox_left": 9,
    "bbox_right": 182,
    "bbox_top": 21,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7d49de7-4746-4952-8749-127255db405f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ae221623-3728-43ae-9d27-df554bf3fd87",
            "compositeImage": {
                "id": "4c66e416-c268-443f-b8a2-d3b69c4d822f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7d49de7-4746-4952-8749-127255db405f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0830c9c5-3486-41f0-b377-620f10ff5860",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7d49de7-4746-4952-8749-127255db405f",
                    "LayerId": "6faad642-d909-4d6f-b02f-c6429d9b115c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "6faad642-d909-4d6f-b02f-c6429d9b115c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ae221623-3728-43ae-9d27-df554bf3fd87",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 192,
    "xorig": 96,
    "yorig": 48
}