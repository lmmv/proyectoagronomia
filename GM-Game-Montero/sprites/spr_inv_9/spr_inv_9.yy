{
    "id": "3e038775-85c7-4ddc-b301-e25302b76db1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inv_9",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "02b3ae02-e743-458b-b770-20b613accb50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3e038775-85c7-4ddc-b301-e25302b76db1",
            "compositeImage": {
                "id": "30eb39a1-6e88-4651-9330-b7b2345c7bf9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02b3ae02-e743-458b-b770-20b613accb50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9402df37-cb29-4814-b399-ea1705e35069",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02b3ae02-e743-458b-b770-20b613accb50",
                    "LayerId": "125729bd-87c2-4716-acb3-77abb89f1b11"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "125729bd-87c2-4716-acb3-77abb89f1b11",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3e038775-85c7-4ddc-b301-e25302b76db1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}