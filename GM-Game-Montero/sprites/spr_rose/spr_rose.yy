{
    "id": "edb76a97-f303-4b54-8051-8d15a8fd2704",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_rose",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 135,
    "bbox_left": 0,
    "bbox_right": 135,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03f5b9c2-e545-44d7-9da8-c657b6c0b235",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "edb76a97-f303-4b54-8051-8d15a8fd2704",
            "compositeImage": {
                "id": "fe9261d9-fe5c-49c9-984e-e41bb7857e8e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03f5b9c2-e545-44d7-9da8-c657b6c0b235",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af85f974-4120-46e5-9d8d-b5bc1f25cdeb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03f5b9c2-e545-44d7-9da8-c657b6c0b235",
                    "LayerId": "4e7e59ce-745e-4969-8ea8-9f00e9897648"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 136,
    "layers": [
        {
            "id": "4e7e59ce-745e-4969-8ea8-9f00e9897648",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "edb76a97-f303-4b54-8051-8d15a8fd2704",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 136,
    "xorig": 0,
    "yorig": 0
}