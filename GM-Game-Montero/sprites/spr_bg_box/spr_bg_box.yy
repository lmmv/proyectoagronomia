{
    "id": "cedd8f84-a093-4999-bbf4-f16fd021d96c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg_box",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 446,
    "bbox_left": 0,
    "bbox_right": 424,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2ce9a274-a337-400e-9640-9e4d33ce496b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "cedd8f84-a093-4999-bbf4-f16fd021d96c",
            "compositeImage": {
                "id": "e9e0ca9e-e76d-468f-9d97-015efb4c7e09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ce9a274-a337-400e-9640-9e4d33ce496b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0a07b049-73ab-49ce-bbc0-b10d6c8b87af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ce9a274-a337-400e-9640-9e4d33ce496b",
                    "LayerId": "b2ee95f9-2735-4ba3-b7af-c504cc190e74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 447,
    "layers": [
        {
            "id": "b2ee95f9-2735-4ba3-b7af-c504cc190e74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "cedd8f84-a093-4999-bbf4-f16fd021d96c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 425,
    "xorig": 212,
    "yorig": 223
}