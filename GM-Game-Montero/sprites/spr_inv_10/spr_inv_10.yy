{
    "id": "56be057e-b218-4900-b7e6-6cf636d61c34",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inv_10",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "18924665-1df1-44b0-b759-7e0d25c1fcf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "56be057e-b218-4900-b7e6-6cf636d61c34",
            "compositeImage": {
                "id": "2b89d870-5a96-47a1-894a-d50012ccc432",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "18924665-1df1-44b0-b759-7e0d25c1fcf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0fc2e04f-3a67-4d62-b7ad-ca660419c67e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "18924665-1df1-44b0-b759-7e0d25c1fcf2",
                    "LayerId": "62957b8e-1809-49a6-848f-0ad9a8d98ba8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "62957b8e-1809-49a6-848f-0ad9a8d98ba8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "56be057e-b218-4900-b7e6-6cf636d61c34",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}