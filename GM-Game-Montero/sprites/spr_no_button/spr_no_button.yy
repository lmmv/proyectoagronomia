{
    "id": "ad67352d-0892-469f-b25d-9ece3de385b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_no_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 138,
    "bbox_left": 0,
    "bbox_right": 499,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d5d11bb0-08b9-4f70-b47c-dea99aa668c4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad67352d-0892-469f-b25d-9ece3de385b9",
            "compositeImage": {
                "id": "76646558-ed86-4529-bd24-1def0e3dbd6b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d5d11bb0-08b9-4f70-b47c-dea99aa668c4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7460fed9-c47e-4d1a-afa9-01b510a56f91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d5d11bb0-08b9-4f70-b47c-dea99aa668c4",
                    "LayerId": "51799e19-026d-4226-b9e6-5dd72bfc53a4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 139,
    "layers": [
        {
            "id": "51799e19-026d-4226-b9e6-5dd72bfc53a4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad67352d-0892-469f-b25d-9ece3de385b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 500,
    "xorig": 250,
    "yorig": 69
}