{
    "id": "c465905e-0aa5-4be9-8eea-bed0fba2f50b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_coffee_mm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 58,
    "bbox_left": 5,
    "bbox_right": 58,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0cf7d967-4b6f-4069-9947-c167f0ad9f0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c465905e-0aa5-4be9-8eea-bed0fba2f50b",
            "compositeImage": {
                "id": "8acbf1b4-387f-4059-80ec-5aa8fe0d9cd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0cf7d967-4b6f-4069-9947-c167f0ad9f0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29a86c00-90c0-4afd-9cf0-b20a69fb481c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0cf7d967-4b6f-4069-9947-c167f0ad9f0a",
                    "LayerId": "fc9c3cc7-d438-4375-b0a3-e59e784c7843"
                }
            ]
        },
        {
            "id": "172de0d9-7959-448e-b25c-bde4da86ce25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c465905e-0aa5-4be9-8eea-bed0fba2f50b",
            "compositeImage": {
                "id": "c46f4304-2f8b-4927-a7d7-40f84453f45d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "172de0d9-7959-448e-b25c-bde4da86ce25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d194804-7418-4dcc-baf5-0a92cbf255e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "172de0d9-7959-448e-b25c-bde4da86ce25",
                    "LayerId": "fc9c3cc7-d438-4375-b0a3-e59e784c7843"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fc9c3cc7-d438-4375-b0a3-e59e784c7843",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c465905e-0aa5-4be9-8eea-bed0fba2f50b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 63
}