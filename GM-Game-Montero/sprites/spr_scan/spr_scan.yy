{
    "id": "3dae4dd8-c032-43ad-8a87-6a02cd36a1f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_scan",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7304b16a-1323-4f33-92a2-549f747ac104",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3dae4dd8-c032-43ad-8a87-6a02cd36a1f9",
            "compositeImage": {
                "id": "f3526685-5cc8-4221-b909-3ab7da2b9f7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7304b16a-1323-4f33-92a2-549f747ac104",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "91ab30d3-6e2a-45a7-8580-ed62eae9ddae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7304b16a-1323-4f33-92a2-549f747ac104",
                    "LayerId": "dba08fa2-cb89-4ba9-85a8-164af3b02ea0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "dba08fa2-cb89-4ba9-85a8-164af3b02ea0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3dae4dd8-c032-43ad-8a87-6a02cd36a1f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}