{
    "id": "23e46ef2-fd2d-4fec-a950-3f0628548415",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inv_11",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "83413449-ff68-4e69-b736-239bfa791735",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23e46ef2-fd2d-4fec-a950-3f0628548415",
            "compositeImage": {
                "id": "702741aa-44aa-40d1-b27c-8f6c8e19fa9f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "83413449-ff68-4e69-b736-239bfa791735",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8a82ffe-70ad-4862-9a09-5ed714f17ee5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "83413449-ff68-4e69-b736-239bfa791735",
                    "LayerId": "95cf5b20-d518-4885-83ca-ee1e2541d00e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "95cf5b20-d518-4885-83ca-ee1e2541d00e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23e46ef2-fd2d-4fec-a950-3f0628548415",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}