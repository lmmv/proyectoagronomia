{
    "id": "229fbc8f-81db-407b-8dab-671a9e2c50bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_warning",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 95,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fcf8ffd3-7760-486c-8e86-ec78c9fded36",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "229fbc8f-81db-407b-8dab-671a9e2c50bb",
            "compositeImage": {
                "id": "4f50edf2-bef4-4afb-a217-aed31ab17d4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fcf8ffd3-7760-486c-8e86-ec78c9fded36",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d48f3a89-f99a-4b37-ae6d-a1d85a877512",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fcf8ffd3-7760-486c-8e86-ec78c9fded36",
                    "LayerId": "c69a6925-16f0-4376-9b66-59f6eb68b418"
                }
            ]
        },
        {
            "id": "05b6a700-a216-44ed-bc26-90dc43663ed9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "229fbc8f-81db-407b-8dab-671a9e2c50bb",
            "compositeImage": {
                "id": "5602c882-8e3f-499f-88c6-558a0a96ecdc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05b6a700-a216-44ed-bc26-90dc43663ed9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bbf4a63-c634-4a5c-a589-e22e633c4dc0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05b6a700-a216-44ed-bc26-90dc43663ed9",
                    "LayerId": "c69a6925-16f0-4376-9b66-59f6eb68b418"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 96,
    "layers": [
        {
            "id": "c69a6925-16f0-4376-9b66-59f6eb68b418",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "229fbc8f-81db-407b-8dab-671a9e2c50bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 48,
    "yorig": 48
}