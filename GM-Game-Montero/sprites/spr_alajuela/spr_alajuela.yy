{
    "id": "e22222e0-df24-4ef4-a1f3-d745ad1f75b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_alajuela",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 342,
    "bbox_left": 0,
    "bbox_right": 347,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "09399c57-f82c-43b2-a10b-8ca0ca2a066d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22222e0-df24-4ef4-a1f3-d745ad1f75b0",
            "compositeImage": {
                "id": "90e559a2-f8fa-4bc7-9db1-c78aeaa65f47",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09399c57-f82c-43b2-a10b-8ca0ca2a066d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6de3bd28-9008-4e88-ad31-c3739a27890d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09399c57-f82c-43b2-a10b-8ca0ca2a066d",
                    "LayerId": "6fb3f47f-8afa-4c12-8e63-f7e6e4c2afb7"
                }
            ]
        },
        {
            "id": "46614172-253e-4e67-b3fc-40627f7b6cdc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e22222e0-df24-4ef4-a1f3-d745ad1f75b0",
            "compositeImage": {
                "id": "1cf14e07-6c99-486a-8001-49c622160d1f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "46614172-253e-4e67-b3fc-40627f7b6cdc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3a4449f-f8ac-45c1-b641-9afd5a5ca41e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "46614172-253e-4e67-b3fc-40627f7b6cdc",
                    "LayerId": "6fb3f47f-8afa-4c12-8e63-f7e6e4c2afb7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 343,
    "layers": [
        {
            "id": "6fb3f47f-8afa-4c12-8e63-f7e6e4c2afb7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e22222e0-df24-4ef4-a1f3-d745ad1f75b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 348,
    "xorig": 0,
    "yorig": 0
}