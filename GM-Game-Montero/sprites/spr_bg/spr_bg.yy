{
    "id": "e636957e-4664-4608-9507-44dd510a92c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 514,
    "bbox_left": 31,
    "bbox_right": 928,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b046ba5c-1f0a-463b-876a-4af5b3d2aa53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e636957e-4664-4608-9507-44dd510a92c4",
            "compositeImage": {
                "id": "23c07af8-3dbb-4879-a5dd-f7851c0fc0ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b046ba5c-1f0a-463b-876a-4af5b3d2aa53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a59fbc7-a6e2-424e-91db-0552a1ddf019",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b046ba5c-1f0a-463b-876a-4af5b3d2aa53",
                    "LayerId": "a4035fe6-149f-4a8e-bf24-1ae892e2705f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 540,
    "layers": [
        {
            "id": "a4035fe6-149f-4a8e-bf24-1ae892e2705f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e636957e-4664-4608-9507-44dd510a92c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 81,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 960,
    "xorig": 480,
    "yorig": 270
}