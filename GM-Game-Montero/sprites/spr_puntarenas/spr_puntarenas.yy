{
    "id": "8b0a0318-14ed-4ee2-9ee6-b7c8bd88f75b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_puntarenas",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 629,
    "bbox_left": 0,
    "bbox_right": 699,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d2208c0a-9a37-4c6c-a19a-5b427f8fd197",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b0a0318-14ed-4ee2-9ee6-b7c8bd88f75b",
            "compositeImage": {
                "id": "e5eb5a42-7c8a-4456-abe1-636aa132cc1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d2208c0a-9a37-4c6c-a19a-5b427f8fd197",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "99983d00-72cf-46ec-8bd2-07e58c8bdc0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d2208c0a-9a37-4c6c-a19a-5b427f8fd197",
                    "LayerId": "9103e0f1-ac50-4425-8956-31dd2ba4d939"
                }
            ]
        },
        {
            "id": "6ad87f70-b709-41c2-80ca-9d67ddf1a0ee",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8b0a0318-14ed-4ee2-9ee6-b7c8bd88f75b",
            "compositeImage": {
                "id": "2320cbcb-ab36-4b63-b2a9-77f206bf86a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ad87f70-b709-41c2-80ca-9d67ddf1a0ee",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55b6caef-54d9-40e8-aa64-3db2ff00e243",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ad87f70-b709-41c2-80ca-9d67ddf1a0ee",
                    "LayerId": "9103e0f1-ac50-4425-8956-31dd2ba4d939"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 630,
    "layers": [
        {
            "id": "9103e0f1-ac50-4425-8956-31dd2ba4d939",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8b0a0318-14ed-4ee2-9ee6-b7c8bd88f75b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 700,
    "xorig": 0,
    "yorig": 0
}