{
    "id": "4f8ccdc8-ac05-4bd1-8d56-71738a1857bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_limon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 510,
    "bbox_left": 0,
    "bbox_right": 373,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fa8ef526-7754-44bb-b4b0-46e46ea676df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f8ccdc8-ac05-4bd1-8d56-71738a1857bd",
            "compositeImage": {
                "id": "bc9341a2-2979-4350-9ade-01cac466d37a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa8ef526-7754-44bb-b4b0-46e46ea676df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a83c7126-fa68-43b6-95c9-6ecf4bccce18",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa8ef526-7754-44bb-b4b0-46e46ea676df",
                    "LayerId": "37612fb8-4a73-4c66-a221-80d0fe4dedce"
                }
            ]
        },
        {
            "id": "ecebfb34-b5d3-4f07-8de9-493662a769a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f8ccdc8-ac05-4bd1-8d56-71738a1857bd",
            "compositeImage": {
                "id": "f59cd8ac-77b4-4d25-8306-0d87da43111e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ecebfb34-b5d3-4f07-8de9-493662a769a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f08128c7-0c56-4c22-8b4e-a7bc8080ff26",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ecebfb34-b5d3-4f07-8de9-493662a769a5",
                    "LayerId": "37612fb8-4a73-4c66-a221-80d0fe4dedce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 511,
    "layers": [
        {
            "id": "37612fb8-4a73-4c66-a221-80d0fe4dedce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f8ccdc8-ac05-4bd1-8d56-71738a1857bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 374,
    "xorig": 0,
    "yorig": 0
}