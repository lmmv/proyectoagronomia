{
    "id": "c427d804-0ff2-4b5c-9056-1aa23bd6106a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_play_button",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 345,
    "bbox_left": 0,
    "bbox_right": 522,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d1e1f3e-d95b-44b9-bba7-b178f0151027",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c427d804-0ff2-4b5c-9056-1aa23bd6106a",
            "compositeImage": {
                "id": "e8eeeae6-3c70-47ae-9196-5274603e08e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d1e1f3e-d95b-44b9-bba7-b178f0151027",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9a9932d8-0b90-42ae-a6fb-3b2b177d5270",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d1e1f3e-d95b-44b9-bba7-b178f0151027",
                    "LayerId": "632f0648-48d4-4bd0-b856-9aa73d98513e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 346,
    "layers": [
        {
            "id": "632f0648-48d4-4bd0-b856-9aa73d98513e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c427d804-0ff2-4b5c-9056-1aa23bd6106a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 523,
    "xorig": 261,
    "yorig": 173
}