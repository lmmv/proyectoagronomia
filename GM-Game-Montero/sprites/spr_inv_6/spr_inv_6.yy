{
    "id": "4676d9cf-3bb6-4405-af8a-330db2d5395b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inv_6",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7f1d54c1-57ec-4af6-8de5-c2361ef6aca5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4676d9cf-3bb6-4405-af8a-330db2d5395b",
            "compositeImage": {
                "id": "0154aecf-8c97-4ba3-aa7c-3cbad00432d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f1d54c1-57ec-4af6-8de5-c2361ef6aca5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee3f85ad-3055-4634-a850-ba0230cee05b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f1d54c1-57ec-4af6-8de5-c2361ef6aca5",
                    "LayerId": "ba831181-37ed-494f-88f4-f85b8c9a3ab7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "ba831181-37ed-494f-88f4-f85b8c9a3ab7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4676d9cf-3bb6-4405-af8a-330db2d5395b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}