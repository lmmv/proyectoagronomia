{
    "id": "292f39af-9b0e-43ff-8eec-46d2b10eaec5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_mentor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 642,
    "bbox_left": 4,
    "bbox_right": 368,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0137ab9a-1c0d-4b76-9d2a-f274a5197c0f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "292f39af-9b0e-43ff-8eec-46d2b10eaec5",
            "compositeImage": {
                "id": "3a9d2a6c-c25f-4ea7-ad46-2229ea92fb49",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0137ab9a-1c0d-4b76-9d2a-f274a5197c0f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c0ebb03f-8ae6-4158-af74-266beddf1448",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0137ab9a-1c0d-4b76-9d2a-f274a5197c0f",
                    "LayerId": "045c84b3-6bfb-4530-84a2-e0c10f1aa890"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 645,
    "layers": [
        {
            "id": "045c84b3-6bfb-4530-84a2-e0c10f1aa890",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "292f39af-9b0e-43ff-8eec-46d2b10eaec5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 373,
    "xorig": 186,
    "yorig": 322
}