{
    "id": "fdc055f3-5f6c-4dbf-a934-19fd1f76840b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_unedgames",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1171,
    "bbox_left": 0,
    "bbox_right": 664,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19673328-390d-4da4-89db-c1d323e14cb7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fdc055f3-5f6c-4dbf-a934-19fd1f76840b",
            "compositeImage": {
                "id": "fa036c2d-12d6-4891-a974-60dc1e706ee1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19673328-390d-4da4-89db-c1d323e14cb7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9e69abb7-3789-484c-bd6a-7f6855dcfb02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19673328-390d-4da4-89db-c1d323e14cb7",
                    "LayerId": "f03bb2d5-c780-4af0-8380-31165c3c8d63"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1172,
    "layers": [
        {
            "id": "f03bb2d5-c780-4af0-8380-31165c3c8d63",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fdc055f3-5f6c-4dbf-a934-19fd1f76840b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 665,
    "xorig": 332,
    "yorig": 586
}