{
    "id": "574f2804-3bdf-4768-9693-733c9a538b57",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_inv_7",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4ca1c30d-b46f-4ac9-871e-ae0b32101770",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "574f2804-3bdf-4768-9693-733c9a538b57",
            "compositeImage": {
                "id": "434cd8da-031e-446a-beb8-ac13122cc712",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ca1c30d-b46f-4ac9-871e-ae0b32101770",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73dbbf25-bd57-43f8-a594-7f716ad3ffc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ca1c30d-b46f-4ac9-871e-ae0b32101770",
                    "LayerId": "c73ac155-2e44-4570-a669-aa7666902e52"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "c73ac155-2e44-4570-a669-aa7666902e52",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "574f2804-3bdf-4768-9693-733c9a538b57",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}