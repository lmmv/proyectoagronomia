
{
    "name": "rm_level",
    "id": "98172ecd-4076-4ecd-9c93-d1cdf1c353d0",
    "creationCodeFile": "",
    "inheritCode": false,
    "inheritCreationOrder": false,
    "inheritLayers": false,
    "instanceCreationOrderIDs": [
        "9ac17faa-70fa-4391-be30-bd7af78127d3",
        "e8061924-ca3e-45eb-829b-bcf2165049bc",
        "4b86a688-db96-4504-be17-d17651339154",
        "28e60111-9f27-4484-92a2-f62d5609b5b4",
        "19e6b5da-8826-41e2-9fd9-e896dbc2e6db",
        "d33b4cbb-e1cb-460d-9cdd-350890d31fa8",
        "4195f8b1-fd42-4254-ba92-8aa96d40a0ab",
        "9dae7644-f659-4409-8d35-b7cc6fe401d7"
    ],
    "IsDnD": false,
    "layers": [
        {
            "name": "fld_button_instances",
            "id": "b55d28bf-2a23-4245-9518-3324ade87813",
            "depth": 0,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [
                {
                    "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
                    "name": "ins_buttons",
                    "id": "f51aba4d-9c29-4b0e-95c6-a0845f455322",
                    "depth": 100,
                    "grid_x": 32,
                    "grid_y": 32,
                    "hierarchyFrozen": false,
                    "hierarchyVisible": true,
                    "inheritLayerDepth": false,
                    "inheritLayerSettings": false,
                    "inheritSubLayers": false,
                    "inheritVisibility": false,
                    "instances": [
{"name": "inst_7FAFEE47","id": "9ac17faa-70fa-4391-be30-bd7af78127d3","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_7FAFEE47.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7FAFEE47","objId": "7c629935-7ef8-4aa6-85a7-e0001a2ce9c7","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 832,"y": 704},
{"name": "inst_4906E0CB","id": "e8061924-ca3e-45eb-829b-bcf2165049bc","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_4906E0CB.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_4906E0CB","objId": "7c629935-7ef8-4aa6-85a7-e0001a2ce9c7","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 512,"y": 384},
{"name": "inst_7893F979","id": "4b86a688-db96-4504-be17-d17651339154","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_7893F979.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7893F979","objId": "7c629935-7ef8-4aa6-85a7-e0001a2ce9c7","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 768,"y": 224},
{"name": "inst_7D1EEADA","id": "28e60111-9f27-4484-92a2-f62d5609b5b4","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_7D1EEADA.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_7D1EEADA","objId": "7c629935-7ef8-4aa6-85a7-e0001a2ce9c7","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 384,"y": 32},
{"name": "inst_3C600E93","id": "19e6b5da-8826-41e2-9fd9-e896dbc2e6db","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_3C600E93.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_3C600E93","objId": "7c629935-7ef8-4aa6-85a7-e0001a2ce9c7","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": -32,"y": 160},
{"name": "inst_2F36F34D","id": "d33b4cbb-e1cb-460d-9cdd-350890d31fa8","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_2F36F34D.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_2F36F34D","objId": "7c629935-7ef8-4aa6-85a7-e0001a2ce9c7","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 288,"y": 512},
{"name": "inst_1C62EAF0","id": "4195f8b1-fd42-4254-ba92-8aa96d40a0ab","colour": { "Value": 4294967295 },"creationCodeFile": "InstanceCreationCode_inst_1C62EAF0.gml","creationCodeType": ".gml","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_1C62EAF0","objId": "7c629935-7ef8-4aa6-85a7-e0001a2ce9c7","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 864,"y": 512}
                    ],
                    "layers": [

                    ],
                    "m_parentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "modelName": "GMRInstanceLayer",
                    "mvc": "1.0",
                    "userdefined_depth": false,
                    "visible": true
                },
                {
                    "__type": "GMRInstanceLayer_Model:#YoYoStudio.MVCFormat",
                    "name": "ins_map",
                    "id": "f248039a-3888-4796-92d5-ea8f26994234",
                    "depth": 200,
                    "grid_x": 32,
                    "grid_y": 32,
                    "hierarchyFrozen": false,
                    "hierarchyVisible": true,
                    "inheritLayerDepth": false,
                    "inheritLayerSettings": false,
                    "inheritSubLayers": false,
                    "inheritVisibility": false,
                    "instances": [
{"name": "inst_235EAA13","id": "9dae7644-f659-4409-8d35-b7cc6fe401d7","colour": { "Value": 4294967295 },"creationCodeFile": "","creationCodeType": "","ignore": false,"imageIndex": 0,"imageSpeed": 1,"inheritCode": false,"inheritItemSettings": false,"IsDnD": false,"m_originalParentID": "00000000-0000-0000-0000-000000000000","m_serialiseFrozen": false,"modelName": "GMRInstance","name_with_no_file_rename": "inst_235EAA13","objId": "e621517d-9a77-4788-91a8-f80e8fc17781","properties": null,"rotation": 0,"scaleX": 1,"scaleY": 1,"mvc": "1.1","x": 384,"y": 448}
                    ],
                    "layers": [

                    ],
                    "m_parentID": "00000000-0000-0000-0000-000000000000",
                    "m_serialiseFrozen": false,
                    "modelName": "GMRInstanceLayer",
                    "mvc": "1.0",
                    "userdefined_depth": false,
                    "visible": true
                }
            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRLayer",
            "mvc": "1.0",
            "userdefined_depth": false,
            "visible": true
        },
        {
            "__type": "GMRBackgroundLayer_Model:#YoYoStudio.MVCFormat",
            "name": "Background",
            "id": "49e37506-4b3e-497f-9f77-711c20bad47c",
            "animationFPS": 15,
            "animationSpeedType": "0",
            "colour": { "Value": 4294967295 },
            "depth": 300,
            "grid_x": 32,
            "grid_y": 32,
            "hierarchyFrozen": false,
            "hierarchyVisible": true,
            "hspeed": 0,
            "htiled": false,
            "inheritLayerDepth": false,
            "inheritLayerSettings": false,
            "inheritSubLayers": false,
            "inheritVisibility": false,
            "layers": [

            ],
            "m_parentID": "00000000-0000-0000-0000-000000000000",
            "m_serialiseFrozen": false,
            "modelName": "GMRBackgroundLayer",
            "mvc": "1.0",
            "spriteId": "9e14ed8c-d7b9-4d1a-aafc-a9af62f61c07",
            "stretch": false,
            "userdefined_animFPS": false,
            "userdefined_depth": false,
            "visible": true,
            "vspeed": 0,
            "vtiled": false,
            "x": 0,
            "y": 0
        }
    ],
    "modelName": "GMRoom",
    "parentId": "00000000-0000-0000-0000-000000000000",
    "physicsSettings":     {
        "id": "144750b6-ef4d-43ac-81f0-040c390ee51f",
        "inheritPhysicsSettings": false,
        "modelName": "GMRoomPhysicsSettings",
        "PhysicsWorld": false,
        "PhysicsWorldGravityX": 0,
        "PhysicsWorldGravityY": 10,
        "PhysicsWorldPixToMeters": 0.1,
        "mvc": "1.0"
    },
    "roomSettings":     {
        "id": "5709bfed-892d-493b-82de-9087553dd79b",
        "Height": 640,
        "inheritRoomSettings": false,
        "modelName": "GMRoomSettings",
        "persistent": false,
        "mvc": "1.0",
        "Width": 960
    },
    "mvc": "1.0",
    "views": [
{"id": "1a0f4368-d684-443f-8a6d-062bf1f87deb","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "0bb6c838-cb25-4f78-a816-51fca99b7576","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "b00e0d51-b36f-499a-a259-67c7050a0262","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "1cbbe275-ecf9-44a5-920c-9ce42434b0e1","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "fad2d5ed-b34e-48c5-82d6-9d7399c65fd4","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "eef07eb7-76b0-47c8-bc05-2facf1ad1000","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "1cdb0d3e-f5e9-4ed8-8a14-9d29c88e9948","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0},
{"id": "f8c77a10-5c16-42a6-bdc7-71a669f36d68","hborder": 32,"hport": 768,"hspeed": -1,"hview": 768,"inherit": false,"modelName": "GMRView","objId": "00000000-0000-0000-0000-000000000000","mvc": "1.0","vborder": 32,"visible": false,"vspeed": -1,"wport": 1024,"wview": 1024,"xport": 0,"xview": 0,"yport": 0,"yview": 0}
    ],
    "viewSettings":     {
        "id": "cc7e036d-70c9-4136-a6c9-ab3ddb364d0f",
        "clearDisplayBuffer": true,
        "clearViewBackground": false,
        "enableViews": false,
        "inheritViewSettings": false,
        "modelName": "GMRoomViewSettings",
        "mvc": "1.0"
    }
}