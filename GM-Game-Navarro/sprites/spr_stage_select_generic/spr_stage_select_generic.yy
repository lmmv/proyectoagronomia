{
    "id": "d774b5d3-e78a-417a-947c-ba1168022f13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_stage_select_generic",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 295,
    "bbox_left": 0,
    "bbox_right": 300,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "23a4970e-d0fe-4c7c-b7db-46fd0d2ab88f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d774b5d3-e78a-417a-947c-ba1168022f13",
            "compositeImage": {
                "id": "d63cc8b8-6873-4ef6-930a-2b2a65753acd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23a4970e-d0fe-4c7c-b7db-46fd0d2ab88f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e196431-a4da-4320-b29c-814b431ce216",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23a4970e-d0fe-4c7c-b7db-46fd0d2ab88f",
                    "LayerId": "c00d0ee1-309f-4215-8883-36760c72a7c1"
                }
            ]
        },
        {
            "id": "92d0db43-a165-4779-972e-8dda763a7b09",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d774b5d3-e78a-417a-947c-ba1168022f13",
            "compositeImage": {
                "id": "56dad13e-fb15-4250-98d5-3c204434f483",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "92d0db43-a165-4779-972e-8dda763a7b09",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc5f261f-59af-479b-9786-554af6eee3d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "92d0db43-a165-4779-972e-8dda763a7b09",
                    "LayerId": "c00d0ee1-309f-4215-8883-36760c72a7c1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 296,
    "layers": [
        {
            "id": "c00d0ee1-309f-4215-8883-36760c72a7c1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d774b5d3-e78a-417a-947c-ba1168022f13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 301,
    "xorig": 0,
    "yorig": 0
}