{
    "id": "9e14ed8c-d7b9-4d1a-aafc-a9af62f61c07",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_background_mainmenu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 639,
    "bbox_left": 0,
    "bbox_right": 959,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "efbca981-9e3d-4729-83c5-8e52754d96c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9e14ed8c-d7b9-4d1a-aafc-a9af62f61c07",
            "compositeImage": {
                "id": "1dc285ef-639f-4ec6-88f2-d7507c831d0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "efbca981-9e3d-4729-83c5-8e52754d96c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b0939016-c80c-4b6c-92a0-a5286f378b8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "efbca981-9e3d-4729-83c5-8e52754d96c2",
                    "LayerId": "25532615-6e26-4d94-8c8b-e45a3483f0ce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 640,
    "layers": [
        {
            "id": "25532615-6e26-4d94-8c8b-e45a3483f0ce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9e14ed8c-d7b9-4d1a-aafc-a9af62f61c07",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 960,
    "xorig": 0,
    "yorig": 0
}