{
    "id": "b316bf50-306f-4fed-bf19-69d2898f9cce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_ui_mapa_cr",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 1268,
    "bbox_left": 296,
    "bbox_right": 1625,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1739870d-eace-4f8b-948f-ac1e63568c1d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b316bf50-306f-4fed-bf19-69d2898f9cce",
            "compositeImage": {
                "id": "5049989a-d3b9-45b4-b2bf-b56b9c56243d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1739870d-eace-4f8b-948f-ac1e63568c1d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "249ba11e-4ad1-4c16-9099-57f55e91197e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1739870d-eace-4f8b-948f-ac1e63568c1d",
                    "LayerId": "5582eab5-c701-43e3-a8f9-8e2ee9a96848"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1280,
    "layers": [
        {
            "id": "5582eab5-c701-43e3-a8f9-8e2ee9a96848",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b316bf50-306f-4fed-bf19-69d2898f9cce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1920,
    "xorig": 960,
    "yorig": 640
}