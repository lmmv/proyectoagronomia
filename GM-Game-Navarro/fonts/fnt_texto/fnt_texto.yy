{
    "id": "07461f90-e9d7-4694-9ddd-8893d4691f76",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_texto",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Helvetica Neue",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "8b2127ea-583a-414d-987f-31d7b4786de2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 49,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "5a626a0c-6ea2-494b-9ce1-6a592ee8be23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 49,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 410,
                "y": 104
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "aceed1c0-ce2e-4ad9-9f59-ed62803d3a11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 49,
                "offset": 3,
                "shift": 18,
                "w": 12,
                "x": 396,
                "y": 104
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "c79a24cb-6910-4a07-a861-a9390028c380",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 49,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 374,
                "y": 104
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "b245bcdb-8bb6-4bed-a037-ed90693ea67b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 49,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 350,
                "y": 104
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "344e89a7-33d4-4f9f-afd0-076003f70f2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 49,
                "offset": 3,
                "shift": 40,
                "w": 34,
                "x": 314,
                "y": 104
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "06b73596-0a34-4a14-8f2c-3f67d08e0ef5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 49,
                "offset": 1,
                "shift": 26,
                "w": 26,
                "x": 286,
                "y": 104
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "c99a9d8d-7a5b-4b1a-b111-8d9dc70748ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 49,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 279,
                "y": 104
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "699336dc-4ae0-4bff-af73-f48ec0713f11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 49,
                "offset": 1,
                "shift": 11,
                "w": 11,
                "x": 266,
                "y": 104
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "5d2896e9-2b91-49cb-8f3b-652369bb881b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 49,
                "offset": -1,
                "shift": 11,
                "w": 11,
                "x": 253,
                "y": 104
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "1bfac99b-5fa8-4d0e-b904-7bf6c950e3d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 49,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 419,
                "y": 104
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "bae322f6-4199-4a1a-a59e-bac0ecb08516",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 229,
                "y": 104
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "bc237576-ed58-4772-bd77-90d8b3b19621",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 49,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 198,
                "y": 104
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "b95c7c33-1378-45b2-a186-28075c1c13d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 49,
                "offset": 1,
                "shift": 16,
                "w": 13,
                "x": 183,
                "y": 104
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "bc580939-b63d-441e-81f5-b6ccff683e93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 49,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 174,
                "y": 104
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "10b22259-500d-4909-b6d9-f183be38648e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 49,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 156,
                "y": 104
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "bc62640f-5dd0-44ec-bb00-7b76d9205d85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 49,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 134,
                "y": 104
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "2e61f688-c1f5-4c13-8f46-4056fe68428a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 49,
                "offset": 2,
                "shift": 22,
                "w": 12,
                "x": 120,
                "y": 104
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "dd677df6-2c1e-4802-9e10-76aa1309a65b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 49,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 98,
                "y": 104
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "d7d25967-ebae-4481-9617-aecb06265561",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 49,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 76,
                "y": 104
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "265e1813-b4d9-448d-b262-81b5907116b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 49,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 53,
                "y": 104
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5475247a-82d6-41da-8fb3-1a354c247326",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 49,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 207,
                "y": 104
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "1767dc69-5b18-441d-bba8-1717f62e0fd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 49,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 436,
                "y": 104
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e3400867-57f2-4c5b-9de0-4b18d4afc9f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 49,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 458,
                "y": 104
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "df147d62-d38e-48e7-baa0-4195c96928ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 49,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 480,
                "y": 104
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "c4089d92-32d1-4afd-afbf-d878d4c561b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 49,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 469,
                "y": 155
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e455ec6a-ee78-4f03-92ec-0b2a39130c7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 49,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 460,
                "y": 155
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "7465748e-5ef3-411f-beea-3b37046ae52f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 49,
                "offset": 2,
                "shift": 11,
                "w": 7,
                "x": 451,
                "y": 155
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3bda2cf9-057a-4de1-a1df-b7ba5fbeccd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 427,
                "y": 155
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "146281c7-2049-417e-844a-654fd7b6f856",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 403,
                "y": 155
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "1b8191a4-b834-4f07-ba4a-631b30f7c644",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 379,
                "y": 155
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "86dc6994-0a78-4e1e-820b-210374b5547c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 49,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 357,
                "y": 155
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "9734d156-ecd2-464f-94cc-4c03e36a60ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 49,
                "offset": 1,
                "shift": 32,
                "w": 30,
                "x": 325,
                "y": 155
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e89711c9-5dc9-4e86-96bc-e3429a29ffb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 49,
                "offset": -1,
                "shift": 27,
                "w": 28,
                "x": 295,
                "y": 155
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "57c1519d-c79b-4919-9617-c1a137ad48a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 49,
                "offset": 3,
                "shift": 28,
                "w": 24,
                "x": 269,
                "y": 155
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "9e6e680c-a440-4a90-b293-e3c3bd529f38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 49,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 240,
                "y": 155
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "4ec065ab-8022-4a05-8967-1ed709ae0213",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 49,
                "offset": 3,
                "shift": 29,
                "w": 25,
                "x": 213,
                "y": 155
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "06a0ec09-f4df-4cee-bbc4-6fcc2073edf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 49,
                "offset": 3,
                "shift": 25,
                "w": 21,
                "x": 190,
                "y": 155
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "65e8731f-2ff0-49e6-9c90-83c0d0455088",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 49,
                "offset": 3,
                "shift": 24,
                "w": 20,
                "x": 168,
                "y": 155
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "cb7e3da1-f58e-43b7-b75a-aa90af717165",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 49,
                "offset": 1,
                "shift": 30,
                "w": 27,
                "x": 139,
                "y": 155
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "31d11416-63ee-487d-9e6a-243141f0d8c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 49,
                "offset": 2,
                "shift": 29,
                "w": 24,
                "x": 113,
                "y": 155
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "9874d92a-adf9-42f0-a033-1ce8fd2861be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 49,
                "offset": 3,
                "shift": 11,
                "w": 6,
                "x": 105,
                "y": 155
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "fdeec027-31eb-4056-9a1b-ba9a13a62c3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 49,
                "offset": 0,
                "shift": 21,
                "w": 19,
                "x": 84,
                "y": 155
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "7d696022-9474-4541-a2a4-e0cef8a0ffc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 49,
                "offset": 3,
                "shift": 27,
                "w": 25,
                "x": 57,
                "y": 155
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "aaca2453-6607-454c-b9f0-60849b1b0c9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 49,
                "offset": 3,
                "shift": 23,
                "w": 20,
                "x": 35,
                "y": 155
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "0b8c61f5-0d03-4331-8b57-3cd3f757877b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 49,
                "offset": 2,
                "shift": 36,
                "w": 31,
                "x": 2,
                "y": 155
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "c51438bc-07b4-4797-b824-ad8501753dbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 49,
                "offset": 2,
                "shift": 29,
                "w": 24,
                "x": 27,
                "y": 104
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "41a3309b-3f2d-4699-aca5-1baac20a6070",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 49,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 2,
                "y": 206
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f4662ab2-41d1-45e4-b9f7-4bc010abb751",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 49,
                "offset": 3,
                "shift": 27,
                "w": 23,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "72a37ea7-30c2-4c8b-8d89-3a20ebf13fcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 49,
                "offset": 1,
                "shift": 30,
                "w": 28,
                "x": 476,
                "y": 53
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "84ba4f9a-1d48-4633-aa87-49226908f9e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 49,
                "offset": 3,
                "shift": 28,
                "w": 25,
                "x": 461,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d7e9ec23-8668-4619-9153-20ea2534bb40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 49,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 435,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "83c117d2-d2c5-4525-9fcc-0269bd0eaf49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 49,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 409,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b5bfb360-e16b-421a-a557-da31ad373117",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 49,
                "offset": 2,
                "shift": 29,
                "w": 25,
                "x": 382,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "f6f2df33-3734-41f0-b9d7-55bd248fd2f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 49,
                "offset": -1,
                "shift": 24,
                "w": 26,
                "x": 354,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "4572257f-15fe-44ce-9261-6c867675661f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 49,
                "offset": 0,
                "shift": 38,
                "w": 38,
                "x": 314,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "131c371b-4661-420e-bf65-bd576035e78b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 49,
                "offset": -1,
                "shift": 26,
                "w": 28,
                "x": 284,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "1c308b58-b7d4-409c-a798-d3cae1682a38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 49,
                "offset": -1,
                "shift": 26,
                "w": 28,
                "x": 254,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "dbf5599c-acb4-42d3-82f7-86e18b7dfb91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 49,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "91885c79-322a-4537-bd71-684369b4b525",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 49,
                "offset": 2,
                "shift": 12,
                "w": 10,
                "x": 488,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "d1636874-db08-4461-b41d-154516c7f37b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 49,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "60b13386-8a81-452d-89e3-9ac0da6aa187",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 49,
                "offset": 0,
                "shift": 12,
                "w": 9,
                "x": 183,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "32bae6f9-9706-4a7e-a1db-5cb610466a3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 49,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 163,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "aeba2918-d306-48cd-88bd-537c1f5b544c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 49,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 141,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "0801c1a7-cce4-4622-8ec5-9f5af51d92d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 49,
                "offset": -2,
                "shift": 10,
                "w": 10,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "ea0d39cb-dc4a-4a7a-b2c9-923736811b61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 49,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "3c222bfa-ac37-4b30-9921-e356e98cad20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 49,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 83,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "1218f54f-247b-4024-81df-cfa3d5835b24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 49,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "2afdb5c1-80b3-44cf-bf55-af2e4cf97fcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 38,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a5fef85c-2996-49f5-b562-3420e84d2b73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 49,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 15,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "98175efd-9dad-4c19-a47d-caa980352bc9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 49,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "e688b51d-b117-4942-9889-e46ca0e63c6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "5db60250-65ec-43a4-a1c3-534641c8839b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 49,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 25,
                "y": 53
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "265f5987-83cb-4450-bc3b-44d27fc9b77f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 49,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 46,
                "y": 53
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "a8fad6fc-2418-49ee-bcc4-67628e926a94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 49,
                "offset": -1,
                "shift": 10,
                "w": 9,
                "x": 465,
                "y": 53
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "da46cffb-4777-46dd-8e09-26cca47fc7e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 49,
                "offset": 2,
                "shift": 21,
                "w": 20,
                "x": 443,
                "y": 53
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "45f7fe90-f709-4e5f-ab40-a238189be059",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 49,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 435,
                "y": 53
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "ce999201-a299-4c7c-bcd8-df67ed32be3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 49,
                "offset": 2,
                "shift": 35,
                "w": 31,
                "x": 402,
                "y": 53
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "9dd4933f-bcb9-417e-93a2-09c87a9c50a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 49,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 381,
                "y": 53
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "63acd13d-9dd4-4e1c-bdd8-bb07e4055677",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 357,
                "y": 53
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "75fbe957-97e6-4a8d-8287-76e6b25bb501",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 49,
                "offset": 2,
                "shift": 24,
                "w": 21,
                "x": 334,
                "y": 53
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "25913235-558e-4f7b-a09e-ffe2b94579e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 311,
                "y": 53
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "01da9575-2083-4ac8-a93c-04c3e8701e9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 49,
                "offset": 2,
                "shift": 14,
                "w": 13,
                "x": 296,
                "y": 53
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "4224b819-cb73-438d-a867-ac26065f55f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 49,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 275,
                "y": 53
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c7307ad6-3faa-46bb-a11f-c1fe087bc43d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 49,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 260,
                "y": 53
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "a0fee846-fdef-4482-9a88-b55e208582c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 49,
                "offset": 2,
                "shift": 23,
                "w": 19,
                "x": 239,
                "y": 53
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3a903c01-9108-4393-9350-f84627fe76fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 49,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 216,
                "y": 53
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "c7a07884-c90e-4d72-9f52-c5e4a1e8a722",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 49,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 183,
                "y": 53
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "62fb11a9-a2a1-4745-b5d3-69d8e06cca0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 49,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 159,
                "y": 53
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "05d96def-4f5a-44ec-97db-cedf6078cda6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 49,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 136,
                "y": 53
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "d956ae6a-1e66-4467-a945-096091dc9788",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 49,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 114,
                "y": 53
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "fb0c2b74-d43c-459a-a795-4217bc16f855",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 49,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 98,
                "y": 53
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "67b058fd-3b90-43be-953c-2f070c8977d9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 49,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 91,
                "y": 53
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "9dcde06c-fc66-4a04-b3df-b31ec149ee04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 49,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 76,
                "y": 53
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "6fb0e912-42ee-4588-ac4b-177e43d3fcd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 49,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 54,
                "y": 53
            }
        },
        {
            "Key": 127,
            "Value": {
                "id": "26552009-2c17-4ed8-af80-efa917a6bc61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 127,
                "h": 49,
                "offset": 0,
                "shift": 0,
                "w": 0,
                "x": 506,
                "y": 53
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "5cdee588-62d9-4f78-9a31-75c89bc71b72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 49,
                "offset": 4,
                "shift": 25,
                "w": 18,
                "x": 32,
                "y": 206
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "b6dc1570-7261-400c-90b2-4c81b8eff855",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 84
        },
        {
            "id": "531b07fb-f86d-4d7b-9666-d4d290941272",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "e4055b5d-2e0d-48ab-8110-1e12f23ac334",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "e2bf4879-a36a-465c-948e-f332f9a14af8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 89
        },
        {
            "id": "0d7616a8-2108-465c-8caa-087a0fed3e2e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "1ba833a6-9dde-41a3-a8e1-4e8d00e29baa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "efbe6311-a582-47d6-861f-d3ee8cffc90a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "adc69d9f-13d0-42ca-ae22-d8f499d9b79d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 221
        },
        {
            "id": "04635351-aa4d-4d82-8ba2-b35015a8745e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 253
        },
        {
            "id": "1c040f98-b877-40cc-bfed-9955e250cd49",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 255
        },
        {
            "id": "0b3f73e4-f159-4d0f-a797-0291641fe68e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 354
        },
        {
            "id": "e7c5b44c-0535-416d-b160-1bd26162bd82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 356
        },
        {
            "id": "4b28ccdf-a4a8-4f6c-9a5b-0bae84aebd94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 372
        },
        {
            "id": "9cbcabde-66f8-4749-9461-a5f5fc80b542",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 373
        },
        {
            "id": "bdcb86f8-5386-446f-a51b-a92f2485312d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 374
        },
        {
            "id": "49840721-cbe5-4c72-9c8a-5b8deb9ae4a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 375
        },
        {
            "id": "4e5c10a6-f171-4a10-b126-ae7026b1ec0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 376
        },
        {
            "id": "8d034829-0188-43de-ac88-8ee0f3c92b99",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 538
        },
        {
            "id": "11213cc2-3f24-4807-8cf6-3991cc14b3fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "f9bcbb0b-370f-4dbe-b171-870e0d898e45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 44
        },
        {
            "id": "74f73ca5-1991-4f26-ac4f-c8768702f243",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 46
        },
        {
            "id": "0650eb7e-a509-42ca-b0ee-f54efc6201e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "b342cf02-5d8a-433e-9532-1d73943944bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 192
        },
        {
            "id": "7cb49d46-60e0-4375-ba4c-6c5ceec4d648",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 193
        },
        {
            "id": "6104ee3e-a7bc-4455-9eee-0073bd06042e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 194
        },
        {
            "id": "8d6bb47e-2a74-44ca-b749-ce4788026919",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 195
        },
        {
            "id": "febf4945-7108-487f-af5b-3487990f61a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 196
        },
        {
            "id": "bde4db83-3fa1-47cb-abcb-271a1bb57746",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 197
        },
        {
            "id": "9ae9f4ab-2549-41f6-a710-fd1c1ba84284",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 256
        },
        {
            "id": "b5b11a88-8077-46b8-8f46-9d1f49e46faf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 258
        },
        {
            "id": "07f26e4c-df12-4e0a-b832-a30f0f82c9ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 260
        },
        {
            "id": "ecbb5216-b4c2-4fb3-b0c2-6f36fc96ec6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 84
        },
        {
            "id": "05e7c227-efb9-4bc0-839d-ca54b673519b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 86
        },
        {
            "id": "3125c3e1-a133-46b0-9771-cc3489bb9155",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "9edcdeec-bab7-4d86-8126-6b934e09e8fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 89
        },
        {
            "id": "5a060e1b-2e69-46dd-9929-249d7d9c9758",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "06c9a0d0-b6f1-446e-9e44-a3f28743f845",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 221
        },
        {
            "id": "5e5afd7d-8c18-46b6-afff-cc4e7566331d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 253
        },
        {
            "id": "61b611cc-846f-4bb4-9f92-b88d4021b326",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 255
        },
        {
            "id": "ce65baec-385f-4deb-a942-7048c243ff6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 354
        },
        {
            "id": "3e44f40d-3516-4881-86a4-3ad99b6e6200",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 356
        },
        {
            "id": "ee525693-a4d1-407e-9dd7-aa061ecf94a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 372
        },
        {
            "id": "e591e9c2-5640-464c-9171-0b7ac7ee7e98",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 374
        },
        {
            "id": "e7c9cd34-de43-4c40-b6b1-1ed61d7fee0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 375
        },
        {
            "id": "aeedc0c4-7a13-44ba-aa98-2d455d950042",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 376
        },
        {
            "id": "e6018a31-e869-43c9-87bc-7a55d661eb62",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 538
        },
        {
            "id": "e7b58aaa-bb26-4cef-a4cd-93ad652f86fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8217
        },
        {
            "id": "39c2a2a1-8e48-49d1-852d-e79cf025b9a8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 44
        },
        {
            "id": "67c69721-4494-4e8b-a2f7-5bc59a75a124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -7,
            "first": 80,
            "second": 46
        },
        {
            "id": "fced9457-decb-4c82-80a1-79785d1f9c39",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 65
        },
        {
            "id": "1de10fd9-32eb-47e0-9351-9b37332db2e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 192
        },
        {
            "id": "967f466b-40f0-41ab-93b2-3ddd2e794052",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 193
        },
        {
            "id": "7f526a61-10c1-45b7-a056-33aa2b9272e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 194
        },
        {
            "id": "039449dd-2bd3-479d-8d88-95c28bb859a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 195
        },
        {
            "id": "e030c0bd-1fe4-486e-b56c-1d60508a95b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 196
        },
        {
            "id": "d811c760-7ad2-40cb-b866-44483a187a90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 197
        },
        {
            "id": "da04c574-5b06-48dc-aeb2-e824337f5f31",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 256
        },
        {
            "id": "021dd091-ae42-4df5-991e-23a868ab5a7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 258
        },
        {
            "id": "d4efc41f-27ee-4993-a4bf-2400e9b04431",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 260
        },
        {
            "id": "c22beb2b-3bcd-4a29-8a9a-01640bf675da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "bc8f765d-96a0-4060-8622-1f8ee70b5b6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "9693a467-279a-4be7-aa59-df6ebccf7e50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "f077bf87-019f-419b-8ac3-1153829f5a8e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "409f2f1d-8272-47e0-a194-be272d9c1a73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 221
        },
        {
            "id": "d00a9c4a-823c-412f-8db7-53146ad9b8c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 354
        },
        {
            "id": "d9f33170-ce5f-423d-aaf7-b6469b601f5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 356
        },
        {
            "id": "e51db218-04d5-4ec2-a50c-d7aa537134e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 372
        },
        {
            "id": "5e536529-e153-4cb2-9ac5-637fefcc5af8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 374
        },
        {
            "id": "70f8cfaa-f353-4504-bfd6-bede3eb06607",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 376
        },
        {
            "id": "fa138c67-9bd2-4391-92ba-31c9edde661c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 538
        },
        {
            "id": "3337194a-aa17-4aec-868a-bfa1592a0101",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 44
        },
        {
            "id": "259c33d1-0eb2-4c85-b789-2738828fd6dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 45
        },
        {
            "id": "bcb69a11-36a9-4142-af50-d9a4837d8d03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 46
        },
        {
            "id": "506ed335-98c6-4e8e-8c05-ff5431ee0e8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 58
        },
        {
            "id": "115d106d-7fb4-4789-ad09-b98f6865e351",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 59
        },
        {
            "id": "0af5bc5e-d1f1-47b8-ba94-c8c7038a545b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 65
        },
        {
            "id": "fe2d52d3-43d4-4801-bf6f-e27d8e6efd00",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 97
        },
        {
            "id": "5ac685bf-2792-44f6-9cc2-9e0dca64ba60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 99
        },
        {
            "id": "754a6c94-6dce-4e19-841e-3d81fe32ca09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 101
        },
        {
            "id": "db9ba0f7-95ee-46a8-9cab-ef36d809d04d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 111
        },
        {
            "id": "19cce0bc-e88b-4e26-b5de-ec54a7fa4eaa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 114
        },
        {
            "id": "9c1d3b0a-cc39-4154-a3a8-74b226161db4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 115
        },
        {
            "id": "07ebdedf-dc8a-483c-8237-cb0ad66a9266",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 117
        },
        {
            "id": "3b60d109-f521-4a56-a0e4-e9be3daa3369",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 119
        },
        {
            "id": "4e7c3fff-c8ab-49fa-90ad-b3e39c140245",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 121
        },
        {
            "id": "0a5922f0-bcfe-4a7e-8c09-961ec7258c3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 192
        },
        {
            "id": "58252f37-66cc-463b-b6f8-0c92dcc0f0ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 193
        },
        {
            "id": "560665e6-0e0c-43b1-b2f5-a1bf5bf39211",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 194
        },
        {
            "id": "b9b7cf8a-7703-48f8-9e53-02802020ec03",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 195
        },
        {
            "id": "bd5ee066-5082-48d6-a811-d26d7384484f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 196
        },
        {
            "id": "b62b30a6-ac2e-4c87-a9e9-03230ba789dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 197
        },
        {
            "id": "689cc2d0-f315-49a5-b50d-57ba614d0af5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 224
        },
        {
            "id": "6fbc7192-1543-47ec-8170-d2092adc5ee7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 225
        },
        {
            "id": "05a43ad0-cb87-40d7-9458-7e50d77af92d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 226
        },
        {
            "id": "2f053b46-0be8-42be-a2c5-16f292b21510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 227
        },
        {
            "id": "0574e773-9b0f-4ef9-852a-0159fb19cfbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 228
        },
        {
            "id": "dcbfc740-3cb1-427d-bff6-de2ed73afec5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 229
        },
        {
            "id": "8f4346f8-ff10-4702-9185-723e64ca7490",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 231
        },
        {
            "id": "0882c1fc-d276-490f-878f-ffc4aaada4ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 232
        },
        {
            "id": "da8ba0d0-183b-4a81-8c7a-bf1b8a4bba95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 233
        },
        {
            "id": "2cae8b80-1a22-4707-ad25-311491aafacc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 234
        },
        {
            "id": "4cd2c13b-886d-4fe7-a4b7-42d183f9840e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 235
        },
        {
            "id": "03d75286-ac59-49e1-80c0-cbea3756fe5b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 242
        },
        {
            "id": "ae0303cc-a7a2-42b9-8f2c-7b8a3b353014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 243
        },
        {
            "id": "23657df2-5076-4ea2-a386-2a50096cf62c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 244
        },
        {
            "id": "2c70fdfa-7ee2-4c9f-a973-917bf5b45017",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 245
        },
        {
            "id": "110a5ebc-31c1-4f90-b10c-5ae97424301c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 246
        },
        {
            "id": "08612bc8-6636-441c-a372-19e37051698a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 249
        },
        {
            "id": "6828bda0-38a4-4163-b97c-78c9a78a362f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 250
        },
        {
            "id": "bf42ecf6-0b75-470b-acba-6555bd2dcd13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 251
        },
        {
            "id": "da098795-96f7-45ec-8627-28ec3e0c7e38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 252
        },
        {
            "id": "53292aaa-2710-47fe-a2b4-ce53b892f531",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 253
        },
        {
            "id": "7fb83556-b5ad-4297-8f8c-bf7ea9eade5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 255
        },
        {
            "id": "862cd84f-0654-4d66-a8fc-de5de0cf5b73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 256
        },
        {
            "id": "430e86eb-f68b-48b9-a8a9-fb9f5ed06c0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 257
        },
        {
            "id": "b48c1188-1809-4b91-822e-311b1ab7e8e9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 258
        },
        {
            "id": "410dc865-0808-4121-9189-702614ac5322",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 259
        },
        {
            "id": "83ed3578-2d8d-4d2a-8da2-36c038e044f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 260
        },
        {
            "id": "6cc32c73-fc31-4be8-a321-d40db7295f76",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 261
        },
        {
            "id": "d822d005-4e51-4d6d-919f-83811e3e8a23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 263
        },
        {
            "id": "357c21ab-403e-4789-929a-9ee66dcce3cf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 265
        },
        {
            "id": "37627a28-0bcc-4f61-86eb-5eb025beedb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 267
        },
        {
            "id": "64c444f0-d34e-4a0b-ab52-5011ee596e1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 269
        },
        {
            "id": "6227cd67-3c6e-4ce0-85c1-1a5c30c8298e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 275
        },
        {
            "id": "00ee7da9-7e12-4a4a-ac0d-ab458139244f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 277
        },
        {
            "id": "79a8d355-6194-4cf9-b5d1-2a947658b6c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 279
        },
        {
            "id": "5b0a9b6d-5f92-46b4-afc0-b5aa24a0f432",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 281
        },
        {
            "id": "3e0a31a9-a88e-4e02-b117-21b6abf5c6d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 283
        },
        {
            "id": "c2da713b-793e-4dce-b654-fc4821044ef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 333
        },
        {
            "id": "c8d4d978-1265-4f5c-9b62-a522e7f09036",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 335
        },
        {
            "id": "1825c516-2402-46a0-b98f-f69959e3e7e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 337
        },
        {
            "id": "2c5d6249-5336-4763-b44b-0d28eb411d2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 341
        },
        {
            "id": "c11fed8a-c115-4ee9-a99c-678e60e28283",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 343
        },
        {
            "id": "520b3206-d432-4c3d-9220-d571639c3fee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 345
        },
        {
            "id": "0f906548-4f2e-461c-9bd1-bc83851b994f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 347
        },
        {
            "id": "0122e56d-41a0-4434-8d56-dd56900419da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 349
        },
        {
            "id": "592c678f-2ba9-4d8f-9618-dcd0c62cbf90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 351
        },
        {
            "id": "dc89bca7-58e1-4bc4-b254-64d2a348e05d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 353
        },
        {
            "id": "d9e635c6-a552-476e-add6-833b209ed800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 361
        },
        {
            "id": "79617560-1c5f-4b73-b5f6-17f72bd7d675",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 363
        },
        {
            "id": "3980024b-3db1-434f-af6f-568d9d8d70e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 365
        },
        {
            "id": "d503c85b-623e-4e30-9182-15abb2d9dfe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 367
        },
        {
            "id": "1e950e12-a4cc-42a5-ace5-6e7bdf220922",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 369
        },
        {
            "id": "916dfec9-240e-4d9a-be86-7a84183e847f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 371
        },
        {
            "id": "fa0819d3-c6fc-4869-a8c1-dbdc6e8fe28c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 373
        },
        {
            "id": "8112554c-878f-45ba-a7df-996ea4008029",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 375
        },
        {
            "id": "05e91d8c-dfaf-4ebf-9f6c-f31f74a9c773",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 537
        },
        {
            "id": "a07913fb-ccc4-45a1-bd79-1b3527c86da1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 44
        },
        {
            "id": "c4a0572d-4fee-4ba8-8d46-5591b6402837",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 45
        },
        {
            "id": "b662b1cb-0ac7-4eb3-bf1f-bcfe92e099b1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 86,
            "second": 46
        },
        {
            "id": "f9e7f147-c6c5-431f-8f63-d316f518939b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 58
        },
        {
            "id": "93544abc-2d06-4bae-9f26-de94df490a07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 59
        },
        {
            "id": "4674b4af-4e5f-461e-9cd0-a133849b9eb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "72c2eea2-ecb9-45b0-ad9c-12e1ed23a1d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "0ce9c3ec-78e0-4a95-a076-8ef2afa55a2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "1db28c5a-9fad-4021-9918-9874b51728ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 111
        },
        {
            "id": "528b030b-0307-4d1f-87ca-17f476f082b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "7bd2ab67-37bf-4139-b769-dbb222f70dad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "4908c15b-2925-4ee0-b3fc-2d6798f07ac9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "be430797-90c6-4a2e-a692-94d1b5548caf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 192
        },
        {
            "id": "c045e6dc-887a-4302-b540-9728b834a297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 193
        },
        {
            "id": "e11137ff-c07c-491d-a90d-93f4cc00874b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 194
        },
        {
            "id": "b2cb6d4d-505f-47fd-90cd-4bc11f3d451e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 195
        },
        {
            "id": "95723a24-5dd9-488a-a676-0ebafe2a5bbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 196
        },
        {
            "id": "8aa4c520-c3bf-4736-ac07-0c3e6255c92d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 197
        },
        {
            "id": "51971f34-92e5-4b34-a787-dcf368274814",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 224
        },
        {
            "id": "c514d8a8-8c1e-403d-9d8d-1d2600dac083",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 225
        },
        {
            "id": "0f4844ea-e276-49fa-ac4c-309cbad6d5e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 226
        },
        {
            "id": "34ef4bfc-ffd0-4ca5-ac1f-61083b0d1cdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 227
        },
        {
            "id": "0a7ae7ef-45f7-40c1-b905-5616baeda308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 228
        },
        {
            "id": "6e641336-0b31-40dd-b6a6-21e68c17f631",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 229
        },
        {
            "id": "ab4284d0-2029-401e-9090-6399f653f5d3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 232
        },
        {
            "id": "af3a1365-158b-4a05-a097-5b2f8a5a3919",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 233
        },
        {
            "id": "b598af84-d167-4e54-b15f-0f27ff995703",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 234
        },
        {
            "id": "34f2eea5-27c3-4525-8da0-96b12a499d72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 235
        },
        {
            "id": "814b86f9-0041-49ab-9a21-6e3118514f2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 242
        },
        {
            "id": "1cea823d-c0bd-423e-ab8c-894852a0a2d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 243
        },
        {
            "id": "5ce59dbe-7740-467c-a296-e54c050d7fc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 244
        },
        {
            "id": "138cd7fa-c155-4bd0-b6e7-8e0ccfd0271b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 245
        },
        {
            "id": "2badefb7-07e3-4160-96f2-6aaa41145f1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 246
        },
        {
            "id": "e242a010-942b-4d24-a702-d8a23c6009f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 249
        },
        {
            "id": "fac297b6-44ac-4041-8815-5bb67fc72a3e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 250
        },
        {
            "id": "33c92daf-65d4-4c55-97c5-8649897b5232",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 251
        },
        {
            "id": "69665156-4ce8-4c9c-a9dc-0f33258f1eea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 252
        },
        {
            "id": "276138f9-5309-40d9-9597-fc6a604f2db5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 253
        },
        {
            "id": "cabff2c9-57cd-4e2d-82c9-042d5bcf5846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 255
        },
        {
            "id": "d6061478-7620-410e-853a-e69acc020183",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 256
        },
        {
            "id": "ca8f262f-79db-4efb-a498-178a597e43b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 257
        },
        {
            "id": "918eabd3-90c4-47b6-9b9e-a1135a0d50ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 258
        },
        {
            "id": "e6586693-f62c-41a3-9b62-0df8e7de8e7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 259
        },
        {
            "id": "754ef49f-ac5a-4deb-8f2e-bc51e34efa0c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 260
        },
        {
            "id": "b6a5210d-5c24-4dc6-a9ba-d99d69016b75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 261
        },
        {
            "id": "331bd6d0-ba12-443d-aa80-78cebe0b8424",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 275
        },
        {
            "id": "f12595c2-5b67-40e0-a0c1-82ecf9393c84",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 277
        },
        {
            "id": "444fc090-02d4-4459-a340-193a86d71b9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 279
        },
        {
            "id": "9e22c3e4-5155-4ebf-a9cf-6f429078f782",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 281
        },
        {
            "id": "69c746b9-3172-428b-9264-efd92eae50ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 283
        },
        {
            "id": "1d874626-6f10-4c2a-bb70-83204870e2fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 333
        },
        {
            "id": "26c371e2-6a12-468a-8cf9-d830f518ab90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 335
        },
        {
            "id": "1306d9b1-bc5a-4145-bb54-14410c381b91",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 337
        },
        {
            "id": "aeb303ca-b1f4-4681-9fcc-0a2b9152910b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 341
        },
        {
            "id": "eac95127-d16e-4a99-a295-1d2fa326c5e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 343
        },
        {
            "id": "fdfecf79-5738-419a-b6f6-bb8ecff2fbf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 345
        },
        {
            "id": "46ed2f3c-1124-47f1-9548-0cbcb88e2e2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 361
        },
        {
            "id": "972576c8-269b-45ae-a98f-5241f2467b32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 363
        },
        {
            "id": "ac7309b3-e02e-467c-8816-655f236a36d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 365
        },
        {
            "id": "3d635dc2-96e2-4e0e-b777-347bb5c50f05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 367
        },
        {
            "id": "aa56f819-42c5-490d-b2a6-02d1308047ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 369
        },
        {
            "id": "d099a7ce-d641-4d2e-962e-198df4d9e014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 371
        },
        {
            "id": "5cfb5eec-5156-4424-b6cc-a0deb0cf2288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 375
        },
        {
            "id": "7a8f3ebc-7722-44de-a7f0-abd71b45e88e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 44
        },
        {
            "id": "3ec3d236-46d6-4dda-9965-d7248b2d727d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "844077cb-cf1f-4807-9420-e35b4cacc3b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 46
        },
        {
            "id": "82355b7b-0c19-472a-aad3-a3d2795673b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "6eaee208-cf52-487a-81ce-d8a88004cef0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "fa8a505c-53a8-4a40-bc43-6a354c6e8564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "f1c6b598-864d-4870-aa5e-97e68a52332b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "542ae060-1f21-4a2a-bdbb-cd9eb10f4546",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "1adc789b-f461-46a5-9c03-ab29108fa3b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "9c580ab2-f308-464e-b55d-d3b95a6fa12e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "e550c6f9-7e71-4efb-9b34-e5fc9aa1ed92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "34f6a421-e261-4d0a-bd44-172c5320309d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 192
        },
        {
            "id": "9c1571aa-97b8-48ac-bfa0-6f5c8d3e871c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 193
        },
        {
            "id": "7bfd17fb-8628-4068-92f0-b6d82e6130bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 194
        },
        {
            "id": "606ff5e4-e72b-4d44-a2fd-493f5b86e2d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 195
        },
        {
            "id": "e56f0fac-c4f4-4153-bc82-526c1b927745",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 196
        },
        {
            "id": "ec391450-6527-472d-a11f-95f31af93060",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 197
        },
        {
            "id": "b4508ceb-4c7f-4ad9-b402-742356801569",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 224
        },
        {
            "id": "b573c991-90ef-40df-8852-5317f9747885",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 225
        },
        {
            "id": "3b609f62-d416-4983-803b-2833feb81d46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 226
        },
        {
            "id": "92d8426f-4aac-4dd7-9119-9a179ab47e3b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 227
        },
        {
            "id": "567a0f10-a230-47f2-9d6e-74ffaae33add",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 228
        },
        {
            "id": "14710241-49d0-418d-820c-db9585d8832e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 229
        },
        {
            "id": "2400feee-bdc4-4ec9-98ff-91cd62dd9b2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 232
        },
        {
            "id": "0fb4e77f-1994-49b2-90ad-ee977a1b547b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 233
        },
        {
            "id": "0f3a7779-426d-4253-a59f-039c0ec11791",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 234
        },
        {
            "id": "a80dfeb6-35be-463e-b12a-7a7b6b8c2742",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 235
        },
        {
            "id": "20e47863-f09c-4b61-81bf-bf39d2f72307",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 242
        },
        {
            "id": "e613fb0e-ce00-470a-81a2-b5788dc99d72",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 243
        },
        {
            "id": "ce1e06a5-08c9-405f-a92a-7d8258ea3e9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 244
        },
        {
            "id": "c9532e6a-8304-4488-9323-f895ee31769f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 245
        },
        {
            "id": "a301f925-cfb8-4a3a-9a65-13d1b7261ff7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 246
        },
        {
            "id": "aa3fa9fb-febc-4acd-9c89-fce63788d955",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 249
        },
        {
            "id": "665d6fb1-fee3-4cea-8efc-7b8e260b2be9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 250
        },
        {
            "id": "be694358-cd9a-46f9-9784-f3703d41d726",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 251
        },
        {
            "id": "7b973ee6-593a-4d7d-9af2-a18936f42b7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 252
        },
        {
            "id": "64693e97-194b-4a4c-8c19-f17e96538fe3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 256
        },
        {
            "id": "4fbd5fd0-79b4-412c-b73f-883afcaf82f5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 257
        },
        {
            "id": "60334141-a01e-4e56-9bfd-7fa473859d15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 258
        },
        {
            "id": "5beb740e-4324-47ad-bf33-1a40818f8267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 259
        },
        {
            "id": "2764f1b7-c02f-4a16-bf05-ebfa74dbcefa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 260
        },
        {
            "id": "d3648a48-d94e-45f2-b64d-5904038802bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 261
        },
        {
            "id": "d5b906dc-a0a0-4059-9665-7e8eb033e9f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 275
        },
        {
            "id": "a273bf1e-ea99-40f6-bafe-3b5dea45ac7e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 277
        },
        {
            "id": "555d8976-7d21-4018-bd74-cb3cbab90ba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 279
        },
        {
            "id": "ad505caf-5457-4cb8-b933-3759a3250401",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 281
        },
        {
            "id": "440c6d31-f5c9-4556-938b-02ff585261f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 283
        },
        {
            "id": "5be5c28e-48d3-4723-b577-b17ba6a4d498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 333
        },
        {
            "id": "1d04be74-fe31-4c40-9d6a-ef5e3d136edc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 335
        },
        {
            "id": "649cc8dd-e120-468e-b816-7a249801f570",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 337
        },
        {
            "id": "452c1048-23c8-4910-a54d-68608d7174f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 341
        },
        {
            "id": "c262a000-0014-40dd-834c-a0473c614cf5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 343
        },
        {
            "id": "66f80c46-8572-423c-b4cd-ae3c33079800",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 345
        },
        {
            "id": "d71136dc-cfff-4646-a1a7-62d3cc77d609",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 361
        },
        {
            "id": "1b1446b7-a418-4d4e-9933-0d0f549e0f86",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 363
        },
        {
            "id": "8e8cdbc3-bc13-4cb3-989d-8f72235010c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 365
        },
        {
            "id": "af8b102d-8843-4976-8394-63d44276c69d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 367
        },
        {
            "id": "e2248595-3310-497e-ab57-c5a671c7b2b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 369
        },
        {
            "id": "62232dfa-bd4c-4e6b-b960-d6d633478b6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 371
        },
        {
            "id": "e6cb3b79-0853-417c-9e54-9a2b23c1e54f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 44
        },
        {
            "id": "209c8dbd-45ec-4920-bcab-91e9f03e382e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 45
        },
        {
            "id": "19c74ed4-e620-4344-be95-09be93611f3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 46
        },
        {
            "id": "20eb3f62-d2e9-4b05-a6f0-4306b61bcb08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 58
        },
        {
            "id": "1fb79040-2caa-4419-ae1f-38675e82a0d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 59
        },
        {
            "id": "62ca387c-b764-474e-b3dc-afff97044e06",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 65
        },
        {
            "id": "9f9defcc-bbc0-4673-8c57-95bb02cd61cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 97
        },
        {
            "id": "278c93b3-9306-403e-a874-cd131e43968c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 101
        },
        {
            "id": "f7e6b403-df70-4e63-857b-e7f8479ba297",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 111
        },
        {
            "id": "f1b92a22-1566-48f3-93bc-e4fc7001ff81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 112
        },
        {
            "id": "5680d3b4-2fa0-4b9e-9ab2-872f81312fbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 113
        },
        {
            "id": "df21814d-95b1-4e58-8eee-bbce03a43c1d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 117
        },
        {
            "id": "9a82477a-b2be-4f2c-b208-f4096c39b58d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 118
        },
        {
            "id": "c62987e5-d327-4a11-9d22-3ac351a3da82",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 192
        },
        {
            "id": "f871fd2f-20c5-44ee-b88f-d4295e0a8a52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 193
        },
        {
            "id": "3f0aeec0-3c1c-42db-ada6-3610b57d6e28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 194
        },
        {
            "id": "900e6573-4b46-4574-9f5f-25c5685cdef1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 195
        },
        {
            "id": "ea4ab853-8d82-49bd-9ebb-6b02c805c5b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 196
        },
        {
            "id": "6d54ff16-a59f-4630-b621-e522dd97e523",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 197
        },
        {
            "id": "6ce19eea-9451-450a-af0e-29e3659cd9b9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 224
        },
        {
            "id": "c5a5631f-d50b-4a21-88b7-2fba170029f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 225
        },
        {
            "id": "37414bf9-a65c-4fed-aa85-285186b1e013",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 226
        },
        {
            "id": "402a88cb-5044-49b7-8df7-92f3804df7df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 227
        },
        {
            "id": "62998c85-9d1d-4d4d-a485-d522ae38aece",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 228
        },
        {
            "id": "8b5b27ab-dcea-4a23-952e-ace9679eea10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 229
        },
        {
            "id": "2c6437dc-79dc-447d-af69-363bd2086422",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 232
        },
        {
            "id": "345b5494-55ab-4feb-8dac-aeb800607009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 233
        },
        {
            "id": "c746d9d5-d792-4a13-8d81-19532c74cd81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 234
        },
        {
            "id": "3aa9f0a7-562b-4a8a-b065-84cf79a87ea4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 235
        },
        {
            "id": "26e82f29-5d2f-4d5b-9e10-7d54738726b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 242
        },
        {
            "id": "6c766312-f3b8-488c-b576-47b2c8a70fd0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 243
        },
        {
            "id": "20fcbc95-83dd-4110-9e91-62c1efaa0563",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 244
        },
        {
            "id": "e2b6ad79-bbf9-483c-9888-102ce272341b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 245
        },
        {
            "id": "f1a163df-cb6e-4c12-90a5-f7bbd0e3f801",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 246
        },
        {
            "id": "646512a9-0efd-4353-bf62-b0965e663b27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 249
        },
        {
            "id": "26d2e0d6-635c-4955-8a4b-03898668431e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 250
        },
        {
            "id": "d505e4f6-23e1-4e5e-97bf-6884ef4bf8a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 251
        },
        {
            "id": "2ffa2473-0187-4da5-863c-2af53a377b33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 252
        },
        {
            "id": "bbb1924c-d7ff-4d4d-a248-bed0eb2af6b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 256
        },
        {
            "id": "58e816eb-ec2f-45bf-9dc9-818a55f632db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 257
        },
        {
            "id": "0e735deb-a54b-4b7a-89e1-13e8aeb36367",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 258
        },
        {
            "id": "420ef941-ca74-4276-ae45-28a9187eb98a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 259
        },
        {
            "id": "19db86fa-58e6-403f-95e9-ce8d0a775487",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 260
        },
        {
            "id": "b1060eeb-10c7-48c2-9b9c-ca2e3ea64c55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 261
        },
        {
            "id": "99bd32d0-d6b9-45b8-8695-3d76ea243471",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 275
        },
        {
            "id": "8f3800c2-e0af-4c08-8683-c6920e486b0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 277
        },
        {
            "id": "d1a044d1-5907-4092-916b-504401f6d42c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 279
        },
        {
            "id": "dc3905b3-a94d-41cc-9f86-65082377c60c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 281
        },
        {
            "id": "a2ee34c5-9bc4-4937-acac-26a4ccb23629",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 283
        },
        {
            "id": "3e6d277c-0203-478f-9d8f-0bc693508abb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 333
        },
        {
            "id": "92a4aa3a-34f3-4d92-9e75-28e2a41cdd1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 335
        },
        {
            "id": "ff3b8efb-f453-4a80-9e83-03661df294d5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 337
        },
        {
            "id": "616a91e4-c715-4fd4-9531-28cfde64e418",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 361
        },
        {
            "id": "601f9ba3-1292-418b-978e-364d02f56d7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 363
        },
        {
            "id": "7313653d-a97a-4ae7-b389-741fa7a79055",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 365
        },
        {
            "id": "55ca764d-594c-469d-8443-de0283b2d155",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 367
        },
        {
            "id": "8c1c339d-c9aa-48c4-8547-ad1e007c3dd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 369
        },
        {
            "id": "43915b8d-a320-4b57-9c98-723acbd2317d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 371
        },
        {
            "id": "dde20f3b-92f7-497c-b8f7-777cc812d201",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "f316ec39-6204-4938-b5ff-40064ec4861b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "a72a65f1-18be-40c2-97c3-5423d3053a4d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 44
        },
        {
            "id": "e402fa62-d71b-4bd0-8170-fc684da7969d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 45
        },
        {
            "id": "de604e05-e8f0-4da1-a073-3d16e4ad7f19",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 114,
            "second": 46
        },
        {
            "id": "1f53304d-c95a-4618-b654-6527d8d3dd50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 99
        },
        {
            "id": "8dcf3e93-9f54-4a82-8196-92b08dab3f29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 100
        },
        {
            "id": "115f05dc-6dbc-45e4-8d54-523a5e40eae9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 101
        },
        {
            "id": "88bdbf0a-0261-4100-a9ea-e9ab73d39f90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 110
        },
        {
            "id": "2092288b-5036-45bf-9954-2dd900624eb2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 111
        },
        {
            "id": "e17ac34e-8ffd-4d8a-ba93-9904c8e964c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 113
        },
        {
            "id": "b1ae57a5-9cff-4d96-87b5-dfa0a0f8ef1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 231
        },
        {
            "id": "380a72c9-a717-432b-a985-aa7b7394cccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 232
        },
        {
            "id": "a559d0f3-7d98-47d0-a09b-13b046b7b583",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 233
        },
        {
            "id": "69645ada-7d46-4388-bcc1-0db5f4a64cde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 234
        },
        {
            "id": "19ff1cd7-c8ee-49cd-8fba-83bfbcc77e08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 235
        },
        {
            "id": "2242b2d1-da84-4d80-9ae7-0320b739ecda",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 241
        },
        {
            "id": "37a43e49-4df6-4409-9e04-af200933425b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 242
        },
        {
            "id": "72b404a7-c9d5-4f45-a094-5d63de311c4c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 243
        },
        {
            "id": "d2c884e3-c52c-44cc-83b5-b28d09a69430",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 244
        },
        {
            "id": "f79cccaa-0c41-4095-89ff-6d9dcde15851",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 245
        },
        {
            "id": "9295138f-6b95-44f9-b20e-e046b74122db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 246
        },
        {
            "id": "99931483-e2f4-4b8f-bdd4-7d4dbb0de1dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 263
        },
        {
            "id": "a665fc8f-8df3-4702-8b58-53e4b540d7f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 265
        },
        {
            "id": "46a1c7bb-d3ac-4893-acb7-08b685782947",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 267
        },
        {
            "id": "42ce0aab-cbd8-4451-92db-65bd7cdf10e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 269
        },
        {
            "id": "11e8a31b-6145-4dba-b614-22aaca2d0574",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 271
        },
        {
            "id": "8dad335f-0f44-4b5a-8411-f358a5dff493",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 275
        },
        {
            "id": "7ea4334e-73e4-49fd-9f1d-22d4f2b105e4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 277
        },
        {
            "id": "321f6cde-f2e4-4aa4-b4f9-1efbb0344922",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 279
        },
        {
            "id": "c9b25c50-91e3-4300-9687-1dc4c1c7da2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 281
        },
        {
            "id": "5ee28db8-0942-4a38-9a70-6fcb3e9df535",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 283
        },
        {
            "id": "caa61d19-b931-4cb7-bc38-43ea35d301e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 324
        },
        {
            "id": "b509e5d2-f24e-4cad-9e74-f6bc72bb941f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 326
        },
        {
            "id": "cb8019c6-080f-4b09-842b-a013d027bd30",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 328
        },
        {
            "id": "94ea8cb2-f716-4fcd-8054-ef4798e80c33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 329
        },
        {
            "id": "ddd84c03-fc93-4571-95b8-8d73493147d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 333
        },
        {
            "id": "cc577ee7-56b3-4256-bf7e-f21e0aab5394",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 335
        },
        {
            "id": "e9ee3515-0918-4364-9822-28f53eb8f516",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 337
        },
        {
            "id": "57fa7a50-d156-4e54-82d7-7e0d71679c57",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 44
        },
        {
            "id": "adee77f5-0116-4a23-bbf2-e90772441148",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 46
        },
        {
            "id": "f1b85875-c68e-4e36-8d84-46d1f493b9ef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "7694f4b5-d8fb-4664-929f-50d3dd215498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "0a258c45-fcaa-4ae4-aa37-c1c74c37e8fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 44
        },
        {
            "id": "71619730-a530-4667-b597-77c0b5660c3a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\n0123456789 .,<>\"'&!?\\nthe quick brown fox jumps over the lazy dog\\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\nDefault character: ▯ (9647)",
    "size": 30,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}