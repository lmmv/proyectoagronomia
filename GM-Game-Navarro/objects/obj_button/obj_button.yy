{
    "id": "7c629935-7ef8-4aa6-85a7-e0001a2ce9c7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button",
    "eventList": [
        {
            "id": "359f0650-93fc-4d16-8e8c-732c5a9bc763",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7c629935-7ef8-4aa6-85a7-e0001a2ce9c7"
        },
        {
            "id": "a0f81d7a-1d8e-4646-b780-502358fb27c4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "7c629935-7ef8-4aa6-85a7-e0001a2ce9c7"
        },
        {
            "id": "b58e7f79-da9e-42f5-b694-27a4df18df34",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "7c629935-7ef8-4aa6-85a7-e0001a2ce9c7"
        },
        {
            "id": "1f3c19c1-a3f4-46cb-9c73-7d6b30c98a92",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "7c629935-7ef8-4aa6-85a7-e0001a2ce9c7"
        },
        {
            "id": "1fe0752f-d956-4be2-8673-55e326b4845d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "7c629935-7ef8-4aa6-85a7-e0001a2ce9c7"
        },
        {
            "id": "c77a2280-8216-4cf8-8f2e-0b8f7b6b84bb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "7c629935-7ef8-4aa6-85a7-e0001a2ce9c7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "629e7f6f-6524-4786-9320-9ae546bf1d7e",
    "visible": true
}