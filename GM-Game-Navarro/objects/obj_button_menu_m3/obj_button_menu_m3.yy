{
    "id": "36197831-163c-428f-b700-b8e144a7f20f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button_menu_m3",
    "eventList": [
        {
            "id": "3e4a0f9f-52e2-4608-aeba-e02be13dc198",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "36197831-163c-428f-b700-b8e144a7f20f"
        },
        {
            "id": "148dc7b0-7294-4bbb-ae47-38bbc68575bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "36197831-163c-428f-b700-b8e144a7f20f"
        },
        {
            "id": "584cd76c-60a7-484b-a691-570ddc30386e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "36197831-163c-428f-b700-b8e144a7f20f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87008af0-26c7-4b84-b816-d5d7cc972021",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d774b5d3-e78a-417a-947c-ba1168022f13",
    "visible": true
}