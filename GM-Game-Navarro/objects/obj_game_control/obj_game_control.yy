{
    "id": "979cc259-2314-40de-9564-b5265ba03847",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_game_control",
    "eventList": [
        {
            "id": "1c55fbc9-2860-4b7c-a6d1-80ab09e3671e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "979cc259-2314-40de-9564-b5265ba03847"
        },
        {
            "id": "869ead86-5907-4201-80b7-97995d603c12",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "979cc259-2314-40de-9564-b5265ba03847"
        },
        {
            "id": "73c70a0b-2fc1-4231-a777-30fdaa0fe6fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "979cc259-2314-40de-9564-b5265ba03847"
        },
        {
            "id": "52ebde6d-3b9c-416a-aa54-5ac6a182f67d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "979cc259-2314-40de-9564-b5265ba03847"
        },
        {
            "id": "7ecbc5bc-56cc-4a08-aff0-bfbeaf4299f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 7,
            "m_owner": "979cc259-2314-40de-9564-b5265ba03847"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}