// Juan Pablo Navarro Fennell
if(keyboard_check_pressed(vk_right)) 
{
	if(obj_button_menu_m1.selected)
	{
		obj_button_menu_m1.selected = false;
		obj_button_menu_m2.selected = true;
		obj_button_menu_m3.selected = false;
		obj_button_menu_m4.selected = false;
		obj_button_menu_m5.selected = false;
		obj_button_menu_m6.selected = false;
		obj_button_menu_m7.selected = false;
	}
	if(obj_button_menu_m2.selected)
	{
		obj_button_menu_m1.selected = false;
		obj_button_menu_m2.selected = false;
		obj_button_menu_m3.selected = true;
		obj_button_menu_m4.selected = false;
		obj_button_menu_m5.selected = false;
		obj_button_menu_m6.selected = false;
		obj_button_menu_m7.selected = false;
	}
	if(obj_button_menu_m3.selected)
	{
		obj_button_menu_m1.selected = false;
		obj_button_menu_m2.selected = false;
		obj_button_menu_m3.selected = false;
		obj_button_menu_m4.selected = true;
		obj_button_menu_m5.selected = false;
		obj_button_menu_m6.selected = false;
		obj_button_menu_m7.selected = false;
	}
	if(obj_button_menu_m4.selected)
	{
		obj_button_menu_m1.selected = false;
		obj_button_menu_m2.selected = false;
		obj_button_menu_m3.selected = false;
		obj_button_menu_m4.selected = false;
		obj_button_menu_m5.selected = true;
		obj_button_menu_m6.selected = false;
		obj_button_menu_m7.selected = false;
	}
	if(obj_button_menu_m5.selected)
	{
		obj_button_menu_m1.selected = false;
		obj_button_menu_m2.selected = false;
		obj_button_menu_m3.selected = false;
		obj_button_menu_m4.selected = false;
		obj_button_menu_m5.selected = false;
		obj_button_menu_m6.selected = true;
		obj_button_menu_m7.selected = false;
	}
	if(obj_button_menu_m6.selected)
	{
		obj_button_menu_m1.selected = false;
		obj_button_menu_m2.selected = false;
		obj_button_menu_m3.selected = false;
		obj_button_menu_m4.selected = false;
		obj_button_menu_m5.selected = false;
		obj_button_menu_m6.selected = false;
		obj_button_menu_m7.selected = true;
	}
	if(obj_button_menu_m7.selected)
	{
		obj_button_menu_m1.selected = true;
		obj_button_menu_m2.selected = false;
		obj_button_menu_m3.selected = false;
		obj_button_menu_m4.selected = false;
		obj_button_menu_m5.selected = false;
		obj_button_menu_m6.selected = false;
		obj_button_menu_m7.selected = false;
	}
}
else if(keyboard_check_pressed(vk_left)) 
{
	if(obj_button_menu_m1.selected)
	{
		obj_button_menu_m1.selected = false;
		obj_button_menu_m2.selected = false;
		obj_button_menu_m3.selected = false;
		obj_button_menu_m4.selected = false;
		obj_button_menu_m5.selected = false;
		obj_button_menu_m6.selected = false;
		obj_button_menu_m7.selected = true;
	}
	if(obj_button_menu_m2.selected)
	{
		obj_button_menu_m1.selected = true;
		obj_button_menu_m2.selected = false;
		obj_button_menu_m3.selected = false;
		obj_button_menu_m4.selected = false;
		obj_button_menu_m5.selected = false;
		obj_button_menu_m6.selected = false;
		obj_button_menu_m7.selected = false;
	}
	if(obj_button_menu_m3.selected)
	{
		obj_button_menu_m1.selected = false;
		obj_button_menu_m2.selected = true;
		obj_button_menu_m3.selected = false;
		obj_button_menu_m4.selected = false;
		obj_button_menu_m5.selected = false;
		obj_button_menu_m6.selected = false;
		obj_button_menu_m7.selected = false;
	}
	if(obj_button_menu_m4.selected)
	{
		obj_button_menu_m1.selected = false;
		obj_button_menu_m2.selected = false;
		obj_button_menu_m3.selected = true;
		obj_button_menu_m4.selected = false;
		obj_button_menu_m5.selected = false;
		obj_button_menu_m6.selected = false;
		obj_button_menu_m7.selected = false;
	}
	if(obj_button_menu_m5.selected)
	{
		obj_button_menu_m1.selected = false;
		obj_button_menu_m2.selected = false;
		obj_button_menu_m3.selected = false;
		obj_button_menu_m4.selected = true;
		obj_button_menu_m5.selected = false;
		obj_button_menu_m6.selected = false;
		obj_button_menu_m7.selected = false;
	}
	if(obj_button_menu_m6.selected)
	{
		obj_button_menu_m1.selected = false;
		obj_button_menu_m2.selected = false;
		obj_button_menu_m3.selected = false;
		obj_button_menu_m4.selected = false;
		obj_button_menu_m5.selected = true;
		obj_button_menu_m6.selected = false;
		obj_button_menu_m7.selected = false;
	}
	if(obj_button_menu_m7.selected)
	{
		obj_button_menu_m1.selected = false;
		obj_button_menu_m2.selected = false;
		obj_button_menu_m3.selected = false;
		obj_button_menu_m4.selected = false;
		obj_button_menu_m5.selected = false;
		obj_button_menu_m6.selected = true;
		obj_button_menu_m7.selected = false;
	}
}