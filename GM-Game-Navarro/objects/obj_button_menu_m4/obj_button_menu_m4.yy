{
    "id": "866cbb3d-50c8-41b9-ad15-38d8f9d236d0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button_menu_m4",
    "eventList": [
        {
            "id": "100869f8-c968-4120-ac2b-09eb6482ab6b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "866cbb3d-50c8-41b9-ad15-38d8f9d236d0"
        },
        {
            "id": "b99da530-18ed-4053-b216-03b231387189",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "866cbb3d-50c8-41b9-ad15-38d8f9d236d0"
        },
        {
            "id": "3e55b34d-688c-457e-9c1e-372464b8e869",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "866cbb3d-50c8-41b9-ad15-38d8f9d236d0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87008af0-26c7-4b84-b816-d5d7cc972021",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d774b5d3-e78a-417a-947c-ba1168022f13",
    "visible": true
}