{
    "id": "6e2c4bfe-0bd3-42c8-9dfa-761e5a378414",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button_menu_m2",
    "eventList": [
        {
            "id": "d8389654-c16d-4d4f-8424-db76e751f46b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6e2c4bfe-0bd3-42c8-9dfa-761e5a378414"
        },
        {
            "id": "9f7c04c2-30d9-458e-ab25-501d6d2a8180",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6e2c4bfe-0bd3-42c8-9dfa-761e5a378414"
        },
        {
            "id": "29bda1df-f084-4056-b977-0b7a76a1d8b3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "6e2c4bfe-0bd3-42c8-9dfa-761e5a378414"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "87008af0-26c7-4b84-b816-d5d7cc972021",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d774b5d3-e78a-417a-947c-ba1168022f13",
    "visible": true
}