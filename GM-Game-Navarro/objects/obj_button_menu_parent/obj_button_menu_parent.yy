{
    "id": "87008af0-26c7-4b84-b816-d5d7cc972021",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_button_menu_parent",
    "eventList": [
        {
            "id": "4a9a7834-e0a6-4936-bd77-503f70c42f9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "87008af0-26c7-4b84-b816-d5d7cc972021"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d774b5d3-e78a-417a-947c-ba1168022f13",
    "visible": true
}