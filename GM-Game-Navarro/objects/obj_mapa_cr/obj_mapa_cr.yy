{
    "id": "e621517d-9a77-4788-91a8-f80e8fc17781",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_mapa_cr",
    "eventList": [
        {
            "id": "d883623b-4f60-4167-a21f-2b7e10a79424",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e621517d-9a77-4788-91a8-f80e8fc17781"
        },
        {
            "id": "acd79de0-0e65-45f7-bc12-9cb0ea015c24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e621517d-9a77-4788-91a8-f80e8fc17781"
        },
        {
            "id": "ea9c7bb1-c6d7-45aa-bf0b-5c45f0b7e72e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "e621517d-9a77-4788-91a8-f80e8fc17781"
        },
        {
            "id": "3a18cbb2-107b-413a-9203-db6b210355c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "e621517d-9a77-4788-91a8-f80e8fc17781"
        }
    ],
    "maskSpriteId": "b316bf50-306f-4fed-bf19-69d2898f9cce",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b316bf50-306f-4fed-bf19-69d2898f9cce",
    "visible": true
}